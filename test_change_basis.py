"""
Code testing the following methods of operetors.py:
- op_array_to_vector
- op_vector_to_array
- op_mapping

DO NOT CONSIDER WALL TRANSPIRATION
-> it changes the indexing for the wall-normal direction to consider v+ and v-
"""
import numpy as np
import h5py
from flowfield import FlowField
from plotfield import plot_slice
from operators import Operators

# parameters 
eq = 1
Nx = 4
Ny = 15
Nz = 4

# plot parameters
# usage = "plotfield [--output=plot.png] [--cut=0] [--lines=u|v|w [--contourstep=0.1]] [--quiver] [--field=u|v|w|mean] [--normal=x|y|z|mean] [--addmean=mean.asc] flowfield.hd5"
cut = 0
field = 'u'
normal = 'z'

# equilibrium loading
filename_eq = "./database/eq/"+\
        "eq"+str(eq)+"_"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)+".h5"
ff_eq = FlowField(filename_eq)
print('ff_eq.u : ', ff_eq.u.shape)

Mx = ff_eq.Mx
Mz = ff_eq.Mz

# plot a slice before and after the transformations
plot_slice(ff_eq, cut=cut, field=field, normal=normal)
ff_eq.make_spectral('xz')

## CHANGE OF BASE WITHOUT WALL ACTUATION
op = Operators(ff_eq)

# 0. reduce ff_eq.u to remove the first/last row/column
ff_eq_u_array0 = ff_eq.u[:, :, 1:Ny-1, :]

# 1. transform ff_eq_u into a vector
ff_eq_u_vector_uvw0 = op.transform_spectral_uvw_array_to_vector(ff_eq, ff_eq_u_array0)

# 2. test matrix C and Cinv
C, Cinv = op.op_mapping(ff_eq)
W, Winv = op.op_weighting(ff_eq)
#vector_uvw = np.dot(C, vector_v_eta)
ff_eq_u_vector_veta = np.dot(np.dot(Cinv, Winv), ff_eq_u_vector_uvw0)
#ff_eq_u_vector_veta = np.dot(np.dot(Cinv, Winv), ff_eq_u_vector)

ff_eq_u_vector_uvw1 = np.dot(np.dot(W, C), ff_eq_u_vector_veta)
#ff_eq_u_vector_uvw = np.dot(np.dot(W, C), ff_eq_u_vector_veta)

# 3. transform the result back into an array
ff_eq_u_array1 = op.transform_spectral_uvw_vector_to_array(ff_eq, ff_eq_u_vector_uvw1)

# 4. expand ff_eq_u_array1 to add first/last row/column with 0
ff_eq_u_array2 = np.zeros(shape=(3, Mx, Ny, Mz), dtype=complex)
ff_eq_u_array2[:, :, 1:Ny-1, :] = ff_eq_u_array1[:, :, :, :]
ff_eq_u_array2 = ff_eq.make_physical_dataset(ff_eq_u_array2, nz=Nz, index="xz")

# 5. create a corresponding flowfield and plot
filename_ff = "eigenvector.h5"
file = h5py.File(filename_ff, 'w')
data = file.create_group('data')
geom = file.create_group('geom')
for a in ff_eq.attributes:
    file.attrs[a] = ff_eq.attributes[a]
x = geom.create_dataset('x', data=ff_eq.x)
y = geom.create_dataset('y', data=ff_eq.y)
z = geom.create_dataset('z', data=ff_eq.z)
#u = data.create_dataset('u', data=ff_eq.u)#data.create_dataset('u', data=array_uvw)
u = data.create_dataset('u', data=ff_eq_u_array2)#data.create_dataset('u', data=array_uvw)
# u is in spectral
file.close()

# 6. load the FlowField
ff = FlowField(filename_ff)
ff.state['xz'] = 'Physical'
ff.make_physical('xz')

# 7. plotfield
plot_slice(ff, cut=cut, field=field, normal=normal)
