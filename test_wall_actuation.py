import numpy as np
import chebyshev
import scipy as sp
import scipy.integrate as spi

import matplotlib.pyplot as plt

def f_nabla2(N):#, kx, kz, Lx, Lz):
    kx = 0
    kz = 0
    Lx = 2 * np.pi
    Lz = 2 * np.pi
    nodes, DM = chebyshev.chebdiff(N, 2)
    D2_v = DM[1, :, :]
    alpha = 2 * np.pi * kx / Lx
    beta = 2 * np.pi * kz / Lz
    return (-1 * (alpha**2 + beta**2) * np.eye(N) + D2_v)

def f_inverse_dirichlet(nabla2):
    inverse = np.zeros_like(nabla2)
    inverse[1:-1, 1:-1] = np.linalg.inv(nabla2[1:-1, 1:-1])
    return inverse

def f_nabla4(N):
    kx = 0
    kz = 0
    Lx = 2 * np.pi
    Lz = 2 * np.pi
    nodes, DM = chebyshev.chebdiff(N, 4)
    D2_v = DM[1, :, :]
    D4_v = DM[3, :, :]
    alpha = 2 * np.pi * kx / Lx
    beta = 2 * np.pi * kz / Lz
    return  D4_v + (alpha**2 + beta**2)**2 * np.eye(N) - 2 * (alpha**2 + beta**2) * D2_v

def f_nabla4_clamped(N):
    """
    shape [N-1, N-1]
    """
    nabla4_clamped = np.zeros((N, N), dtype=complex)
    kx = 0
    kz = 0
    Lx = 2 * np.pi
    Lz = 2 * np.pi
    nodes, DM = chebyshev.chebdiff(N, 4)
    D2_v = DM[1, :, :]
    D4_v_clamped = chebyshev.cheb4c(N)[1]
    alpha = 2 * np.pi * kx / Lx
    beta = 2 * np.pi * kz / Lz
    nabla4_clamped[1:-1, 1:-1] = D4_v_clamped \
                    + (alpha**2 + beta**2)**2 * np.eye(N)[1:N-1, 1:N-1] \
                    - 2 * (alpha**2 + beta**2) * D2_v[1:N-1, 1:N-1]
    return nabla4_clamped

def f_u(y):
    return 0.25 * (2 * y**4 - y**3 - 4 * y**2 + 3*y + 4)
    #return 0.25 * (- y**3 + 3 * y + 2)

def f_l(y):
    return 0.25 * (2 * y**4 + y**3 - 4 * y**2 - 3*y + 4)
    #return 0.25 * (+ y**3 - 3 * y + 2)

def f_init(x, e, f, g):
    """
    Respects Dirichlet + Neumann BCs
    """
    a = e + 2 * g
    b = f
    c = -2*e -3*g
    d = -2*f
    return a*x**6 + b*x**5 + c*x**4 + d*x**3 + e*x**2 + f*x + g

N = 50
T = 20
Nt = T * 1
tau = 0.05

# initial function coefficients
e = +5
f = -3
g = -1

# intialisation of some variables
t_saved = np.linspace(0, T, Nt)
y, weights = chebyshev.clencurt(N)

print(">> INITIAL CONDITION")
x_0 = (+1 + 1j) * f_init(y, e, f, g)#* np.random.randn(N)
#x_0[0] = 0 + 0j
#x_0[-1] = 0 + 0j

print(">> DEFINITION OF E & INVERSE")
E = f_nabla2(N)

E_di = f_inverse_dirichlet(E)
E_di[0, 0] = 1
E_di[-1, -1] = 1

print(">> DEFINITION OF A")
coef_1 = 1
coef_2 = 1

A_1 = np.zeros((N, N), dtype = complex)
A_1[0, 0] = - 1 / tau 
A_1[1:-1,  0] = + 1 / tau * np.dot(E, f_u(y))[1:-1]
A_1[1:-1, -1] = + 1 / tau * np.dot(E, f_l(y))[1:-1]
A_1[-1, -1] = - 1 / tau

L = f_nabla4(N)
L_clamped = f_nabla4_clamped(N)

A_2 = np.zeros((N, N), dtype = complex)
A_2[1:-1,  0] = + np.dot(L, f_u(y))[1:-1]
A_2[1:-1, 1:-1] = L_clamped[1:-1, 1:-1]
A_2[1:-1, -1] = + np.dot(L, f_l(y))[1:-1]
#A_2[1:-1, 1:-1] = np.dot(E_di[1:-1, 1:-1], L_clamped[1:-1, 1:-1])

A = coef_1 * A_1 + coef_2 * A_2

print(">> DEFINITION OF A_0")
A_0_1 = np.zeros((N, N), dtype = complex)

A_0_2 = np.zeros((N, N), dtype = complex)
A_0_2[1:-1, 1:-1] = np.dot(E_di[1:-1, 1:-1], L_clamped[1:-1, 1:-1])
#A_0_2[1:-1, 1:-1] = np.dot(E_di, L_clamped)[1:-1, 1:-1]

A_0 = coef_1 * A_0_1 + coef_2 * A_0_2

print(">> DEFINITION OF B")
B = np.zeros((N, 2), dtype = complex)
B[0, 0] = 1 / tau
B[1:-1, 0] = - 1 / tau * np.dot(E, f_u(y))[1:-1]
B[1:-1, 1] = - 1 / tau * np.dot(E, f_l(y))[1:-1]
B[-1, -1] = 1 / tau

forcing = [2, 0]

def direct_eq(t, x):
    # A x + B q
    # [  -1/tau    0      0     ] [v_u]   [   1/tau         0      ] [ q_u ] 
    # [ 1/tau*f_u  0  1/tau*f_l ] [v_0] + [ -1/tau*f_u  -1/tau*f_l ] [ q_l ] + A_2 x
    # [     0      0   -1/tau   ] [v_l]   [     0         1/tau    ]
    return np.dot(E_di, np.dot(A, x)) + np.dot(E_di, np.dot(B, forcing))
    #return np.dot(A, x) + np.dot(B, forcing)

def direct_eq_original(t, x):
    return np.dot(A_0, x)

print(">> INTEGRATION ACTUATED")
sol = spi.solve_ivp(fun = direct_eq,
                    t_span = (0,T),
                    y0 = x_0,
                    method = 'BDF',
                    t_eval = t_saved)

print(">> INTEGRATION ORIGINAL")
sol_original = spi.solve_ivp(fun = direct_eq_original,
                    t_span = (0,T),
                    y0 = x_0,
                    method = 'BDF',
                    t_eval = t_saved)


for it in [19]:#t_saved:
    x_t = sol.y[:, int(it)]
    x_original = sol_original.y[:, int(it)]

    v = np.zeros((N), dtype=complex)
    v[1:-1] = x_t[1:-1]
    v[:] += np.dot(f_u(y), x_t[0]) + np.dot(f_l(y), x_t[-1])
    #v = x_t + np.dot(f_u(y), forcing[0]) + np.dot(f_l(y), forcing[1])
    #v[0] = x_t[0]
    #v[1:-1] = x_t[1:-1] #+ np.dot(f_u(y), x_t[0])[1:-1] + np.dot(f_l(y), x_t[-1])[1:-1]
    #v[-1] = x_t[-1]
     
    v_original = np.zeros((N), dtype=complex)
    v_original[0] = 0 
    v_original[1:-1] = x_original[1:-1]
    v_original[-1] = 0
       
    plt.plot(y, v.real - v_original.real, 'o-', color='g')
    #plt.plot(y, v.imag - v_original.imag, 'o-', color='g')

    # WITH ACTUATION
    plt.plot(y, v.real, '-', color='b')
    #plt.plot(y, v.imag, '-', color='b')

    # ORIGINAL WITHOUT ACTUATION
    plt.plot(y, x_0.real, 'o', color='r')
    #plt.plot(y, x_0.imag, 'o', color='r')
    plt.plot(y, v_original.real, '-', color='r')
    #plt.plot(y, v_original.imag, '-', color='r')

    # F_U
    #plt.plot(y, np.dot(E_di, np.dot(E, f_u(y))), 'x-', color='k', label='x')
    plt.plot(y, np.dot(f_u(y), forcing[0]), 'x-', color='k', label='x')
    # F_l
    #plt.plot(y, f_l(y), 'x-', color='b', label='x')
    plt.show()
