"""
channel.py

implementing:
Channel(FlowField)
DiffOperator_Channel(DiffOperator_FlowField)

Flowfield-inheriting class that loads and saves hdf5 channel data
using conventions compatible with that produced by
John Gibson's channelflow.
A Sharma 2015, 2016
G Claisse 2016
"""
import numpy as np
import numpy.polynomial.chebyshev as np_cheb

from scipy import interpolate
from scipy.sparse.linalg import svds

import datetime
import warnings

from flowfield import FlowField
from flowfield import DiffOperator_FlowField
from flowfield import BoundaryCondition_FlowField
from flowfield import MeanFlow_FlowField
import chebyshev


class Channel(FlowField):
    """
    class for loading and holding Channel objects.
    """

    def __init__(self, filename=None):
        super().__init__(filename=filename)
        self.version_str = u'Channel v1.0.1b'
        self.attributes['version_str'] = self.version_str
        if filename is None:
            self.state = {'xz': 'Physical', 'y': 'Physical'}
        # Variables for DiffOperator method
        self.dop = DiffOperator_Channel(self)
        self.mean = MeanFlow_Channel(self)
        #self.bc = BoundaryCondition_Channel(self)

    def copy(self, ff):
        """
        copy another channel flowfield to this one
        """
        super().copy(ff)
        y, self.w = chebyshev.clencurt(self.y.shape[0])

    def load(self, filename):
        """
        loads a flowfield from a specified file, moves mean to u0
        """
        super().load(filename)
        self.includes_mean = True
        # self.subtract_mean()
        y, self.w = chebyshev.clencurt(self.y.shape[0])

    def add_mean(self):
        """
        adds a mean flow (as a function of y only) back to u, without parabolic
        base flow
        """
        if self.includes_mean is False:
            self.u[0, :, :, :] += self.u0 - 1.0 + self.y * self.y
            self.includes_mean = True

    def subtract_mean(self):
        """
        finds and subtracts the spatial mean from the flow,
        sets self.u0, assuming parabolic base flow.
        """
        if self.includes_mean is True:
            self.includes_mean = False
            mean = np.mean(np.mean(self.u, axis=3), axis=1)
            self.u -= mean.reshape(mean.shape[0], 1, mean.shape[1], 1)
            self.u0 = mean[0, :] + 1.0 - self.y * self.y

    def shift(self, shift, axis='x'):
        """
        translates the flowfield along the x or z axis by a given amount.
        The shift must be negative.
        """
        if axis not in ('x', 'z'):
            raise ValueError("Shift by this index not possible: only x and z")
        else:
            super().shift(shift, axis)

    def make_spectral_dataset(self, dataset, index='xz'):
        """
        Convert dataset to spectral state
        by computing the spectral coefficients:
        Fourier for index 'x' and 'z'.
        Spectral Chebyshev for index 'y'.

        Keyword arguments:
        dataset -- dataset of size (3,Nx,Ny,Nz) or (1,Nx,Ny,Nz)
        index -- direction 'xz', 'y' or 'xyz'.

        Return:
        data -- Spectral state of 'dataset'.
        """
        if index == 'xz' or index == 'xyz':
            data = super().make_spectral_dataset(dataset, 'xz')

        if index == 'y' or index == 'xyz':
            # IF IMPLEMENTATION IS NEEDED, LOOK AT:
            # DIFFOPERATOR_CHANNEL.__DERIV -> method 3
            raise NotImplementedError
            pass

        if index != 'xz' and index != 'y' and index != 'xyz':
                warnings.warn("Index needs to be " +
                              "'x', 'y', 'z' or combination.")
        return data

    def make_spectral(self, index='xz', baseflow='no'):
        """
        Convert Channel to spectral state
        by computing the spectral coefficients:
        Fourier for index 'x' and 'z'.
        Spectral Chebyshev for index 'y'.

        Keyword arguments:
        index -- direction 'xy', 'y' or 'xyz'.
        baseflow -- if the baseflow dataset need to be made spectral.
            'yes': baseflow is made spectral
            'no': baseflow is not made spectral
            'only': baseflow is made spectral but not the main dataset
        """
        if index == 'xz' or index == 'xyz':
            super().make_spectral(index, baseflow)

        if index == 'y' or index == 'xyz':
            # IF IMPLEMENTATION IS NEEDED, LOOK AT:
            # DIFFOPERATOR_CHANNEL.__DERIV -> method 3
            raise NotImplementedError
            pass

        if index != 'xz' and index != 'y' and index != 'xyz':
            warnings.warn("Index needs to be " +
                              "'x', 'y', 'z' or combination.")

    def make_physical_dataset(self, dataset, nz, index='xz'):
        """
        Convert dataset to physical state.
        (from Fourier for 'x' or 'z',
        from Spectral Chebyshev for 'y').

        Keyword arguments:
        dataset -- dataset of size (3,Nx,Ny,Nz) or (1,Nx,Ny,Nz)
        index -- direction 'x', 'y' or 'z'.

        Return:
        data -- Physical state of 'dataset'.
        """
        if index == 'xz' or index == 'xyz':
            data = super().make_physical_dataset(dataset, nz, index)

        if index == 'y' or index == 'xyz':
            # IF IMPLEMENTATION IS NEEDED, LOOK AT:
            # DIFFOPERATOR_CHANNEL.__DERIV -> method 3
            raise NotImplementedError
            pass

        if index != 'xz' and index != 'y' and index != 'xyz':
            warnings.warn("Index needs to be " +
                              "'x', 'y', 'z' or combination.")
        return data

    def make_physical(self, index='xz', baseflow='no'):
        """
        Convert Channel to physical state.
        (from Fourier for 'x' or 'z',
        from Spectral Chebyshev for 'y').

        Keyword arguments:
        index -- direction 'x', 'y' or 'z'.
        baseflow -- if the baseflow dataset need to be made spectral.
            'yes': baseflow is made spectral
            'no': baseflow is not made spectral
            'only': baseflow is made spectral but not the main dataset
        """
        if index == 'xz' or index == 'xyz':
            super().make_physical(index, baseflow)

        if index == 'y' or index == 'xyz':
            # IF IMPLEMENTATION IS NEEDED, LOOK AT:
            # DIFFOPERATOR_CHANNEL.__DERIV -> method 3
            raise NotImplementedError
            pass

        if index != 'xz' and index != 'y' and index != 'xyz':
            warnings.warn("Index needs to be " +
                              "'x', 'y', 'z' or combination.")

    def uniform2chebyshev(self, interp_method=3):
        """
        Convert Channel wall-nornal axis ('y')
        from an uniform discretisation
        to a Chebyshev discretisation
        and interpolate dataset 'u' and 'p'.

        Keyword arguments:
        interp_method -- integer, method to interpolate data
                         into Chebyshev coefficients.
                1: Use of package of Weideman.
                    It does not work for complex cases (very unprecise)
                2: Use of numpy.interp1d(),
                    works but not optimized for Chebyshev
                3: (HIGHLY RECOMMENDED) Fastest (10x), most precise and
                    reliable method with usage of
                    numpy.polynomial.chebyshev.chebyshev package.

        IMPORTANT REMARK:
        It is necessary to use the same method
        for methods uniform2chebyshev() and chebyshev2uniform().
        """
        ###############################################################
        if self.conf_y_axis is 'Uniform':

            ############
            # If dataset in Chebyshev configuration was stored
            # If current uniform dataset is egal to former uniform dataset
            # and if corresponding dataset in Chebyshev is known
            # just used the saved Chebyshev datasets.
            if hasattr(self, 'u_saved_uniform') \
                    and np.array_equal(self.u, self.u_saved_uniform) \
                    and np.array_equal(self.p, self.p_saved_uniform) \
                    and hasattr(self, 'u_saved_cheb'):
                self.y_saved_uniform = self.y.copy()
                self.y = self.y_saved_cheb.copy()
                self.u = self.u_saved_cheb.copy()
                self.p = self.p_saved_cheb.copy()

            ############
            # Otherwise, calculate new values.
            else:
                # BE CAREFUL WITH THAT FUNCTION, DATA IS REQUIRED AT WALL y
                # =(-1, 1)
                self.y_saved_uniform = self.y.copy()
                self.u_saved_uniform = self.u.copy()
                self.p_saved_uniform = self.p.copy()

                # Transform Y axis into a Chebyshev nodes
                if hasattr(self, 'y_saved_cheb'):
                    self.y = self.y_saved_cheb.copy()
                else:
                    self.y = chebyshev.chebnodes(self.y.shape[0])

                # -------------------------------------------------------
                # 1ST METHOD (CHEBINT OF WEIDEMAN)
                # -------------------------------------------------------
                if interp_method == 1:
                    for nx in range(self.u.shape[1]):
                        for nz in range(self.u.shape[3]):
                            self.p[nx, :, nz] = chebyshev.chebint(
                                self.p[nx, :, nz],
                                self.y,
                                self.y_saved_uniform)[:, 0]
                            for i in range(self.u.shape[0]):
                                self.u[i, nx, :, nz] = chebyshev.chebint(
                                    self.u[i, nx, :, nz],
                                    self.y,
                                    self.y_saved_uniform)[:, 0]

                # -------------------------------------------------------
                # 2ND METHOD (INTERP1D)
                # -------------------------------------------------------
                elif interp_method == 2:
                    for nx in range(self.u.shape[1]):
                        for nz in range(self.u.shape[3]):
                            fct_p = interpolate.interp1d(
                                self.y_saved_uniform[:],
                                self.p[nx, :, nz],
                                bounds_error=False,
                                kind='cubic')
                            self.p[nx, :, nz] = fct_p(self.y[:])

                            for i in range(self.u.shape[0]):
                                fct = interpolate.interp1d(
                                    self.y_saved_uniform[:],
                                    self.u[i, nx, :, nz],
                                    bounds_error=False,
                                    kind='cubic')
                                self.u[i, nx, :, nz] = fct(self.y[:])

                # -------------------------------------------------------
                # 3TH METHOD (POLYNOMIAL.CHEBYSHEV.CHEBFIT + CHEBVAL
                # TODO : HOW TO FIND THE BEST DEGREE ?
                # -------------------------------------------------------
                elif interp_method == 3:
                    if not hasattr(self, 'degree_interp'):
                        self.degree_interp = int(0.50 * self.y.shape[0])

                    # Reshape and transpose to an array of shape (Ny, K)
                    #   to fit method chebfit().
                    self.u = np.reshape(
                        np.transpose(self.u, axes=(2, 0, 1, 3)),
                        (self.attributes['Ny'],
                         3 * self.attributes['Nx'] * self.attributes['Nz'])
                    )

                    # Declare polynom to interpolate uniform dataset
                    #   on new Chebyshev modes
                    polynom_modal = np_cheb.chebfit(
                        self.y_saved_uniform,
                        self.u[:, :],
                        self.degree_interp)

                    # Interpolation the polynom on the new Chebyshev modes
                    interp_val = np_cheb.chebval(
                        self.y,
                        polynom_modal[:, :])

                    # Return self.u to its original shape
                    self.u = np.transpose(
                        np.reshape(interp_val,
                                   (3, self.attributes['Nx'],
                                    self.attributes['Nz'],
                                    self.attributes['Ny'])),
                        axes=(0, 1, 3, 2))

                    # same operation for p
                    self.p = np.reshape(
                        np.transpose(self.p, axes=(1, 0, 2)),
                        (self.attributes['Ny'],
                         1 * self.attributes['Nx'] * self.attributes['Nz'])
                    )
                    polynom_modal_p = np_cheb.chebfit(
                        self.y_saved_uniform,
                        self.p[:, :],
                        self.degree_interp)
                    interp_val_p = np_cheb.chebval(
                        self.y,
                        polynom_modal_p[:, :])
                    self.polynom_nodal_p = np_cheb.chebfit(
                        self.y,
                        interp_val_p.T,
                        self.degree_interp)
                    self.p = np.transpose(
                        np.reshape(interp_val_p,
                                   (self.attributes['Nx'],
                                    self.attributes['Nz'],
                                    self.attributes['Ny'])),
                        axes=(0, 2, 1))

                # -------------------------------------------------------
                # ERROR
                # -------------------------------------------------------
                else:
                    raise ValueError(
                        """
                        No correspondance with value of interp_method.
                        It should be an integer: 1, 2 or 3.
                        """)

                self.y_saved_cheb = self.y.copy()
                self.u_saved_cheb = self.u.copy()
                self.p_saved_cheb = self.p.copy()

            self.conf_y_axis = 'Chebyshev'

        ###############################################################
        elif self.conf_y_axis is 'Chebyshev':
            self.y_saved_cheb = self.y.copy()
            if not hasattr(self, 'y_saved_uniform'):
                self.y_saved_uniform = np.linspace(self.attributes['a'],
                                                   self.attributes['b'],
                                                   num=self.u.shape[2],
                                                   endpoint=True)
            if not hasattr(self, 'degree_interp'):
                self.degree_interp = int(0.50 * self.y.shape[0])

        ###############################################################
        else:
            raise ValueError(
                """
                Interpolation is not possible as the configuration
                of the y-axis nodes (self.conf_y_axis)
                is neither 'Uniform' nor 'Chebyshev'.
                """)

    def chebyshev2uniform(self, interp_method=3):
        """
        Convert Channel wall-mormal axis ('y')
        from a Chebyshev discretisation
        into a uniform discretisation
        and interpolate dataset 'u' and 'p'.

        Keyword arguments:
        interp_method -- integer, method to interpolate data.
                1: Use of package of Weideman.
                    It does not work for complex cases (very unprecise)
                2: Use of numpy.interp1d(),
                    works but not optimized for Chebyshev
                3: (HIGHLY RECOMMENDED) Fastest (10x), most precise and
                    reliable method with usage of
                    numpy.polynomial.chebyshev.chebyshev package.

        IMPORTANT REMARK:
        It is necessary to use the same method
        for methods uniform2chebyshev() and chebyshev2uniform().
        """
        ###############################################################
        if self.conf_y_axis is 'Chebyshev':
            if not hasattr(self, 'degree_interp'):
                self.degree_interp = int(0.50 * self.y.shape[0])

            ############
            # If former uniform dataset was stored
            # If current Chebyshev dataset is egal to former dataset
            # and if corresponding uniform dataset is known
            # just used the saved uniform datasets.
            if hasattr(self, 'u_saved_cheb') \
                    and np.array_equal(self.u, self.u_saved_cheb) \
                    and np.array_equal(self.p, self.p_saved_cheb) \
                    and hasattr(self, 'u_saved_uniform'):
                self.y_saved_cheb = self.y.copy()
                self.y = self.y_saved_uniform.copy()
                self.u = self.u_saved_uniform.copy()
                self.p = self.p_saved_uniform.copy()

            ############
            # Otherwise, calculate new values.
            else:
                self.y_saved_cheb = self.y.copy()
                self.u_saved_cheb = self.u.copy()
                self.p_saved_cheb = self.p.copy()

                # Transform Y axis into a uniform axis
                if hasattr(self, 'y_saved_uniform'):
                    self.y = self.y_saved_uniform.copy()
                else:
                    self.y = np.linspace(self.attributes['a'],
                                         self.attributes['b'],
                                         num=self.u.shape[2], endpoint=True)

                # -------------------------------------------------------
                # 1ST METHOD (CHEBINT OF WEIDEMAN)
                # -------------------------------------------------------
                if interp_method == 1:
                    # cheb_nodes = chebyshev.chebnodes(self.y.shape[0])
                    for nx in range(self.u.shape[1]):
                        for nz in range(self.u.shape[3]):
                            self.p[nx, :, nz] = chebyshev.chebint(
                                self.p[nx, :, nz],
                                self.y,
                                self.y_saved_cheb)[:, 0]

                            for i in range(self.u.shape[0]):
                                self.u[i, nx, :, nz] = chebyshev.chebint(
                                    self.u[i, nx, :, nz],
                                    self.y,
                                    self.y_saved_cheb)[:, 0]

                # -------------------------------------------------------
                # 2ND METHOD (INTERP1D)
                # -------------------------------------------------------
                elif interp_method == 2:
                    # cheb_nodes = chebyshev.chebnodes(self.y.shape[0])
                    for nx in range(self.u.shape[1]):
                        for nz in range(self.u.shape[3]):
                            fct_p = interpolate.interp1d(
                                self.y_saved_cheb[:],
                                self.p[nx, :, nz],
                                bounds_error=False,
                                kind='cubic')
                            self.p[nx, :, nz] = fct_p(self.y[:])
                            for i in range(self.u.shape[0]):
                                fct = interpolate.interp1d(
                                    self.y_saved_cheb[:],
                                    self.u[i, nx, :, nz],
                                    bounds_error=False,
                                    kind='cubic')
                                self.u[i, nx, :, nz] = fct(self.y[:])

                # -------------------------------------------------------
                # 3TH METHOD (POLYNOMIAL.CHEBYSHEV.CHEBFIT + CHEBVAL)
                # -------------------------------------------------------
                elif interp_method == 3:
                    # Reshape and transpose to an array of shape (Ny, K)
                    # to fit method chebfit().
                    self.u = np.reshape(
                        np.transpose(self.u, axes=(2, 0, 1, 3)),
                        (self.attributes['Ny'],
                         3 * self.attributes['Nx'] * self.attributes['Nz']))

                    if not hasattr(self, 'degree_interp'):
                        self.degree_interp = int(0.50 * self.y.shape[0])

                    # Define former Y coordinates
                    # cheb_nodes = chebyshev.chebnodes(self.y.shape[0])

                    # Declare polynom to interpolate Chebyshev dataset
                    # on uniform nodes
                    polynom_nodal = np_cheb.chebfit(
                        self.y_saved_cheb,
                        self.u[:, :],
                        self.degree_interp)
                    interp_val = np_cheb.chebval(self.y,
                                                 polynom_nodal[:, :])

                    self.u = np.transpose(
                        np.reshape(interp_val,
                                   (3, self.attributes['Nx'],
                                    self.attributes['Nz'],
                                    self.attributes['Ny'])),
                        axes=(0, 1, 3, 2))

                    # same operation for self.p
                    self.p = np.reshape(
                        np.transpose(self.p, axes=(1, 0, 2)),
                        (self.attributes['Ny'],
                         self.attributes['Nx'] * self.attributes['Nz']))
                    polynom_nodal_p = np_cheb.chebfit(self.y_saved_cheb,
                                                      self.p[:, :],
                                                      self.degree_interp)
                    interp_val_p = np_cheb.chebval(self.y,
                                                   polynom_nodal_p[:, :])
                    self.p = np.transpose(
                        np.reshape(interp_val_p,
                                   (self.attributes['Nx'],
                                    self.attributes['Nz'],
                                    self.attributes['Ny'])), axes=(0, 2, 1))

                # -------------------------------------------------------
                # ERROR
                # -------------------------------------------------------
                else:
                    raise ValueError(
                        """
                        No correspondance with value of interp_method.
                        It should be an integer: 1, 2 or 3.
                        """)

                self.y_saved_uniform = self.y.copy()
                self.u_saved_uniform = self.u.copy()
                self.p_saved_uniform = self.p.copy()

            self.conf_y_axis = 'Uniform'

        ###############################################################
        elif self.conf_y_axis is 'Uniform':
            self.y_saved_uniform = self.y.copy()
            if not hasattr(self, 'y_saved_cheb'):
                self.y_saved_cheb = chebyshev.chebnodes(self.y.shape[0])
            if not hasattr(self, 'degree_interp'):
                self.degree_interp = int(0.50 * self.y.shape[0])

        ###############################################################
        else:
            raise ValueError(
                """
                Interpolation is not possible as the configuration
                of the y-axis nodes (self.conf_y_axis)
                is neither 'Uniform' nor 'Chebyshev'.
                """)

    def dataset_chebyshev2uniform(self, dataset):
        """
        This method transform any dataset based on Chebyshev nodes
        to a dataset based on an uniform axis. (in y-direction only)
        For example, it can be use to transpose the results of
        a differential operator to an uniform axis.

        Keyword arguments:
        dataset -- a dataset of the forn [i,x y,z] or [x,y,z]
        """
        data = dataset.copy()

        if dataset.ndim == 3:
            data = data[np.newaxis, :, :, :]

        ni = data.shape[0]
        nx = data.shape[1]
        ny = data.shape[2]
        nz = data.shape[3]

        # reshape to be uysed with chebfit
        data = np.reshape(np.transpose(data[:, :, :, :], axes=(2, 0, 1, 3)),
                          (ny, ni * nx * nz))
        # spectral coefficient
        polynom_nodal = np_cheb.chebfit(self.y,
                                        data[:, :],
                                        int(0.5 * self.y.shape[0]))
        # calculate values on nodes
        data = np_cheb.chebval(self.y_saved_uniform[:],
                               polynom_nodal[:, :])

        # reshape derivative into original dataset's shape
        data = np.transpose(np.reshape(data, (ni, nx, nz, ny)),
                            axes=(0, 1, 3, 2))
        if dataset.ndim == 3:
            return data[0, :, :, :]
        else:
            return data

    def notch_filter(self, n, index='z'):
        """
        set to zero all Fourier components except at n
        """
        if index in ('x', 'z'):
            super().notch_filter(n, index)

    def extract_fourier_plane(self, n, index='z'):
        """
        return a flattened array over one x- or z-Fourier mode (both parts)
        """
        if index in ('x', 'z'):
            return super().extract_fourier_plane(n, index)

    def insert_fourier_plane(self, plane, n, index='z'):
        """
        insert a plane generated by .extract_fourier_plane()
        """
        if index in ('x', 'z'):
            return super().insert_fourier_plane(plane, n, index)

    def scale(self):
        """ scale for correct inner product """
        if self.scaled is not True:
            w = np.sqrt(self.w)
            self.u *= w[None, None, :, None]
            self.p *= w[None, :, None]
            self.scaled = True

    def unscale(self):
        """ unscale from correct inner product to physical units """
        if self.scaled is True:
            w = np.sqrt(self.w)
            self.u /= w[None, None, :, None]
            self.p /= w[None, :, None]
            self.scaled = False

    def pressure_solve(self):
        """ find the pressure field from the velocity field """
        pass
        return NotImplementedError

    def resolvent(omega, alpha, beta, Ny, Re, mean):
        """
        Returns the resolvent operator.
        """
        L = oss(alpha, beta, Ny, Re, mean)
        return np.linalg.inv((-1j * omega) - L)

    def resolvent_modes(omega, alpha, beta, Ny, Re, mean, W, modes=10):
        """
        Returns the singular value decomposition of the resolvent.
        While the SVD must be weighted, return the unweighted modes.
        """
        H = resolvent(omega, alpha, beta, Ny, Re, mean)
        return svds(np.dot(np.dot(W, H), W), modes)


class DiffOperator_Channel(DiffOperator_FlowField):
    """
    Class inhereting from DiffOperator_FlowField.
    Manages differential operation on the Channel object.
    Contains differential operator dedicated to Channel object.
    Inherits from DiffOperator_FLowField for operation like divergence,
        gradient, curl or laplacian.

    Attributes :
    self.ff -- channel on which apply the operations.

    self.DM -- Matrix containing differentiation coefficients.
    self.DM_order -- Order of maximal differentiation calculated.
    """

    def __init__(self, channel):
        self.ff = channel
        self.DM = None  # see method DiffOperator_Channel.__deriv()
        self.DM_order = 0  # see method DiffOperator_Channel.__deriv()

    def __deriv(self, dataset, order=1,
                deriv_method=1,
                timer='off',
                bc='none',
                transform='none'):
        """
        Derivative of the dataset 'dataset' of the channel 'self.ff'
        in direction 'y' and order 'order'.

        Considering the channel V whose shape of dataset u is (3, Nx, Ny, Nz):
        __deriv(V.u, 'y', n) = (dn_V.ux/dy**n, dn_V.uy/dy**n, dn_V.uz/dy**n)
                                                                        (x,y,z)

        IMPORTANT REMARKS:
        - The y-axis needs to be in its Chebyshev configuration.
            Aply method uniform2chebyshev() if not.
        - This function changes instances of variable:
            DM, DM_order

        Keyword arguments:
        dataset -- dataset self.ff.u or self.ff.p
                    on which apply the derivative.

        Optional keyword arguments:
        order -- integer, order of the derivative om y-direction
        deriv_method -- interger, method used to differentiate
            1: (RECOMMEND) Use of package of Weideman.
                Declare derivative of high order before
                the lower one to spare working time.
            2: (NOT RECOMMEND FOR HIGH ORDER) Slow and unprecise
                at boundary y= -1 and y = 1.
                No use of the Chebyshev properties,
                based on calculation of gradient.
            3: (NOT RECOMMEND FOR HIGH ORDER) Slow and unpreice
                at boundary y = -1 and y = 1. Use of
                numpy.polynomial.chebyshev.chebyshev package.
        timer -- 'on'/'off', if 'on' will print
            the elapsed time for this method.
        bc -- kind of boundary condition,
            see class BoundaryCondition_Channel
        transform -- (only for deriv_method = 1)
            if differentiation matrice DM needs transformation
            before being used, default is 'none'.
            'conjugate'
            'transpose'
            'hermitian' (conjugate-transpose/adjoint)

        Return:
        diff -- Array (3, Nx, Ny, Nz) for dataset (3, Nx, Ny, Nz).
                Array (1, Nx, Ny, Nz) for dataset (1, Nx, Ny, Nz).

        REMARKS about methods after some tests:
            Method 1 seems quicker, more precise and more realiable.
            Method 2 is as fast for small order (1,2) but
        slower for higher order. Moreover, values at y=-1 and y=1
        are sometimes very bad (and worst as order increases).
            Depending on the dataset, method 2 can sometimes be more
        precise than method 1. Away from the walls, both methods are
        usualy very close.
            Method 3 is slower as other method, and the problems
        observer at y=-1 and y=1 for method 2 are still worst. Only use
        this method to compare the two others.

        """
        assert self.ff.conf_y_axis == 'Chebyshev', \
            """Needs Chebyshev configuration in y-direction to calculate
            derivatives, apply method uniform2chebyshev()."""

        if timer == 'on':
            begin = np.datetime64(datetime.datetime.now())

        diff = dataset.copy()

        ###############################################################
        # 1ST METHOD (PACKAGE CHEBYSHEV OF WEIDEMAN + EINSUM)
        ###############################################################
        if deriv_method == 1:
            # Condition to prevent building a new matrix DM
            # at each call of __deriv()
            if self.DM is None \
                    or order > self.DM_order \
                    or bc != self.bc_type:
                self.DM_order = order
                self.bc_type = bc
                cheb_nodes, self.DM = BoundaryCondition_Channel.chebdiff_bc(
                                                self.ff.y.shape[0],
                                                order,
                                                bc)

            if transform == 'conjugate':
                DM = np.conjugate(self.DM)
            elif transform == 'transpose':
                DM = np.transpose(self.DM, (0, 2, 1))
            elif transform == 'hermitian':
                DM = np.conjugate(np.transpose(self.DM, (0, 2, 1)))
            else:
                DM = self.DM

            diff[:, :, :, :] = np.einsum(
                'ij,mnjp->mnip',
                DM[order - 1, :, :],
                diff[:, :, :, :])

        ###############################################################
        # 1ST METHOD (PACKAGE CHEBYSHEV OF WEIDEMAN + DOT)
        # 2 000 to 4 000ms
        ###############################################################
        elif deriv_method == 11:
            # Condition to prevent building a new matrix DM
            # at each call of __deriv()
            if self.DM is None or order > self.DM_order:
                self.DM_order = order
                cheb_nodes, self.DM = chebyshev.chebdiff(self.ff.y.shape[0],
                                                         order)

            # reshape the dataset
            diff = np.reshape(
                np.transpose(
                    diff[:, :, :, :],
                    axes=(2, 0, 1, 3)),
                (self.ff.attributes['Ny'],
                 dataset.shape[0] *
                 self.ff.attributes['Nx'] *
                 self.ff.attributes['Nz']))

            # differentiate
            diff[:, :] = np.dot(self.DM[order - 1, :, :], diff[:, :])

            # reshape derivative into original dataset's shape
            diff = np.transpose(np.reshape(diff, (self.ff.attributes['Ny'],
                                                  dataset.shape[0],
                                                  self.ff.attributes['Nx'],
                                                  self.ff.attributes['Nz'])),
                                axes=(1, 2, 0, 3))

        ###############################################################
        # 2ND METHOD (GRADIENT)
        # 9 000ms
        ###############################################################
        elif deriv_method == 2:
            Ny = self.ff.y.shape[0]
            dy = np.zeros((self.ff.y.shape[0]))
            dy[0] = abs(self.ff.y[1] - self.ff.y[0])
            dy[Ny - 1] = abs(self.ff.y[Ny - 1] - self.ff.y[Ny - 2])
            for i in range(1, Ny - 1):
                dy[i] = abs(self.ff.y[i - 1] - self.ff.y[i + 1]) / 2

            for ord in range(order):
                diff[:, :, :, :] = -np.gradient(
                    diff[:, :, :, :],
                    dy[np.newaxis, np.newaxis, :, np.newaxis],
                    axis=2)

        ###############################################################
        # 3TH METHOD (CHEBFIT + CHEBDER)
        # 3 000ms
        ###############################################################
        elif deriv_method == 3:
            if not hasattr(self.ff, 'degree_interp'):
                self.ff.degree_interp = int(0.50 * self.ff.y.shape[0])

            # reshape diff to be used with np_cheb.chebfit
            diff = np.reshape(
                np.transpose(diff[:, :, :, :], axes=(2, 0, 1, 3)),
                (self.ff.attributes['Ny'],
                 dataset.shape[0] *
                 self.ff.attributes['Nx'] *
                 self.ff.attributes['Nz']))
            # calculate the Chebyshev series
            polynom_modal = np_cheb.chebfit(self.ff.y,
                                            diff[:, :],
                                            self.ff.degree_interp)
            # differentiate the Chebyshev series
            deriv_coeff = np_cheb.chebder(polynom_modal[:, ], m=order)

            # evaluate differentiated Chebyshev series
            # on wall-normal axis nodes
            deriv_val_cheb = np_cheb.chebval(self.ff.y, deriv_coeff)

            # reshape diff to its original shape
            diff = np.transpose(np.reshape(deriv_val_cheb,
                                           (dataset.shape[0],
                                            self.ff.attributes['Nx'],
                                            self.ff.attributes['Nz'],
                                            self.ff.attributes['Ny'])),
                                axes=(0, 1, 3, 2))

        ###############################################################
        # ERROR
        ###############################################################
        else:
            raise ValueError(
                """
                No correspondance with value of deriv_method.
                It should be an integer: 1, 2 or 3.
                """)

        if timer == 'on':
            end = np.datetime64(datetime.datetime.now())
            print('CALCULATION TIME __deriv() : \n', end - begin)

        return diff

    def deriv(self, dataset, x_order=0, y_order=0, z_order=0, method=1,
              bc='none', transform='none', state=None):
        """
        Wrapper of method __deriv() for any dataset.
        Derivative of the dataset of the channel 'self.ff'
        in direction 'index' and order 'order'.

        Considering the channel V whose shape of dataset D is (3, Nx, Ny, Nz):
        deriv(D, 'x', n) = (dn_Dx/dx**n, dn_Dy/dx**n, dn_Dz/dx**n) (x,y,z)
        deriv(D, 'y', n) = (dn_Dx/dy**n, dn_Dy/dy**n, dn_Dz/dy**n) (x,y,z)
        deriv(D, 'z', n) = (dn_Dx/dz**n, dn_Dy/dz**n, dn_Dz/dz**n) (x,y,z)

        Keyword arguments:
        dataset -- dataset of size (3,Nx,Ny,Nz) or (Nx,Ny,Nz)
            in physical form in x and z direction
            and Chebyshev discretized in y direction
        x_order -- order of differentiation in direction 'x'.
        y_order -- order of differentiation in direction 'y'.
        z_order -- order of differentiation in direction 'z'.
        method, bc, transform -- see docstring on __deriv().

        Return:
        diff -- (physical) Array (3, Nx, Ny, Nz) for dataset (3,Nx,Ny,Nz)
                (physical) Array (Nx, Ny, Nz) for dataset (Nx,Ny,Nz)
        """
        assert isinstance(x_order, int) \
            and isinstance(y_order, int) \
            and isinstance(z_order, int), \
            "Order of derivation must be integer."

        if x_order == 0 and y_order == 0 and z_order == 0:
            return dataset.copy()

        diff = dataset.copy()
        if x_order > 0 or z_order > 0:
            diff = super().deriv(dataset, x_order, 0, z_order, state=state)
        if y_order > 0:
            # self.ff.uniform2chebyshev()
            if dataset.ndim == 4:
                diff = self.__deriv(diff, y_order, method,
                                    bc=bc, transform=transform)
            elif dataset.ndim == 3:
                diff = self.__deriv(diff[np.newaxis, :, :, :],
                                    y_order,
                                    method,
                                    bc=bc,
                                    transform=transform)[0, :, :, :]
            else:
                raise ValueError(
                    """
                    Derivative method deriv is defined
                    to only apply on 1D or 3D dataset.
                    """)
        return diff

    def deriv_u(self, x_order=0, y_order=0, z_order=0,
                method=1, bc='none', transform='none'):
        """
        Wrapper of method __deriv() dedicated to dataset u.
        Derivative of the dataset 'u' of the channel 'self.ff'
        in direction 'index' and order 'order'.

        Considering the channel V whose shape of dataset u is (3, Nx, Ny, Nz):
        deriv_u('x', n) = (dn_V.ux/dx**n, dn_V.uy/dx**n, dn_V.uz/dx**n) (x,y,z)
        deriv_u('y', n) = (dn_V.ux/dy**n, dn_V.uy/dy**n, dn_V.uz/dy**n) (x,y,z)
        deriv_u('z', n) = (dn_V.ux/dz**n, dn_V.uy/dz**n, dn_V.uz/dz**n) (x,y,z)

        Keyword arguments:
        x_order -- order of differentiation in direction 'x'.
        y_order -- order of differentiation in direction 'y'.
        z_order -- order of differentiation in direction 'z'.
        method, bc, transform -- see docstring on __deriv().

        Return:
        diff -- Array (3, Nx, Ny, Nz).
        """
        assert isinstance(x_order, int) \
            and isinstance(y_order, int) \
            and isinstance(z_order, int), \
            "Order of derivation must be integer."

        if x_order == 0 and y_order == 0 and z_order == 0:
            return self.ff.u.copy()

        diff = self.ff.u.copy()
        if x_order > 0 or z_order > 0:
            diff = super().deriv_u(x_order, 0, z_order)
        if y_order > 0:
            assert self.ff.u.shape[0] == 3, \
                """
                Derivative method deriv_u is defined
                to only apply on 3D dataset 'u'.
                """
            # self.ff.uniform2chebyshev()
            diff = self.__deriv(diff, y_order, method,
                                bc=bc, transform=transform)
        return diff

    def deriv_p(self, x_order=0, y_order=0, z_order=0,
                method=1, bc='none', transform='none'):
        """
        Wrapper of method __deriv() dedicated to dataset p.
        Derivative of the dataset 'p' of the channel 'self.ff'
        in direction 'index' and order 'order'.

        Considering the channel V whose shape of dataset p is (3, Nx, Ny, Nz):
        deriv_p('x', n) = (dn_V.p/dx**n) (x,y,z)
        deriv_p('y', n) = (dn_V.p/dy**n) (x,y,z)
        deriv_p('z', n) = (dn_V.p/dz**n) (x,y,z)

        Keyword arguments:
        x_order -- order of differentiation in direction 'x'.
        y_order -- order of differentiation in direction 'y'.
        z_order -- order of differentiation in direction 'z'.
        method, bc, transform -- see docstring on __deriv().

        Return:
        diff -- Array (Nx, Ny, Nz).
        """
        assert isinstance(x_order, int) \
            and isinstance(y_order, int) \
            and isinstance(z_order, int), \
            "Order of derivation must be integer."

        if x_order == 0 and y_order == 0 and z_order == 0:
            return self.ff.p.copy()

        diff = self.ff.p.copy()
        if x_order > 0 or z_order > 0:
            diff = super().deriv_p(x_order, 0, z_order)
        if y_order > 0:
            assert self.ff.p.ndim == 3, \
                """
                Derivative method deriv_p is defined
                to only apply on 1D dataset 'p'.
                """
            # self.ff.uniform2chebyshev()
            diff = self.__deriv(diff[np.newaxis, :, :, :],
                                y_order,
                                method,
                                bc=bc,
                                transform=transform)[0, :, :, :]
        return diff


class BoundaryCondition_Channel(BoundaryCondition_FlowField):
    """
    Class to apply the boundary condition on a channel.
    """

    def __init__(self, channel):
        #self.ff = channel
        pass

    def chebdiff_bc(N, order, bc='none'):
        """
        Wrapper of method chebyshev.chebdiff of package Weideman
        with boundary condition implementation.

        Keyword arguments:
        N -- wall-normal discretisation size
        order -- maximal order
        bc -- kind of boundary condition:
            'none'
            'dirichlet' : NOT validated
                U(Y = -1) = U(Y = +1) = 0
            'clamped' : validated with OSS methods in operators.py
                U(Y = -1) = U(Y = +1) = 0
                dU/dy(Y = -1) = dU/dy(Y = +1) = 0
        """
        cheb_nodes, DM = chebyshev.chebdiff(N, order)

        if bc is 'none':
            pass
        elif bc is 'dirichlet': # or bc is 'clamped':
            # TODO: VALIDATE THIS BOUNDARY CONDITION
            DM[:, 0, :] = 0
            DM[:, N-1, :] = 0
            DM[:, :, 0] = 0
            DM[:, :, N-1] = 0
        elif bc == 'clamped' and order >= 3:
            yy, D4 = chebyshev.cheb4c(N)
            DM[3, 1:N-1, 1:N-1] = D4[:, :]

        return cheb_nodes, DM


class MeanFlow_Channel(MeanFlow_FlowField):

    def __init__(self, channel):
        self.ff = channel

    def apply_mean_flow(self, kind='poiseuille'):
        self.ff.make_physical()
        self.ff.state0 = {'xz': 'Physical', 'y': 'Physical'}
        self.ff.conf_y0_axis = self.ff.evaluate_conf_axis('y')
        if kind == 'poiseuille':
            self.ff.u0 = np.ndarray(shape=(self.ff.u.shape[0],
                                           self.ff.u.shape[1],
                                           self.ff.u.shape[2],
                                           self.ff.u.shape[3] ))
            for ny in range(self.ff.u.shape[2]):
                self.ff.u0[0, :, ny, :] = 1 - self.ff.y[ny]**2
                self.ff.u0[1, :, ny, :] = 0
                self.ff.u0[2, :, ny, :] = 0
        elif kind == 'couette':
            self.ff.u0 = np.ndarray(shape=(self.ff.u.shape[0],
                                           self.ff.u.shape[1],
                                           self.ff.u.shape[2],
                                           self.ff.u.shape[3] ))
            for ny in range(self.ff.u.shape[2]):
                self.ff.u0[0, :, ny, :] = self.ff.y[ny]
                self.ff.u0[1, :, ny, :] = 0
                self.ff.u0[2, :, ny, :] = 0
        elif kind == 'zero':
            self.ff.u0 = np.ndarray(shape=(self.ff.u.shape[0],
                                           self.ff.u.shape[1],
                                           self.ff.u.shape[2],
                                           self.ff.u.shape[3] ))
            for ny in range(self.ff.u.shape[2]):
                self.ff.u0[0, :, ny, :] = 0
                self.ff.u0[1, :, ny, :] = 0
                self.ff.u0[2, :, ny, :] = 0


            pass
