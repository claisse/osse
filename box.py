"""
box.py implementing Box
This is a class for 3D periodic boundary conditions
A Sharma 2015, 2016
"""
import numpy as np
import warnings
from flowfield import FlowField


class box(FlowField):
    """
    class to implement 3D periodic box turbulence.
    Actually very little to do; FlowField does it all.
    """
