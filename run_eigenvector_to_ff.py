"""
run_eigenvector_to_ff.py

create an HDF5 file for an eigenvector from a .npy file

"""
import numpy as np
import h5py
from flowfield import FlowField
from plotfield import plot_slice
from operators import Operators
import itertools

# parameters
eq = 1
#eigenmode_space = {0, 1, 2, 8, 9}#, 11, 12, 13, 14, 15, 16, 17, 18, 19}
eigenmode_space = {0, 1, 2, 3, 4, 5}
Nx = 4
Ny = 15
Nz = 4
act = True
if act:
    _act = "_act"
else:
    _act = ""

# LOAD THE EQ SOLUTION
filename_eq = "./database/eq/"+\
        "eq"+str(eq)+"_"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)+".h5"
filename = "./database/eigenfolder/" \
            +"eq"+str(eq)+"/EIGS"+_act
ff_eq = FlowField(filename_eq)

op = Operators(ff_eq)

filename_eigenvalues_npy = filename+"/eq"+str(eq)+\
            "_"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)+"_EIG"+_act+".npy"
eigenvalues = np.load(filename_eigenvalues_npy)

for index in eigenmode_space:
    print("***************\n to FF -> Eigenmode: ", index, " : ",\
            eigenvalues[index], "\n***************")

    # LOAD THE EIGENVECTOR
    filename_eigenvector_npy = filename+"/eq"+str(eq)+\
            "_"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)+"_VEC"+_act+".npy"
    eigenvector = np.load(filename_eigenvector_npy)
    #print('eigenvector.shape : ', eigenvector.shape)
    eigenvector_veta = eigenvector[:, index]

    # TRANSFORMING INTO HDF5
    filename_ff = filename+"/eigenvectors/eigenvector"\
            +str(index)+"_eq"+str(eq)+"_"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)+_act+".h5"
    op.transform_eigenvector_to_ff(ff_eq, eigenvector_veta, filename_ff, act)

    # plot parameters
    # usage = "plotfield [--output=plot.png] [--cut=0]
    #               [--lines=u|v|w [--contourstep=0.1]] [--quiver]
    #               [--field=u|v|w|mean] [--normal=x|y|z|mean]
    #               [--addmean=mean.asc] flowfield.hd5"
    lines='u'
    cut = 0
    field = 'u'
    normal = 'y'

    # load the FlowField and  plotfield
    #ff = FlowField(filename_ff)
    #plot_slice(ff, cut=cut, field=field, normal=normal)#, lines=lines)
