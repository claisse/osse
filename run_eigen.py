"""
run_eigen.py:

Python3 file containing the program calling methods
from operators.py in order to:
    load a FlowField and its baseflow,
    calculate the associated nonlaminar baseflow OSS model associated,
    calculate the eigenvalues and eigenvectors of this model
    print and plot the eigenvalues

This is used in order to test the eigenvalues obtained and thus
validated the method of operators.py:
    oss_nonlaminar_baseflow()
    oss_nonlaminar_baseflow_actuated()
    oss_eigen()
    oss_eigen_print()
    oss_eigen_plot()

G Claisse 2017
"""
import numpy as np
from math import *
import itertools
import sys
import argparse

#matplotlib.rcParams['text.usetex'] = True
#matplotlib.rcParams['text.latex.unicode'] = True
import matplotlib
matplotlib.use('TkAgg')
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
import matplotlib.pyplot as plt

from channel import Channel
from operators import Operators as OP

def eigenvalues(n_eq, Nx, Ny, Nz):
    # PARAMETERS
    filename='database/eigenfolder/'
    Re = 400 # from Gibson 2008
    tau = 10E-5
    tau_v = tau
    tau_eta = tau
    tau_u = tau
    tau_w = tau
    actuation = False

    #Lx = 2*np.pi#/1.14 #5.51156605893 from Gibson 2008
    #Lz = 2*np.pi#*2/5 #2.51327412287 from Gibson 2008
    #Lx = 2*np.pi #2*np.pi/1.14 #5.51156605893
    #Lz = 2*np.pi #4*np.pi/5 #2.51327412287

    #list_Nx = {4}#{4, 8, 12, 16, 20}#, 24, 28, 32}
    #list_Ny = {15} #20
    #list_Nz = {4} #10
    #N_eq = {1} #{'SYM', 'ASYM'} #{2, 5, 9, 11}

    #for n_eq in N_eq:
        #for Nx, Ny, Nz in itertools.product(list_Nx, list_Ny, list_Nz):
    kind_of_flow = 'eq'+str(n_eq)+'_'+str(Nx)+'x'+str(Ny)+'x'+str(Nz)

    print('\n##############################################################################')
    print('database/eq/'+kind_of_flow)
    print('Actuation : ', actuation)
    print('Nx x Ny x Nz : ', Nx, ' x ', Ny, ' x ', Nz, '\n')

    # BUILDING FLOWFIELD
    ff = Channel('database/eq/'+kind_of_flow+'.h5')
    ff.load_baseflow('database/eq/'+kind_of_flow+'.h5')
    #ff.mean.apply_mean_flow(kind="couette")

    # add a Couette baseflow U(y) = y
    for ny in range(ff.u.shape[2]):
        ff.u0[0, :, ny, :] += ff.y[ny]
        #ff.u0[0, :, ny, :] = 1 - ff.y[ny]**2
        #ff.u0[1, :, ny, :] = 0 #ff.y[ny]
        #ff.u0[2, :, ny, :] = 0 #ff.y[ny]

    # BUILDING OPERATOR
    oss_op = OP(ff)
    if not(actuation):
        print('Not actuated')
        E, A = oss_op.op_nonlaminar_baseflow(ff, Re)
        EB = None
        act_str = ''
    else:
        print('Actuated')
        E, A, B = oss_op.op_nonlaminar_baseflow_actuated(ff, Re,
                                        tau_v=tau_v,
                                        tau_eta=tau_eta,
                                        tau_u=tau_u,
                                        tau_w=tau_w)
        EB = np.dot(E,B)
        act_str = '_act'

    # EIGENVALUES
    filename0 = filename+'eq'+str(n_eq)+'/EIGS'+act_str+'/'+kind_of_flow
    vals, vecs = oss_op.eigen(
                    filename=filename0,
                    EA=np.dot(E, A),
                    EB=EB,
                    timer=True,
                    memory=True,
                    tolerance=1E-8)

    oss_op.eigen_print(10, filename0+'_EIG'+act_str+'.npy')
    #oss_op.eigen_plot(filename0+'_EIG'+act_str+'.npy')

    #C1, D12D12 = oss_op.op_nonlaminar_baseflow_objectives(ff)

def main():
    parser = argparse.ArgumentParser(description='Find eigenvalues of a given EQ.')
    parser.add_argument("Neq", type=int,
            help="Equilibrium")
    parser.add_argument("Nx", type=int,
            help="Streamwise resolution")
    parser.add_argument("Ny", type=int,
            help="Wall-normal resolution")
    parser.add_argument("Nz", type=int,
            help="Spanwise resolution")
    #parser.add_argument("-Nx", "--Nx",
    #        type=int,
    #        help='Nx input')

    args = parser.parse_args()

    eigenvalues(args.Neq, args.Nx, args.Ny, args.Nz)

    return 0

if __name__=="__main__":
    sys.exit(main())
