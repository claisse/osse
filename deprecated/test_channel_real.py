import h5py
import numpy as np
from math import *
import unittest
import warnings

from flowfield import FlowField
from flowfield import DiffOperator_FlowField
from channel import Channel
from channel import DiffOperator_Channel
import chebyshev

import datetime

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

"""
TO DO IN THAT CLASS:
- Test with other flowfield, which are more turbulent
- Test with flowfield with a dataset 'p'

"""


class ChannelTestReal(unittest.TestCase):

    def setUp(self):
        #warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
        self.ff = Channel('database/eq1.h5')

    def test_interpolation(self):
        plt.figure()

        index = 2
        X = 25
        # Y =
        Z = 20

        #self.ff = Channel('database/eq1.h5')

        # TEST uniform2chebyshev() and chebyshev2uniform()
        original = plt.plot(
            self.ff.y[:],
            self.ff.u[
                index,
                X,
                :,
                Z],
            'o-',
            label='chebyshev')

        self.ff.chebyshev2uniform()
        original_physical = plt.plot(
            self.ff.y[:], self.ff.u[
                index, X, :, Z], 'o', label='uniform')

        ######
        self.ff.uniform2chebyshev()

        print('---')
        self.ff = Channel('database/eq1.h5')
        print(self.ff.state['y'])
        self.ff.uniform2chebyshev()
        self.ff.chebyshev2uniform()
        self.ff.chebyshev2uniform()

        print('---')
        self.ff = Channel('database/eq1.h5')
        print(self.ff.state['y'])
        #self.ff.state['y'] = 'Chebyshev'
        self.ff.uniform2chebyshev()
        #plot_spectral = plt.plot(self.ff.y[:], vector, 'ko')
        # plt.show()

        plt.legend(fontsize='small')
        plt. show()

    def test_deriv_real_u(self):
        #self.ff = Channel('database/eq1.h5')
        _order = 1
        index = 0
        Z = 25
        # Y =
        X = 20

        # Original Curve
        original = plt.plot(
            self.ff.y[:], self.ff.u[
                index, X, :, Z], '-', label='Original')

        # Gradient
        self.ff.chebyshev2uniform()
        N = self.ff.y.shape[0]
        y = np.linspace(self.ff.y[0], self.ff.y[N - 1], num=N)
        dy = y[1] - y[0]
        dudy = np.empty((4, self.ff.y.shape[0]))
        dudy[0] = np.gradient(self.ff.u[index, X, :, Z], dy)
        dudy[1] = np.gradient(dudy[0], dy)
        dudy[2] = np.gradient(dudy[1], dy)
        dudy[3] = np.gradient(dudy[2], dy)
        deriv_1 = plt.plot(y, dudy[_order - 1, :], 'x-',
                           label='Gradient order ' + str(_order))

        self.ff.uniform2chebyshev()
        # Differentiated
        print('\n***** method 1 ***** ')
        begin1 = np.datetime64(datetime.datetime.now())
        for counter in range(10):
            diff1 = self.ff.dop.deriv_u(y_order=_order, method=1)
        end1 = np.datetime64(datetime.datetime.now())

        # THIS COMMENTED CODE REQUIRE NEW VERSION OF NUMPY

        #print('\n***** method 2 ***** ')
        #begin2 = np.datetime64(datetime.datetime.now())
        # for counter in range(10):
        #    diff2 = self.ff.dop.deriv_u(y_order=_order, method=2)
        #end2 = np.datetime64(datetime.datetime.now())

        #print('\n***** method 3 ***** ')
        #begin3 = np.datetime64(datetime.datetime.now())
        # for counter in range(10):
        #    diff3 = self.ff.dop.deriv_u(y_order=_order, method=3)
        #end3 = np.datetime64(datetime.datetime.now())

        print('Derivative 10x method 1: elapsed time :', end1 - begin1)
        #print('Derivative 10x method 2: elapsed time :', end2-begin2)
        #print('Derivative 10x method 3: elapsed time :', end3-begin3)
        plot_deriv = plt.plot(self.ff.y[:], diff1[index, X, :, Z], 'x-',
                              label='Diff 1')
        # plot_deriv = plt.plot(self.ff.y[:], diff2[index,X,:,Z], 'x-',
        #                label='Diff 2')
        # plot_deriv = plt.plot(self.ff.y[:], diff3[index,X,:,Z], 'x-',
        #                label='Diff 3')
        plt.legend(fontsize='small')
        plt. show()

    def test_deriv_real_p(self):
        #self.ff = Channel('database/eq1.h5')
        _order = 4
        #index = 2
        X = 25
        # Y =
        Z = 20

        # Original Curve
        original = plt.plot(
            self.ff.y[:], self.ff.p[
                X, :, Z], '-', label='Original')

        # Gradient
        self.ff.chebyshev2uniform()
        Ny = self.ff.y.shape[0]
        y = np.linspace(-1, 1, num=Ny)
        dy = y[1] - y[0]
        dpdy = np.empty((_order, self.ff.y.shape[0]))
        dpdy[0] = np.gradient(self.ff.p[X, :, Z], dy)
        dpdy[1] = np.gradient(dpdy[0], dy)
        dpdy[2] = np.gradient(dpdy[1], dy)
        dpdy[3] = np.gradient(dpdy[2], dy)
        deriv_1 = plt.plot(y, dpdy[_order - 1, :], 'x-',
                           label='Gradient order ' + str(_order))

        self.ff.uniform2chebyshev()
        # Differentiated
        print('\n***** method 1 ***** ')
        begin1 = np.datetime64(datetime.datetime.now())
        for counter in range(10):
            diff1 = self.ff.dop.deriv_p(y_order=_order, method=1)
        end1 = np.datetime64(datetime.datetime.now())

        # THIS COMMENTED CODE REQUIRE NEW VERSION OF NUMPY

        #print('\n***** method 2 ***** ')
        #begin2 = np.datetime64(datetime.datetime.now())
        # for counter in range(10):
        #    diff2 = self.ff.dop.deriv_p(y_order=_order, method=2)
        #end2 = np.datetime64(datetime.datetime.now())

        #print('\n***** method 3 ***** ')
        #begin3 = np.datetime64(datetime.datetime.now())
        # for counter in range(10):
        #    diff3 = self.ff.dop.deriv_p(y_order=_order, method=3)
        #end3 = np.datetime64(datetime.datetime.now())

        print('Derivative 10x method 1: elapsed time :', end1 - begin1)
        #print('Derivative 10x method 2: elapsed time :', end2-begin2)
        #print('Derivative 10x method 3: elapsed time :', end3-begin3)
        plot_deriv = plt.plot(self.ff.y[:], diff1[X, :, Z], 'x-',
                              label='Diff 1')
        # plot_deriv = plt.plot(self.ff.y[:], diff2[X,:,Z], 'x-',
        #                label='Diff 2')
        # plot_deriv = plt.plot(self.ff.y[:], diff3[X,:,Z], 'x-',
        #                label='Diff 3')
        plt.legend(fontsize='small')
        plt. show()

    def test_mean_u(self):
        print('self.u.shape', self.ff.u.shape)
        # self.ff.subtract_mean()
        # self.ff.add_mean()

        mean_x, mean_y, mean_z = self.ff.calculate_mean()

        plt.figure()
        # mean_x = np.mean(np.mean(self.ff.u, axis=2, keepdims=True),
        #            axis=3, keepdims=True)
        print('mean in x direction, x-component of (U,V,W):', mean_x.shape)
        print("mean_x : \n", mean_x[0, 0, 0, 0])
        plt.plot(self.ff.x, mean_x[0, :, 0, 0])
        plt.show()

        plt.figure()
        # mean_y = np.mean(np.mean(self.ff.u, axis=1, keepdims=True),
        #            axis=3, keepdims=True)
        print('mean in x direction, y-component of (U,V,W):', mean_y.shape)
        print("mean_U_y : \n", mean_y[0, 0, 0, 0])
        plt.plot(self.ff.y, mean_y[0, 0, :, 0])
        plt.show()

        plt.figure()
        # mean_z = np.mean(np.mean(self.ff.u, axis=1, keepdims=True),
        #            axis=2, keepdims=True)
        print('mean in x direction, z-component of (U,V,W):', mean_z.shape)
        print("mean_U_z : \n", mean_z[0, 0, 0, 0])
        plt.plot(self.ff.z, mean_z[0, 0, 0, :])
        plt.show()

test = unittest.TestSuite()
test.addTest(ChannelTestReal("test_interpolation"))
test.addTest(ChannelTestReal("test_deriv_real_u"))
test.addTest(ChannelTestReal("test_deriv_real_p"))
# test.addTest(ChannelTestReal("test_mean_u"))

runner = unittest.TextTestRunner()
runner.run(test)
