"""
DEPRECATED

operators_lo.py
"""

import numpy as np
import scipy
import scipy.io

import sympy

from scipy.sparse.linalg import aslinearoperator
from scipy.sparse.linalg import eigs
from scipy.sparse.linalg import LinearOperator

from flowfield import FlowField
from flowfield import DiffOperator_FlowField
from channel import Channel
from channel import DiffOperator_Channel
from boundary_layer import Boundary_Layer
from boundary_layer import DiffOperator_Boundary_Layer

import chebyshev

import matplotlib.pyplot as plt


class Operators_lo:
    """
    perhaps generate resolvent modes on demand?
    Consider using either arpack svds
    or scipy interpolative matrix decompositon
    http://docs.scipy.org/doc/scipy-0.13.0/reference/linalg.interpolative.html
    """

    def __init__(self, ff: FlowField):
        """
        implicitly passed mean property of ff
        """
        pass

    def lnse_operator(self, ff: FlowField, Re, baseflow, alpha, beta):
        """
        Reurns Linearized Navier-Stokes equation operator
        for couple (alpha, beta):

        d [ u ]       [ A11   A12    0    -d/dx ]   [ u ]
          [ v ]     = [  0    A22    0    -d/dy ] . [ v ]
          [ w ]       [  0     0    A33   -d/dz ]   [ w ]
          [ p ] /dt   [ d/dx  d/dy  d/dz    0   ]   [ p ]

        Keyword arguments:
        ff:FlowField -- FlowField flow to access differential operators.
        Re -- Reynolds number
        baseflow -- U = U(y), V = 0, W = 0
        alpha -- mode streamwise = 2.pi.kx/Lx
        beta -- mode spanwise = 2.pi.kz/Lz

        Return:
        Lnse_lo -- scipy.sparse.linalg.LinearOperator,
            for LNSE operator

        LinearOperator comes with its adjoint (to call with .rmatvec())
        """
        # Constant definition
        j = np.complex(0, 1)
        k2 = (alpha**2 + beta**2)
        N = ff.u.shape[2]
        BC = 'none'

        def Lnse(V):
            # Function needs to handle V of size (N) or (N,1),
            # see documentation of LinearOperator
            V = V.ravel()

            # v need to be a vector (a ravel/flatten ndarray)
            u = V[0 * N:1 * N][np.newaxis, :, np.newaxis]
            v = V[1 * N:2 * N][np.newaxis, :, np.newaxis]
            w = V[2 * N:3 * N][np.newaxis, :, np.newaxis]
            p = V[3 * N:4 * N][np.newaxis, :, np.newaxis]

            du_dt = (1 / Re) * (ff.dop.deriv(u,
                                           0, 2, 0,
                                           bc=BC)[0, :, 0]
                              - k2 * u[0, :, 0])\
                    - baseflow[0, 0, :, 0] * j * alpha * u[0, :, 0]\
                    - v[0, :, 0] * ff.dop.deriv(
                                        baseflow,
                                        0, 1, 0,
                                        bc='none')[0, 0, :, 0]\
                    - j * alpha * p[0, :, 0]

            dv_dt = (1 / Re) * (ff.dop.deriv(v,
                                           0, 2, 0,
                                           bc=BC)[0, :, 0]
                              - k2 * v[0, :, 0]) \
                    - baseflow[0, 0, :, 0] * j * alpha * v[0, :, 0]\
                    - ff.dop.deriv(p,
                                   0, 1, 0,
                                   bc=BC)[0, :, 0]

            dw_dt = (1 / Re) * (ff.dop.deriv(w,
                                           0, 2, 0,
                                           bc=BC)[0, :, 0]
                              - k2 * w[0, :, 0])\
                    - baseflow[0, 0, :, 0] * j * alpha * w[0, :, 0]\
                    - j * beta * p[0, :, 0]

            dp_ = + j * alpha * u[0, :, 0]\
                  + ff.dop.deriv(v,
                                 0, 1, 0,
                                 bc=BC)[0, :, 0]\
                  + j * beta * w[0, :, 0]

            return np.concatenate((du_dt, dv_dt, dw_dt, dp_))

        def Lnse_adjoint(V):
            # Function needs to handle V of size (N) or (N,1),
            # see documentation of LinearOperator
            V = V.ravel()

            # v need to be a vector (a ravel/flatten ndarray)
            u = V[0 * N:1 * N][np.newaxis, :, np.newaxis]
            v = V[1 * N:2 * N][np.newaxis, :, np.newaxis]
            w = V[2 * N:3 * N][np.newaxis, :, np.newaxis]
            p = V[3 * N:4 * N][np.newaxis, :, np.newaxis]

            du_dt = (1 / Re) * (ff.dop.deriv(u, 0, 2, 0,
                                           bc=BC,
                                           transform='hermitian')
                                    [0, :, 0]
                              - k2 * u[0, :, 0]) \
                    - np.conjugate(baseflow[0, 0, :, 0]) \
                        * (-j) * alpha * u[0, :, 0] \
                    + (-j) * alpha * p[0, :, 0]

            dv_dt = - u[0, :, 0] \
                        * np.conjugate(ff.dop.deriv(baseflow,
                                                    0, 1, 0,
                                                    bc='none',
                                                    transform='none')
                                        [0, 0, :, 0]) \
                    + (1 / Re) * (ff.dop.deriv(v, 0, 2, 0,
                                             bc=BC,
                                             transform='hermitian')
                                    [0, :, 0]
                              - k2 * v[0, :, 0]) \
                    - np.conjugate(baseflow[0, 0, :, 0]) \
                        * (-j) * alpha * v[0, :, 0] \
                    + ff.dop.deriv(p,
                                   0, 1, 0,
                                   bc=BC,
                                   transform='hermitian')[0, :, 0]

            dw_dt = (1 / Re) * (ff.dop.deriv(w, 0, 2, 0,
                                           bc=BC,
                                           transform='hermitian')
                                    [0, :, 0]
                              - k2 * w[0, :, 0]) \
                    - np.conjugate(baseflow[0, 0, :, 0]) \
                        * (-j) * alpha * w[0, :, 0] \
                    + (-j) * beta * p[0, :, 0]

            dp_ = - (-j) * alpha * u[0, :, 0] \
                  - ff.dop.deriv(v,
                                 0, 1, 0,
                                 bc=BC,
                                 transform='hermitian')[0, :, 0] \
                  - (-j) * beta * w[0, :, 0]

            return np.concatenate((du_dt, dv_dt, dw_dt, dp_))

        Lnse_lo = LinearOperator(dtype=complex,
                                 shape=(4 * N, 4 * N),
                                 matvec=Lnse,
                                 rmatvec=Lnse_adjoint)
        return Lnse_lo

    def poisson(self, ff: FlowField):
        """
        Apply the Poisson equation.
        """
        print('p', ff.p.shape)
        print('grad', ff.dop.grad_p().shape)
        matrix = np.einsum('jklm,jiklm-> iklm',
                           ff.u,
                           ff.dop.grad_u())
        laplacian_p = -1 * ff.dop.div(matrix)
        # p = (nabla^2) ^(-1) * laplacian_p
        return laplacian_p

    def jacobian(self, ff: FlowField):
        """
        apply the LNSE operator for a channel
        """
        pass

    def pseudoInverse(self, L, rank):
        """
        THIS METHOD IS POSSIBLY NOT WORKING CORRECTLY.

        Compute the pseudo-inverse of a matrix or LinearOperator L
        for a given rank by computing its SVD decomposition.

        svd(L) = U . sigma . V.adjoint
        pseudoInverse(L) = V . sigma.inverse . U.adjoint

        Keyword arguments:
        L -- matrix or LinearOperator.
        rank -- rank of the evaluated pseudo-inverse.

        Return:
        u_p -- output eigenvectors of the pseudo-inverse (V).
        s -- list of invert eigenvalues.
        v_p -- input eigenvectors of the pseudo-inverse (U.adjoint).
        pseudo-inverse -- matrix of the evaluated pseudo-inverse.
        """
        u, s, vh = scipy.sparse.linalg.svds(L, rank)

        # for each non zero element of s, taking the reciprocal
        # depending on tolerance
        # Tolerance is define as (in matlab)
        # tol = machine_epsilon * max_dimension_s * max(eigenvalues)

        # tolerance = np.finfo(s.dtype).eps * np.size(s) * np.max(s)
        tolerance = 10E-15
        print(tolerance)

        for i in range(s.shape[0]):
            if s[i] > tolerance:
                s[i] = 1.0 / s[i]
            else:
                s[i] = 0

        ss_p = np.diagflat(s)
        u_p = np.conjugate(np.transpose(vh))
        v_p = np.conjugate(np.transpose(u))
        return u_p, s, v_p, np.dot(u_p, np.dot(ss_p, v_p))

    def lnse_resolvent(self,
                                ff: FlowField,
                                Re, baseflow, alpha, beta,
                                omega, rank=-1):
        """
        Returns Linear Navier-Stokes invese Resolvent Operator
        for couple (alpha, beta)
        and its singular value decomposition (SVD).

        ------------------RESOLVENT------------------
        [               [ 1 0 0 0 ]   [          ]  ]   [ u ]   [   ]
        [ - j * omega * [ 0 1 0 0 ] - [   LNSE   ]  ] . [ v ] = [ F ]
        [               [ 0 0 1 0 ]   [ OPERATOR ]  ]   [ w ]   [   ]
        [               [ 0 0 0 0 ]   [          ]  ]   [ p ]   [   ]

        Keyword arguments:
        ff:FlowField -- FlowField flow to access differential operators.
        Re -- Reynolds number
        baseflow -- U = U(y), V = 0, W = 0
        alpha -- mode streamwise = 2.pi.kx/Lx
        beta -- mode spanwise = 2.pi.kz/Lz
        omega -- frequency
        rank -- number of singular values in the SVD.
            if rank = -1, SVD is not calculated.
            rank need to be inferior to size Ny-1.

        Return:
        resolvent_lnse -- scipy.sparse.linalg.LinearOperator,
            for the resolvent of the LNSE.
        u -- SVD matrix U.
        s -- SVD matrix of singular value of given rank.
        vt -- SVD matrix V.

        References:
        - Resolvent definition:
            Experimental manipulation of wall turbulence: a systems approach
            McKeon, Sharma, Jacobi
            Phys. FLuids 25, 031301 (2013)
            doi:10.1063/1.4793444
        - Resolvent definition with pressure:
            Opposition control within the resolvent analysis framework
            Luhar, Sharma, McKeon,
            JFM 2014, vol 749, pp 597-626
            doi:10.101/jfm.2014.209
        """
        j = np.complex(0, 1)
        N = ff.u.shape[2]

        lnse_lo = self.lnse_operator(ff, Re, baseflow, alpha, beta)

        def resolvent_lnse(V):
            """
            Function to build Resolvent LinearOperator.
            """
            # DC = diffusion + convection component of LNSE

            DC = lnse_lo.matvec(V)

            u = V[0 * N:1 * N]
            v = V[1 * N:2 * N]
            w = V[2 * N:3 * N]
            p = V[3 * N:4 * N]

            resolvent_u = - j * omega * u - DC[0 * N:1 * N]
            resolvent_v = - j * omega * v - DC[1 * N:2 * N]
            resolvent_w = - j * omega * w - DC[2 * N:3 * N]
            resolvent_p = - DC[3 * N:4 * N]

            return np.concatenate((resolvent_u,
                                   resolvent_v,
                                   resolvent_w,
                                   resolvent_p))

        def resolvent_lnse_adjoint(V):
            """
            Function to build Resolvent LinearOperator adjoint.
            """
            # DC = diffusion + convection component of LNSE

            DC = lnse_lo.rmatvec(V)

            u = V[0 * N:1 * N]
            v = V[1 * N:2 * N]
            w = V[2 * N:3 * N]
            p = V[3 * N:4 * N]

            resolvent_adjoint_u = + j * omega * u - DC[0 * N:1 * N]
            resolvent_adjoint_v = + j * omega * v - DC[1 * N:2 * N]
            resolvent_adjoint_w = + j * omega * w - DC[2 * N:3 * N]
            resolvent_adjoint_p = - DC[3 * N:4 * N]

            return np.concatenate((resolvent_adjoint_u,
                                   resolvent_adjoint_v,
                                   resolvent_adjoint_w,
                                   resolvent_adjoint_p))

        resolvent_lnse_lo = LinearOperator(dtype=complex,
                                           shape=(4 * N, 4 * N),
                                           matvec=resolvent_lnse,
                                           rmatvec=resolvent_lnse_adjoint)
        if rank != -1:
            u, s, vt = scipy.sparse.linalg.svds(resolvent_lnse_lo, rank)
        elif rank == -1:
            u, s, vt = 'none', 'none', 'none'

        return resolvent_lnse_lo, u, s, vt

    def oss_resolvent(self,
                               ff: FlowField,
                               Re, baseflow, alpha, beta,
                               omega=1, rank=1):
        """
        A DIFFERENCE IS OBSERVED IF COMPARE WITH RESOLVENT
        USING MATRICES IN MATLAB OR IN PYTHON.
        THE CAUSE COULD BE THE METHOD PSEUDO_INVERSE.

        Returns Orr-Sommerfeld Squire Resolvent Operator
        for couple (alpha, beta)
        and its singular value decomposition (SVD).

                  -------------------RESOLVENT---------------------
        [  u  ] = [ - j * omega * [ Lap  0 ] - [ Los   0  ]  ]^(-1) . [  Fu  ]
        [ eta ]   [               [  0   1 ]   [ Lc   Lsq ]  ]        [ Feta ]

        with weightings and transformation (u, eta) <-> [u,v,w]
        [u,v,w].T = W . C . R . C^(-1) . W^(-1) . [Fu, Fv, Fw].T

        Keyword arguments:
        ff:FlowField -- FlowField flow to access differential operators.
        Re -- Reynolds number
        baseflow -- U = U(y), V = 0, W = 0
        alpha -- mode streamwise = 2.pi.kx/Lx
        beta -- mode spanwise = 2.pi.kz/Lz
        omega -- frequency
        rank -- number of singular values in the SVD.
            if rank = -1, SVD is not calculated.
            rank need to be inferior to size Ny-1.

        Return:
        resolvent -- scipy.sparse.linalg.LinearOperator,
            for the resolvent of the OSS.
        u -- SVD matrix U.
        s -- SVD matrix of singular value of given rank.
        vt -- SVD matrix V.

        Remark:
        - Phase has not been checked and set.
        - See Bewley and Liu for the definition of Q in the inner product
            in its discrete form (paragraph 2.4, p312).
        - Could use a Cholesky factorization to replace Q with two matrices.

        References:
        - Resolvent for :

        - Matrix (W.C) definition:
            Model-based scaling of the streamwise energy density
                in high-Reynolds-number turbulent channels.
            Moarref, Sharma, Tropp, McKeon
            JFM, 2013, vol 734, pp 275-346
            doi:10.1017/jfm.2013.457
        """

        ###################################
        # 0: SETTING CONSTANTS, OPERATORS #
        ###################################

        j = np.complex(0, 1)
        N = ff.u.shape[2]
        k2 = (alpha**2 + beta**2)
        zeros = np.zeros(shape=(N - 2))

        # factor for energy norm
        W = np.diag(np.sqrt(ff.w[1:N - 1]))
        Winv = np.linalg.inv(W)
        Wadj = np.conjugate(np.transpose(W))

        Los_lo, Lap_lo, OS_lo, Lsq_lo, Lc_lo = self.oss_operator(
                                                            ff,
                                                            Re,
                                                            baseflow,
                                                            alpha, beta,
                                                            orr=True,
                                                            squire=True,
                                                            omega=-1)

        ###################################################
        # 1: DEFINITION OF THE UNMAPPED INVERSE RESOLVENT #
        ###################################################

        def resolvent_inv_raw(V):
            """
            Function to build Resolvent LinearOperator.

            The comment line correspond to the case where the Laplacian
            is used to define -dv- but not to define -resolvent_v-.
            """
            V = V.ravel()
            v = V[0:N - 2]
            eta = V[N - 2:2 * (N - 2)]

            # dv = OS_lo.matvec(v)
            dv = Los_lo.matvec(v)
            deta = Lc_lo.matvec(v) + Lsq_lo.matvec(eta)

            # resolvent_v = - j * omega * v - dv
            resolvent_v = - j * omega * Lap_lo.matvec(v) - dv
            resolvent_eta = - j * omega * eta - deta

            return np.concatenate((resolvent_v, resolvent_eta, zeros))

        def resolvent_inv_raw_adjoint(V):
            """
            Function to build Resolvent LinearOperator adjoint.

            The comment line correspond to the case where the Laplacian
            is used to define -dv- but not to define -resolvent_v-.
            """
            V = V.ravel()
            v = V[0:N - 2]
            eta = V[N - 2:2 * (N - 2)]

            # dv = OS_lo.rmatvec(v) + Lc_lo.rmatvec(eta)
            dv = Los_lo.rmatvec(v) + Lc_lo.rmatvec(eta)
            deta = Lsq_lo.rmatvec(eta)

            # resolvent_v = - (-j) * omega * v - dv
            resolvent_v = - (-j) * omega * Lap_lo.rmatvec(v) - dv
            resolvent_eta = - (-j) * omega * eta - deta

            return np.concatenate((resolvent_v, resolvent_eta, zeros))

        # The raw inverse resolvent is
        # of size (3.N, 3.N) for (v, eta, zeros)
        # instead of (2.N, 2.N) for (v, eta)
        # to fit the mapping functions defined later.
        resolvent_inv_raw_lo = LinearOperator(
                                        dtype=complex,
                                        shape=(3 * (N - 2), 3 * (N - 2)),
                                        matvec=resolvent_inv_raw,
                                        rmatvec=resolvent_inv_raw_adjoint)

        #########################################
        # 2: DEFINITION OF THE MAPPING FUNCTION #
        #########################################

        def mapping(V):
            """
            Function to map the state vector [v, eta] into
            a velocity vector [u, v, w] as:

            [ u ]          [ i.kx.d/dy  -i.kz  0 ]   [  v  ]
            [ v ] = 1/k2 * [    k2        0    0 ] . [ eta ]
            [ w ]          [ i.kz.d/dy   i.kx  0 ]   [  0  ]

            Keyword argument:
            [v, eta] -- state vector

            Return:
            [u, v, w] -- velocity vector
            """
            V = V.ravel()

            v = V[0:N - 2]
            eta = V[N - 2:2 * (N - 2)]

            # expand v to be used with differentiation methods
            v_ex = np.insert(np.append(v, 0),
                             0, 0)[np.newaxis, :, np.newaxis]

            u = (j * alpha * ff.dop.deriv(v_ex, 0, 1, 0)[0, 1:N - 1, 0]
                 - j * beta * eta) / k2
            w = (j * beta * ff.dop.deriv(v_ex, 0, 1, 0)[0, 1:N - 1, 0]
                 + j * alpha * eta) / k2

            u = np.dot(W, u)
            v = np.dot(W, v)
            w = np.dot(W, w)

            return np.concatenate((u, v, w))

        def mapping_adjoint(V):
            """
            Function to map the state vector [v, eta] into
            a velocity vector [u, v, w] as:

            [  v  ] = 1/k2 * [ -i.kx.d/dy  k2  -i.kz.d/dy ]   [ u ]
            [ eta ]          [     i.kz    0      -i.kx   ] . [ v ]
            [  0  ]          [      0      0        0     ]   [ w ]
            Keyword argument:
            [u, v, w] -- velocity vector

            Return:
            [v, eta, 0] -- state vector
            """
            V = V.ravel()
            u = V[0 * (N - 2):1 * (N - 2)]
            v0 = V[1 * (N - 2):2 * (N - 2)]
            w = V[2 * (N - 2):3 * (N - 2)]

            u = np.dot(Wadj, u)
            v0 = np.dot(Wadj, v0)
            w = np.dot(Wadj, w)

            # expand u and w to be used with differentiation methods
            u_ex = np.insert(np.append(u, 0),
                                 0, 0)[np.newaxis, :, np.newaxis]
            w_ex = np.insert(np.append(w, 0),
                                 0, 0)[np.newaxis, :, np.newaxis]

            v = - j * alpha \
                    * ff.dop.deriv(u_ex,
                                   0, 1, 0,
                                   transform='hermitian')[0, 1:N - 1, 0]\
                    / k2\
                + v0\
                - j * beta \
                    * ff.dop.deriv(w_ex,
                                   0, 1, 0,
                                   transform='hermitian')[0, 1:N - 1, 0]\
                    / k2
            eta = (j * beta * u - j * alpha * w) / k2

            return np.concatenate((v.ravel(), eta.ravel(), zeros))

        # LinearOperator function needs a square matrix or
        # the same size of input and output to be defined.
        # For that reason, we expand the output of mapping() and
        # mapping_adjoint() with zeros.
        # Possibility to use a Cholesky factorisation on Q = C.H . w . C.
        mapping_lo = LinearOperator(dtype=complex,
                                    shape=(3 * (N - 2), 3 * (N - 2)),
                                    matvec=mapping,
                                    rmatvec=mapping_adjoint)

        mapping_adjoint_lo = LinearOperator(dtype=complex,
                                    shape=(3 * (N - 2), 3 * (N - 2)),
                                    matvec=mapping_adjoint,
                                    rmatvec=mapping)

        U, S, V, C = self.pseudoInverse(mapping_lo, rank)
        UH, SH, VH, CH = self.pseudoInverse(mapping_adjoint_lo, rank)

        def mapping_inverse(V):
            return np.dot(C, V)

        def mapping_inverse_adjoint(V):
            return np.dot(CH, V)

        #################################################
        # 3: DEFINITION OF THE MAPPED INVERSE RESOLVENT #
        #################################################

        def resolvent_inv_mapped(F):
            """
            Function to apply the mapping to the resolvent operator.
            """
            CI_F = mapping_inverse(np.ravel(F))
            R_CI_F = resolvent_inv_raw_lo.matvec(CI_F)
            C_R_CI_F = mapping(R_CI_F)
            return C_R_CI_F

        def resolvent_inv_mapped_adjoint(F):
            """
            Function to apply the mapping to the adjoint resolvent operator.
            """
            CH_F = mapping_adjoint(np.ravel(F))
            RH_CH_F = resolvent_inv_raw_lo.rmatvec(CH_F)
            CIH_RH_CH_F = mapping_inverse_adjoint(RH_CH_F)
            return CIH_RH_CH_F

        resolvent_inv_mapped_lo = LinearOperator(dtype=complex,
                                          shape=(3 * (N - 2), 3 * (N - 2)),
                                          matvec=resolvent_inv_mapped,
                                          rmatvec=resolvent_inv_mapped_adjoint)

        ###################################################
        # 4: PSEUDO INVERSION TO DEFINE THE RESOLVENT SVD #
        ###################################################

        if rank != -1:
            u, s, vt, resolvent = self.pseudoInverse(
                                        resolvent_inv_mapped_lo,
                                        rank)
            # u, s, vt = scipy.sparse.linalg.svds(
            #                            resolvent_inv_mapped_lo,
            #                            rank)
            # resolvent = np.dot(u, np.dot(np.diagflat(s), vt))

        else:
            u, s, vt, resolvent = 'none', 'none', 'none', 'none'

        return resolvent, u, s, vt

    def swirl(self, ff: FlowField):
        pass

    def oss_operator(self, ff: FlowField, Re, baseflow, alpha, beta,
            orr=True, squire=True, omega=-1):
        """
        Returns Orr-Sommerfeld-Squire equation operator:

        K_freq . [ Lap  0 ] d [ v ]     = [ Los   0  ] . [ v ]
                 [  0   I ]   [ n ] /dt   [ Lc   Lsq ]   [ n ]

        OR:

        K_freq . d [ v ]     = [ OS   0  ] . [ v ]
                   [ n ] /dt   [ Lc  Lsq ]   [ n ]

        with :
        - Lap     Laplacian operator
        - I       Identity
        - Los     Orr-Sommerfeld operator
        - Lc      Coupling operator
        - Lsq     Squire operator
        - OS      inv(Laplacian)*Orr-Sommerfeld operator
        - K_freq  frequency constant:
                    if omega != -1:
                        K_freq = - j . omega = - j . alpha . c
                            with omega  frequency
                                 alpha  wave mode
                                 c      wave speed
                    elif omaga = -1:
                        K_freq = 1

        ----------
        Keyword arguments:
        ff:FlowField -- FlowField flow to access differential operators.
        Re -- Reynolds number
        baseflow -- U = U(y), V = 0, W = 0
        alpha -- mode streamwise = 2.pi.kx/Lx
        beta -- mode spanwise = 2.pi.kz/Lz
        orr -- boolean
            if 'True' : return LinearOperators for Orr-Sommerfeld equation.
            if 'False': Los_lo, Lap_lo, OS_lo are None.
        squire -- boolean
            if 'True' : return LinearOperators for Squire equation.
            if 'False': Lsq_lo, Lc_lo are None.
        omega -- frequency use in the OSS model.
            set to omega = -1 (default) if not in use.

        ----------
        Return:
        Los_lo -- scipy.sparse.linalg.LinearOperator,
            for Orr-Sommerfeld operator
        Lap_lo -- scipy.sparse.linalg.LinearOperator,
            for Laplacian operator
        OS_lo -- scipy.sparse.linalg.LinearOperator, corresponding to
            OS_lo = inv(-1*Lap_lo) . (-1)*Los_lo
            using scipy.sparse.linalg.gmres as solver.
        Lsq_lo -- scipy.sparse.lingalg.LinearOperator,
            for Squire operator
        Lc_lo -- scipy.sparse.linalg.LinearOperator,
            for Coupling operator

        Each LinearOperator comes with its adjoint (to call with .rmatvec())

        ----------
        REMARKS:
        - Method scipy.sparse.linalg.eigs() returns some errors
            if used with Los_lo and Lap_lo, but works fine with OS_lo.

        - Method scipy.sparse.linalg.eigs() requires
            a generalized eigenvalues problem:
                    A.x = lambda.B.x
            with matrix/LinearOperator B being positive-definite
            (all eigenvalues positive).
            However, as Lap_lo is negative definite,
            we solved that problem by using:
                    (-A).x = lambda.(-B).x

        - Omitting first and last column and row let matrix B
            be non-singular.

        - Need improvement:
            To improve speed, operators only operate for N-2 x N-2 dataset,
            but derivative methods requires N x N dataset as they use boundary
            conditions. To use these methods, velocity field are extended as:
                v_ex[1 : N - 1] = v[1 : N - 1]
                v_ex[0] = v_ex[N] = 0
            So be careful is v[0] or v[N] are not zero.

        ----------
        VALIDATION:
        - Eigenvalues validated for Orr-Sommerfeld and Squire equations
            in a Channel, with omega = -1
            with table found in:
                Optimal and robust control and estimation
                    of linear paths to transition
                Thomas BEWLEY and Sharon LIU
                J. Fluid Mech 1998, vol 365, pp. 305-349
                Table 1 for alpha=1, beta=0, Re=10000
                Table 2 for alpha=0, beta=2.044, Re=5000
        - Eigenvalue for omaga = +1 validated for Orr-SOmmerfeld equations
            in a Channel
            with table 1 and figure 2 in:
                The eigenvalue spectrum of the Orr-Sommereld problem
                Basil N. ANTAR
                NASA Technical Report NASA TR R-469, 1976
        - No validation has been made for a boundary layer, as the
            differentiation matrices should be changed.
        """
        # Constant definition
        j = np.complex(0, 1)
        k2 = (alpha**2 + beta**2)
        N = ff.u.shape[2]
        BC = 'clamped'  # 'dirichlet'
        if omega == -1:
            K_freq = 1
        else:
            K_freq = -j * omega

        # For Boundary layer, use baseflow-1 for derivative method
        # as boundaryLayer derivative methods requires data to
        # tend to zero at inifity.

        ##########################################
        # DEFINITION OF ORR-SOMMERFELD OPERATORS #
        ##########################################
        if orr:
            # v need to be a vector (a ravel/flatten ndarray)
            def Los(v):
                """
                Function to build Orr-Sommerfled LinearOperator.
                """
                v = v.ravel()
                N = v.shape[0] + 2
                # Definition of v_ex,
                # extended v to size [0, N, 0] to apply method deriv()
                v_ex = np.insert(np.append(v, 0),
                                 0, 0)[np.newaxis, :, np.newaxis]
                __Los = \
                    + ff.dop.deriv(baseflow - 1,
                                   0, 2, 0,
                                   bc=BC)[0, 0, 1:N - 1, 0] \
                        * j * alpha * v \
                    - baseflow[0, 0, 1:N - 1, 0] * j * alpha \
                        * (ff.dop.deriv(v_ex, 0, 2, 0,
                                        bc=BC)[0, 1:N - 1, 0]
                           - k2 * v) \
                    + (1 / Re) * (ff.dop.deriv(v_ex,
                                               0, 4, 0,
                                               bc=BC)[0, 1:N - 1, 0]
                                  - 2 * k2 * ff.dop.deriv(v_ex,
                                                          0, 2, 0,
                                                          bc=BC)
                                                          [0, 1:N - 1, 0]
                                  + k2**2 * v)
                return +1 * __Los

            def Los_adjoint(v):
                """
                Function to build Orr-Sommerfled LinearOperator adjoint.

                Remark:
                1. Do not apply conjugate in the return statement as we need:
                    v' = D.H * v and not v'.H = D.H * v.H.
                2. Differentiation matrices in ff.dop.deriv() methods are
                    only useful if v(+1) = v(-1) = 0, which is actually not
                    the case if use wit svds.
                        -> SOLUTION ?
                        -> IS IT TRUE ?
                """
                v = v.ravel()
                N = v.shape[0] + 2
                # Definition of v_ex,
                # extended v to size [0, N, 0] to apply method deriv()
                v_ex = np.insert(np.append(v, 0),
                                 0, 0)[np.newaxis, :, np.newaxis]
                __Los = \
                    + np.conjugate(
                        ff.dop.deriv(baseflow - 1,
                                   0, 2, 0,
                                   bc=BC,
                                   transform='none')[0, 0, 1:N - 1, 0]) \
                        * (-j) * alpha * v \
                    - np.conjugate(baseflow[0, 0, 1:N - 1, 0]) * (-j) * alpha\
                        * (ff.dop.deriv(v_ex, 0, 2, 0,
                                        bc=BC,
                                        transform='hermitian')[0, 1:N - 1, 0]
                           - k2 * v) \
                    + (1 / Re) * (ff.dop.deriv(
                                            v_ex,
                                            0, 4, 0,
                                            transform='hermitian',
                                            bc=BC)[0, 1:N - 1, 0]
                                  - 2 * k2 * ff.dop.deriv(
                                                    v_ex,
                                                    0, 2, 0,
                                                    bc=BC,
                                                    transform='hermitian')
                                                    [0, 1:N - 1, 0]
                                  + k2**2 * v)
                return +1 * __Los

            Los_lo = LinearOperator((N - 2, N - 2),
                                    dtype=complex,
                                    matvec=Los,
                                    rmatvec=Los_adjoint)

            def Lap(v):
                """
                Function to build Laplacian LinearOperator.
                """
                v = v.ravel()
                N = v.shape[0] + 2
                # Definition of v_ex,
                # extended v to size [0, N, 0] to apply method deriv()
                v_ex = np.insert(np.append(v, 0),
                                 0, 0)[np.newaxis, :, np.newaxis]
                __Lap = (ff.dop.deriv(v_ex,
                                      0, 2, 0,
                                      bc=BC)[0, 1:N - 1, 0]
                         - k2 * v)
                # __Lap = np.dot(D, v_ex[0,:,0]) [1:N - 1]
                return +1 * K_freq * __Lap

            def Lap_adjoint(v):
                """
                Function to build Laplacian LinearOperator adjoint.
                """
                v = v.ravel()
                N = v.shape[0] + 2
                # Definition of v_ex,
                # extended v to size [0, N, 0] to apply method deriv()
                v_ex = np.insert(np.append(v, 0),
                                 0, 0)[np.newaxis, :, np.newaxis]
                __Lap = (ff.dop.deriv(v_ex,
                                      0, 2, 0,
                                      bc=BC,
                                      transform='hermitian')[0, 1:N - 1, 0]
                         - k2 * v)
                # __Lap = np.dot(D.T, v_ex[0,:,0]) [1:N - 1]
                return +1 * np.conjugate(K_freq) * __Lap

            Lap_lo = LinearOperator((N - 2, N - 2),
                                    matvec=Lap,
                                    rmatvec=Lap_adjoint,
                                    dtype=complex)
            Lap_lo_H = LinearOperator((N - 2, N - 2),
                                             matvec=Lap_adjoint,
                                             dtype=complex,
                                             rmatvec=Lap)

            # HOW TO REPLACE MATRIX INVERSE GMRES ?
            def OS(v):
                """
                Function to build
                inv((-1)*Laplacian)*(-1)*Orr-Sommerfeld LinearOperator.
                """
                v = v.ravel()
                # possibility to find a preconditionner for GMRES calculation
                sol, info = scipy.sparse.linalg.bicgstab(-Lap_lo,
                                                      -Los_lo.matvec(v),
                                                      tol=10e-17,
                                                      maxiter=30000)

                # Use to test different cases:
                # sol = np.dot(np.linalg.inv(D[1:N-1, 1:N-1]), v)
                # sol = np.dot(np.linalg.inv(D[1:N-1, 1:N-1]),
                #             Los_lo.matvec(v))
                # info = 0

                if info == 0:
                    print('.', end="", flush=True)
                else:
                    print('BICGSTAB fails, number of iterations :', info)
                return sol

            def OS_adjoint(v):
                # Solution of : x = -Los^H . -Lap^(-1)^H . v
                # 1. x0 = -Lap^(-1)^H . v
                #    with gmres (-Lap^h, v)
                # 2. x = -Los^H . x0
                v = v.ravel()
                x0, info = scipy.sparse.linalg.bicgstab(-Lap_lo_H,
                                                      v,
                                                      tol=10e-17,
                                                      maxiter=30000)
                x = -Los_lo.rmatvec(x0)

                # Use to test different cases:
                # x = np.dot(np.linalg.inv(D.T[1:N-1, 1:N-1]), v)
                # x = Los_lo.rmatvec( np.dot(np.linalg.inv(D.T[1:N-1, 1:N-1]),
                #                            v))
                # info = 0

                if info == 0:
                    print('.', end="", flush=True)
                else:
                    print('Adjoint BICGSTAB fails, number of iterations :',
                          info)
                return x

            OS_lo = LinearOperator((N - 2, N - 2),
                                    matvec=OS,
                                    rmatvec=OS_adjoint,
                                    dtype=complex)
        else:
            Los_lo = None
            Lap_lo = None
            OS_lo = None

        ########################################
        #  DEFINITION OF THE SQUIRE OPERATORS  #
        ########################################
        if squire:
            def Lsq(eta):
                """
                Function to build Squire LinearOperator.
                """
                eta = eta.ravel()
                N = eta.shape[0] + 2
                # Definition of eta_ex,
                # extended eta to size [0, N, 0] to apply method deriv()
                eta_ex = np.insert(np.append(eta, 0),
                                   0, 0)[np.newaxis, :, np.newaxis]
                __Lsq = \
                    + (1 / Re) \
                        * (ff.dop.deriv(eta_ex, 0, 2, 0, bc=BC)
                                                        [0, 1:N - 1, 0]
                           - k2 * eta) \
                    - j * alpha * baseflow[0, 0, 1:N - 1, 0] * eta
                return __Lsq / K_freq

            def Lsq_adjoint(eta):
                """
                Function to build Squire LinearOperator adjoint.
                """
                eta = eta.ravel()
                N = eta.shape[0] + 2

                # Definition of eta_ex,
                # extended eta to size [0, N, 0] to apply method deriv()
                eta_ex = np.insert(np.append(eta.ravel(), 0),
                                   0,
                                   0)[np.newaxis, :, np.newaxis]
                __Lsq = \
                    + (1 / Re) \
                        * (ff.dop.deriv(
                                    eta_ex, 0, 2, 0,
                                    bc=BC,
                                    transform='hermitian')[0, 1:N - 1, 0]
                           - k2 * eta) \
                    - (-j) * alpha * np.conjugate(
                                        baseflow[0, 0, 1:N - 1, 0]) * eta
                return __Lsq / np.conjugate(K_freq)

            Lsq_lo = LinearOperator((N - 2, N - 2),
                                    matvec=Lsq,
                                    rmatvec=Lsq_adjoint,
                                    dtype=complex)

            def Lc(v):
                """
                Function to build Coupling LinearOperator.
                """
                v = v.ravel()
                N = v.shape[0] + 2
                __Lc = \
                    - j * beta * ff.dop.deriv(baseflow - 1,
                                              0, 1, 0,
                                              bc=BC)[0, 0, 1:N - 1, 0] * v
                return __Lc / K_freq

            def Lc_adjoint(eta):
                """
                Function to build Coupling LinearOperator adjoint.
                """
                eta = eta.ravel()
                N = eta.shape[0] + 2
                __Lc = \
                    - (-j) * beta * np.conjugate(
                                        ff.dop.deriv(baseflow - 1,
                                                     0, 1, 0,
                                                     bc=BC,
                                                     transform='none')
                                        [0, 0, 1:N - 1, 0]) * eta
                return __Lc / np.conjugate(K_freq)

            Lc_lo = LinearOperator((N - 2, N - 2),
                                   matvec=Lc,
                                   rmatvec=Lc_adjoint,
                                   dtype=complex)
        else:
            Lsq_lo = None
            Lc_lo = None

        return Los_lo, Lap_lo, OS_lo, Lsq_lo, Lc_lo

    def oss_eigen(
            self, ff: Channel,
            Re, baseflow, alpha, beta,
            orr=True, squire=True,
            omega=-1, tol=1E-5, maxiter=10000, which='LR'):
        """
        Wrapper of mathod scipy.sparse.linalg.eigs to determine eigenvalues
        of the Orr-Sommerfeld equation with LinearOperator only.

        Keyword Arguments:
        ff:Channel -- Channel(FlowField) flow to access differential operators.
        Re -- Reynolds number
        baseflow -- U = U(y), V = 0, W = 0
        alpha -- mode streamwise = 2.pi.kx/Lx
        beta -- mode spanwise = 2.pi.kz/Lz
        orr -- boolean, return values for Orr-Sommerfeld equation,
            default 'True'.
        squire -- boolean, return values for Squire equation,
            default 'True'.
        omega -- see method oss_operator()

        Keyword Arguments for scipy.sparse.linalg.eigs (see documentation):
        tol -- tolerance of scipy.sparse.linalg.eigs,
            default 1E-5
        maxtir -- maximal number of iteration of scipy.sparse.linalg.eigs,
            default 10000
        which -- orientation of research of eigenvalues
            of scipy.sparse.linalg.eigs, default 'LR'.

        Return:
        eigenvalue_os -- eigenvalues of the Orr-Sommerfeld eq.
        eigenvector_os -- eigenvectors of the Orr-Sommerfeld eq.
        eigenvalue_sq -- eigenvalues of the Squire eq.
        eigenvector_sq -- eigenvectors of the Squire eq.

        REMARK:
        eigenvector_os and eigenvector_sq are respectively eigenvectors of the
        Orr-Sommerfeld eq. and Squire eq. but are no eigenvectors
        for the whole Orr-Sommerfeld-Squire system.
        """
        Los_lo, Lap_lo, OS_lo, Lsq_lo, Lc_lo = \
            self.oss_operator(ff,
                              Re,
                              baseflow,
                              alpha,
                              beta,
                              orr=orr,
                              squire=squire,
                              omega=omega)

        k = Los_lo.shape[0] - 2

        if orr:
            eigenvalue_os, eigenvector_os = \
                scipy.sparse.linalg.eigs(OS_lo,
                                         k,
                                         which=which,
                                         maxiter=maxiter,
                                         tol=tol)
            # Uncomment next lines to compute eigs with Los_lo and Lap_lo.
            # The computation often crash with these inputs.
            # eigenvalue_os, eigenvector_os = scipy.sparse.linalg.eigs(
            #                                               Los_lo,
            #                                               k,
            #                                               which=which,
            #                                               maxiter=maxiter,
            #                                               tol=tol,
            #                                               M=Lap_lo)
        else:
            eigenvalue_os = None
            eigenvector_os = None

        if squire:
            eigenvalue_sq, eigenvector_sq = \
                scipy.sparse.linalg.eigs(Lsq_lo,
                                         k,
                                         which=which,
                                         maxiter=maxiter,
                                         tol=tol)
        else:
            eigenvalue_sq = None
            eigenvector_sq = None

        return eigenvalue_os, eigenvector_os, eigenvalue_sq, eigenvector_sq

    def oss_wall_actuation_v(
            self, ff: Channel,
            Re, baseflow, alpha, beta,
            tau, q_upper, q_lower):
        """
        """
        N = ff.u.shape[2]

        Los_lo, Lap_lo, OS_lo, Lsq_lo, Lc_lo = \
            self.oss_operator(ff,
                              Re,
                              baseflow,
                              alpha,
                              beta)

        def f_upper(y):
            return 0.25 * (2 * y**4 - y**3 - 4 * y**2 + 3 * y + 4)

        def f_lower(y):
            return 0.25 * (2 * y**4 + y**3 - 4 * y**2 - 3 * y + 4)

        # at t=0
        # v_+1 = 0
        # v_-1 = 0
        # and V = np.concat((self.ff.u[1,alpha,:,beta], v_+1, v_-1))

        def Los_act(V):
            V = V.ravel()
            v0 = V[0:N - 2]
            v_u = V[N - 2]
            v_l = V[N - 1]

            fvu = f_upper(v_u)
            fvl = f_lower(v_l)

            dv_u = - v_u / tau + q_upper / tau
            dv_l = - v_l / tau + q_lower / tau
            dv_0 = Los_lo.matvec(v0 + fvu + fvl)  # \
            # + Los_lo.matvec(fvu) \
            # + Los_lo.matvec(fvl)

            return np.concatenate((dv_u, dv_l, dv_0))

        def Lap_act(V):
            V = V.ravel()
            v0 = V[0:N - 2]
            v_u = V[N - 2]
            v_l = V[N - 1]

            # fvu = f_upper(v_u)
            # fvl = f_lower(v_l)

            dv = Lap_lo.matvec(v0 + f_upper(v_u) + f_lower(v_l))  # \
            # + Lap_lo.matvec(fvu) \
            # + Lap_lo.matvec(fvl)

            return np.concatenate((dv, v_u, v_l))

        Los_act_lo = LinearOperator((N, N),
                                    matvec=Los_act,
                                    dtype=complex)
        Lap_act_lo = LinearOperator((N, N),
                                    matvec=Lap_act,
                                    dtype=complex)

        return Los_act_lo, Lap_act_lo, Lsq_lo, Lc_lo


class OperatorsValidation_lo:
    """
    Class similar to class Operators
    but that one uses matrices in order to compare results
    using LinearOperator of class Operators
    with Matrices of this class.
    """

    def __init__(self, ff: FlowField):
        self.operator = Operators_lo(ff)
        pass

    def oss_operator_matrices(self, ff: Channel, Re, baseflow, alpha, beta):
        """
        Operate as method oss_operator
        but use full matrices instead of LinearOperator.
        (see method oss_operator for documentation)

        Return:
        Los_mat -- Orr-Sommerfeld matrix.
        Lap_mat -- Laplacian matrix.
        OS_mat --  inv(Lap_mat).Los_mat matrix.
        Los_aslo -- scipy.sparse.linalg.asLinearOperator for Los_mat.
        Lap_aslo -- scipy.sparse.linalg.asLinearOperator for Lap_mat.
        OS_aslo -- scipy.sparse.linalg.asLinearOperator for OS_mat
        """
        # Constant definition
        j = np.complex(0, 1)
        k2 = (alpha**2 + beta**2)
        N = ff.u.shape[2]

        ##################################################################
        np.random.seed(0)
        Real = np.random.random((N, N)) - 0.5
        # Real = np.dot(Real, Real.T)
        Im = np.random.random((N, N)) - 0.5
        # Im = np.dot(Im, Im.T)
        # Los_mat = Real + j*Im
        ##################################################################

        nodes, DM = chebyshev.chebdiff(N, 4)
        yy, D4 = chebyshev.cheb4c(N)

        # boundary condition
        DM[:, 0, :] = 0
        DM[:, N - 1, :] = 0
        DM[:, :, 0] = 0
        DM[:, :, N - 1] = 0
        DM[3, 1:N - 1, 1:N - 1] = D4[:, :]

        Los_mat = + np.dot(DM[1, :, :], baseflow[0, 0, :, 0]) \
                    * j * alpha * np.eye(N) \
                - np.dot(baseflow[0, 0, :, 0] * np.eye(N) * j * alpha,
                         1 * DM[1, :, :] - k2 * np.eye(N)) \
                + (DM[3, :, :]
                   - 2 * DM[1, :, :] * k2
                   + k2**2 * np.eye(N)) / Re
        Los_mat = Los_mat[1:N - 1, 1:N - 1]

        Lap_mat = DM[1, :, :] - k2 * np.eye(N)
        Lap_mat = Lap_mat[1:N - 1, 1:N - 1]

        OS_mat = np.dot(np.linalg.inv(Lap_mat), Los_mat)

        if 0:
            # Test symetric
            print('Matrix symetric ? \n',
                  (Los_mat.transpose() == Los_mat).all())
            # Test Hermitian
            print('Matrix hermitian ? \n',
                  (Los_mat.transpose().real == Los_mat.real).all()
                  and
                  (Los_mat.transpose().imag == -Los_mat.imag).all())

        # scipy.io.savemat('matlab_scripts/L'+str(N)+'.mat',
        #                    mdict={'Los': -Los_mat,
        #                           'Lap': -Lap_mat,
        #                           'OS': OS_mat})

        # Building LinearOperator from matrices
        Los_aslo = aslinearoperator(Los_mat)
        Lap_aslo = aslinearoperator(Lap_mat)
        OS_aslo = aslinearoperator(OS_mat)

        return -Los_mat, -Lap_mat, OS_mat, Los_aslo, Lap_aslo, OS_aslo

    def oss_plot_diff(self, ff: Channel, Re, baseflow, alpha, beta):
        """
        PLot the difference between LinearOperaotor and Matrices
        for the Orr-Sommerfeld equation.
        """
        Los_mat, Lap_mat, OS_diff, Los_aslo, Lap_aslo, OS_aslo = \
            self.oss_operator_matrices(ff,
                                       Re,
                                       baseflow,
                                       alpha,
                                       beta)
        Los_lo, Lap_lo, OS_lo, Lsq_lo, Lc_lo = \
            self.operator.oss_operator(ff,
                                       Re,
                                       baseflow,
                                       alpha,
                                       beta)

        N = ff.u.shape[2]
        V = ff.u[1, alpha, 1:N - 1, beta]

        RHS_os_mat = np.dot(Los_mat, V)
        RHS_os_lo = Los_lo.matvec(V)
        RHS_os_aslo = Los_aslo.matvec(V)
        # RHS_os_lo = Los_lo.matvec(V.ravel()).reshape(original_shape)

        Lap_v_mat = np.dot(Lap_mat, V)
        Lap_v_lo = Lap_lo.matvec(V)
        Lap_v_aslo = Lap_aslo.matvec(V)

        print('DIFF between MAT and LO: \n ', RHS_os_mat - RHS_os_lo)

        Y = ff.y[1:N - 1]
        plt.figure()
        if 1:
            plt.plot(Y, RHS_os_mat.real, 'go-')
            plt.plot(Y, RHS_os_mat.imag, 'gx--')
            plt.plot(Y, RHS_os_lo.real, 'ro-')
            plt.plot(Y, RHS_os_lo.imag, 'rx--')
            plt.plot(Y, RHS_os_aslo.real, 'k-')
            plt.plot(Y, RHS_os_aslo.imag, 'k--')

        if 0:
            plt.plot(Y, Lap_v_mat.real, 'go-')
            plt.plot(Y, Lap_v_mat.imag, 'gx--')
            plt.plot(Y, Lap_v_lo.real, 'ro-')
            plt.plot(Y, Lap_v_lo.imag, 'rx--')
            plt.plot(Y, Lap_v_aslo.real, 'k-')
            plt.plot(Y, Lap_v_aslo.imag, 'k--')

        plt.show()

    def oss_eigen_full(self, ff: Channel, Re, baseflow, alpha, beta):
        """
        - Determine the eigenvalues and eigenvectors
            of the Orr-Sommerfeld equations,
            using matrices, LinearOperator and asLinearOperator.
        - Print the eigenvalues and their accuracy:
            norm(Los*eigenvector - eigenvalue*Lap*eigenvector).
        - Plot the eigenvalues determined
            with different methods.

        Keyword Arguments:
        ff:Channel -- Channel(FlowField) flow
            to access differential operators.
        Re -- Reynolds number
        baseflow -- U = U(y), V = 0, W = 0
        alpha -- mode streamwise = 2.pi.kx/Lx
        beta -- mode spanwise = 2.pi.kz/Lz

        Return:
        vals_sp -- scipy eigenvalue of the matrix form of OSS eq.
        """

        Los_mat, Lap_mat, OS_mat, Los_aslo, Lap_aslo, OS_aslo = \
            self.oss_operator_matrices(ff,
                                       Re,
                                       baseflow,
                                       alpha,
                                       beta)
        Los_lo, Lap_lo, OS_lo, Lsq_lo, Lc_lo = \
            self.operator.oss_operator(ff,
                                       Re,
                                       baseflow,
                                       alpha,
                                       beta)

        plt.figure()

        # print('positive definite ?\n', 1)
        # print('det :\n ', np.linalg.det(Los_mat))
        # print('\n sparse matrix ?\n', scipy.sparse.isspmatrix(Los_mat))

        # SCIPY SPARSE LINALG LINEAR OPERATOR
        k = ff.u.shape[2] - 4
        if 1:
            vals_sparse1, vecs1 = scipy.sparse.linalg.eigs(OS_lo,
                                                           k,
                                                           which='LR',
                                                           maxiter=100000,
                                                           tol=1E-5)
            # vals_sparse1, vecs1 = scipy.sparse.linalg.eigs(Los_lo,
            #                                                k,
            #                                                which='SR',
            #                                                maxiter=100000,
            #                                                tol=1E-3,
            #                                                M=Lap_lo)
            print('\nEigenvalues (scipy.sparse.linalg LO) : \n',
                  np.round(np.sort(vals_sparse1), 6))

            norm_sparse1 = np.ndarray((k))
            for index in range(k):
                norm_sparse1[index] = np.linalg.norm(
                    np.dot(Los_mat, vecs1[:, index])
                    - vals_sparse1[index]
                    * np.dot(Lap_mat, vecs1[:, index]))
            print('Max norm : ', norm_sparse1.max())
            print('Min norm : ', norm_sparse1.min())
            print('Avg norm : ', norm_sparse1.mean())

            plt.plot(vals_sparse1.real,
                     vals_sparse1.imag,
                     'rx',
                     ms='5',
                     mew='2',
                     label='Eigenvalues scipy.SPARSE.linalg.eigs LO')

        # SCIPY SPARSE ARRAY
        if 1:
            vals_sparse2, vecs2 = scipy.sparse.linalg.eigs(OS_mat,
                                                           k,
                                                           which='LR',
                                                           maxiter=1000000,
                                                           tol=1E-5)
            # vals_sparse2, vecs2 = scipy.sparse.linalg.eigs(Los_mat,
            #                                                k,
            #                                                which='SR',
            #                                                maxiter=10000,
            #                                                tol=1E-5,
            #                                                M=Lap_mat)
            print('\nEigenvalues (scipy.sparse.linalg MAT) : \n',
                  np.round(np.sort(vals_sparse2), 6))

            norm_sparse2 = np.ndarray((k))
            for index in range(k):
                norm_sparse2[index] = np.linalg.norm(
                    np.dot(Los_mat, vecs2[:, index])
                    - vals_sparse2[index]
                    * np.dot(Lap_mat, vecs2[:, index]))
            print('Max norm : ', norm_sparse2.max())
            print('Min norm : ', norm_sparse2.min())
            print('Avg norm : ', norm_sparse2.mean())

            plt.plot(vals_sparse2.real,
                     vals_sparse2.imag,
                     'g+',
                     ms='7',
                     mew='2',
                     label='Eigenvalues scipy.SPARSE.linalg.eigs MAT ')

        # SCIPY SPARSE AS LINEAR OPERATOR
        if 1:
            vals_sparse3, vecs3 = scipy.sparse.linalg.eigs(OS_aslo,
                                                           k,
                                                           which='LR',
                                                           maxiter=100000,
                                                           tol=1E-5)
            # vals_sparse3, vecs3 = scipy.sparse.linalg.eigs(Los_aslo,
            #                                                k,
            #                                                which='SR',
            #                                                maxiter = 100000,
            #                                                tol=1E-5,
            #                                                M=Lap_aslo)
            print('\nEigenvalues (scipy.sparse.linalg AS LO) : \n',
                  np.round(np.sort(vals_sparse3), 6))

            norm_sparse3 = np.ndarray((k))
            for index in range(k):
                norm_sparse3[index] = np.linalg.norm(
                    np.dot(Los_mat, vecs3[:, index])
                    - vals_sparse3[index]
                    * np.dot(Lap_mat, vecs3[:, index]))
            print('Max norm : ', norm_sparse3.max())
            print('Min norm : ', norm_sparse3.min())
            print('Avg norm : ', norm_sparse3.mean())

            plt.plot(vals_sparse3.real, vals_sparse3.imag,
                     'c+', ms='7',
                     mew='2',
                     label='Eigenvalues scipy.SPARSE.linalg.eigs MAT AS LO ')

        # SCIPY LINALG
        if 1:
            vals_sp, vecs_sp = scipy.linalg.eig(Los_mat, b=Lap_mat)
            print('\nEigenvalues (scipy.linalg.eig) : \n',
                  np.round(np.sort(vals_sp), 6))

            norm_sp = np.ndarray((vals_sp.shape[0]))
            for index in range(vals_sp.shape[0]):
                norm_sp[index] = np.linalg.norm(
                    np.dot(Los_mat, vecs_sp[:, index])
                    - vals_sp[index]
                    * np.dot(Lap_mat, vecs_sp[:, index]))
            print('Max norm : ', norm_sp.max())
            print('Min norm : ', norm_sp.min())
            print('Avg norm : ', norm_sp.mean())

            plt.plot(vals_sp.real, vals_sp.imag, 'm^',
                     label='Eigenvalues SCIPY.linalg.eig')

        # SYMPY
        if 0:
            x = sympy.Symbol('x')
            D = sympy.Matrix(Los_mat)
            print('det D :', D.det())
            E = D - x * np.eye(Los_mat.shape[0])
            eig_sympy = sympy.solve(E.det(), x)
            print('\nEigenvalues (sympy) : \n', eig_sympy)
            # for i in range(Los_mat.shape[0]):
            #    plt.plot(sympy.re[i], sympy.im[i].imag, 'bv')

        # Save matrices for Matlab.
        # scipy.io.savemat('matlab_scripts/eig'+str(Los_mat.shape[0]+2)+'.mat',
        #                 mdict={'scipy': vals_sp,
        #                        'Los': Los_mat,
        #                        'Lap': Lap_mat})

        plt.legend(fontsize='small')
        plt.title('Eigenvalues of the Orr-Sommerfeld equation.')
        plt.grid(b=True, which='Major')
        plt.xlim(-0.5, 0.1)
        plt.ylim(-1, 0)
        plt.xlabel('real')
        plt.ylabel('imag')
        plt.show()
        # plt.close()

        return vals_sp
