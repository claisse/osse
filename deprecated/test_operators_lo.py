"""
DEPRECATED

test_operators_lo.py
"""

from __future__ import unicode_literals
import numpy as np
from math import *
import unittest
import warnings
import h5py
import itertools

import matplotlib
#matplotlib.rcParams['text.usetex'] = True
#matplotlib.rcParams['text.latex.unicode'] = True
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
import matplotlib.pyplot as plt

from scipy.sparse.linalg import eigs
from scipy.sparse.linalg import LinearOperator
from scipy.sparse.linalg import svds

import scipy
import scipy.integrate
import scipy.sparse.linalg
from numpy import linalg

from flowfield import FlowField
from flowfield import DiffOperator_FlowField
from flowfield import BoundaryCondition_FlowField
from flowfield import MeanFlow_FlowField
from channel import Channel
from channel import DiffOperator_Channel
from channel import BoundaryCondition_Channel
from channel import MeanFlow_Channel
from boundary_layer import Boundary_Layer
from boundary_layer import DiffOperator_Boundary_Layer
from boundary_layer import BoundaryCondition_Boundary_Layer
from boundary_layer import MeanFlow_Boundary_Layer
from operators_lo import Operators_lo as OP
from operators_lo import OperatorsValidation_lo as OpVal
import chebyshev


class Operators_lo_Test(unittest.TestCase):
    """ Test of methods implemented in Operators_lo."""

    def init_flow(self, kind_of_flow):
        warnings.filterwarnings('ignore',
                                category=np.VisibleDeprecationWarning)

        filename = 'database/channel_sample.h5'

        # Creation of a new h5-file similar to a ChannelFlow flowfield
        file = h5py.File(filename, 'w')
        data = file.create_group('data')
        geom = file.create_group('geom')

        # DOMAIN SIZE
        self.a = -1
        self.b = 1
        self.Ly = self.b - self.a
        file.attrs['Lx'] = 2*pi
        file.attrs['Ly'] = self.Ly
        file.attrs['Lz'] = 2*pi
        file.attrs['a'] = self.a
        file.attrs['b'] = self.b

        # BE CAREFUL, VALUES BELOW 10 DOES NOT WORK
        self.Nx = 11
        self.Ny = 20
        self.Nz = 13
        file.attrs['Nx'] = self.Nx
        file.attrs['Ny'] = self.Ny
        file.attrs['Nz'] = self.Nz

        # Creation of a basis (x, y, z) of dim (Nx, Ny, Nz)
        array_x = np.arange(0, file.attrs['Lx'], file.attrs['Lx']/self.Nx)

        if kind_of_flow == 'channel':
            array_y = np.linspace(self.a, self.b, num=self.Ny, endpoint=True)
        elif kind_of_flow == 'bl':
            stretching_factor = 5
            array_y_temp = chebyshev.chebnodes(2 * self.Ny)
            array_y = - stretching_factor * np.log(array_y_temp[:self.Ny])

        array_z = np.arange(0, file.attrs['Lz'], file.attrs['Lz']/self.Nz)

        # Creation of a dataset u(x,y,z)
        coef_cos_Z = 3
        coef_sin_Z = 2
        offset = np.power(np.tan(array_y[0]*(np.pi/2-0.5)), 2)
        array_u = np.ndarray(shape=(3, self.Nx, self.Ny, self.Nz))
        for i, nx, ny, nz in itertools.product(
                                range(array_u.shape[0]),
                                range(array_u.shape[1]),
                                range(array_u.shape[2]),
                                range(array_u.shape[3])):
            array_u[i, nx, ny, nz] = \
                - 0 * (np.power(np.tan(array_y[ny]*(np.pi/2-0.5)), 2)
                       - offset) \
                + 0 * np.power(2*(ny-(self.Ny-1)/2)/(self.Ny-1), 4) \
                + 0 * np.sin(2*np.pi * 4*array_x[nx]/file.attrs['Lx']) \
                + 1 * np.cos(
                        2*np.pi * 2*array_y[ny]/(self.b-self.a) + np.pi/2)\
                + 0 * np.cos(
                        2*np.pi * coef_cos_Z*array_z[nz]/file.attrs['Lz'])\
                    * np.sin(
                        2*np.pi * coef_sin_Z*array_z[nz]/file.attrs['Lz'])

        x = geom.create_dataset('x', data=array_x)
        y = geom.create_dataset('y', data=array_y)
        z = geom.create_dataset('z', data=array_z)
        u = data.create_dataset('u', data=array_u)

        file.close()

        # Load the saved h5-file to a new flowfield
        if kind_of_flow == 'channel':
            self.ff = Channel(filename)
            self.ff.uniform2chebyshev()
            self.ff.meanFlow.apply_mean_flow('laminar')

        elif kind_of_flow == 'bl':
            self.ff = Boundary_Layer(filename)
            self.ff.Y = stretching_factor
            self.ff.conf_y_axis = 'Boundary_Layer'
            # self.ff.chebyshev2boundarylayer()
            self.ff.meanFlow.apply_mean_flow('blasius')

    def test_oss(self):
        kind_of_flow = 'channel'
        self.init_flow(kind_of_flow)

        # self.ff = Channel('database/eq1.h5')
        alpha = 1
        beta = 0 
        Re = 10000

        oss_op = OP(self.ff)
        oss_op_val = OpVal(self.ff)

        # oss_op_val.oss_plot_diff(self.ff, Re, baseflow, alpha, beta)

        vals, vecs, vals2, vecs2 = oss_op.oss_eigen(self.ff,
                                                    Re,
                                                    self.ff.u0,
                                                    alpha,
                                                    beta,
                                                    omega=-1)

        Los_mat, Lap_mat, OS_mat, a, b, c = oss_op_val.oss_operator_matrices(
                                                            self.ff,
                                                            Re,
                                                            self.ff.u0,
                                                            alpha,
                                                            beta)
        # oss_op_val.oss_eigen_full(self.ff, Re, baseflow, alpha, beta)

        #print('\nEigenvalues (scipy.sparse.linalg LO) : \n',
        #      np.round(np.sort(vals), 6))
        print('\nEigenvalues (scipy.sparse.linalg LO) : \n',
              np.sort(vals))
        print('\nEigenvalues (scipy.sparse.linalg LO) : \n',
              np.sort(vals2))

        # norm_vals = np.ndarray((vals.shape[0]))
        # for index in range(vals.shape[0]):
        #    norm_vals[index] = np.linalg.norm(
        #                            np.dot(Los_mat,
        #                                   vecs[:,index])\
        #                            - vals[index]
        #                            * np.dot(Lap_mat, vecs[:,index]))
        # print('Max norm : ', norm_vals.max())
        # print('Min norm : ', norm_vals.min())
        # print('Avg norm : ', norm_vals.mean())

        plt.figure()
        plt.plot(vals.real, np.absolute(vals.imag),
                 'rx', ms='20', mew='2',
                 label='Eigenvalues Orr-Sommerfeld')
        plt.plot(vals2.real, np.absolute(vals2.imag),
                 'bx', ms='20', mew='2',
                 label='Eigenvalues Squire')

        plt.legend(fontsize=36, loc=2)
        #plt.title('Eigenvalues of the Orr-Sommerfeld-Squire equation.', fontsize=20)
        plt.grid(b=True, which='Major')
        plt.xlim(-0.5, 0.1)
        plt.ylim(0, 1)
        plt.tick_params(labelsize=24)
        #plt.xticks(frontsize=16)
        #plt.xlabel('Absolute Imaginary part', fontsize=20)
        plt.xlabel(r'$\mathfrak{Re}(\lambda)$', fontsize=36)
        #plt.ylabel('Real part', fontsize=20)
        plt.ylabel(r'$|\mathfrak{Im}(\lambda)|$', fontsize=36)
        plt.show()

    def test_oss_resolvent(self):
        self.init_flow('channel')
        # self.ff = Channel('database/eq1.h5')
        # self.ff.meanFlow.apply_mean_flow('laminar')
        self.ff.make_spectral('xz')
        N = self.ff.u.shape[2]

        alpha = 5
        beta = 5
        Re = 100
        omega = 10
        rank = 3*(N-2)-2

        op = OP(self.ff)

        ###############################
        # CREATION OF LINEAR OPERATOR
        ###############################
        resolvent, U, S, VT = op.oss_resolvent(self.ff,
                                               Re,
                                               self.ff.u0,
                                               alpha,
                                               beta,
                                               omega,
                                               rank)

        np.set_printoptions(suppress=True, precision=6)
        # print('U: \n', U)
        # print('VT: \n', VT)
        print('S :\n', S)
        # print('resolvent : \n', resolvent)

        ###############################
        # Definition of vector = [u,v,w]
        ###############################
        X = 1
        vector1 = 10 * (np.ones(N-2*X) - self.ff.y[X:N-X]**2)
        # vector2 = 2* (np.ones(N-2*X) - self.ff.y[X:N-X]**2)
        vector2 = 5 * np.cos(2 * pi * 2 * np.linspace(0, N-2-1, N-2) / (N-2))
        vector = np.concatenate((vector1, vector1, vector1))

        #L = resolvent.matvec(vector)  # [1+0*N:1*N-1]
        L = np.dot(resolvent, vector)

        abscissa = np.linspace(0, 3*(N-2)-1, 3*(N-2))
        plt.figure()
        # plt.plot(abscissa, np.absolute(L), 'b+-')
        plt.plot(abscissa, L.real, 'b+-')
        plt.plot(abscissa, L.imag, 'b+--')
        plt.show()

    def test_lnse_resolvent(self):
        self.init_flow('channel')
        # self.ff = Channel('database/eq1.h5')
        # self.ff.meanFlow.apply_mean_flow('laminar')
        self.ff.make_spectral('xz')
        N = self.ff.u.shape[2]

        alpha = 5
        beta = 5
        Re = 10000
        omega = 1
        rank = 4*(N)-2

        op = OP(self.ff)

        ###############################
        # CREATION OF LINEAR OPERATOR
        ###############################
        resolvent_lo, U, S, VT = op.lnse_resolvent(
                                                    self.ff,
                                                    Re,
                                                    self.ff.u0,
                                                    alpha,
                                                    beta,
                                                    omega,
                                                    rank)

        ###############################
        # SVDS
        ###############################
        print('\n --- SVDS --- \n')
        SS = np.diagflat(S)
        M = np.dot(U, np.dot(SS, VT))

        np.set_printoptions(suppress=True, precision=3)
        print('U: \n', np.round(U, 3))
        print('VT: \n', np.round(VT))
        print('SS :\n', SS)
        # print(np.round(M,3))
        print('\n --- SVDS --- \n')

        ###############################
        # COMPARISON SVDS / MATVEC
        ###############################
        vector = 10 * (np.ones(N) - self.ff.y**4)
        # vector = 5 * np.cos( np.linspace(1, N-2, N-2) / (2*pi))
        zeros = np.zeros(N)
        f = np.concatenate((vector, vector, vector, zeros))
        Y = np.linspace(0, 4*N-1, 4*N)

        L = resolvent_lo.matvec(f)  # [1+0*N:1*N-1]
        L_svd = np.dot(M, f)  # [1+0*N:1*N-1]

        plt.figure()
        # plt.plot(Y, vector, 'r--')
        plt.plot(Y, L.real, 'b+-')
        plt.plot(Y, L.imag, 'b+--')
        plt.plot(Y, L_svd.real, 'go-')
        plt.plot(Y, L_svd.imag, 'g+--')
        plt.show()

    def test_oss_wall_actuation_v(self):
        self.init_flow('channel')
        # self.ff = Channel('database/eq1.h5')
        # self.ff.meanFlow.apply_mean_flow('laminar')
        self.ff.make_spectral('xz')
        N = self.ff.u.shape[2]

        alpha = 5
        beta = 5
        Re = 100
        tau = 0.1
        q_upper = 10
        q_lower = 10

        op = OP(self.ff)

        Los_act_lo, Lap_act_lo, Lsq_lo, Lc_lo = op.oss_wall_actuation_v(
                        self.ff,
                        Re,
                        self.ff.u0,
                        alpha, beta,
                        tau, q_upper, q_lower)

        X=1
        vector1 = 10 * (np.ones(N-2*X) - self.ff.y[X:N-X]**2)
        
test = unittest.TestSuite()
# test.addTest(Operators_lo_Test("test_oss"))
test.addTest(Operators_lo_Test("test_oss_resolvent"))
# test.addTest(Operators_lo_Test("test_lnse_resolvent"))
# test.addTest(Operators_lo_Test("test_oss_wall_actuation_v"))

runner = unittest.TextTestRunner()
runner.run(test)
