"""
DEPRECATED

operators_sympy.py

"""

#import warnings
from sympy import *
#import numpy as np

#from flowfield import FlowField
#from flowfield import DiffOperator_FlowField
#from channel import Channel
#from channel import DiffOperator_Channel
#from boundary_layer import Boundary_Layer
#from boundary_layer import DiffOperator_Boundary_Layer

#import chebyshev

# BASIC NOTION OF SYMPY
#
# Immuable expressions
# x_variable = symbols('x_symbol')
# expr = x_variable + 1
# x_variable = 3
# expr stays the same, to express expr(x_variable = 3), do:
# expr.subs(x, 3)
#
# Equality :
# == checks if both expressions are EXCATLY the same
# to check if they represent the same equations, use :
# simplyfy( expression A - expression B )
# or
# exprA.equals(exprB) to evaluate numerically
#
# Rationnal:
# 1/2 = Rational(1,2) different from 1/2 = 0.5
#
# for Differentiation
# either use
# diff(expr, variable, order, variable...)
# or
# expr_d = Derivative(expr, variable, order, ...)
# and then
# expr_d.doit()

# ABout derivative / diff:
# 1. sometimes substituing diff(v,x) by I*alpha*v results in the multiplication
#    of diff(v,y) by alpha, which is a bug.
#    For this reason, do not substitute any derivative with sympy but use the Fourier
#    expression of v and eta instead.
# 2. Using 'Derivative' instead of 'diff' makes the calculation much faster, but we need to then
#    aplly .doit() at the right position.

def oss_sympy():
    # initialisation
    init_printing(use_unicode=True)

    # Constants definition
    alpha, beta, Re = symbols('alpha beta Re', contant=True)
    k = symbols('k', constant=True) # k**2 = alpha**2 + beta**2
    D = symbols('D', constant=True) # differentiation matrix

    ###########
    ## old codes, trying to implement matrices:
    # print(N)
    # print(D1.shape)
    # Ep = MatrixSymbol('Ep', N-2, N-2)
    # E = Matrix(Ep)
    # print(np.random.rand(16))
    # expression = 2*E
    # E.subs(list(zip(Ep, np.random.rand(16))))
    ###########

    # Variables definition
    x, y, z = symbols('x y z')

    # Flow components
    u = Function('u')(x,y,z)
    w = Function('w')(x,y,z)
    p = Function('p')(x,y,z)
    
    # v and eta defined with Fourier form
    v_y = Function('v')(y)
    v = exp(I*alpha*x) * exp(I*beta*z) * v_y
    eta_y = Function('eta')(y)
    eta = exp(I*alpha*x) * exp(I*beta*z) * eta_y

    # Base-flow components
    U = Function('U')(x,y,z)
    #U = 1 - y**2
    V = Function('V')(x,y,z)
    #V = 0
    W = Function('W')(x,y,z)
    #W = 0 

    #eta = I*beta*u(x,y,z) - I*alpha*w(x,y,z)
    u2 = (diff(v,x,y) - diff(eta,z)) /k**2
    w2 = (diff(v,y,z) + diff(eta,x)) /k**2
    
    # LNSE
    dudt = (diff(u,x,2) + diff(u,y,2) + diff(u,z,2))/Re \
            - diff(p,x) \
            - u*diff(U, x) - U*diff(u,x) \
            - v*diff(U, y) - V*diff(u,y) \
            - w*diff(U, z) - W*diff(u,z)
    dvdt = (diff(v,x,2) + diff(v,y,2) + diff(v,z,2))/Re \
            - diff(p,y) \
            - u*diff(V, x) - U*diff(v,x) \
            - v*diff(V, y) - V*diff(v,y) \
            - w*diff(V, z) - W*diff(v,z)
    dwdt = (diff(w,x,2) + diff(w,y,2) + diff(w,z,2))/Re \
            - diff(p,z) \
            - u*diff(W, x) - U*diff(w,x) \
            - v*diff(W, y) - V*diff(w,y) \
            - w*diff(W, z) - W*diff(w,z)
    # Conservation eq. is included in the expression of Lp
    # Eq(diff(u,x) + diff(v,y) + diff(w,z), 0)

    #################
    # Laplacian of P to replace for the OSS model
    #################
    # Lp = d2pdx2 + d2pdy2 + d2pdz2
    # Lp = - k**2 * p + diff(p, y, 2)
    Lp0 = -2*diff(U,x)*diff(u,x) \
          -2*diff(V,x)*diff(u,y) \
          -2*diff(W,x)*diff(u,z) \
          -2*diff(U,y)*diff(v,x) \
          -2*diff(V,y)*diff(v,y) \
          -2*diff(W,y)*diff(v,z) \
          -2*diff(U,z)*diff(w,x) \
          -2*diff(V,z)*diff(w,y) \
          -2*diff(W,z)*diff(w,z)
    Lp1 = Lp0.subs(u, u2)
    Lp = Lp1.subs(w, w2)

    #print('Lp =')
    #pprint(Lp)
    
    ################
    # ETA
    ################
    if True:
        print('\n##########################################################################################')
        print('##########################################################################################')
        detadt1 = diff(dudt, z) - diff(dwdt, x)
        detadt2 = detadt1.subs(u, u2)
        detadt3 = detadt2.subs(w, w2)
        detadt4 = detadt3.expand().doit()
        detadt5 = detadt4 * exp(-I*alpha*x) * exp(-I*beta*z)
        detadt = detadt5.expand()
        
        print('detadt =')
        pprint(detadt)
        #pprint(latex(detadt))

    ################
    # V
    ################
    if True:
        print('\n##########################################################################################')
        print('##########################################################################################')
        dLvdt1 = (diff(dvdt,x,x) + diff(dvdt,y,y) + diff(dvdt,z,z))
        dLvdt2 = use(dLvdt1, expand, level = 0)#(dLvdt)
        dLvdt3 = dLvdt2.subs(diff(p,y,x,x) + diff(p,y,y,y) + diff(p,z,z,y), diff(Lp,y))
        dLvdt30 = dLvdt3.subs(u, u2)
        dLvdt31 = dLvdt30.subs(w, w2)
        dLvdt4 = dLvdt31.expand()
        dLvdt5 = dLvdt4.simplify().doit().expand()

        dLvdt6 = dLvdt5 * exp(-I*alpha*x) * exp(-I*beta*z)
        dLvdt = dLvdt6.expand()
        print('dLvdt =')
        pprint(dLvdt)
        #pprint(latex(dLvdt))

    print('\n##########################################################################################')
    print('##########################################################################################')

    ###########
    ## old codes, if matrices are implemented

    # replace diff(u,y) with D*u

    #U0 = 1-y**2
    #V0 = 0
    #W0 = 0

    # detadt21 = detadt.subs(U, U0)
    # detadt22 = detadt21.subs(V, V0)
    # detadt23 = detadt22.subs(W, W0)
    # detadt20 = detadt23.subs(D, D1)
    # #detadt24 = detadt23.subs('D'**4, D4)
    # #detadt25 = detadt24.subs(D**2, D2)
    # print('detadt =')
    # pprint(detadt20.doit())

    # dLvdt21 = dLvdt.subs(U, U0)
    # dLvdt22 = dLvdt21.subs(V, V0)
    # dLvdt23 = dLvdt22.subs(W, W0)
    # dLvdt20 = detadt23.subs(D, D1)
    # #dLvdt24 = detadt23.subs(D**4, D4)
    # #dLvdt25 = detadt24.subs(D**2, D2)
    # print('dLvdt =')
    # pprint(dLvdt20.doit())

    # substitute v and eta with their expression
    # to do so :
    # create a array vp and etap of size [NX, NY, NZ]
    # and substitute for each nx and nz the associated ( u v w )
    # return V ETA of size [NX NY NZ]


    #for nx in range(ff.u.shape[1]):
    #    for nz in range(ff.u.shape[2]):

    #v_p

    #eta_p = detadt8.subs(U, ff.u0[0,nx,1:N,nz])
    ###########

oss_sympy()
