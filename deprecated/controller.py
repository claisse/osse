"""
DEPRECATED

controller.py
"""

import numpy as np
import scipy

from scipy.sparse.linalg import eigs
import itertools

from flowfield import FlowField
from flowfield import DiffOperator_FlowField
from channel import Channel
from channel import DiffOperator_Channel
from boundary_layer import Boundary_Layer
from boundary_layer import DiffOperator_Boundary_Layer
from operators import Operators as OP

import chebyshev


class Controller:

    #def __init__(self):
    #    pass

    def wall_actuation(ff:Channel,
                       Re, baseflow,
                       alpha, beta,
                       tau,
                       nx=0, nz=0):
        """
        WRONG METHOD JUST JUST WRITTEN FOR A FIRST TRY

        Wall-normal velocity actuation for OSS model around a given baseflow.

        [ Lap  .  .  . ] [ v0  ]  
        [  .   1  0  . ] [ v+1 ] =
        [  .   0  1  . ] [ v-1 ]   
        [  .   .  .  I ] [ eta ]   

            [ Los   Los*f_ur+Lap*f_ur/tau  Los*f_lr+Lap*f_ul/tau  Lc2  ][ v0  ]
            [ .              -1/tau                0               .  ][ v+1 ]
            [ .                0                 -1/tau            .  ][ v-1 ] 
            [ Lc            Lc*f_ur              Lc*f_lr          Lsq ][ eta ]

            +   [  -1/tau*Lap*f_ur      -1/tau*Lap*f_lr  ]
                [       1/tau                  0         ] [ q_u ]
                [         0                  1/tau       ] [ q_l ]
                [ np.zeros((N-2,1))    np.zeros((N-2,1)) ]

        Symbol '.' means a matrix of zeros.

                   [ v0  ]               [ v0  ]
        [ Lap2 . ] [ v+1 ] = [ A11 A12 ] [ v+1 ] + B [ q_u ]
        [  .   I ] [ v-1 ]   [ A21 A22 ] [ v-1 ]     [ q_l ]
                   [ eta ]               [ eta ]

        or
        E x = A x + B u

        Sizes:
        A11: N x N
        A12: N x N-2
        A21: N-2 x N
        A22: N-2 x N-2
        B: (N-2 + 2 + N-2) x 2
        Lap2: N x N
        I: N-2 x N-2

        (from Peter Heins thesis p40)
        
        Return:
        A11, A12, Lap2, A21, A22, B
        """
        N = ff.y.shape[0]
        y, weight = chebyshev.clencurt(N) 

        def f_upper(y):
            return 0.25 * (2 * y**4 - y**3 - 4 * y**2 + 3 * y + 4)

        def f_lower(y):
            return 0.25 * (2 * y**4 + y**3 - 4 * y**2 - 3 * y + 4)

        #f_u = f_upper(y)
        f_ur = f_upper(y[1:N-1])
        #f_l = f_lower(y)
        f_lr = f_lower(y[1:N-1])
        
        oss_op = OP(ff)

        #Los0, Lap0, OS0, Lsq0, Lc0 = \
        #            oss_op.oss_operator(ff,
        #                            Re,
        #                            ff.u0,
        #                            alpha,
        #                            beta,
        #                            omega=-1)
        #print('Los0 : \n', Los0)
        #print('Lap0 : \n', Lap0)
        #print('Lsq0 : \n', Lsq0)
        #print('Lc0 : \n', Lc0)
        #print('----------------------------------')

        Los, Lap, OS, Lsq, Lc, Lc2 = \
                    oss_op.oss_full(ff,
                                    Re,
                                    alpha,
                                    beta,
                                    omega=-1)
        

        # ADD LOOP TO APPLY IT TO ALL NX AND NZ AND NOT ONLY A COUPLE            
        #for nx, nz in itertools.product(range(1),
        #                                range(1)):
        Los = Los[nx, nz, :, :]
        Lsq = Lsq[nx, nz, :, :]
        Lc = Lc[nx, nz, :, :]
        Lc2 = Lc2[nx, nz, :, :]

        #print('Los : \n', Los)
        #print('Lap : \n', Lap)
        #print('Lsq : \n', Lsq)
        #print('Lc : \n', Lc)
        #print('Lc2 : \n', Lc2)
        #print('----------------------------------')
        ################################################
        # A11 = [ Los   Los*f_ur + 1/tau*Lap*f_ur    Los*f_lr + 1/tau*Lap*f_lr ]
        #       [ 0              -1/tau                         0              ]
        #       [ 0                 0                         -1/tau           ]

        A11_up = np.concatenate((Los,
                                 np.dot(Los, f_ur)[:, np.newaxis] \
                                         + np.dot(Lap, f_ur)[:, np.newaxis]/tau,
                                 np.dot(Los, f_lr)[:, np.newaxis] \
                                         + np.dot(Lap, f_lr)[:, np.newaxis]/tau),
                                axis=1)
        A11_mid = np.concatenate((np.zeros(N-2), [-1/tau], [0]))
        A11_down = np.concatenate((np.zeros(N-2), [0], [-1/tau]))

        A11 = np.concatenate((A11_up,
                              A11_mid[np.newaxis,:],
                              A11_down[np.newaxis,:]), axis=0)

        ################################################
        # A12 = [ Lc2 ]
        #       [  .  ]
        #       [  .  ]
        A12 = np.concatenate((Lc2,
                              np.zeros((1,N-2)),
                              np.zeros((1,N-2))), axis=0)

        ################################################
        # A21 = [ Lc   Lc*f_ur    Lc*f_lr ]
        A21 = np.concatenate((Lc,
                              np.dot(Lc, f_ur)[:, np.newaxis],
                              np.dot(Lc, f_lr)[:, np.newaxis]), axis=1)

        ################################################
        # A22 = Lsq
        A22 = Lsq

        ################################################
        # B = [  -1/tau*Lap*f_ur      -1/tau*Lap*f_lr  ]
        #     [       1/tau                  0         ]
        #     [         0                  1/tau       ]
        #     [ np.zeros((N-2,1))    np.zeros((N-2,1)) ]
        B1 = np.concatenate((-1/tau*np.dot(Lap, f_ur)[:, np.newaxis],
                             [[1/tau]],
                             [[0]],
                             np.zeros((N-2,1))))
        B2 = np.concatenate((-1/tau*np.dot(Lap, f_lr)[:, np.newaxis],
                             [[0]],
                             [[1/tau]],
                             np.zeros((N-2,1))))
        B = np.concatenate((B1,B2), axis=1)

        ################################################
        # Laplacian
        # Lap2 = [ Lap 0 0 ]
        #        [  0  1 0 ]
        #        [  0  0 1 ]
        Lap2_up = np.concatenate((Lap, np.zeros((N-2, 2))), axis=1)
        Lap2_down = np.concatenate((np.zeros((2,N-2)), np.eye(2)), axis=1)
        Lap2 = np.concatenate((Lap2_up, Lap2_down), axis=0)

        ################################################
        A = np.concatenate((np.concatenate((A11,A21)),
                            np.concatenate((A12,A22))), axis=1)

        ################################################
        # E
        # E = [ Lap2 0 ]
        #     [  0   I ]
        I = np.eye((N-2))
        E = np.concatenate((np.concatenate((Lap2, np.zeros((N,N-2))), axis=1),
                            np.concatenate((np.zeros((N-2,N)), I), axis=1)),
                            axis=0)


        return E, A, B, A11, A12, A21, A22, Lap2

    def wall_sensing(ff:Channel,
                     Re, baseflow,
                     alpha, beta,
                     nx=0, nz=0):
        """
        Peter Heins p 85/86
        but need some changes to take into account invariant solution as baseflow
        """
