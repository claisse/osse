"""
DEPRECATED

martinelli.py:

Python3 file containing method to reproduce the results
of Martinelli in his paper of 2011.

The methods are deprecated as they call old-wrong methods.
"""

import numpy as np
from math import *
import h5py
import itertools
from pathlib import Path

#import matplotlib.pyplot as plt

#matplotlib.rcParams['text.usetex'] = True
#matplotlib.rcParams['text.latex.unicode'] = True

import matplotlib
matplotlib.use('TkAgg')
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
import matplotlib.pyplot as plt
#import psutil
import resource
from memory_profiler import profile

from memory_profiler import profile

import scipy

import chebyshev
from flowfield import FlowField
from flowfield import DiffOperator_FlowField
from flowfield import BoundaryCondition_FlowField
from flowfield import MeanFlow_FlowField
from channel import Channel
from channel import DiffOperator_Channel
from channel import BoundaryCondition_Channel
from channel import MeanFlow_Channel
from boundary_layer import Boundary_Layer
from boundary_layer import DiffOperator_Boundary_Layer
from boundary_layer import BoundaryCondition_Boundary_Layer
from boundary_layer import MeanFlow_Boundary_Layer
from operators import Operators as OP
from math_function_box import left_null_space, null_space

class Martinelli():
    """
    """
    
    def init_flow_simple(self, kind_of_flow):
        self.ff = Channel('database/eq/'+kind_of_flow+'.h5')
        self.ff.load_baseflow('database/eq/'+kind_of_flow+'.h5')
        # add a Couette baseflow U(y) = y
        for ny in range(self.ff.u.shape[2]):
            self.ff.u0[0, :, ny, :] += self.ff.y[ny]
            #self.ff.u0[0, :, ny, :] = 1-self.ff.y[ny]**2
            #self.ff.u0[1, :, ny, :] = 0#self.ff.y[ny]
            #self.ff.u0[2, :, ny, :] = 0#self.ff.y[ny]

    def init_flow(self, kind_of_flow, Ny, Nx=5, Nz=7, Lx=2*pi, Lz=2*pi):
        #warnings.filterwarnings('ignore',
        #                        category=np.VisibleDeprecationWarning)

        filename = 'database/channel_sample.h5'

        # Creation of a new h5-file similar to a ChannelFlow flowfield
        file = h5py.File(filename, 'w')
        data = file.create_group('data')
        geom = file.create_group('geom')

        # DOMAIN SIZE
        self.a = -1
        self.b = 1
        self.Ly = self.b - self.a
        file.attrs['Lx'] = Lx
        file.attrs['Ly'] = self.Ly
        file.attrs['Lz'] = Lz
        file.attrs['a'] = self.a
        file.attrs['b'] = self.b
        file.attrs['nu'] = 0
        file.attrs['Nd'] = 3

        # BE CAREFUL, VALUES BELOW 10 DOES NOT WORK
        self.Nx = Nx
        self.Ny = Ny
        self.Nz = Nz
        file.attrs['Nx'] = self.Nx
        file.attrs['Ny'] = self.Ny
        file.attrs['Nz'] = self.Nz
        file.attrs['Nxpad'] = self.Nz*3/2
        file.attrs['Nypad'] = self.Ny
        file.attrs['Nzpad'] = self.Nz*3/2

        # Creation of a basis (x, y, z) of dim (Nx, Ny, Nz)
        array_x = np.arange(0, file.attrs['Lx'], file.attrs['Lx']/self.Nx)

        if kind_of_flow == 'couette' \
                or kind_of_flow == 'poiseuille' \
                or kind_of_flow == 'channel-inv-sol':
            # be careful with the direction or array_y
            array_y = np.linspace(self.b, self.a, num=self.Ny, endpoint=True)
        elif kind_of_flow == 'bl':
            stretching_factor = 5
            array_y_temp = chebyshev.chebnodes(2 * self.Ny)
            array_y = - stretching_factor * np.log(array_y_temp[:self.Ny])
        else:
            array_y = chebyshev.chebnodes(self.Ny)

        array_z = np.arange(0, file.attrs['Lz'], file.attrs['Lz']/self.Nz)

        # Creation of a dataset u(x,y,z)
        array_u = np.zeros(shape=(3, self.Nx, self.Ny, self.Nz))
        """
        coef_cos_Z = 3
        coef_sin_Z = 2
        offset = np.power(np.tan(array_y[0]*(np.pi/2-0.5)), 2)
        array_u = np.ndarray(shape=(3, self.Nx, self.Ny, self.Nz))
        for i, nx, ny, nz in itertools.product(
                                range(array_u.shape[0]),
                                range(array_u.shape[1]),
                                range(array_u.shape[2]),
                                range(array_u.shape[3])):
            array_u[i, nx, ny, nz] = 0#\
                - 0 * (np.power(np.tan(array_y[ny]*(np.pi/2-0.5)), 2)
                       - offset) \
                + 0 * np.power(2*(ny-(self.Ny-1)/2)/(self.Ny-1), 4) \
                + 0 * np.sin(2*np.pi * 4*array_x[nx]/file.attrs['Lx']) \
                + 0 * np.cos(
                        2*np.pi * 2*array_y[ny]/(self.b-self.a) + np.pi/2)\
                + 0 * np.cos(
                        2*np.pi * coef_cos_Z*array_z[nz]/file.attrs['Lz'])\
                    * np.sin(
                        2*np.pi * coef_sin_Z*array_z[nz]/file.attrs['Lz'])
        """

        x = geom.create_dataset('x', data=array_x)
        y = geom.create_dataset('y', data=array_y)
        z = geom.create_dataset('z', data=array_z)
        u = data.create_dataset('u', data=array_u)

        file.close()

        # Load the saved h5-file to a new flowfield
        if kind_of_flow == 'couette':
            self.ff = Channel(filename)
            self.ff.uniform2chebyshev()
            self.ff.meanFlow.apply_mean_flow('couette')

        elif kind_of_flow == 'poiseuille':
            self.ff = Channel(filename)
            self.ff.uniform2chebyshev()
            self.ff.meanFlow.apply_mean_flow('poiseuille')

        elif kind_of_flow == 'channel-inv-sol':
            self.ff = Channel(filename)
            self.ff.uniform2chebyshev()
            self.ff.u0 = np.ndarray(shape=(self.ff.u.shape[0],
                                           self.ff.u.shape[1],
                                           self.ff.u.shape[2],
                                           self.ff.u.shape[3] ))
            self.ff.state0 = {'x': 'Physical', 'y': 'Physical', 'z': 'Physical'}
            self.ff.conf_y0_axis = self.ff.evaluate_conf_axis('y')
            for nx in range(self.ff.u.shape[1]):
                for ny in range(self.ff.u.shape[2]):
                    for nz in range(self.ff.u.shape[3]):
                        self.ff.u0[0, nx, ny, nz] = 1 - self.ff.y[ny]**2 + nx**2 + nz**2
                        self.ff.u0[1, nx, ny, nz] = 1 - self.ff.y[ny]**2 + nx**2 + nz**2
                        self.ff.u0[2, nx, ny, nz] = 1 - self.ff.y[ny]**2 + nx**2 + nz**2

        elif kind_of_flow == 'bl':
            self.ff = Boundary_Layer(filename)
            self.ff.Y = stretching_factor
            self.ff.conf_y_axis = 'Boundary_Layer'
            # self.ff.chebyshev2boundarylayer()
            self.ff.meanFlow.apply_mean_flow('blasius')
    
        else:
            self.ff = Channel(filename)
            # apply for u and p datasets if set on uniforme grid:
            # but should recognize if the grid is uniform or Chebyshev
            self.ff.uniform2chebyshev()
            self.ff.load_baseflow('database/eq1/'+kind_of_flow+'.h5')
            # add a Couette baseflow U(y) = y
            for ny in range(self.ff.u.shape[2]):
                self.ff.u0[0, :, ny, :] = self.ff.u0[0, :, ny, :] + self.ff.y[ny]
   
    # DEPRECATED
    def lambda_max(self, Ny, Re, alpha, beta, nx, nz, tau):
        printing = False 
        np.set_printoptions(suppress=True, precision=6)
        
        # Setting the flow
        kind_of_flow = 'poiseuille'
        self.init_flow(kind_of_flow, Ny)

        # self.ff = Channel('database/eq1.h5')
        self.ff.make_spectral('xz')
        N = self.ff.u.shape[2]
       
        # System with wall actuation
        E, A, B, A11, A12, A21, A22, Lap2 = \
                Controller.wall_actuation(self.ff,
                                          Re, self.ff.u0,
                                          alpha, beta,
                                          tau,
                                          nx,nz)

        #print('E :\n', E.shape)
        # print(np.linalg.matrix_rank(E))
        # SHOULD REPLACE THE INV
        Einv = np.linalg.inv(E)
        A = np.dot(Einv, A)
        B = np.dot(Einv, B)

        #print('A11 :\n', A11)
        #print('A12 :\n', A12)
        #print('Lap2 : \n', Lap2)
        #print('A21 : \n', A21)
        #print('A22 : \n', A22)
        #print('B : \n', B)

        ##########
        # Definition of the weight matrix Q = W.T W
        # W [w[1:N-1], w[N-1], w[1], w[1:N-1]) for x=(v0, v+1, v-1, eta)
        W = np.concatenate((self.ff.w[1:N-1],
                            [self.ff.w[N-1]],
                            [self.ff.w[0]],
                            self.ff.w[1:N-1]), axis=0)[np.newaxis,:]
        Q = np.dot(W.T, W)
        # print('Q :\n', Q.shape)

        # Definition of Aw and Bw, the weighted matrices A and B
        Aw = np.dot(Q, A)
        #print('B : \n', B)
        Bw = np.dot(Q, B)
        #print('Bw : \n', Bw)
      
        ##########
        BHB = np.dot(B.conj().T, B)
        #print('BHB : \n', BHB)
        w_BHB, v_BHB = np.linalg.eig(BHB)
        if printing:
            print('--------------')
            print('-- B.H B --')
            print('rank : ', np.linalg.matrix_rank(BHB))
            print('Eigenvalues :\n', w_BHB)
        # The assumption B.T.B > 0 is true as B has full column rank

        ##########
        BBH = np.dot(B, B.conj().T)
        #print('BBH : \n', BBH)
        w_BBH, v_BBH = np.linalg.eig(BBH)
        if printing:
            print('--------------')
            print('-- B B.H --')
            print('rank : ', np.linalg.matrix_rank(BBH))
            print('min(Eigenvalues) :\n', min(w_BBH))
        # BBT is not positive definite, as its eigenvalues are not all positive
        # it implies that condition B.B.T > 0 is not fullfilled.

        ##########
        if printing:
            print('--------------')
            print('-- For laminar flow --')
            print('-- QB_ (QA + (QA).H) (QB_).H --')
        
        # Left null space of weighted B
        Bw_ = left_null_space(Bw)
        #print('Bw_ :\n', Bw_.shape)

        if Bw_.size == 0:
            Bw_ = 1
            Bw_H = 1
            print('Left null space of Bw is empty.')
        else:
            #B_ = np.dot(Q, B_)
            Bw_H = Bw_.conj().T

        M = np.dot(Bw_, np.dot((Aw + Aw.conj().T), Bw_H))
        #print('M shape : ', M.shape)

        eig, eigV = np.linalg.eig(M)
        return max(eig.real)

    # DEPRECATED
    def plot_lambda_max(self, Ny, Re, tau, nx, nz):
        step = 1
        alpha_min = 0
        alpha_max = 5
        alpha_range = np.arange(alpha_min, alpha_max+1*step, step)
        beta_min = 0
        beta_max = 5
        beta_range = np.arange(beta_min, beta_max+1*step, step)

        lambda_array = np.ndarray((alpha_range.size, beta_range.size))
        for alpha_i, beta_i in itertools.product(range(0, alpha_range.size),
                                                 range(0, beta_range.size)):
            if alpha_range[alpha_i] == 0 and beta_range[beta_i] == 0:
                pass #lambdamax = 0
            else:
                lambdamax = self.lambda_max(Ny, Re,
                                            alpha_range[alpha_i],
                                            beta_range[beta_i],
                                            nx, nz, tau)
                lambda_array[alpha_i, beta_i] = lambdamax
                print('lambda_max(', alpha_range[alpha_i],
                        ', ', beta_range[beta_i], ') = ', lambdamax)

        print(lambda_array)
        fig, ax = plt.subplots()
        cs = ax.contourf(alpha_range, beta_range, lambda_array.T)
        cbar = fig.colorbar(cs)
        ax.set_title('Maximum real-part eigenvalue for each (alpha, beta) at given (X,Z)')
        ax.set_xlabel('alpha')
        ax.set_ylabel('beta')
        plt.show()

    # DEPRECATED
    def oss_baseflow_comparison(self, kind_of_flow, Nx, Ny, Nz, Re, Lx, Lz):
        """
        Comparison of the expression of the OSS extended for
        an invariant solution and the expression of the usual OSS model.

        I set a baseflow 'poiseuille' or 'couette' and verified that
        the extented OSS model leads to the usual OSS model.
        """
        printing = False 
        np.set_printoptions(suppress=True, precision=2)

        # Setting the flow
        #kind_of_flow = 'poiseuille'
        #kind_of_flow = 'couette'
        #kind_of_flow = 'channel-inv-sol'
        #kind_of_flow = 'eq1'
        N = Ny #N = self.ff.u.shape[2]
        self.init_flow(kind_of_flow, Ny, Nx, Nz, Lx=Lx, Lz=Lz)

        # self.ff = Channel('database/eq1.h5')
        oss_op = OP(self.ff)

        # Building OSS model for each wavenumber pair.
        self.ff.make_spectral('xz', baseflow='no')
        Los00, Lap00, OS00, Lsq00, Lc00 = oss_op.oss_operator(self.ff,
                                  Re,
                                  self.ff.u0,
                                  0, 0)
        Los01, Lap01, OS01, Lsq01, Lc01 = oss_op.oss_operator(self.ff,
                                  Re,
                                  self.ff.u0,
                                  0, -1)
        Los02, Lap02, OS02, Lsq02, Lc02 = oss_op.oss_operator(self.ff,
                                  Re,
                                  self.ff.u0,
                                  0, 2)
        Los10, Lap10, OS10, Lsq10, Lc10 = oss_op.oss_operator(self.ff,
                                  Re,
                                  self.ff.u0,
                                  -1, 0)
        Los11, Lap11, OS11, Lsq11, Lc11 = oss_op.oss_operator(self.ff,
                                  Re,
                                  self.ff.u0,
                                  -1, -1)
        Los20, Lap20, OS20, Lsq20, Lc20 = oss_op.oss_operator(self.ff,
                                  Re,
                                  self.ff.u0,
                                  -2, 0)
        Los21, Lap21, OS21, Lsq21, Lc21 = oss_op.oss_operator(self.ff,
                                  Re,
                                  self.ff.u0,
                                  -2, 1)

        # Building the extended OSS model
        self.ff.make_spectral('xz', baseflow='only')
        Los, Lap_inv, Lsq, Lc, Lc2 = oss_op.oss_nonlaminar_baseflow(self.ff, Re,
                                                     NX_max=0,
                                                     NZ_max=0)

        # Just to allow easy reading of the values.
        Los0 = np.concatenate((Los00, Los01, Los10, Los11))#, Los20, Los21))
        Lc0 = np.concatenate((Lc00, Lc01, Lc10, Lc11))#, Lc20, Lc21))
        Lsq0 = np.concatenate((Lsq00, Lsq01, Lsq10, Lsq11))#, Lsq20, Lsq21))

        # Writting into two files the matrices,
        # 'matrix' is for extended OSS and 'matrix0' for usual OSS.
        matrix = Los
        matrix0 = Los0
        #np.savetxt('database/martinelli/oss_matrix_extended.txt',
        #            np.concatenate((matrix.real,
        #                            np.empty((1,Nx*Nz*(N-2)))*np.nan,
        #                            matrix.imag)), fmt='%-4.0f')
        np.savetxt('database/martinelli/oss_matrix_extended.txt',
                    np.concatenate((matrix.real,
                                    np.empty((1,Nx*Nz))*np.nan,
                                    matrix.imag)), fmt='%-4.0f')
        np.savetxt('database/martinelli/oss_matrix_usual.txt',
                    np.concatenate((matrix0.real,
                                    np.empty((1,(N-2)))*np.nan,
                                    matrix0.imag)), fmt='%-4.0f')
   
Re = 400 # from Gibson 2008
tau = 1
nx = 0
nz = 0
#Lx = 2*np.pi#/1.14 #5.51156605893 from Gibson 2008
#Lz = 2*np.pi#*2/5 #2.51327412287 from Gibson 2008
#Lx = 2*np.pi #2*np.pi/1.14 #5.51156605893
#Lz = 2*np.pi #4*np.pi/5 #2.51327412287

#if kind_of_flow == 'eq1_reduced_high':
#    Nx= 3
#    Ny = 15
#    Nz = 3
#elif kind_of_flow == 'eq1_reduced':
#    Nx= 10
#    Ny = 33
#    Nz = 10
#elif kind_of_flow == 'eq1':
#    Nx= 32
#    Ny = 35
#    Nz = 32
#else:
#    Nx = 4 
#    Ny = 5
#    Nz = 4

#list_Nx = {6,8,10,12,14,16,18,20,22,24,26}
list_Nx = {2}#{4, 8, 12, 16, 20}#, 24, 28, 32}
list_Ny = {3} #20
list_Nz = {3} #10
N_eq = {1} #{'SYM', 'ASYM'} #{2, 5, 9, 11}#, 'SYM', 'ASYM'}

for n_eq in N_eq:
    for Nx, Ny, Nz in itertools.product(list_Nx, list_Ny, list_Nz):
        N_str = str(Nx)+'x'+str(Ny)+'x'+str(Nz)
        #kind_of_flow = 'eq'+str(n_eq)+'_'+N_str
        kind_of_flow = 'couette'#'eq'+str(n_eq)+'_'+N_str

        N = Nx*Ny*Nz
        filename='database/martinelli/nonlaminar_baseflow/EIGS/'
        mart = Martinelli()
        print('\n##############################################################################')
        print(filename+kind_of_flow)
        print('Nx x Ny x Nz : ', Nx, ' x ', Ny, ' x ', Nz, '\n')

        if 1:
            mart.init_flow(kind_of_flow, Ny, Nx, Nz)
            oss_op = OP(mart.ff)
            oss_op.op_mapping(mart.ff)


        if 0:
            alpha = 0
            beta = 0
            print(mart.lambda_max(Ny, Re, alpha, beta, nx, nz, tau))

        if 0:
            mart.plot_lambda_max(Ny, Re, tau, nx, nz)

        if 0:
            mart.oss_baseflow_comparison(kind_of_flow, Nx, Ny, Nz, Re, Lx=Lx, Lz=Lz)

        if 0:
            print('-- INITIALIZATION --')
            #mem0 = psutil.virtual_memory()[1]
            #print('---- Used memory before INIT :', mem0/1000/1000, ' MB')
            #mart.init_flow(kind_of_flow, Ny, Nx, Nz, Lx=Lx, Lz=Lz)
            mart.init_flow_simple(kind_of_flow)
            print('Memory used for initialization :',
                    resource.getrusage(resource.RUSAGE_SELF).ru_maxrss /1000, 'MB \n')

            print('-- EIGENVALUES CALCULATION --')
            mart.oss_nonlaminar_baseflow_EIG(filename+kind_of_flow, Re)

        if 0:
            print('\n -- EIGENVALUES PLOT --')
            Martinelli.plot_eigenvalues(filename+kind_of_flow+'_EIG.npy')
