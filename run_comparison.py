"""
run_comparison.py
based on :
cvfc_validation_comp.py

Compare the dataset of different simulations.

It compares the dataset :
    - u
    - v
    - w
    - eta

"""
import numpy as np
import scipy as sp
import h5py
import chebyshev
import itertools

import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
font = {'size':20}
matplotlib.rc('font', **font)

from channel import Channel
from operators import Operators as OP

home = "/home/gcpc1m15/"

NORM_1 = False
NORM_2 = False
PLOT = 0
timestep_plot = [40, 100, 150]#[0, 50, 100]
PLOT_NORM = True
LOG = 1

# channelflow simulation
CHFL1 = True
CHFL2 = 0
# OSSE simulation
OSSE3 = 1
OSSE4 = 1

# difference
DIFF21 = False # CHFL2 - CHFL1
DIFF43 = False # OSSE4 - OSSE3

#nonlinearity_CHFL1 = "linearaboutprofile"
nonlinearity_CHFL1 = "skewsymmetric"
#baseflow = "couette"
baseflow = "eq1"

Re = 400
# major difference with 18 x Ny x 20 for (1,1)
Nx_CHFL = 4
Ny_CHFL = 15
Nz_CHFL = 4

Nx_OSSE = 4#Nx_CHFL
Ny_OSSE = 15
Nz_OSSE = 4#Nz_CHFL

# modes to be displayed if not the ones actuated
# REM : data for (1,0) is produced with actuation x2
#01, 10, 11
mx = +1 #mx = [0, 1, 2, 3, 4, 5] for kx = [0, 1, 2, 3, -2, -1] for Nx = 6
mz = +1 #mz = [0, 1, 2, 3] for kz = [0, 1, 2, 3] for Nz = 6
# alpha = 2 * np.pi * x / Lx
# beta = 2 * np.pi * z / Lz

freq_x_CHFL = np.fft.fftfreq(Nx_CHFL, 1/Nx_CHFL).astype(np.integer, copy=False)
freq_z_CHFL = np.fft.rfftfreq(Nz_CHFL, 1/Nz_CHFL).astype(np.integer, copy=False)
kx_CHFL = freq_x_CHFL[mx]
kz_CHFL = freq_z_CHFL[mz]
freq_x_OSSE = np.fft.fftfreq(Nx_OSSE, 1/Nx_OSSE).astype(np.integer, copy=False)
freq_z_OSSE = np.fft.rfftfreq(Nz_OSSE, 1/Nz_OSSE).astype(np.integer, copy=False)
kx_OSSE = freq_x_OSSE[mx]
kz_OSSE = freq_z_OSSE[mz]

# just use to evaluate manually the expected result
Lx = 2 * np.pi / 1.14
Lz = 2 * np.pi / 2.5

# time to skip at the beginning
T0 = 0
T1 = 200 # = T
Nt = T1 - T0 + 1

t_saved = np.linspace(T0, T1, Nt)
if NORM_1 or NORM_2 or PLOT_NORM:
    timestep = t_saved
elif PLOT:
    timestep = timestep_plot

# DELTA Norms
if NORM_1:
    # norm of all the modes
    norm1_CHFL1 = np.zeros((3))
    norm1_CHFL2 = np.zeros((3))
    norm1_OSSE3 = np.zeros((3))
    norm1_OSSE4 = np.zeros((3))

    norm1_DIFF21 = np.zeros((3))
    norm1_DIFF43 = np.zeros((3))

    # norm of a given mode
    norm1_CHFL1_mode = np.zeros((3))
    norm1_CHFL2_mode = np.zeros((3))
    norm1_OSSE3_mode = np.zeros((3))
    norm1_OSSE4_mode = np.zeros((3))

    norm1_DIFF21_mode = np.zeros((3))
    norm1_DIFF43_mode = np.zeros((3))

if NORM_2:
    # norm of all the modes
    norm2_CHFL1 = np.zeros((3))
    norm2_CHFL2 = np.zeros((3))
    norm2_OSSE3 = np.zeros((3))
    norm2_OSSE4 = np.zeros((3))

    norm2_DIFF21 = np.zeros((3))
    norm2_DIFF43 = np.zeros((3))

    # norm of a given mode
    norm2_CHFL1_mode = np.zeros((3))
    norm2_CHFL2_mode = np.zeros((3))
    norm2_OSSE3_mode = np.zeros((3))
    norm2_OSSE4_mode = np.zeros((3))

    norm2_DIFF21_mode = np.zeros((3))
    norm2_DIFF43_mode = np.zeros((3))

    nodes, weights2_CHFL = chebyshev.clencurt(Ny_CHFL)
    W2_CHFL = np.diag(weights2_CHFL)
    nodes, weights2_OSSE = chebyshev.clencurt(Ny_OSSE)
    W2_OSSE = np.diag(weights2_OSSE)

# Norms
if PLOT_NORM:
    # norm array of all the modes
    norm_CHFL1 = np.zeros((3, Nt))
    norm_CHFL2 = np.zeros((3, Nt))
    norm_OSSE3 = np.zeros((3, Nt))
    norm_OSSE4 = np.zeros((3, Nt))

    norm_DIFF21 = np.zeros((3, Nt))
    norm_DIFF43 = np.zeros((3, Nt))

    # norm array of a given mode
    norm_CHFL1_mode = np.zeros((3, Nt))
    norm_CHFL2_mode = np.zeros((3, Nt))
    norm_OSSE3_mode = np.zeros((3, Nt))
    norm_OSSE4_mode = np.zeros((3, Nt))

    norm_DIFF21_mode = np.zeros((3, Nt))
    norm_DIFF43_mode = np.zeros((3, Nt))

    nodes, weights_CHFL = chebyshev.clencurt(Ny_CHFL)
    W_CHFL = np.diag(weights_CHFL)
    #W_CHFL = np.eye(Ny_CHFL)
    nodes, weights_OSSE = chebyshev.clencurt(Ny_OSSE)
    W_OSSE = np.diag(weights_OSSE)
    #W_OSSE = np.eye(Ny_OSSE)

    # load EQ1 to substract it from the channelflow flowfield
    kind_of_flow = 'eq1_'+str(Nx_CHFL)+'x'+str(Ny_CHFL)+'x'+str(Nz_CHFL)
    print(kind_of_flow)
    ff1 = Channel('database/eq/'+kind_of_flow+'.h5')
    # ff1.make_spectral('xz')
    print("FF1 : ", ff1.u.shape)

#---------------------------------------
#---------------------------------------
# LOOP OVER ALL TIMESTEPS
for it in timestep:
    print("-------------")
    print("it: ", it)

    #---------------------------------------
    # CREATE FILENAMES
    basename_CHFL1 = home+"channelflow/database/PHD/controller_K/"\
            +baseflow+"/Re="+str(Re)+"/"+nonlinearity_CHFL1+"/"
    basename_CHFL2 = home+"channelflow/database/PHD/controller_K/"\
            +baseflow+"/Re="+str(Re)+"/"+nonlinearity_CHFL1+"/"
    basename_OSSE3 = home+"osse/database/OSSE/"\
            +baseflow+"/Re="+str(Re)+"/"
    basename_OSSE4 = home+"osse/database/OSSE/"\
            +baseflow+"/Re="+str(Re)+"/"

    compname_CHFL1 = str(Nx_CHFL) + "x" + str(Ny_CHFL) + "x" + str(Nz_CHFL) + "_cont=false_pert=false/"
    compname_CHFL2 = str(Nx_CHFL) + "x" + str(Ny_CHFL) + "x" + str(Nz_CHFL) + "_cont=true_pert=false/"
    compname_OSSE3 = str(Nx_OSSE) + "x" + str(Ny_OSSE) + "x" + str(Nz_OSSE) + "/integration-non-actuated/"
    compname_OSSE4 = str(Nx_OSSE) + "x" + str(Ny_OSSE) + "x" + str(Nz_OSSE) + "/integration-actuated/"

    compname_DIFF21 = str(Nx_CHFL) + "x" + str(Ny_CHFL) + "x" + str(Nz_CHFL) + "/"
    compname_DIFF43 = str(Nx_OSSE) + "x" + str(Ny_OSSE) + "x" + str(Nz_OSSE) + "/"

    filename_CHFL1 = "u"+str(int(it))+".h5"
    filename_CHFL2 = "u"+str(int(it))+".h5"
    filename_OSSE3 = "x_"+str(Nx_OSSE)+"x"+str(Ny_OSSE)\
            +"x"+str(Nz_OSSE)+"_Re="+str(Re) \
            + "_T=" + str(T1) + "_t=" + str(int(it)) + ".h5"
    filename_OSSE4 = "x_"+str(Nx_OSSE)+"x"+str(Ny_OSSE)\
            +"x"+str(Nz_OSSE)+"_Re="+str(Re) \
            + "_T=" + str(T1) + "_t=" + str(int(it)) + ".h5"

    if CHFL1:
        name_CHFL1 = basename_CHFL1 + compname_CHFL1 + filename_CHFL1
        if it == 0:
            print("CHFL1 : ", name_CHFL1)
    if CHFL2:
        name_CHFL2 = basename_CHFL2 + compname_CHFL2 + filename_CHFL2
        if it == 0:
            print("CHFL2 : ", name_CHFL2)
    if OSSE3:
        name_OSSE3 = basename_OSSE3 + compname_OSSE3 + filename_OSSE3
        if it == 0:
            print("OSSE3 : ", name_OSSE3)
    if OSSE4:
        name_OSSE4 = basename_OSSE4 + compname_OSSE4 + filename_OSSE4
        if it == 0:
            print("OSSE4 : ", name_OSSE4)
    if DIFF21:
        name_DIFF21 = basename_CHFL + compname_DIFF21 + filename_CHFL
    if DIFF43:
        name_DIFF43 = basename_OSSE + compname_DIFF43 + filename_OSSE

    #---------------------------------------
    # LOADING FILES
    # COMPARE RESULT IN SPECTRAL AND NOT IN PHYSICAL
    if CHFL1:
        ff_CHFL1 = Channel(name_CHFL1)
        print("CHFL1 :", ff_CHFL1.u.shape)
        y_CHFL1 = ff_CHFL1.y
        data_CHFL1 = ff_CHFL1.u[:, :, :, :] - ff1.u
        ff_CHFL1.make_spectral('xz')
    if CHFL2:
        ff_CHFL2 = Channel(name_CHFL2)
        ff_CHFL2.make_spectral('xz')
        y_CHFL2 = ff_CHFL2.y
        data_CHFL2 = ff_CHFL2.u[:, :, :, :] - ff1.u

    if OSSE3:
        ff_OSSE3 = Channel(name_OSSE3)
        ff_OSSE3.make_spectral('xz')
        y_OSSE3 = ff_OSSE3.y
        data_OSSE3 = ff_OSSE3.u
    if OSSE4:
        ff_OSSE4 = Channel(name_OSSE4)
        ff_OSSE4.make_spectral('xz')
        y_OSSE4 = ff_OSSE4.y
        data_OSSE4 = ff_OSSE4.u

    if DIFF21:
        data_DIFF21 = data_CHFL2 - data_CHFL1
    if DIFF43:
        data_DIFF43 = data_OSSE4 - data_OSSE3

    Mx_CHFL = ff_CHFL1.Mx
    Mz_CHFL = ff_CHFL1.Mz
    Mx_OSSE = ff_OSSE3.Mx
    Mz_OSSE = ff_OSSE3.Mz

    #---------------------------------------
    # NORM 1 DELTA
    if NORM_1:
        for ii in range(3):
            # calculation of the norm of all the modes
            for ix, iz in itertools.product(range(Mx_CHFL), range(Mz_CHFL)):
                if CHFL1:
                    norm1_CHFL1[ii] += np.linalg.norm(data_CHFL1[ii, ix, :, iz])/Ny_CHFL
                if CHFL2:
                    norm1_CHFL2[ii] += np.linalg.norm(data_CHFL2[ii, ix, :, iz])/Ny_CHFL
                if DIFF21:
                    norm1_DIFF21[ii] += np.linalg.norm(data_DIFF21[ii, ix, :, iz])/Ny_CHFL

            # calculation of the delta in norm for a given mode
            if CHFL1:
                norm1_CHFL1_mode[ii] += np.linalg.norm(data_CHFL1[ii, mx, :, mz])/Ny_CHFL
            if CHFL2:
                norm1_CHFL2_mode[ii] += np.linalg.norm(data_CHFL2[ii, mx, :, mz])/Ny_CHFL
            if DIFF21:
                norm1_DIFF21_mode[ii] += np.linalg.norm(data_DIFF21[ii, mx, :, mz])/Ny_CHFL

            # calculation of the norm of all the modes
            for ix, iz in itertools.product(range(Mx_OSSE), range(Mz_OSSE)):
                if OSSE3:
                    norm1_OSSE3[ii] += np.linalg.norm(data_OSSE3[ii, ix, :, iz])/Ny_OSSE
                if OSSE4:
                    norm1_OSSE4[ii] += np.linalg.norm(data_OSSE4[ii, ix, :, iz])/Ny_OSSE
                if DIFF43:
                    norm1_DIFF43[ii] += np.linalg.norm(data_DIFF43[ii, ix, :, iz])/Ny_OSSE

            # calculation of the delta in norm for a given mode
            if OSSE3:
                norm1_OSSE3_mode[ii] += np.linalg.norm(data_OSSE3[ii, mx, :, mz])/Ny_OSSE
            if OSSE4:
                norm1_OSSE4_mode[ii] += np.linalg.norm(data_OSSE4[ii, mx, :, mz])/Ny_OSSE
            if DIFF43:
                norm1_DIFF43_mode[ii] += np.linalg.norm(data_DIFF43[ii, mx, :, mz])/Ny_OSSE

    # END OF IF NORM 1

    #---------------------------------------
    # NORM 2 DELTA
    if NORM_2:
        for ii in range(3):
            # calculation of the norm of all the modes
            for ix, iz in itertools.product(range(Mx_CHFL), range(Mz_CHFL)):
                if CHFL1:
                    norm2_CHFL1[ii] += np.linalg.multi_dot(
                        [np.conjugate(data_CHFL1[ii, ix, :, iz]).T,
                         np.conjugate(W2_CHFL).T, W2_CHFL,
                         data_CHFL1[ii, ix, :, iz]]).real
                if CHFL2:
                    norm2_CHFL2[ii] += np.linalg.multi_dot(
                        [np.conjugate(data_CHFL2[ii, ix, :, iz]).T,
                         np.conjugate(W2_CHFL).T, W2_CHFL,
                         data_CHFL2[ii, ix, :, iz]]).real
                if DIFF21:
                    norm2_DIFF21[ii] += np.linalg.multi_dot(
                        [np.conjugate(data_DIFF21[ii, ix, :, iz]).T,
                         np.conjugate(W2_CHFL).T, W2_CHFL,
                         data_DIFF21[ii, ix, :, iz]]).real

            #calculation of the delta in norm for a given mode
            if CHFL1:
                norm2_CHFL1_mode[ii] += np.linalg.multi_dot(
                    [np.conjugate(data_CHFL1[ii, mx, :, mz]).T,
                     np.conjugate(W2_CHFL).T, W2_CHFL,
                     data_CHFL1[ii, mx, :, mz]]).real
            if CHFL2:
                norm2_CHFL2_mode[ii] += np.linalg.multi_dot(
                    [np.conjugate(data_CHFL2[ii, mx, :, mz]).T,
                     np.conjugate(W2_CHFL).T, W2_CHFL,
                     data_CHFL2[ii, mx, :, mz]]).real
            if DIFF21:
                norm2_DIFF21_mode[ii] += np.linalg.multi_dot(
                    [np.conjugate(data_DIFF21[ii, mx, :, mz]).T,
                     np.conjugate(W2_CHFL).T, W2_CHFL,
                     data_DIFF21[ii, mx, :, mz]]).real

            # calculation of the norm of all the modes
            for ix, iz in itertools.product(range(Mx_OSSE), range(Mz_OSSE)):
                if OSSE3:
                    norm2_OSSE3[ii] += np.linalg.multi_dot(
                        [np.conjugate(data_OSSE3[ii, ix, :, iz]).T,
                         np.conjugate(W2_OSSE).T, W2_OSSE,
                         data_OSSE3[ii, ix, :, iz]]).real
                if OSSE4:
                    norm2_OSSE4[ii] += np.linalg.multi_dot(
                        [np.conjugate(data_OSSE4[ii, ix, :, iz]).T,
                         np.conjugate(W2_OSSE).T, W2_OSSE,
                         data_OSSE4[ii, ix, :, iz]]).real
                if DIFF43:
                    norm2_DIFF43[ii] += np.linalg.multi_dot(
                        [np.conjugate(data_DIFF43[ii, ix, :, iz]).T,
                         np.conjugate(W2_OSSE).T, W2_OSSE,
                         data_DIFF43[ii, ix, :, iz]]).real

            #calculation of the delta in norm for a given mode
            if OSSE3:
                norm2_OSSE3_mode[ii] += np.linalg.multi_dot(
                    [np.conjugate(data_OSSE3[ii, mx, :, mz]).T,
                     np.conjugate(W2_OSSE).T, W2_OSSE,
                     data_OSSE3[ii, mx, :, mz]]).real
            if OSSE4:
                norm2_OSSE4_mode[ii] += np.linalg.multi_dot(
                    [np.conjugate(data_OSSE4[ii, mx, :, mz]).T,
                     np.conjugate(W2_OSSE).T, W2_OSSE,
                     data_OSSE4[ii, mx, :, mz]]).real
            if DIFF43:
                norm2_DIFF43_mode[ii] += np.linalg.multi_dot(
                    [np.conjugate(data_DIFF43[ii, mx, :, mz]).T,
                     np.conjugate(W2_OSSE).T, W2_OSSE,
                     data_DIFF43[ii, mx, :, mz]]).real
    # END OF IF NORM 2

    #---------------------------------------
    # NORM OVER TIME
    if PLOT_NORM:
        for ii in range(3):
            # calculation of the norm of all the modes
            for ix, iz in itertools.product(range(Mx_CHFL), range(Mz_CHFL)):
                if CHFL1 :
                    norm_CHFL1[ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                        [np.conjugate(data_CHFL1[ii, ix, :, iz]).T,
                         np.conjugate(W_CHFL).T, W_CHFL,
                         data_CHFL1[ii, ix, :, iz]]).real/Ny_CHFL)
                if CHFL2 :
                    norm_CHFL2[ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                        [np.conjugate(data_CHFL2[ii, ix, :, iz]).T,
                         np.conjugate(W_CHFL).T, W_CHFL,
                         data_CHFL2[ii, ix, :, iz]]).real/Ny_CHFL)
                if DIFF21:
                    norm_DIFF21[ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                        [np.conjugate(data_DIFF21[ii, ix, :, iz]).T,
                         np.conjugate(W_CHFL).T, W_CHFL,
                         data_DIFF21[ii, ix, :, iz]]).real/Ny_CHFL)

            #calculation of the delta in norm for a given mode
            if CHFL1:
                norm_CHFL1_mode[ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                    [np.conjugate(data_CHFL1[ii, mx, :, mz]).T,
                     np.conjugate(W_CHFL).T, W_CHFL,
                     data_CHFL1[ii, mx, :, mz]]).real/Ny_CHFL)
            if CHFL2:
                norm_CHFL2_mode[ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                    [np.conjugate(data_CHFL2[ii, mx, :, mz]).T,
                     np.conjugate(W_CHFL).T, W_CHFL,
                     data_CHFL2[ii, mx, :, mz]]).real/Ny_CHFL)
            if DIFF21:
                norm_DIFF21_mode[ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                    [np.conjugate(data_DIFF21[ii, mx, :, mz]).T,
                     np.conjugate(W_CHFL).T, W_CHFL,
                     data_DIFF21[ii, mx, :, mz]]).real/Ny_CHFL)

            # calculation of the norm of all the modes
            for ix, iz in itertools.product(range(Mx_OSSE), range(Mz_OSSE)):
                if OSSE3:
                    norm_OSSE3[ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                                [np.conjugate(data_OSSE3[ii, ix, :, iz]).T,
                                 np.conjugate(W_OSSE).T, W_OSSE,
                                 data_OSSE3[ii, ix, :, iz]]).real/Ny_OSSE)
                if OSSE4:
                    norm_OSSE4[ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                                [np.conjugate(data_OSSE4[ii, ix, :, iz]).T,
                                 np.conjugate(W_OSSE).T, W_OSSE,
                                 data_OSSE4[ii, ix, :, iz]]).real/Ny_OSSE)
                if DIFF43:
                    norm_DIFF43[ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                        [np.conjugate(data_DIFF43[ii, ix, :, iz]).T,
                         np.conjugate(W_OSSE).T, W_OSSE,
                         data_DIFF43[ii, ix, :, iz]]).real/Ny_OSSE)

            #calculation of the delta in norm for a given mode
            if OSSE3:
                norm_OSSE3_mode[ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                        [np.conjugate(data_OSSE3[ii, mx, :, mz]).T,
                         np.conjugate(W_OSSE).T, W_OSSE,
                         data_OSSE3[ii, mx, :, mz]]).real/Ny_OSSE)
            if OSSE4:
                norm_OSSE4_mode[ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                        [np.conjugate(data_OSSE4[ii, mx, :, mz]).T,
                         np.conjugate(W_OSSE).T, W_OSSE,
                         data_OSSE4[ii, mx, :, mz]]).real/Ny_OSSE)
            if DIFF43:
                norm_DIFF43_mode[ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                    [np.conjugate(data_DIFF43[ii, mx, :, mz]).T,
                     np.conjugate(W_OSSE).T, W_OSSE,
                     data_DIFF43[ii, mx, :, mz]]).real/Ny_OSSE)
    # END OF IF PLOT_NORM

    #---------------------------------------
    # PLOT PROFILES AT TIME IT FOR MODE (x, z)
    marker_style = dict(markersize = 25)
    if PLOT and it in timestep_plot:
        if True:
            fig0, ((ax0, ax1, ax2)) = plt.subplots(ncols=3)
            #fig0, ((ax0, ax1, ax2)) = plt.subplots(ncols=3, figsize =(25,15))
            plt.set_cmap('bwr')

            ###################################
            if True:
                # u
                ax0.set_title('u')
                if CHFL1:
                    if baseflow == "eq1":
                        ax0.plot(abs(data_CHFL1[0, mx, :, mz]), y_CHFL, '1-', color='b', label='CHFL1 abs', **marker_style)
                    else:
                        ax0.plot(data_CHFL1[0, mx, :, mz].real, y_CHFL, '1-', color='b', label='CHFL1 real', **marker_style)
                        ax0.plot(data_CHFL1[0, mx, :, mz].imag, y_CHFL, '1:', color='b', label='CHFL1 imag', **marker_style)
                if CHFL2:
                    if baseflow == "eq1":
                        ax0.plot(abs(data_CHFL2[0, mx, :, mz]), y_CHFL, '1-', color='r', label='CHFL2 abs', **marker_style)
                    else:
                        ax0.plot(data_CHFL2[0, mx, :, mz].real, y_CHFL, '1-', color='r', label='CHFL2 real', **marker_style)
                        ax0.plot(data_CHFL2[0, mx, :, mz].imag, y_CHFL, '1:', color='r', label='CHFL2 imag', **marker_style)
                if OSSE3:
                    if baseflow == "eq1":
                        ax0.plot(abs(data_OSSE3[0, mx, :, mz]), y_OSSE, '3-', color='g', label='OSSE3 abs', **marker_style)
                    else:
                        ax0.plot(data_OSSE3[0, mx, :, mz].real, y_OSSE, '3-', color='g', label='OSSE3 real', **marker_style)
                        ax0.plot(data_OSSE3[0, mx, :, mz].imag, y_OSSE, '3:', color='g', label='OSSE3 imag', **marker_style)
                if OSSE4:
                    if baseflow == "eq1":
                        ax0.plot(abs(data_OSSE4[0, mx, :, mz]), y_OSSE, '3-', color='g', label='OSSE4 abs', **marker_style)
                    else:
                        ax0.plot(data_OSSE4[0, mx, :, mz].real, y_OSSE, '3-', color='g', label='OSSE4 real', **marker_style)
                        ax0.plot(data_OSSE4[0, mx, :, mz].imag, y_OSSE, '3:', color='g', label='OSSE4 imag', **marker_style)

            ###################################
            # v
            ax1.set_title('v')
            if CHFL1:
                if baseflow == "eq1":
                    ax1.plot(abs(data_CHFL1[1, mx, :, mz]), y_CHFL, '1-',color='b', label='CHFL1 abs', **marker_style)
                else:
                    ax1.plot(data_CHFL1[1, mx, :, mz].real, y_CHFL, '1-',color='b', label='CHFL1 real', **marker_style)
                    ax1.plot(data_CHFL1[1, mx, :, mz].imag, y_CHFL, '1:',color='b', label='CHFL1 imag', **marker_style)
            if CHFL2:
                if baseflow == "eq1":
                    ax1.plot(abs(data_CHFL2[1, mx, :, mz]), y_CHFL, '1-',color='r', label='CHFL2 abs', **marker_style)
                else:
                    ax1.plot(data_CHFL2[1, mx, :, mz].real, y_CHFL, '1-',color='r', label='CHFL2 real', **marker_style)
                    ax1.plot(data_CHFL2[1, mx, :, mz].imag, y_CHFL, '1:',color='r', label='CHFL2 imag', **marker_style)
            if OSSE3:
                if baseflow == "eq1":
                    ax1.plot(abs(data_OSSE3[1, mx, :, mz]), y_OSSE, '3-', color='g', label='OSSE3 abs', **marker_style)
                else:
                    ax1.plot(data_OSSE3[1, mx, :, mz].real, y_OSSE, '3-', color='g', label='OSSE3 real', **marker_style)
                    ax1.plot(data_OSSE3[1, mx, :, mz].imag, y_OSSE, '3:', color='g', label='OSSE3 imag', **marker_style)
            if OSSE4:
                if baseflow == "eq1":
                    ax1.plot(abs(data_OSSE4[1, mx, :, mz]), y_OSSE, '3-', color='g', label='OSSE4 abs', **marker_style)
                else:
                    ax1.plot(data_OSSE4[1, mx, :, mz].real, y_OSSE, '3-', color='g', label='OSSE4 real', **marker_style)
                    ax1.plot(data_OSSE4[1, mx, :, mz].imag, y_OSSE, '3:', color='g', label='OSSE4 imag', **marker_style)

            ###################################
            # w
            if True:
                ax2.set_title('w')
                #ax2.set_xlim([-0.01, 0.01])
                if CHFL1:
                    if baseflow == "eq1":
                        ax2.plot(abs(data_CHFL1[2, mx, :, mz]), y_CHFL, '1-', color='b', label='CHFL1 abs', **marker_style)
                    else:
                        ax2.plot(data_CHFL1[2, mx, :, mz].real, y_CHFL, '1-', color='b', label='CHFL1 real', **marker_style)
                        ax2.plot(data_CHFL1[2, mx, :, mz].imag, y_CHFL, '1:', color='b', label='CHFL1_imag', **marker_style)
                if CHFL2:
                    if baseflow == "eq1":
                        ax2.plot(abs(data_CHFL2[2, mx, :, mz]), y_CHFL, '1-', color='r', label='CHFL2 abs', **marker_style)
                    else:
                        ax2.plot(data_CHFL2[2, mx, :, mz].real, y_CHFL, '1-', color='r', label='CHFL2 real', **marker_style)
                        ax2.plot(data_CHFL2[2, mx, :, mz].imag, y_CHFL, '1:', color='r', label='CHFL2 imag', **marker_style)
                if OSSE3:
                    if baseflow == "eq1":
                        ax2.plot(abs(data_OSSE3[2, mx, :, mz]), y_OSSE, '3-', color='g', label='OSSE3 abs', **marker_style)
                    else:
                        ax2.plot(data_OSSE3[2, mx, :, mz].real, y_OSSE, '3-', color='g', label='OSSE3 real', **marker_style)
                        ax2.plot(data_OSSE3[2, mx, :, mz].imag, y_OSSE, '3:', color='g', label='OSSE3 imag', **marker_style)
                if OSSE4:
                    if baseflow == "eq1":
                        ax2.plot(abs(data_OSSE4[2, mx, :, mz]), y_OSSE, '3-', color='g', label='OSSE4 abs', **marker_style)
                    else:
                        ax2.plot(data_OSSE4[2, mx, :, mz].real, y_OSSE, '3-', color='g', label='OSSE4 real', **marker_style)
                        ax2.plot(data_OSSE4[2, mx, :, mz].imag, y_OSSE, '3:', color='g', label='OSSE4 imag', **marker_style)

            ###################################

            ax0.set_ylabel('wall-normal coordinate y')
            #ax1.set_ylabel('wall-normal y')
            #ax2.legend(loc='best')

            fig0.suptitle("Re = "+str(Re)+" / CHFL: "+str(Nx_CHFL)+'x'+str(Ny_CHFL)+'x'+str(Nz_CHFL)+
                          " / OSSE: "+str(Nx_OSSE)+'x'+str(Ny_OSSE)+'x'+str(Nz_OSSE)
                          +' / Modes ('+str(x)+', '+str(z)+') / Time : ' + str(it) + 's')
            #fig0.subplots_adjust(hspace=0.4)
            #fig0.subplots_adjust(bottom=0.05, top=0.93, left=0.05, right=0.99)
            plt.show()

# END OF LOOP FOR IT IN TIMESTEP

if NORM_1:
    norm1_CHFL1 /= (Nt*Mx_CHFL*Mz_CHFL)
    norm1_CHFL2 /= (Nt*Mx_CHFL*Mz_CHFL)
    norm1_OSSE3 /= (Nt*Mx_OSSE*Mz_OSSE)
    norm1_OSSE4 /= (Nt*Mx_OSSE*Mz_OSSE)
    norm1_DIFF21 /= (Nt*Mx_CHFL*Mz_CHFL)
    norm1_DIFF43 /= (Nt*Mx_OSSE*Mz_OSSE)

    norm1_CHFL1_mode /= Nt
    norm1_CHFL2_mode /= Nt
    norm1_OSSE3_mode /= Nt
    norm1_OSSE4_mode /= Nt
    norm1_DIFF21_mode /= Nt
    norm1_DIFF43_mode /= Nt

if NORM_2:
    norm2_CHFL1 /= (Nt*Mx_CHFL*Mz_CHFL)
    norm2_CHFL2 /= (Nt*Mx_CHFL*Mz_CHFL)
    norm2_OSSE3 /= (Nt*Mx_OSSE*Mz_OSSE)
    norm2_OSSE4 /= (Nt*Mx_OSSE*Mz_OSSE)
    norm2_DIFF21 /= (Nt*Mx_CHFL*Mz_CHFL)
    norm2_DIFF43 /= (Nt*Mx_OSSE*Mz_OSSE)

    norm2_CHFL1_mode /= Nt
    norm2_CHFL2_mode /= Nt
    norm2_OSSE3_mode /= Nt
    norm2_OSSE4_mode /= Nt
    norm2_DIFF21_mode /= Nt
    norm2_DIFF43_mode /= Nt

if PLOT_NORM:
    norm_CHFL1 /= (Mx_CHFL*Mz_CHFL)
    norm_CHFL2 /= (Mx_CHFL*Mz_CHFL)
    norm_OSSE3 /= (Mx_OSSE*Mz_OSSE)
    norm_OSSE4 /= (Mx_OSSE*Mz_OSSE)
    norm_DIFF21 /= (Mx_CHFL*Mz_CHFL)
    norm_DIFF43 /= (Mx_OSSE*Mz_OSSE)

    #norm_CHFL1_mode, ...

#---------------------------------------
#---------------------------------------
# PLOT NORM OVER TIME
if PLOT_NORM:
    marker_size = 3#10
    style_CHFL1 = dict(color = 'b',
                         markerfacecolor = "None",
                         markersize = marker_size,
                         markeredgewidth = 2)
    style_CHFL2 = dict(color = 'r',
                         markerfacecolor = "None",
                         markersize = marker_size,
                         markeredgewidth = 2)
    style_OSSE3 = dict(color = 'g',
                         markerfacecolor = 'r',
                         markersize = marker_size,
                         markeredgewidth = 2)
    style_OSSE4 = dict(color = 'm',
                         markerfacecolor = 'r',
                         markersize = marker_size,
                         markeredgewidth = 2)

    fig_log, ((ax_log0, ax_log1, ax_log2)) = plt.subplots(nrows=3)
    ax_log = [ax_log0, ax_log1, ax_log2]

    for ii in range(3):
        if LOG:
            ax_log[ii].set_yscale('log')
            #ax_log[ii].set_ylim(ymin = 1E-5)
            #ax_log[ii].set_ylim([1E-6, 1E-1])
            #ax_log[2].set_ylim([1E-14, 1E-1])

        if CHFL1:
            ax_log[ii].plot(timestep, norm_CHFL1[ii, T0:T1+1], 'd-', label='CHFL1', **style_CHFL1)
        if CHFL2:
            ax_log[ii].plot(timestep, norm_CHFL2[ii, T0:T1+1], 'd-', label='CHFL2', **style_CHFL2)
        if OSSE3:
            ax_log[ii].plot(timestep, norm_OSSE3[ii, T0:T1+1], 'd-', label='OSSE3', **style_OSSE3)
        if OSSE4:
            ax_log[ii].plot(timestep, norm_OSSE4[ii, T0:T1+1], 'd-', label='OSSE4', **style_OSSE4)
        #if DIFF21:
        #    ax_log[ii].plot(timestep, norm_DIFF21[ii, T0:T1+1], 'd:', label='DIFF21', **style_CHFL1)
        #if DIFF43:
        #    ax_log[ii].plot(timestep, norm_DIFF43[ii, T0:T1+1], 'd:', label='DIFF43', **style_OSSE3)

        #if CHFL1:
        #    ax_log[ii].plot(timestep, norm_CHFL1_mode[ii, T0:T1+1], 'd-', label='CHFL1', **style_CHFL1)
        #if CHFL2:
        #    ax_log[ii].plot(timestep, norm_CHFL2_mode[ii, T0:T1+1], 'd-', label='CHFL2', **style_CHFL2)
        #if OSSE3:
        #    ax_log[ii].plot(timestep, norm_OSSE3_mode[ii, T0:T1+1], 'd-', label='OSSE3', **style_OSSE3)
        #if OSSE4:
        #    ax_log[ii].plot(timestep, norm_OSSE4_mode[ii, T0:T1+1], 'd-', label='OSSE4', **style_OSSE4)
        #if DIFF21:
        #    ax_log[ii].plot(timestep, norm_DIFF21_mode[ii, T0:T1+1], 'd:', label='DIFF21', **style_CHFL1)
        #if DIFF43:
        #    ax_log[ii].plot(timestep, norm_DIFF43_mode[ii, T0:T1+1], 'd:', label='DIFF43', **style_OSSE3)

        #ax_log[ii].plot(timestep, 1E-5 * np.exp(0.0501205 * timestep), '-', color='fuchsia')

    ax_log0.set_title('u')
    ax_log1.set_title('v')
    ax_log2.set_title('w')
    ax_log0.set_ylabel('norm2(u)')
    ax_log1.set_ylabel('norm2(v)')
    ax_log2.set_ylabel('norm2(w)')
    ax_log2.set_xlabel('time')
    fig_log.suptitle("Re = "+str(Re)+" / CHFL: "+str(Nx_CHFL)+'x'+str(Ny_CHFL)+'x'+str(Nz_CHFL)
                     +" / OSSE: "+str(Nx_OSSE)+'x'+str(Ny_OSSE)+'x'+str(Nz_OSSE)+' / Modes ('+str(mx)+', '+str(mz)+')')
    ax_log2.legend(loc='best')
    fig_log.subplots_adjust(bottom=0.05, top=0.93, left=0.05, right=0.99)
    plt.show()
# END OF PLOT NORM OVER TIME

#---------------------------------------
# PRINT NORM
if NORM_1:
    print("\nNORM 1")
    if CHFL1:
        print("NORM CHFL1")
        print("norm1_CHFL1_u : ", norm1_CHFL1[0])
        print("norm1_CHFL1_v : ", norm1_CHFL1[1])
        print("norm1_CHFL1_w : ", norm1_CHFL1[2])
    if CHFL2:
        print("NORM CHFL2")
        print("norm1_CHFL2_u : ", norm1_CHFL2[0])
        print("norm1_CHFL2_v : ", norm1_CHFL2[1])
        print("norm1_CHFL2_w : ", norm1_CHFL2[2])
    if OSSE3:
        print("NORM OSSE3")
        print("norm1_OSSE3_u : ", norm1_OSSE3[0])
        print("norm1_OSSE3_v : ", norm1_OSSE3[1])
        print("norm1_OSSE3_w : ", norm1_OSSE3[2])
    if OSSE4:
        print("NORM OSSE4")
        print("norm1_OSSE4_u : ", norm1_OSSE4[0])
        print("norm1_OSSE4_v : ", norm1_OSSE4[1])
        print("norm1_OSSE4_w : ", norm1_OSSE4[2])
    if DIFF21:
        print("NORM DIFF21")
        print("norm1_DIFF21_u : ", norm1_DIFF21[0])
        print("norm1_DIFF21_v : ", norm1_DIFF21[1])
        print("norm1_DIFF21_w : ", norm1_DIFF21[2])
    if DIFF43:
        print("NORM DIFF43")
        print("norm1_DIFF43_u : ", norm1_DIFF43[0])
        print("norm1_DIFF43_v : ", norm1_DIFF43[1])
        print("norm1_DIFF43_w : ", norm1_DIFF43[2])
    if CHFL1:
        print("NORM CHFL1")
        print("norm1_CHFL1_mode_u : ", norm1_CHFL1_mode[0])
        print("norm1_CHFL1_mode_v : ", norm1_CHFL1_mode[1])
        print("norm1_CHFL1_mode_w : ", norm1_CHFL1_mode[2])
    if CHFL2:
        print("NORM CHFL2")
        print("norm1_CHFL2_mode_u : ", norm1_CHFL2_mode[0])
        print("norm1_CHFL2_mode_v : ", norm1_CHFL2_mode[1])
        print("norm1_CHFL2_mode_w : ", norm1_CHFL2_mode[2])
    if OSSE3:
        print("NORM OSSE3")
        print("norm1_OSSE3_mode_u : ", norm1_OSSE3_mode[0])
        print("norm1_OSSE3_mode_v : ", norm1_OSSE3_mode[1])
        print("norm1_OSSE3_mode_w : ", norm1_OSSE3_mode[2])
    if OSSE4:
        print("NORM OSSE4")
        print("norm1_OSSE4_mode_u : ", norm1_OSSE4_mode[0])
        print("norm1_OSSE4_mode_v : ", norm1_OSSE4_mode[1])
        print("norm1_OSSE4_mode_w : ", norm1_OSSE4_mode[2])
    if DIFF21:
        print("NORM DIFF21")
        print("norm1_DIFF21_mode_u : ", norm1_DIFF21_mode[0])
        print("norm1_DIFF21_mode_v : ", norm1_DIFF21_mode[1])
        print("norm1_DIFF21_mode_w : ", norm1_DIFF21_mode[2])
    if DIFF43:
        print("NORM DIFF43")
        print("norm1_DIFF43_mode_u : ", norm1_DIFF43_mode[0])
        print("norm1_DIFF43_mode_v : ", norm1_DIFF43_mode[1])
        print("norm1_DIFF43_mode_w : ", norm1_DIFF43_mode[2])
    print('\n')

if NORM_2:
    print("\nNORM 2")
    if CHFL1:
        print("NORM 2 CHFL1")
        print("norm2_CHFL1_u : ", np.sqrt(norm2_CHFL1[0]))
        print("norm2_CHFL1_v : ", np.sqrt(norm2_CHFL1[1]))
        print("norm2_CHFL1_w : ", np.sqrt(norm2_CHFL1[2]))
    if CHFL2:
        print("NORM 2 CHFL2")
        print("norm2_CHFL2_u : ", np.sqrt(norm2_CHFL2[0]))
        print("norm2_CHFL2_v : ", np.sqrt(norm2_CHFL2[1]))
        print("norm2_CHFL2_w : ", np.sqrt(norm2_CHFL2[2]))
    if OSSE3:
        print("NORM 2 OSSE3")
        print("norm2_OSSE3_u : ", np.sqrt(norm2_OSSE3[0]))
        print("norm2_OSSE3_v : ", np.sqrt(norm2_OSSE3[1]))
        print("norm2_OSSE3_w : ", np.sqrt(norm2_OSSE3[2]))
    if OSSE4:
        print("NORM 2 OSSE4")
        print("norm2_OSSE4_u : ", np.sqrt(norm2_OSSE4[0]))
        print("norm2_OSSE4_v : ", np.sqrt(norm2_OSSE4[1]))
        print("norm2_OSSE4_w : ", np.sqrt(norm2_OSSE4[2]))
    if DIFF21:
        print("NORM DIFF21")
        print("norm2_DIFF21_u : ", np.sqrt(norm2_DIFF21[0]))
        print("norm2_DIFF21_v : ", np.sqrt(norm2_DIFF21[1]))
        print("norm2_DIFF21_w : ", np.sqrt(norm2_DIFF21[2]))
    if DIFF43:
        print("NORM DIFF43")
        print("norm2_DIFF43_u : ", np.sqrt(norm2_DIFF43[0]))
        print("norm2_DIFF43_v : ", np.sqrt(norm2_DIFF43[1]))
        print("norm2_DIFF43_w : ", np.sqrt(norm2_DIFF43[2]))
    if CHFL1:
        print("NORM CHFL1")
        print("norm2_CHFL1_mode_u : ", np.sqrt(norm2_CHFL1_mode[0]))
        print("norm2_CHFL1_mode_v : ", np.sqrt(norm2_CHFL1_mode[1]))
        print("norm2_CHFL1_mode_w : ", np.sqrt(norm2_CHFL1_mode[2]))
    if CHFL2:
        print("NORM CHFL2")
        print("norm2_CHFL2_mode_u : ", np.sqrt(norm2_CHFL2_mode[0]))
        print("norm2_CHFL2_mode_v : ", np.sqrt(norm2_CHFL2_mode[1]))
        print("norm2_CHFL2_mode_w : ", np.sqrt(norm2_CHFL2_mode[2]))
    if OSSE3:
        print("NORM OSSE3")
        print("norm2_OSSE3_mode_u : ", np.sqrt(norm2_OSSE3_mode[0]))
        print("norm2_OSSE3_mode_v : ", np.sqrt(norm2_OSSE3_mode[1]))
        print("norm2_OSSE3_mode_w : ", np.sqrt(norm2_OSSE3_mode[2]))
    if OSSE4:
        print("NORM OSSE4")
        print("norm2_OSSE4_mode_u : ", np.sqrt(norm2_OSSE4_mode[0]))
        print("norm2_OSSE4_mode_v : ", np.sqrt(norm2_OSSE4_mode[1]))
        print("norm2_OSSE4_mode_w : ", np.sqrt(norm2_OSSE4_mode[2]))
    if DIFF21:
        print("NORM DIFF21")
        print("norm2_DIFF21_mode_u : ", np.sqrt(norm2_DIFF21_mode[0]))
        print("norm2_DIFF21_mode_v : ", np.sqrt(norm2_DIFF21_mode[1]))
        print("norm2_DIFF21_mode_w : ", np.sqrt(norm2_DIFF21_mode[2]))
    if DIFF43:
        print("NORM DIFF43")
        print("norm2_DIFF43_mode_u : ", np.sqrt(norm2_DIFF43_mode[0]))
        print("norm2_DIFF43_mode_v : ", np.sqrt(norm2_DIFF43_mode[1]))
        print("norm2_DIFF43_mode_w : ", np.sqrt(norm2_DIFF43_mode[2]))
    print('\n')

