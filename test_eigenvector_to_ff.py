"""
test_eigenvector_to_ff.py

test of the method of operators.py:
- op_eigenvector_to_ff

1. load an equilibrium solution as 'eq',
2. load the same equilibrium as 'ff_test'
3. operate transformation on 'ff_test' to create a .npy file
4. load the .npy file and operate the opposite transformation
    with method op_eigenvector_to_ff
5. compare the plot of 'eq' before and 'ff_test' after transformations

"""
import numpy as np
import h5py
from flowfield import FlowField
from plotfield import plot_slice
from operators import Operators
import itertools

# parameters 
eq = 1
Nx = 4
Ny = 15
Nz = 4

# plot parameters
# usage = "plotfield [--output=plot.png] [--cut=0] [--lines=u|v|w [--contourstep=0.1]] [--quiver] [--field=u|v|w|mean] [--normal=x|y|z|mean] [--addmean=mean.asc] flowfield.hd5"
lines='u'
cut = 0
field = 'u'
normal = 'y'

# 0. loading the equilibrium
filename_eq = "./database/eq/"+\
        "eq"+str(eq)+"_"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)+".h5"
ff_eq = FlowField(filename_eq)

op = Operators(ff_eq)

##########
# TESTING THE METHOD WITH EQUILIBRIUM
##########
# 1. plot the original equilibrium slice
plot_slice(ff_eq, cut=cut, field=field, normal=normal)#, lines=lines)

# 2. transform the equilibrium into a vector [v, eta, u00, w00]
ff_test = FlowField(filename_eq)
ff_test.make_spectral('xz')

# ff_test.u is an uvw 3D array
test_uvw_vector = op.transform_spectral_uvw_array_to_vector(ff_eq, ff_test.u, truncation=False)

# test_uvw_vector is changed into test_veta
C, Cinv = op.op_mapping(ff_eq, truncation=False)
#W, Winv = op.op_weighting(ff_eq, truncation=False)

#test_veta_vector = np.dot(np.dot(Cinv, Winv), test_uvw_vector)
test_veta_vector = np.dot(Cinv, test_uvw_vector)

# 3. save it as .npy
filename_test = "database/eigenmodes/test/eigenvector_test.npy"
np.save(filename_test, test_veta_vector)
# eigenvector_test.npy correspond to the EQ1 solution under the [v, eta, u00, w00] shape in a vector

eigenvector_veta = np.load(filename_test) #test_vector

##########
# TRANSFORMING INTO HDF5
##########
filename_ff = "database/eigenmodes/test/eigenmode"\
        +"_test"+"_eq"+str(eq)+"_"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)+".h5"
op.transform_eigenvector_to_ff(ff_eq, eigenvector_veta, filename_ff)

# 5. load the FlowField and  plotfield
ff = FlowField(filename_ff)
plot_slice(ff, cut=cut, field=field, normal=normal)#, lines=lines)
