"""
run_cfbc_validation_simu.py

Create the matrices of the OSSE actuated and integrated it
for a given actuation mode.
The baseflow is Couette Plane Flow,
so it is possible to compare with the OSS model
and Channelflow linearized.
"""
import numpy as np
import scipy as sp
import scipy.integrate as spi
import datetime
import chebyshev
import itertools
import os, errno
import timeit
import numba

import matplotlib
matplotlib.use("TkAgg")
#import matplotlib.pyplot as plt

from channel import Channel
from operators import Operators as OP

def f_nabla2(Ny, kx, kz, Lx, Lz):
    nodes, DM = chebyshev.chebdiff(Ny, 2)
    D2_v = DM[1, :, :]
    #D2_v[0, :] = 0
    #D2_v[-1, :] = 0
    #D2_v[:, 0] = 0
    #D2_v[:, -1] = 0
    alpha = 2*np.pi*kx/Lx
    beta = 2*np.pi*kz/Lz
    return (-1 * (alpha**2 + beta**2)*np.eye(Ny) + D2_v)
    
def act_vector_v(Nx, Nz, modes_up, modes_low):
    freq_kx = np.fft.fftfreq(Nx, 1/Nx).astype(np.integer, copy=False)
    freq_kz = np.fft.fftfreq(Nz, 1/Nz).astype(np.integer, copy=False)

    vector_v = np.zeros((2 * (Nx * Nz)))
    vector_eta = np.zeros((2 * (Nx * Nz - 1)))
    vector_u00 = np.zeros((2 * (1)))
    vector_w00 = np.zeros((2 * (1)))

    for i_kx, i_kz in itertools.product(range(Nx), range(Nz)):
        kx = freq_kx[i_kx]
        kz = freq_kz[i_kz]
        if (kx, kz) in modes_up:
            vector_v[2 * ((i_kx*Nx) + i_kz)] = 1
        if (kx, kz) in modes_low:
            vector_v[2 * ((i_kx*Nx) + i_kz) + 1] = 1

    # to correspond to actuation on v, eta, u00 and w00
    return np.concatenate((vector_v,
                          vector_eta,
                          vector_u00,
                          vector_w00))

def forcing(t):
    #return 1 * np.cos(0.05 * t + np.pi/2) * (0.05 - 0.01j) # np.cos(radians)
    return 1 * 0.0005 * np.sin(2*np.pi*t/10)
    #return 1 * 0.0005 * (1 - np.exp(-t/10))

###########
# PARAMETERS
###########

Re = 400
Nx = 4
Ny = 15
Nz = 4

baseflow_type = "eq1" # "couette"
basename = "/home/gcpc1m15/osse/" #'/home/gcls/osse/'

#basename = 
T0 = 0
T1 = 150
dt = 0.01
tau = 0.05
tau_v = tau
tau_eta = tau
tau_u = tau
tau_w = tau

# initial function coefficients
e = +5
f = -3
g = -1

# time to save the dataset
Nt = (T1-T0)+1
t_saved = np.linspace(T0, T1, Nt)

kx = 1
kz = 0

restart = False
save_spectral = True
reduction = False
memory = True

print('\n###########################################################################\n')
print('Nx       = ', Nx)
print('Ny       = ', Ny)
print('Nz       = ', Nz)
print('T0, T1   = ', T0, " - ", T1)
print('dt       = ', dt)
print('tau      = ', tau)
print('Re       = ', Re)
print('t_saved  = ', t_saved)
print('kx       = ', kx)
print('kz       = ', kz)
print('\n###########################################################################\n')

spectral_str = "_spectral" if save_spectral else ""

# Lx = 5.51156605893 # 2*pi / 1.14
# Lz = 2.51327412287 # 2*pi / 2.5
Lx = 2 * np.pi / 1.14
Lz = 2 * np.pi / 2.5

sign_x = '+' if np.sign(kx) == 1 else ''
sign_z = '+' if np.sign(kz) == 1 else ''

N = (Ny) * (Nx*Nz + Nx * Nz - 1 + 2)
N_act = 2 * (Nx*Nz + Nx * Nz - 1 + 2)

###########
# BUILDING FLOWFIELD
###########

#just to get the structure
N_eq = 1
kind_of_flow = 'eq'+str(N_eq)+'_'+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
ff = Channel('database/eq/'+kind_of_flow+'.h5')
#ff.make_spectral('xz')

# load an initial state and make it SPECTRAL
if restart:
    ff_t0 = Channel("database/eq/"+baseflow_type+"_cf/"
        +kind_of_flow
        +"_m="+str(kx)+"x"+str(kz)
        +"_f="+str(forcing(1))
        +"_t="+str(T0)+".h5")
    #ff_t0 = Channel('database/eq/'+kind_of_flow+'.h5')
    ff_t0.u -= ff.u
    ff_t0.make_spectral('xz')

# load the corresponding baseflow and keep it PHYSICAL
#ff.make_physical('xz')
ff.u.fill(0)
if baseflow_type == "couette":
    ff.mean.apply_mean_flow('couette')
elif baseflow_type == "eq1":
    ff.load_baseflow("database/eq/"+kind_of_flow+".h5")
    # add a Couette baseflow U(y) = y
    for ny in range(ff.u.shape[2]):
        ff.u0[0, :, ny, :] += ff.y[ny]

#ff.mean.apply_mean_flow('zero')

f_u = OP.f_lift(ff.y, 'up', 'clamped')
f_l = OP.f_lift(ff.y, 'low', 'clamped')

# BUILDING OPERATOR
oss_op = OP(ff)

# if required to create the matrices
if True:
    E, A, B = oss_op.op_nonlaminar_baseflow_actuated(
            ff,
            Re,
            lifting_function = OP.f_lift,
            tau_v=tau_v,
            tau_eta=tau_eta,
            tau_u=tau_u,
            tau_w=tau_w)

    A = np.dot(E, A)
    print("A.shape : ", A.shape)
    B = np.dot(E, B)
    print("B.shape : ", B.shape)
# if matrices already created
else:
    A, A_N1, A_N2 = oss_op.load_mat_hdf5(
            basename+"database/OSSE/"+baseflow_type
            +"/Re="+str(Re)+"/"
            +str(Nx)+"x"+str(Ny)+"x"+str(Nz)+"/A.h5")

    B, B_N1, B_N2 = oss_op.load_mat_hdf5(
            basename+"database/OSSE/"+baseflow_type
            +"/Re="+str(Re)+"/"
            +str(Nx)+'x'+str(Ny)+"x"+str(Nz)+"/B.h5")

# REDUCE B TO ONLY THE MAIN EIGENMODES
if reduction:
    # remove control of wall normal vorticity
    # keep the central modes for the wall normal velocity
    N_act, B, Qq = oss_op.op_reduce_actuation_modes(ff, B, D = None,
            kx_v = 3,
            kz_v = 1,
            kx_eta = -1,
            kz_eta = -1,
            uw = False,
            cholesky=False)
    print("B.shape reduced : ", B.shape)

###########
# INTEGRATION
###########
N_step = (T1-T0)/dt

#factor = np.ones(shape=(Nx*Nz + Nx*Nz -1 +2)) #(1 + 1j) * np.random.rand((Nx*Nz + Nx * Nz - 1 + 2))
#x_0 = factor[:, np.newaxis] * OP.f_initial(ff.y, e, f, g)
#x_0 = x_0.flatten()

if restart:
    x_0 = oss_op.transform_spectral_array_uvw_to_vector_veta_actuated(
        ff,
        ff_t0.u,
        f_u, f_l)
else:
    x_0 = np.zeros((A.shape[0]), dtype=complex)

# (0,0):  kx =  0, kz = 0, kx_v = 0, kz_v = 0
if kx == 0 and kz == 0:
    print("Actuation on mode : (0,0) [NOT ACTUATED]")
    vect = act_vector_v(Nx, Nz, [], [])

# (0,1):  kx =  0, kz = +1, kx_v = 0, kz_v = 1
if kx == 0 and kz == 1:
    print("Actuation on mode : (0,1)")
    vect = act_vector_v(Nx, Nz, [(0,1)], [(0,1)])

# (1,0):  kx = +1, kz =  0, kx_v = 2, kz_v = 2
elif kx == 1 and kz == 0:
    print("Actuation on mode : (1,0)")
    vect = act_vector_v(Nx, Nz, [(1,0)], [(1,0)])

# (0,-1):  kx =  0, kz = -1, kx_v = 2, kz_v = 2
elif kx == 0 and kz == -1:
    print("Actuation on mode : (0,-1)")
    vect = act_vector_v(Nx, Nz, [(0,-1)], [(0,-1)])

# (1,1):  kx = +1, kz = +1, kx_v = 2, kz_v = 2
elif kx == 1 and kz == 1:
    print("Actuation on mode : (1,1)")
    vect = act_vector_v(Nx, Nz, [(1,1)], [(1,1)])

# (2,2):  kx = +2, kz = +2, kx_v = 2, kz_v = 2
elif kx == +2 and kz == +2:
    print("Actuation on mode : (+2,+2)")
    vect = act_vector_v(Nx, Nz, [(+2,+2)], [(+2,+2)])

# (3,1):  kx = +3, kz = +1, kx_v = 3, kz_v = 1
elif kx == +3 and kz == +1:
    print("Actuation on mode : (+3,+1)")
    vect = act_vector_v(Nx, Nz, [(+3,+1)], [(+3,+1)])

# (2,2):  kx = -2, kz = -2, kx_v = 2, kz_v = 2
elif kx == -2 and kz == -2:
    print("Actuation on mode : (-2,-2)")
    vect = act_vector_v(Nx, Nz, [(-2,-2)], [(-2,-2)])

else:
    print("No Actuation")
    vect = act_vector_v(Nx, Nz, [], [])

if True:
    # save actuation vector
    OP.save_mat_hdf5( vect,
        basename+"database/OSSE/eq"
        +str(N_eq)+"/Re="+str(Re)+"/"
        +str(Nx)+'x'+str(Ny)+'x'+str(Nz)+"/",
        "q_kx="+sign_x+str(kx)+"_kz="+sign_z+str(kz)+".h5")

print("vect.shape : ", vect.shape)
if reduction:
    N_act2, vect, vect2 = oss_op.op_reduce_actuation_modes(
                ff, vect, D = None,
            kx_v = 3,
            kz_v = 1,
            kx_eta = -1,
            kz_eta = -1,
            uw = False,
            cholesky=False)
    print("vect.shape reduced : ", vect.shape)

# check which block matrix of A and B is zero
if False:
    for i_k, i_k2 \
        in itertools.product(range(Nx*Nz + Nx*Nz - 1 + 2),
                             range(Nx*Nz + Nx*Nz - 1 + 2)):

        #X = A[(i_k )*Ny: (i_k  + 1)*Ny,
        #      (i_k2)*Ny: (i_k2 + 1)*Ny]
        X2 = B[(i_k )*Ny: (i_k  + 1)*Ny,
               (i_k2)*2 : (i_k2 + 1)*2 ]

        #if np.linalg.norm(X, axis=(0,1)) > 1E-4:
        #    print("(", i_k, " / ", i_k2, ")")
        #    #print("(", i_kx, " / ", i_kz, " / ", i_kx2, " / ", i_kz2, ")")
        #    print("A NONZERO")
        #    if np.linalg.norm(X2, axs=(0,1)) > 1E-4:
        #        print("B NONZERO")
        if np.linalg.norm(X2, axis=(0,1)) > 1E-4:
            print("(", i_k, " / ", i_k2, ")")
            #print("(", i_kx, " / ", i_kz, " / ", i_kx2, " / ", i_kz2, ")")
            #print("B NONZERO")


def direct_eq(t, x, A, B, vect, forcing):
    return np.dot(A, x) + forcing(t) * np.dot(B, vect)

begin = np.datetime64(datetime.datetime.now(), 'm')
list_call = [T0]
call_period = 1
def f_call(t, x):
    t1 = np.floor(t)
    if t1 % call_period == 0 and t1 > list_call[-1]:
        print(">>> ", t1, " / elapsed : ", np.datetime64(datetime.datetime.now(), 'm') - begin)
        list_call.append(t1)
    return t % call_period

print('\nINTEGRATION ACTUATED')
begin1 = np.datetime64(datetime.datetime.now(), 's')
sol = spi.solve_ivp(fun = lambda t, y: direct_eq(t, y, A, B, vect, forcing),
                    t_span = (T0,T1),
                    y0 = x_0,
                    method = 'BDF',
                    t_eval = t_saved
                    events = f_call,
                    rtol = 1e-8,
                    atol = 1e-8,
                    jac = A)
# 1e-8
print(">>> TOTAL elapsed : ", np.datetime64(datetime.datetime.now(), 's') - begin1)

print("success ?! -> ", sol.success)
x = sol.y.T # shape [N_step, N]
print("Shape of solution : ", x.shape)

###########
# SAVING
###########
if T0 == 0 :
    T_str = str(T1)
else:
    T_str = str(T0)+"-"+str(T1)

for it in t_saved:
    if it%10 == 0:
        print(">>>>>>>>> it : ", str(it))

    x_t1 = x[int(it),:]

    x_1 = oss_op.transform_spectral_vector_veta_to_array_uvw_actuated(
                ff,
                x_t1,
                f_u, f_l)
                #make_physical=not(save_spectral))
    if not(save_spectral):
        oss_op.make_physical_dataset(x_1, nz=Nz, index='xz')

    try:
        os.makedirs(basename+"database/CFBC_validation/osse/"
                    +baseflow_type
                    +"/Re="+str(Re)+"/"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)
                    +"/kx="+sign_x+str(kx)+"_kz="+sign_z+str(kz)
                    +spectral_str+"/")
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    #oss_op.save_h5_cmplx(x_1,
    #               filename = basename+"database/CFBC_validation/osse/"
    #                        +baseflow_type
    #                        +"/Re="+str(Re)+"/"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)
    #                        +"/kx="+sign_x+str(kx)+"_kz="+sign_z+str(kz)
    #                        +spectral_str
    #                        +"/x_"+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
    #                        +"_Re="+str(Re)+"_T="+T_str+"_t="+str(int(it))+"_act.h5")

    if True:
        x_1_phys = ff.make_physical_dataset(x_1, nz=Nz, index='xz')

        try:
            os.makedirs(basename+"database/CFBC_validation/osse/"
                        +baseflow_type
                        +"/Re="+str(Re)+"/"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)
                        +"/kx="+sign_x+str(kx)+"_kz="+sign_z+str(kz)+"/")
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        oss_op.save_h5_real(x_1_phys,
                       filename = basename+"database/CFBC_validation/osse/"
                                +baseflow_type
                                +"/Re="+str(Re)+"/"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)
                                +"/kx="+sign_x+str(kx)+"_kz="+sign_z+str(kz)
                                +"/x_"+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
                                +"_Re="+str(Re)+"_T="+T_str+"_t="+str(int(it))+".h5")

    # save in velocity / vorticity array
    if False:
        array_veta = oss_op.transform_spectral_veta_vector_to_array(
                ff, x_t1, truncation=False)
        array_veta_2 = oss_op.apply_wall_actuation_on_v0_veta(
            array_veta, f_u, f_l)

        # check the BC applied at the upper and lower wall.
        if False:
            if np.linalg.norm(array_veta_2[0, :, 0, :], axis=(0,1)) > 1E-4:
                print("V UP  / it : ", str(it))
                print(array_veta_2[0, :, 0, :])
            if np.linalg.norm(array_veta_2[0, :, -1, :], axis=(0,1)) > 1E-4:
                print("V LOW  / it : ", str(it))
                print(array_veta_2[0, :, -1, :])

            if np.linalg.norm(array_veta_2[1, :, 0, :], axis=(0,1)) > 1E-4:
                print("ETA UP  / it : ", str(it))
                print(array_veta_2[1, :, 0, :])
            if np.linalg.norm(array_veta_2[1, :, -1, :], axis=(0,1)) > 1E-4:
                print("ETA LOW  / it : ", str(it))
                print(array_veta_2[1, :, -1, :])

            if np.linalg.norm(array_veta_2[2, :, 0, :], axis=(0,1)) > 1E-4:
                print("U00 UP  / it : ", str(it))
                print(array_veta_2[2, :, 0, :])
            if np.linalg.norm(array_veta_2[2, :, -1, :], axis=(0,1)) > 1E-4:
                print("U00 LOW  / it : ", str(it))
                print(array_veta_2[2, :, -1, :])

            if np.linalg.norm(array_veta_2[3, :, 0, :], axis=(0,1)) > 1E-4:
                print("W00 UP  / it : ", str(it))
                print(array_veta_2[3, :, 0, :])
            if np.linalg.norm(array_veta_2[3, :, -1, :], axis=(0,1)) > 1E-4:
                print("W00 LOW  / it : ", str(it))
                print(array_veta_2[3, :, -1, :])

        # save v and eta as channel shape
        array_veta_3 = np.zeros(shape=(3, Nx, Ny, Nz), dtype=complex)
        array_veta_3[1, :, :, :] = array_veta_2[0, :, :, :]
        array_veta_3[2, :, :, :] = array_veta_2[1, :, :, :]

        oss_op.save_h5_cmplx(
            array_veta_3,
            filename = basename+"database/CFBC_validation/osse/"
                    +baseflow_type
                    +"/Re="+str(Re)+"/"+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
                    +"/kx="+sign_x+str(kx)+"_kz="+sign_z+str(kz)
                    +spectral_str
                    +"/x_"+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
                    +"_Re="+str(Re)+"_T="+T_str+"_t="+str(int(it))+"_veta.h5")
