"""
DEPRECATED : THESE TEST WILL RETURN 'FALSE' EVERYWHERE AS I DID CHANGE
THE METHOD MAKE_SPECTRAL AND MAKE_PHYSICAL BY ADDING A NORMALISATION
(I mean multiplying/dividing by u.shape[.] during the FFT).
"""
import h5py
import numpy as np
from math import *
import unittest
import warnings

from flowfield import FlowField
from flowfield import DiffOperator_FlowField
from channel import Channel
from channel import DiffOperator_Channel
import chebyshev

#import noise
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt


class ChannelTest(unittest.TestCase):
    """ Tests of methods implemented in channel."""

    def setUp(self, filename='database/test_files/channel_sample.h5'):
        """ This method is called before each test.

        Creation of a new flowfield in an hdf5 file.
        Reading of this file to create a new flowfield with method load.

        """
        # delete warning due to code in chebyshev.chebdiff (see details in code)
        #warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)

        filename = 'database/test_files/channel_sample.h5'

        # Creation of a new h5-file similar to a ChannelFlow flowfield
        file = h5py.File(filename, 'w')
        data = file.create_group('data')
        geom = file.create_group('geom')

        file.attrs['Lx'] = 1 * pi
        file.attrs['Lz'] = 0.5 * pi
        file.attrs['a'] = -1
        file.attrs['b'] = 1
        a = file.attrs['a']
        b = file.attrs['b']

        # BE CAREFUL, VALUES BELOW 10 DOES NOT WORK
        file.attrs['Nd'] = 3
        file.attrs['Nx'] = 11
        file.attrs['Ny'] = 100
        file.attrs['Nz'] = 13
        self.Nd = file.attrs['Nd']
        self.Nx = file.attrs['Nx']
        self.Ny = file.attrs['Ny']
        self.Nz = file.attrs['Nz']

        # Creation of a basis (x, y, z) of dim (Nx, Ny, Nz)
        array_x = np.arange(0, file.attrs['Lx'], file.attrs['Lx'] / self.Nx)
        #array_y = np.arange(a, b, (b-a)/self.Ny)
        array_y = np.linspace(a, b, num=self.Ny, endpoint=True)
        array_z = np.arange(0, file.attrs['Lz'], file.attrs['Lz'] / self.Nz)

        x = geom.create_dataset('x', data=array_x)
        y = geom.create_dataset('y', data=array_y)
        z = geom.create_dataset('z', data=array_z)

        # generation of a white noise
        #x = np.linspace(-1, 1, num=self.Ny)
        #noise1d = np.ones(self.Ny)
        # for i in range(self.Ny):
        #    noise1d[i] = 10*noise.pnoise1(x[i], octaves=9, base=5)

        # Creation of a dataset u(x,y,z)
        coef_cos_Z = 3
        coef_sin_Z = 2
        offset = np.power(np.tan(array_y[0] * (np.pi / 2 - 0.5)), 2)
        array_u = np.ndarray(shape=(3, self.Nx, self.Ny, self.Nz))
        for i in range(array_u.shape[0]):
            for nx in range(array_u.shape[1]):
                for ny in range(array_u.shape[2]):
                    for nz in range(array_u.shape[3]):
                        array_u[i, nx, ny, nz] = 0 * i \
                            - 0 * (np.power(np.tan(array_y[ny] * (np.pi / 2 - 0.5)), 2) - offset) \
                            + 0 * np.power(2 * (ny - (self.Ny - 1) / 2) / (self.Ny - 1), 4) \
                            + 1 * np.sin(2 * np.pi * 4 * array_x[nx] / file.attrs['Lx']) \
                            + 1 * np.cos(2 * np.pi * 2 * array_y[ny] / (b - a)) \
                            + 1 * np.cos(2 * np.pi * coef_cos_Z * array_z[nz] / file.attrs['Lz']) * np.sin(2 * np.pi * coef_sin_Z * array_z[nz] / file.attrs['Lz'])
                        #+ 0 * 10 * noise1d[ny] \

        u = data.create_dataset('u', data=array_u)

        # Creation of a dataset p(x,y,z)
        coef_cos_Z = 3
        coef_sin_Z = 2
        array_p = np.ndarray(shape=(self.Nx, self.Ny, self.Nz))
        for nx in range(array_p.shape[0]):
            for ny in range(array_p.shape[1]):
                for nz in range(array_p.shape[2]):
                    array_p[nx, ny, nz] = \
                        + 1 * np.sin(2 * np.pi * 4 * array_x[nx] / file.attrs['Lx']) \
                        + 1 * np.cos(2 * np.pi * 4 * array_y[ny] / (b - a)) \
                        + 1 * np.cos(2 * np.pi * coef_cos_Z * array_z[nz] / file.attrs['Lz']) * np.sin(2 * np.pi * coef_sin_Z * array_z[nz] / file.attrs['Lz'])

        p = data.create_dataset('p', data=array_p)

        file.close()

        # Load the saved h5-file to a new flowfield
        self.ff = Channel(filename)

    def tearDown(self):
        """ This method is called after each test."""

    def test_init(self, filename='database/test_files/channel_sample.h5'):
        """ NOT UPDATED !!!! Test of the flowfield.

        This tests verify if the flowfield in method setUp is correctly defined:
        - 2 groups : data, geom
        - 3 datasets in geom (x,y,z)
        - 1 dataset in data (u)

        """
        pass
        # Test of the dimension of datasets (x,y,z)

        assert self.ff.len() == 630, 'Flowfield does not have th expected size.'
        assert len(
            self.ff.x) == 7, 'Flowfield dataset x does not have the expected size (6).'
        assert len(
            self.ff.y) == 5, 'Flowfield dataset y does not have the expected size (5).'
        assert len(
            self.ff.z) == 6, 'Flowfield dataset z does not have the expected size (6).'
        assert self.ff.x.all() == np.array([0, 2 *
                                            pi /
                                            6, 4 *
                                            pi /
                                            6, 6 *
                                            pi /
                                            6, 8 *
                                            pi /
                                            6, 10 *
                                            pi /
                                            6, 2 *
                                            pi]).all(), 'Flowfield dataset x is not defined as expected.'
        assert self.ff.y.all() == np.array(
            [0, pi / 2, pi, 3 * pi / 2, 2 * pi]).all(), 'Flowfield dataset y is not defined as expected.'
        assert self.ff.z.all() == np.array([0, 2 *
                                            pi /
                                            5, 4 *
                                            pi /
                                            5, 6 *
                                            pi /
                                            5, 8 *
                                            pi /
                                            5, 2 *
                                            pi]).all(), 'Flowfield dataset z is not defined as expected.'

        # Test of the datasets values

        assert self.ff.u.shape[
            0] == 3, 'Flowfield dataset u does not have the expected shape[0].'
        assert self.ff.u.shape[
            1] == 7, 'Flowfield dataset u does not have the expected shape[1].'
        assert self.ff.u.shape[
            2] == 5, 'Flowfield dataset u does not have the expected shape[2].'
        assert self.ff.u.shape[
            3] == 6, 'Flowfield dataset u does not have the expected shape[3].'
        assert self.ff.u[0, :, 0, 0].all() == np.array([0, 2, 4, 6, 8, 10]).all(), \
            'Flowfield dataset u has not the expected values.'
        assert self.ff.u[0, 0, :, 0].all() == np.array([0, 3, 6, 9, 12]).all(), \
            'Flowfield dataset u has not the expected values.'
        assert self.ff.u[0, 0, 0, :].all() == np.array([0, 5, 10, 15, 20]).all(), \
            'Flowfield dataset x is not defined as expected.'
        assert self.ff.u[2, 0, 0, :].all() == np.array([2, 7, 12, 17, 22]).all(), \
            'Flowfield dataset x is not defined as expected.'

    def test_save_load(self):
        """ Test of methods save() and load()."""
        self.ff.save('database/test_files/channel_sample_save.h5')
        ff2 = Channel()
        ff2.load('database/test_files/channel_sample_save.h5')
        #assert ff2.len() ==  630, 'Flowfield does not have th expected size.'
        assert self.ff.isEqual(
            ff2), 'A loaded channel is different from the original one.'

    def test_copy(self):
        """ Test of method copy()."""
        ff3 = Channel()
        ff3.copy(self.ff)
        assert self.ff.isEqual(
            ff3), 'The copied channel is different from the orignal one.'
        #ff4 = dop.derivative(self.ff, 1,0,0)
        #self.assertFalse( (self.ff==ff4).all(), msg='A different flowfield is not seen as different from the orignal one.')

    def test_interpolation(self):
        """
        Test of method uniform2chebyshev()
        and chebyshev2uniform()
        for dataset u and p.
        """
        # Setting of dataset and parameters
        a = self.ff.attributes['a']
        b = self.ff.attributes['b']
        array_y = np.linspace(a, b, num=self.Ny, endpoint=True)

        for nx in range(self.ff.u.shape[1]):
            for ny in range(self.ff.u.shape[2]):
                for nz in range(self.ff.u.shape[3]):
                    for i in range(self.ff.u.shape[0]):
                        self.ff.u[i, nx, ny, nz] = 0 * i + 1 * \
                            np.power(np.cos(2 * np.pi * 4 * array_y[ny] / (b - a)), 2)
                    self.ff.p[ nx, ny, nz] = 3 * \
                        np.power(np.cos(2 * np.pi * 4 * array_y[ny] / (b - a)), 2)

        # Saving dataset into independant variables for later comparison
        u_stored = np.ndarray((self.ff.u.shape[0],
                               self.ff.u.shape[1],
                               self.ff.u.shape[2],
                               self.ff.u.shape[3]))
        p_stored = np.ndarray((self.ff.p.shape[0],
                               self.ff.p.shape[1],
                               self.ff.p.shape[2]))
        u_stored[:, :, :, :] = self.ff.u[:, :, :, :]
        p_stored[:, :, :] = self.ff.p[:, :, :]

        # Building analytical values for Chebyshev configuration
        Ny = self.ff.u.shape[2]
        cheb_nodes = np.sin(np.pi * np.arange(Ny - 1, -1 *
                                              Ny, -2.0)[:, np.newaxis] / (2.0 * (Ny - 1)))
        data_cheb = np.power(
            np.cos(2 * np.pi * 4 * cheb_nodes[:, :] / (b - a)), 2)

        # Comparing analytical and computed values
        self.ff.uniform2chebyshev()
        np.testing.assert_almost_equal(
            self.ff.u[0, 1, :, 2],
            data_cheb[:, 0],
            err_msg='Computed Chebyshev interpolations are not ' +
            'as the analytical ones')
        np.testing.assert_almost_equal(
            self.ff.p[1, :, 2],
            3 * data_cheb[:, 0],
            err_msg='Computed Chebyshev interpolations are not ' +
            'as the analytical ones')

        # Comparing physical values after a long cycle
        for i in range(10):
            self.ff.uniform2chebyshev()
            self.ff.chebyshev2uniform()

        np.testing.assert_almost_equal(
            self.ff.u[:, :, :, :],
            u_stored[:, :, :, :],
            err_msg='After 1000 cycles, dataset u is no more as original')

        np.testing.assert_almost_equal(
            self.ff.p[:, :, :],
            p_stored[:, :, :],
            err_msg='After 1000 cycles, dataset p is no more as original')

    def test_interpolation_u_graphic(self):
        """
        Graphical test of method uniform2chebyshev()
        and chebyshev2uniform() for dataset u.
        """
        plt.figure()
        # plt.ylim([-10,10])

        # ORIGINAL DATA
        original = plt.plot(self.ff.y[:], self.ff.u[0, 1, :, 2], '-o')

        for i in range(2):  # 999
            self.ff.uniform2chebyshev()
            self.ff.chebyshev2uniform()

        self.ff.uniform2chebyshev()
        interpolated_spec_1000 = plt.plot(
            self.ff.y[:], self.ff.u[0, 1, :, 2], 'o')
        self.ff.chebyshev2uniform()
        interpolated_phy_1000 = plt.plot(
            self.ff.y[:], self.ff.u[0, 1, :, 2], 'o')

        plt.legend(('Original Velocity dataset',
                    'Chebyshev interp (after 1000 cycles)',
                    'Uniform interp (after 1000 cycles)'),
                   fontsize='small')
        plt.title("Test of methods uniform2chebyshev()" +
                  " and chebyshev2uniform()" +
                  " for VELOCITY dataset.")
        plt.savefig('figures/test_channel_interp_u.png',
                    bbox_inches='tight')

        plt.show()

    def test_interpolation_p_graphic(self):
        """
        Graphical test of method uniform2chebyshev()
        and chebyshev2uniform() for dataset p.
        """
        plt.figure()
        # plt.ylim([-10,10])

        # ORIGINAL DATA
        original = plt.plot(self.ff.y[:], self.ff.p[1, :, 2], '-o')

        for i in range(2):
            self.ff.uniform2chebyshev()
            self.ff.chebyshev2uniform()

        self.ff.uniform2chebyshev()
        interpolated_spec_1000 = plt.plot(
            self.ff.y[:], self.ff.p[1, :, 2], 'o')
        self.ff.chebyshev2uniform()
        interpolated_phy_1000 = plt.plot(self.ff.y[:], self.ff.p[1, :, 2], 'o')

        plt.legend(('Original Pressure dataset',
                    'Chebyshev interp (after 1000 cycles)',
                    'Uniform interp (after 1000 cycles)'),
                   fontsize='small')
        plt.title("Test of methods uniform2chebyshev()" +
                  " and chebyshev2uniform()" +
                  " for PRESSURE dataset.")
        plt.savefig('figures/test_channel_interp_p.png',
                    bbox_inches='tight')
        plt.show()

    def test_dataset_chebyshev2uniform_u_graphic(self):
        plt.figure()

        self.ff.uniform2chebyshev()
        diff_y = self.ff.dop.deriv_u(0, 1, 0)
        deriv_data = plt.plot(self.ff.y[:], diff_y[0, 1, :, 1], '-o',
                              label='Diffop deriv Chebyshev nodes')

        diff_y_uniform = self.ff.dataset_chebyshev2uniform(diff_y)
        deriv_uniform = plt.plot(self.ff.y_saved_uniform[:],
                                 diff_y_uniform[0, 1, :, 1],
                                 'o',
                                 label='Diffop deriv Uniform nodes')

        plt.legend(fontsize='small')
        plt.title('Test of method dataset_chebyshev2uniform for Velocity')
        plt.savefig('figures/test_channel_u_chebyshev2uniform.png',
                    bbox_inches='tight')
        plt.show()

    def test_dataset_chebyshev2uniform_p_graphic(self):
        plt.figure()

        self.ff.uniform2chebyshev()
        diff_y = self.ff.dop.deriv_p(0, 1, 0)
        deriv_data = plt.plot(self.ff.y[:], diff_y[1, :, 1], '-o',
                              label='Diffop deriv Chebyshev nodes')

        diff_y_uniform = self.ff.dataset_chebyshev2uniform(diff_y)
        deriv_uniform = plt.plot(self.ff.y_saved_uniform[:],
                                 diff_y_uniform[1, :, 1],
                                 'o',
                                 label='Diffop deriv Uniform nodes')

        plt.legend(fontsize='small')
        plt.title('Test of method dataset_chebyshev2uniform for Pressure')
        plt.savefig('figures/test_channel_p_chebyshev2uniform.png',
                    bbox_inches='tight')

        plt.show()

    def test_deriv_u_graphic(self):
        """ Graphical test of method deriv_u."""
        plt.figure()
        Ny = self.ff.y.shape[0]
        x = np.linspace(-1, 1, num=Ny)
        dx = x[1] - x[0]

        # ORIGINAL CURVE
        #offset = np.power(np.tan(-1*(np.pi/2-0.5)), 2)
        #function = plt.plot(x, -np.power(np.tan(x*(np.pi/2-0.5)),2) + offset)
        fct = plt.plot(x, self.ff.u[0, 1, :, 1])
        # plt.ylim([-100,100])

        # GRADIENT CURVE
        dydx = np.gradient(self.ff.u[0, 1, :, 1], dx)
        deriv_1 = plt.plot(x, dydx, label='Gradient order 1')

        # GRADIENT CURVE ORDER 2
        dydx2 = np.gradient(dydx, dx)
        deriv_2 = plt.plot(x, dydx2, label='Gradient order 2')

        # GRADIENT CURVE ORDER 3
        dydx3 = np.gradient(dydx2, dx)
        deriv_3 = plt.plot(x, dydx3, label='Gradient order 3')

        self.ff.uniform2chebyshev()

        # DIFFOP DERIVATIVE ORDER 3
        diff_y3 = self.ff.dop.deriv_u(0, 3, 0)
        #nodes = chebyshev.chebnodes(self.ff.y.shape[0])
        deriv_data_3 = plt.plot(self.ff.y[:], diff_y3[0, 1, :, 1], 'ko',
                                label='Diffop deriv 3')

        # DIFFOP DERIVATIVE ORDER 2
        diff_y2 = self.ff.dop.deriv_u(0, 2, 0)
        deriv_data_2 = plt.plot(self.ff.y[:], diff_y2[0, 1, :, 1], 'ro',
                                label='Diffop deriv 2')

        # DIFFOP DERIVATIVE ORDER 1
        diff_y1 = self.ff.dop.deriv_u(0, 1, 0)
        deriv_data_1 = plt.plot(self.ff.y[:], diff_y1[0, 1, :, 1], 'go',
                                label='Diffop deriv 1')

        plt.legend(fontsize='small')
        plt.title('Test of derivatives of dataset VELOCITY')
        plt.savefig('figures/test_channel_u_deriv_u_graphic.png',
                    bbox_inches='tight')
        plt.show()

    def test_deriv_p_graphic(self):
        """ Graphical test of method deriv_p."""
        plt.figure()
        Ny = self.ff.y.shape[0]
        x = np.linspace(-1, 1, num=Ny)
        dx = x[1] - x[0]

        # ORIGINAL CURVE
        original = plt.plot(x, self.ff.p[1, :, 1], 'b', label='orgine')

        # GRADIENT CURVE
        dpdy = np.gradient(self.ff.p[1, :, 1], dx)
        deriv_p1 = plt.plot(x, dpdy, 'g')

        # GRADIENT CURVE ORDER 2
        dpdy2 = np.gradient(dpdy, dx)
        deriv_p2 = plt.plot(x, dpdy2, 'r')

        self.ff.uniform2chebyshev()

        # DIFFOP DERIVATIVE
        diff_p1 = self.ff.dop.deriv_p(0, 1, 0)
        deriv_data_p1 = plt.plot(self.ff.y[:], diff_p1[1, :, 5], 'go')
        #
        # DIFFOP DERIVATIVE ORDER 2
        diff_p2 = self.ff.dop.deriv_p(0, 2, 0)
        deriv_data_p2 = plt.plot(self.ff.y[:], diff_p2[1, :, 5], 'ro')

        plt.legend(('Pressure dataset',
                    'Gradient order 1',
                    'Gradient order 2',
                    'Diffop deriv ord 1',
                    'Diffop deriv ord 2'),
                   fontsize='small')
        plt.title('Test of derivatives of dataset PRESSURE')
        plt.savefig('figures/test_channel_u_deriv_p_graphic.png',
                    bbox_inches='tight')

        plt.show()

    def test_deriv_u_p(self):
        """ Test of method derivative for all direction and some indexes."""

        # Setting of dataset and parameters
        a = self.ff.attributes['a']
        b = self.ff.attributes['b']
        Lx = self.ff.attributes['Lx']
        Lz = self.ff.attributes['Lz']

        array_x = np.arange(0, Lx, Lx / self.Nx)
        array_y = np.sin(np.pi * np.arange(self.Ny - 1, -1 *
                                           self.Ny, -2.0)[:] / (2.0 * (self.Ny - 1)))
        array_z = np.arange(0, Lz, Lz / self.Nz)

        self.ff.y = array_y
        self.ff.conf_y_axis = 'Chebyshev'

        coef_cos = 3
        coef_sin = 2

        for nx in range(self.ff.u.shape[1]):
            for ny in range(self.ff.u.shape[2]):
                for nz in range(self.ff.u.shape[3]):
                    for i in range(self.ff.u.shape[0]):
                        self.ff.u[i, nx, ny, nz] = 0 * i \
                            + 1 * np.sin(2 * np.pi * 4 * array_x[nx] / Lx) \
                            + 1 * np.cos(2 * np.pi * 4 * array_y[ny] / (b - a)) \
                            + 1 * np.cos(2 * np.pi * coef_cos * array_z[nz] / Lz) * \
                            np.sin(2 * np.pi * coef_sin * array_z[nz] / Lz)
                    self.ff.p[nx, ny, nz] = 0 * i \
                        + 3 * np.sin(2 * np.pi * 4 * array_x[nx] / Lx) \
                        + 3 * np.power(np.cos(2 * np.pi * 4 * array_y[ny] / (b - a)), 1) \
                        + 3 * np.cos(2 * np.pi * coef_cos * array_z[nz] / Lz) * \
                        np.sin(2 * np.pi * coef_sin * array_z[nz] / Lz)

        # Saving dataset into independant variables for later comparison
        u_stored = np.ndarray((self.ff.u.shape[0],
                               self.ff.u.shape[1],
                               self.ff.u.shape[2],
                               self.ff.u.shape[3]))
        p_stored = np.ndarray((self.ff.p.shape[0],
                               self.ff.p.shape[1],
                               self.ff.p.shape[2]))
        u_stored[:, :, :, :] = self.ff.u[:, :, :, :]
        p_stored[:, :, :] = self.ff.p[:, :, :]

        # DERIVATIVE X 1st order
        diff_x = self.ff.dop.deriv_u(1, 0, 0)
        np.testing.assert_almost_equal(
            diff_x[0, :, 0, 0],
            1 * 2 * np.pi / Lx * 4 *
            np.cos(2 * np.pi / Lx * 4 *
                np.arange(
                    0, Lx, Lx / self.Nx)),
            err_msg='Derivative 1st X for dataset u is wrong')

        diff_px = self.ff.dop.deriv_p(1, 0, 0)
        np.testing.assert_almost_equal(
            diff_px[:, 0, 0],
            3 * 2 * np.pi / Lx * 4 *
            np.cos(2 * np.pi / Lx * 4 *
                np.arange(0, Lx, Lx / self.Nx)),
            err_msg='Derivative 1st X for dataset p is wrong')

        # DERIVATIVE Y 1st order
        # THIS ASSERTION IS DEPENDANT OF THE DEGREE
        # USED FOR THE SPECTRAL INTERPOLATION
        diff_y = self.ff.dop.deriv_u(0, 1, 0)
        np.testing.assert_almost_equal(
            diff_y[1, 0, :, 0],
            2 * np.pi / (b - a) * -4 * np.sin(2 * np.pi / (b - a) * 4 * array_y[:]),
            decimal=5,
            err_msg='Derivative 1st Y is wrong')

        # DERIVATIVE Z 1st order
        diff_z = self.ff.dop.deriv_u(0, 0, 1)
        np.testing.assert_almost_equal(
            diff_z[2, 0, 0, :],
            1 * np.pi / Lz *
            ((coef_cos + coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(0, Lz, Lz / self.Nz))
             - (coef_cos - coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz))),
            err_msg='Derivative 1st Z is wrong')

        diff_pz = self.ff.dop.deriv_p(0, 0, 1)
        np.testing.assert_almost_equal(
            diff_pz[0, 0, :],
            3 * np.pi / Lz *
            ((coef_cos + coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(0, Lz, Lz / self.Nz))
             - (coef_cos - coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz))),
            err_msg='Derivative 1st Z is wrong')

        # DERIVATIVE Y 4th order
        #diff_y_4 = self.ff.dop.deriv_u(0,4,0)
        # np.testing.assert_almost_equal(
        #    diff_y_4[1,0,:,0],
        #    (2*2*np.pi/Ly)**4 * self.ff.u[0,0,:,0],
        #    err_msg = 'Derivative 4th Y is wrong')

        # Check if dataset have not been modified
        np.testing.assert_almost_equal(
            self.ff.u[:, :, :, :],
            u_stored[:, :, :, :],
            err_msg='After differentiation, dataset u is no more as original')

        np.testing.assert_almost_equal(
            self.ff.p[:, :, :],
            p_stored[:, :, :],
            err_msg='After differentiation, dataset p is no more as original')

    def test_deriv(self):
        self.ff.uniform2chebyshev()

        deriv_ux = self.ff.dop.deriv_u(1, 0, 0)
        deriv_uy = self.ff.dop.deriv_u(0, 1, 0)
        deriv_uz = self.ff.dop.deriv_u(0, 0, 1)
        deriv_px = self.ff.dop.deriv_p(1, 0, 0)
        deriv_py = self.ff.dop.deriv_p(0, 1, 0)
        deriv_pz = self.ff.dop.deriv_p(0, 0, 1)

        deriv_dat_ux = self.ff.dop.deriv(self.ff.u, 1, 0, 0)
        deriv_dat_uy = self.ff.dop.deriv(self.ff.u, 0, 1, 0)
        deriv_dat_uz = self.ff.dop.deriv(self.ff.u, 0, 0, 1)
        deriv_dat_px = self.ff.dop.deriv(self.ff.p, 1, 0, 0)
        deriv_dat_py = self.ff.dop.deriv(self.ff.p, 0, 1, 0)
        deriv_dat_pz = self.ff.dop.deriv(self.ff.p, 0, 0, 1)

        np.testing.assert_almost_equal(
            deriv_ux,
            deriv_dat_ux,
            err_msg="deriv_u('x') != deriv(self.ff.u, 1,0,0)")
        np.testing.assert_almost_equal(
            deriv_uy,
            deriv_dat_uy,
            err_msg="deriv_u('y') != deriv(self.ff.u, 0,1,0)")
        np.testing.assert_almost_equal(
            deriv_uz,
            deriv_dat_uz,
            err_msg="deriv_u('z') != deriv(self.ff.u, 0,0,1)")

        np.testing.assert_almost_equal(
            deriv_px,
            deriv_dat_px,
            err_msg="deriv_p('x') != deriv(self.ff.p, 1,0,0)")
        np.testing.assert_almost_equal(
            deriv_py,
            deriv_dat_py,
            err_msg="deriv_p('y') != deriv(self.ff.p, 0,1,0)")
        np.testing.assert_almost_equal(
            deriv_pz,
            deriv_dat_pz,
            err_msg="deriv_p('z') != deriv(self.ff.p, 0,0,1)")

    def test_deriv_cross_partial(self):
        filename_bis = 'database/test_files/channel_sample_bis.h5'
        file_bis = h5py.File(filename_bis, 'w')
        data = file_bis.create_group('data')
        geom = file_bis.create_group('geom')

        file_bis.attrs['Lx'] = 1 * pi
        file_bis.attrs['a'] = -1
        file_bis.attrs['b'] = 1
        file_bis.attrs['Lz'] = 0.5 * pi

        Lx = file_bis.attrs['Lx']
        a = file_bis.attrs['a']
        b = file_bis.attrs['b']
        Lz = file_bis.attrs['Lz']

        file_bis.attrs['Nx'] = 12
        file_bis.attrs['Ny'] = 13
        file_bis.attrs['Nz'] = 15
        self.Nx = file_bis.attrs['Nx']
        self.Ny = file_bis.attrs['Ny']
        self.Nz = file_bis.attrs['Nz']

        # Creation of a basis (x, y, z) of dim (Nx, Ny, Nz)
        array_x = np.arange(0, Lx, Lx / self.Nx)
        array_y = np.linspace(a, b, num=self.Ny, endpoint=True)
        array_z = np.arange(0, Lz, Lz / self.Nz)

        x = geom.create_dataset('x', data=array_x)
        y = geom.create_dataset('y', data=array_y)
        z = geom.create_dataset('z', data=array_z)

        # Creation of a dataset u(3,x,y,z)
        array_u = np.ndarray(shape=(3, self.Nx, self.Ny, self.Nz))
        for i in range(array_u.shape[0]):
            for nx in range(array_u.shape[1]):
                for ny in range(array_u.shape[2]):
                    for nz in range(array_u.shape[3]):
                        array_u[i, nx, ny, nz] = \
                            1 * np.sin(2 * np.pi * 1 * array_x[nx] / Lx) \
                            * 1 * np.cos(2 * np.pi * 1 * array_y[ny] / (b - a)) \
                            * 1 * np.cos(2 * np.pi * 1 * array_z[nz] / Lz)

        u = data.create_dataset('u', data=array_u)

        # Creation of a dataset u(3,x,y,z)
        array_p = np.ndarray(shape=(self.Nx, self.Ny, self.Nz))
        for nx in range(array_p.shape[0]):
            for ny in range(array_p.shape[1]):
                for nz in range(array_p.shape[2]):
                    array_p[nx, ny, nz] = \
                        1 * np.sin(2 * np.pi * 1 * array_x[nx] / Lx) \
                        * 1 * np.cos(2 * np.pi * 1 * array_y[ny] / (b - a)) \
                        * 1 * np.cos(2 * np.pi * 1 * array_z[nz] / Lz)

        p = data.create_dataset('p', data=array_p)

        # load the new flowfield and test it
        self.ff.load(filename_bis)

        # TEST
        self.ff.uniform2chebyshev()
        x, y, z = 4, 4, 4
        plt.figure()
        deriv = self.ff.dop.deriv(self.ff.p, x, y, z)
        deriv_p = self.ff.dop.deriv_p(x, y, z)

        plt.plot(self.ff.x, deriv[:, 5, 6], 'b-o')
        plt.plot(self.ff.y, deriv[5, :, 6], 'r-o')
        plt.plot(self.ff.z, deriv[5, 6, :], 'g-o')
        plt.plot(self.ff.x, deriv_p[:, 5, 6], 'b--o')
        plt.plot(self.ff.y, deriv_p[5, :, 6], 'r--o')
        plt.plot(self.ff.z, deriv_p[5, 6, :], 'g--o')
        plt.savefig('figures/test_channel_deriv_cross_partial.png',
                    bbox_inches='tight')
        plt.show()

    def test_div_u(self):
        """Test of method div for different order."""
        b = self.ff.attributes['b']
        a = self.ff.attributes['a']
        Lx = self.ff.attributes['Lx']
        Lz = self.ff.attributes['Lz']

        #diff_x = self.ff.dop.deriv_u(1,0,0)
        #print('--- DIFF X ---\n', np.round(diff_x[0,:,:,:], 2).real)
        #diff_y = self.ff.dop.deriv_u(0,1,0)
        #print('--- DIFF Y ---\n', np.round(diff_y[1,:,:,:], 2).real)
        #diff_z = self.ff.dop.deriv_u(0,0,1)
        #print('--- DIFF Z ---\n', np.round(diff_z[2,:,:,:], 2).real)

        coef_cos = 3
        coef_sin = 2

        # TEST DIVERGENCE ORDER 1
        self.ff.uniform2chebyshev()
        div = self.ff.dop.div_u()
        #print('--- DIV --- \n', np.round(div[:,:,:],3).real)

        # building analytical solution
        sol_x = 2 * np.pi / Lx * 4 * \
            np.cos(2 * np.pi / Lx * 4 * np.arange(0, Lx, Lx / self.Nx)[:])
        sol_y = 2 * np.pi / (b - a) * -2 * np.sin(2 * \
                             np.pi / (b - a) * 2 * self.ff.y[:])
        sol_z = 1 * np.pi / Lz * ((coef_cos + coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(0, Lz, Lz / self.Nz)[
                                  :]) - (coef_cos - coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz)[:]))

        sol_x = 1 * sol_x[:, np.newaxis, np.newaxis]
        sol_y = 1 * sol_y[np.newaxis, :, np.newaxis]
        sol_z = 1 * sol_z[np.newaxis, np.newaxis, :]
        array_solution = sol_x + sol_y + sol_z
        #print('--- SOL --- \n', np.round(array_solution[:,:,:],3).real)

        np.testing.assert_almost_equal(
            div[:, :, :],
            array_solution[:, :, :],
            decimal=5,
            err_msg='Real part of divergence is wrong.')

        np.testing.assert_almost_equal(
            div[:, :, :].imag,
            np.zeros((self.Nx, self.Ny, self.Nz)),
            err_msg='Imaginary part of divergence is not zero.')

        # TEST DIVERGENCE ORDER 2
        div_2 = self.ff.dop.div_u(2)

        # building analytical solution
        array_x_2 = -(2 * np.pi / Lx * 4)**2 * np.sin(2 * np.pi /
                                                      Lx * 4 * np.arange(0, Lx, Lx / self.Nx)[:])
        array_y_2 = -(4 * np.pi / (b - a))**2 * \
            np.cos(2 * np.pi / (b - a) * 2 * self.ff.y[:])
        array_z_2 = -0.5 * (2 * np.pi / Lz)**2 * ((coef_cos + coef_sin)**2 * np.sin(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(
            0, Lz, Lz / self.Nz)[:]) - (coef_cos - coef_sin)**2 * np.sin(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz)[:]))

        #array_x_2 = -(2*np.pi/Lx*4)**2 * np.sin(2*np.pi/Lx * 4*np.arange(0, Lx, Lx/self.Nx)[:])
        #array_y_2 = -(2*np.pi/(b-a)*2)**2 * np.cos(2*np.pi/(b-a) * 2*np.linspace(a, b, num = self.Ny, endpoint=True))
        #array_z_2 = -0.5 * (2*np.pi/Lz)**2 * (  (coef_cos+coef_sin)**2 * np.sin(2*np.pi/Lz*(coef_cos+coef_sin)*np.arange(0,Lz, Lz/self.Nz)[:])  -  (coef_cos-coef_sin)**2 * np.sin(2*np.pi/Lz*(coef_cos-coef_sin)*np.arange(0,Lz,Lz/self.Nz)[:])  )

        array_x_2 = 1 * array_x_2[:, np.newaxis, np.newaxis]
        array_y_2 = 1 * array_y_2[np.newaxis, :, np.newaxis]
        array_z_2 = 1 * array_z_2[np.newaxis, np.newaxis, :]
        array_solution_2 = array_x_2 + array_y_2 + array_z_2

        np.testing.assert_almost_equal(
            div_2[:, :, :],
            array_solution_2[:, :, :],
            decimal=5,
            err_msg='Real part of divergence order 2 is wrong.')

        np.testing.assert_almost_equal(
            div_2[:, :, :].imag,
            np.zeros((self.Nx, self.Ny, self.Nz)),
            err_msg='Imaginary part of divergence order 2 is not zero.')

    def test_grad_u_order1(self):
        # Definition of parameters.
        b = self.ff.attributes['b']
        a = self.ff.attributes['a']
        Lx = self.ff.attributes['Lx']
        Lz = self.ff.attributes['Lz']

        coef_cos = 3
        coef_sin = 2

        # Calculation of gradient order 1
        self.ff.uniform2chebyshev()
        gradient = self.ff.dop.grad_u(1)

        # Calculation of the expected results for order 1
        array_x = 2 * np.pi * 4 / Lx * \
            np.cos(2 * np.pi * 4 * np.arange(0, Lx, Lx / self.Nx)[:] / Lx)
        array_y = -2 * np.pi / (b - a) * 2 * \
            np.sin(2 * np.pi * 2 * self.ff.y[:] / (b - a))
        array_z = 1 * np.pi / Lz * ((coef_cos + coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(0, Lz, Lz / self.Nz)[
                                    :]) - (coef_cos - coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz)[:]))

        # Comparison

        # gradient( dx/dy/dz, ux/uy/uz, x, y, z)
        # ux = uy = uz

        # shape
        np.testing.assert_almost_equal(
            gradient.shape,
            (3, 3, self.Nx, self.Ny, self.Nz),
            err_msg='Shape is wrong.')

        # dux/dx
        np.testing.assert_almost_equal(
            gradient[0, 0, :, 0, 0],
            array_x[:],
            err_msg='dux/dx is wrong.')
        # duy/dx
        np.testing.assert_almost_equal(
            gradient[0, 1, :, 0, 0],
            array_x[:],
            err_msg='duy/dx is wrong.')
        # duz/dx
        np.testing.assert_almost_equal(
            gradient[0, 2, :, 0, 0],
            array_x[:],
            err_msg='duz/dx is wrong.')
        # /dx is constant in y and z direction
        np.testing.assert_almost_equal(
            gradient[0, 2, 0, :, 0],
            array_x[0],
            err_msg='d./dx is not constant in y direction.')
        np.testing.assert_almost_equal(
            gradient[0, 2, 0, 0, :],
            array_x[0],
            err_msg='d./dx is not constant in z direction.')

        # dux/dy
        """
        plt.figure()
        plt.plot(np.linspace(a, b, num = self.Ny, endpoint=True), gradient[1,0,0,:,0].real)
        plt.plot(np.linspace(a, b, num = self.Ny, endpoint=True), array_y[:])
        plt.ylim([-100,100])
        plt.show()
        """

        np.testing.assert_almost_equal(
            gradient[1, 0, 0, :, 0],
            array_y[:],
            err_msg='dux/dy is wrong.')
        # duy/dy
        np.testing.assert_almost_equal(
            gradient[1, 1, 0, :, 0],
            array_y[:],
            err_msg='duy/dy is wrong.')
        # duz/dy
        np.testing.assert_almost_equal(
            gradient[1, 2, 0, :, 0],
            array_y[:],
            err_msg='duz/dy is wrong.')
        # /dy is constant in x and z direction
        np.testing.assert_almost_equal(
            gradient[1, 2, :, 0, 0],
            array_y[0],
            err_msg='d./dy is not constant in x direction.')
        np.testing.assert_almost_equal(
            gradient[1, 2, 0, 0, :],
            array_y[0],
            err_msg='d./dy is not constant in z direction.')

        # dux/dz
        np.testing.assert_almost_equal(
            gradient[2, 0, 0, 0, :],
            array_z[:],
            err_msg='dux/dz is wrong.')
        # duy/dz
        np.testing.assert_almost_equal(
            gradient[2, 1, 0, 0, :],
            array_z[:],
            err_msg='duy/dz is wrong.')
        # duz/dz
        np.testing.assert_almost_equal(
            gradient[2, 2, 0, 0, :],
            array_z[:],
            err_msg='duz/dz is wrong.')
        # /dz is constant in x and y direction
        np.testing.assert_almost_equal(
            gradient[2, 2, :, 0, 0],
            array_z[0],
            err_msg='d./dz is not constant in x direction.')
        np.testing.assert_almost_equal(
            gradient[2, 2, :, 0, 0],
            array_z[0],
            err_msg='d./dz is not constant in y direction.')

    def test_grad_u_order2(self):
        # TEST SUCCESSFULLY WITH : Ny = 100, degree_interp = 50

        # Definition of parameters.
        b = self.ff.attributes['b']
        a = self.ff.attributes['a']
        Lx = self.ff.attributes['Lx']
        Lz = self.ff.attributes['Lz']

        coef_cos = 3
        coef_sin = 2

        # Calculation of gradient order 2
        self.ff.uniform2chebyshev()
        gradient = self.ff.dop.grad_u(2)

        # Calculation of the expected results for order 2
        array_x = -(2 * np.pi / Lx * 4)**2 * np.sin(2 * np.pi /
                                                    Lx * 4 * np.arange(0, Lx, Lx / self.Nx)[:])
        array_y = -(4 * np.pi / (b - a))**2 * \
            np.cos(2 * np.pi / (b - a) * 2 * self.ff.y[:])
        array_z = -0.5 * (2 * np.pi / Lz)**2 * ((coef_cos + coef_sin)**2 * np.sin(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(
            0, Lz, Lz / self.Nz)[:]) - (coef_cos - coef_sin)**2 * np.sin(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz)[:]))

        # Comparison

        # gradient( dx/dy/dz, ux/uy/uz, x, y, z)
        # ux = uy = uz

        # shape
        np.testing.assert_almost_equal(
            gradient.shape,
            (3, 3, self.Nx, self.Ny, self.Nz),
            err_msg='Shape is wrong.')

        # dux/dx
        np.testing.assert_almost_equal(
            gradient[0, 0, :, 0, 0],
            array_x[:],
            err_msg='dux/dx is wrong.')
        # duy/dx
        np.testing.assert_almost_equal(
            gradient[0, 1, :, 0, 0],
            array_x[:],
            err_msg='duy/dx is wrong.')
        # duz/dx
        np.testing.assert_almost_equal(
            gradient[0, 2, :, 0, 0],
            array_x[:],
            err_msg='duz/dx is wrong.')
        # /dx is constant in y and z direction
        np.testing.assert_almost_equal(
            gradient[0, 2, 0, :, 0],
            array_x[0],
            err_msg='d./dx is not constant in y direction.')
        np.testing.assert_almost_equal(
            gradient[0, 2, 0, 0, :],
            array_x[0],
            err_msg='d./dx is not constant in z direction.')

        # dux/dy
        """
        plt.figure()
        plt.plot(np.linspace(a, b, num = self.Ny, endpoint=True), gradient[1,0,0,:,0].real)
        plt.plot(np.linspace(a, b, num = self.Ny, endpoint=True), array_y[:])
        #plt.ylim([-100,100])
        plt.show()
        """

        np.testing.assert_almost_equal(
            gradient[1, 0, 0, :, 0],
            array_y[:],
            decimal=5,
            err_msg='dux/dy is wrong.')
        # duy/dy
        np.testing.assert_almost_equal(
            gradient[1, 1, 0, :, 0],
            array_y[:],
            decimal=5,
            err_msg='duy/dy is wrong.')
        # duz/dy
        np.testing.assert_almost_equal(
            gradient[1, 2, 0, :, 0],
            array_y[:],
            decimal=5,
            err_msg='duz/dy is wrong.')
        # /dy is constant in x and z direction
        np.testing.assert_almost_equal(
            gradient[1, 2, :, 0, 0],
            array_y[0],
            decimal=5,
            err_msg='d./dy is not constant in x direction.')
        np.testing.assert_almost_equal(
            gradient[1, 2, 0, 0, :],
            array_y[0],
            decimal=5,
            err_msg='d./dy is not constant in z direction.')

        # dux/dz
        np.testing.assert_almost_equal(
            gradient[2, 0, 0, 0, :],
            array_z[:],
            err_msg='dux/dz is wrong.')
        # duy/dz
        np.testing.assert_almost_equal(
            gradient[2, 1, 0, 0, :],
            array_z[:],
            err_msg='duy/dz is wrong.')
        # duz/dz
        np.testing.assert_almost_equal(
            gradient[2, 2, 0, 0, :],
            array_z[:],
            err_msg='duz/dz is wrong.')
        # /dz is constant in x and y direction
        np.testing.assert_almost_equal(
            gradient[2, 2, :, 0, 0],
            array_z[0],
            err_msg='d./dz is not constant in x direction.')
        np.testing.assert_almost_equal(
            gradient[2, 2, :, 0, 0],
            array_z[0],
            err_msg='d./dz is not constant in y direction.')

    def test_grad_u_order4(self):
        # TEST SUCCESSFULLY WITH : Ny = 100, degree_interp = 50

        # Definition of parameters.
        b = self.ff.attributes['b']
        a = self.ff.attributes['a']
        Lx = self.ff.attributes['Lx']
        Lz = self.ff.attributes['Lz']

        coef_cos = 3
        coef_sin = 2

        # Calculation of gradient order 4
        self.ff.uniform2chebyshev()
        gradient = self.ff.dop.grad_u(4)

        # Calculation of the expected results for order 4
        array_x = (2 * np.pi / Lx * 4)**4 * np.sin(2 * np.pi /
                                                   Lx * 4 * np.arange(0, Lx, Lx / self.Nx)[:])
        array_y = (4 * np.pi / (b - a))**4 * \
            np.cos(2 * np.pi / (b - a) * 2 * self.ff.y[:])
        array_z = +0.5 * (2 * np.pi / Lz)**4 * ((coef_cos + coef_sin)**4 * np.sin(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(
            0, Lz, Lz / self.Nz)[:]) - (coef_cos - coef_sin)**4 * np.sin(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz)[:]))

        # Comparison

        # gradient( dx/dy/dz, ux/uy/uz, x, y, z)
        # ux = uy = uz

        # shape
        np.testing.assert_almost_equal(
            gradient.shape,
            (3, 3, self.Nx, self.Ny, self.Nz),
            err_msg='Shape is wrong.')

        # dux/dx
        np.testing.assert_almost_equal(
            gradient[0, 0, :, 0, 0],
            array_x[:],
            err_msg='dux/dx is wrong.')
        # duy/dx
        np.testing.assert_almost_equal(
            gradient[0, 1, :, 0, 0],
            array_x[:],
            err_msg='duy/dx is wrong.')
        # duz/dx
        np.testing.assert_almost_equal(
            gradient[0, 2, :, 0, 0],
            array_x[:],
            err_msg='duz/dx is wrong.')
        # /dx is constant in y and z direction
        np.testing.assert_almost_equal(
            gradient[0, 2, 0, :, 0],
            array_x[0],
            err_msg='d./dx is not constant in y direction.')
        np.testing.assert_almost_equal(
            gradient[0, 2, 0, 0, :],
            array_x[0],
            err_msg='d./dx is not constant in z direction.')

        # dux/dy
        """
        plt.figure()
        plt.plot(np.linspace(a, b, num = self.Ny, endpoint=True), gradient[1,0,0,:,0].real)
        plt.plot(np.linspace(a, b, num = self.Ny, endpoint=True), array_y[:])
        #plt.ylim([-100,100])
        plt.show()
        """

        np.testing.assert_almost_equal(
            gradient[1, 0, 0, :, 0],
            array_y[:],
            decimal=1,
            err_msg='dux/dy is wrong.')
        # duy/dy
        np.testing.assert_almost_equal(
            gradient[1, 1, 0, :, 0],
            array_y[:],
            decimal=1,
            err_msg='duy/dy is wrong.')
        # duz/dy
        np.testing.assert_almost_equal(
            gradient[1, 2, 0, :, 0],
            array_y[:],
            decimal=1,
            err_msg='duz/dy is wrong.')
        # /dy is constant in x and z direction
        np.testing.assert_almost_equal(
            gradient[1, 2, :, 0, 0],
            array_y[0],
            decimal=1,
            err_msg='d./dy is not constant in x direction.')
        np.testing.assert_almost_equal(
            gradient[1, 2, 0, 0, :],
            array_y[0],
            decimal=1,
            err_msg='d./dy is not constant in z direction.')

        # dux/dz
        np.testing.assert_almost_equal(
            gradient[2, 0, 0, 0, :],
            array_z[:],
            err_msg='dux/dz is wrong.')
        # duy/dz
        np.testing.assert_almost_equal(
            gradient[2, 1, 0, 0, :],
            array_z[:],
            err_msg='duy/dz is wrong.')
        # duz/dz
        np.testing.assert_almost_equal(
            gradient[2, 2, 0, 0, :],
            array_z[:],
            err_msg='duz/dz is wrong.')
        # /dz is constant in x and y direction
        np.testing.assert_almost_equal(
            gradient[2, 2, :, 0, 0],
            array_z[0],
            err_msg='d./dz is not constant in x direction.')
        np.testing.assert_almost_equal(
            gradient[2, 2, :, 0, 0],
            array_z[0],
            err_msg='d./dz is not constant in y direction.')

    def test_grad_p_order1(self):
        # Definition of parameters.
        b = self.ff.attributes['b']
        a = self.ff.attributes['a']
        Lx = self.ff.attributes['Lx']
        Lz = self.ff.attributes['Lz']
        coef_cos = 3
        coef_sin = 2

        # Calculation of gradient order 1
        self.ff.uniform2chebyshev()
        gradient = self.ff.dop.grad_p(1)

        # Calculation of the expected results
        array_x = 2 * np.pi * 4 / Lx * \
            np.cos(2 * np.pi * 4 * np.arange(0, Lx, Lx / self.Nx)[:] / Lx)
        array_y = -2 * np.pi / (b - a) * 4 * \
            np.sin(2 * np.pi / (b - a) * 4 * self.ff.y[:])
        array_z = 1 * np.pi / Lz * ((coef_cos + coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(0, Lz, Lz / self.Nz)[
                                    :]) - (coef_cos - coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz)[:]))

        # Comparison
        # gradient( dx/dy/dz, x, y, z)
        # shape
        np.testing.assert_almost_equal(
            gradient.shape,
            (3, 1, self.Nx, self.Ny, self.Nz),
            decimal=1,
            err_msg='Shape is wrong.')

        # dp/dx
        np.testing.assert_almost_equal(
            gradient[0, 0, :, 0, 0],
            array_x[:],
            decimal=1,
            err_msg='dp/dx is wrong.')

        # dp/dy
        np.testing.assert_almost_equal(
            gradient[1, 0, 0, :, 0],
            array_y[:],
            decimal=1,
            err_msg='dp/dy is wrong.')

        # dp/dz
        np.testing.assert_almost_equal(
            gradient[2, 0, 0, 0, :],
            array_z[:],
            decimal=1,
            err_msg='dp/dz is wrong.')

    def test_grad_p_order2(self):
        # Definition of parameters.
        b = self.ff.attributes['b']
        a = self.ff.attributes['a']
        Lx = self.ff.attributes['Lx']
        Lz = self.ff.attributes['Lz']
        coef_cos = 3
        coef_sin = 2

        # Calculation of gradient order 2
        self.ff.uniform2chebyshev()
        gradient = self.ff.dop.grad_p(2)

        # Calculation of the expected results
        array_x = -(2 * np.pi / Lx * 4)**2 * np.sin(2 * np.pi /
                                                    Lx * 4 * np.arange(0, Lx, Lx / self.Nx)[:])
        array_y = -(8 * np.pi / (b - a))**2 * \
            np.cos(2 * np.pi / (b - a) * 4 * self.ff.y[:])
        array_z = -0.5 * (2 * np.pi / Lz)**2 * ((coef_cos + coef_sin)**2 * np.sin(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(
            0, Lz, Lz / self.Nz)[:]) - (coef_cos - coef_sin)**2 * np.sin(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz)[:]))

        # Comparison
        # gradient( dx/dy/dz, x, y, z)
        # shape
        np.testing.assert_almost_equal(
            gradient.shape,
            (3, 1, self.Nx, self.Ny, self.Nz),
            decimal=1,
            err_msg='Shape is wrong.')

        # dp/dx
        np.testing.assert_almost_equal(
            gradient[0, 0, :, 0, 0],
            array_x[:],
            decimal=1,
            err_msg='dp/dx is wrong.')

        # dp/dy
        np.testing.assert_almost_equal(
            gradient[1, 0, 0, :, 0],
            array_y[:],
            decimal=1,
            err_msg='dp/dy is wrong.')

        # dp/dz
        np.testing.assert_almost_equal(
            gradient[2, 0, 0, 0, :],
            array_z[:],
            decimal=1,
            err_msg='dp/dz is wrong.')

    def test_grad_p_order4(self):
        # Definition of parameters.
        b = self.ff.attributes['b']
        a = self.ff.attributes['a']
        Lx = self.ff.attributes['Lx']
        Lz = self.ff.attributes['Lz']
        coef_cos = 3
        coef_sin = 2

        # Calculation of gradient order 4
        self.ff.uniform2chebyshev()
        gradient = self.ff.dop.grad_p(4)

        # Calculation of the expected results
        array_x = (2 * np.pi / Lx * 4)**4 * np.sin(2 * np.pi /
                                                   Lx * 4 * np.arange(0, Lx, Lx / self.Nx)[:])
        array_y = (8 * np.pi / (b - a))**4 * \
            np.cos(2 * np.pi / (b - a) * 4 * self.ff.y[:])
        array_z = +0.5 * (2 * np.pi / Lz)**4 * ((coef_cos + coef_sin)**4 * np.sin(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(
            0, Lz, Lz / self.Nz)[:]) - (coef_cos - coef_sin)**4 * np.sin(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz)[:]))

        # Comparison
        # gradient( dx/dy/dz, x, y, z)
        # shape
        np.testing.assert_almost_equal(
            gradient.shape,
            (3, 1, self.Nx, self.Ny, self.Nz),
            decimal=1,
            err_msg='Shape is wrong.')

        # dp/dx
        np.testing.assert_almost_equal(
            gradient[0, 0, :, 0, 0],
            array_x[:],
            decimal=1,
            err_msg='dp/dx is wrong.')

        # dp/dy
        np.testing.assert_almost_equal(
            gradient[1, 0, 0, :, 0],
            array_y[:],
            decimal=1,
            err_msg='dp/dy is wrong.')

        # dp/dz
        np.testing.assert_almost_equal(
            gradient[2, 0, 0, 0, :],
            array_z[:],
            decimal=1,
            err_msg='dp/dz is wrong.')

    def test_curl_u(self):
        # Definition of parameters.
        b = self.ff.attributes['b']
        a = self.ff.attributes['a']
        Lx = self.ff.attributes['Lx']
        Lz = self.ff.attributes['Lz']
        coef_cos = 3
        coef_sin = 2

        # Calculation of curl
        self.ff.uniform2chebyshev()
        curl = self.ff.dop.curl_u()

        # Calculation of the expected results
        array_x = 2 * np.pi * 4 / Lx * \
            np.cos(2 * np.pi * 4 * np.arange(0, Lx, Lx / self.Nx)[:] / Lx)
        array_y = -2 * np.pi / (b - a) * 2 * \
            np.sin(2 * np.pi * 2 * self.ff.y[:] / (b - a))
        array_z = 1 * np.pi / Lz * ((coef_cos + coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(0, Lz, Lz / self.Nz)[
                                    :]) - (coef_cos - coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz)[:]))

        # Comparison

        # shape
        np.testing.assert_almost_equal(
            curl.shape,
            (3, self.Nx, self.Ny, self.Nz),
            err_msg='shape is wrong.')

        np.testing.assert_almost_equal(
            curl[0, 0, :, :],
            array_y[:, np.newaxis] - array_z[np.newaxis, :],
            err_msg='First component is wrong.')

        np.testing.assert_almost_equal(
            curl[1, :, 0, :],
            array_z[np.newaxis, :] - array_x[:, np.newaxis],
            err_msg='Second component is wrong.')

        np.testing.assert_almost_equal(
            curl[2, :, :, 0],
            array_x[:, np.newaxis] - array_y[np.newaxis, :],
            err_msg='Third component is wrong.')

    def test_laplacian_u(self):
        self.ff.uniform2chebyshev()
        laplacian = self.ff.dop.laplacian_u()

        np.testing.assert_almost_equal(
            laplacian[0, :, :, :],
            self.ff.dop.div_u(2),
            decimal=2,
            err_msg='X-component of Laplacian is wrong.')

        np.testing.assert_almost_equal(
            laplacian[1, :, :, :],
            self.ff.dop.div_u(2),
            decimal=2,
            err_msg='Y-component of Laplacian is wrong.')

        np.testing.assert_almost_equal(
            laplacian[2, :, :, :],
            self.ff.dop.div_u(2),
            decimal=2,
            err_msg='Z-component of Laplacian is wrong.')

        pass

    def test_generate_stress_tensor(self):
        array_u = np.ndarray(shape=(3, self.Nx, self.Ny, self.Nz))
        for i in range(array_u.shape[0]):
            for nx in range(array_u.shape[1]):
                for ny in range(array_u.shape[2]):
                    for nz in range(array_u.shape[3]):
                        array_u[i, nx, ny, nz] = 1 * \
                            i + 2 * nx + 3 * ny + 4 * nz
        self.ff.u = array_u
        self.ff.generate_stress_tensor()

        np.testing.assert_almost_equal(
            self.ff.uu[0, 0],
            array_u[0, :, :, :] * array_u[0, :, :, :],
            err_msg='ux.ux component of stress tensor is wrong.')

        np.testing.assert_almost_equal(
            self.ff.uu[0, 1],
            array_u[0, :, :, :] * array_u[1, :, :, :],
            err_msg='ux.uy component of stress tensor is wrong.')

        np.testing.assert_almost_equal(
            self.ff.uu[0, 2],
            array_u[0, :, :, :] * array_u[2, :, :, :],
            err_msg='ux.uz component of stress tensor is wrong.')

        np.testing.assert_almost_equal(
            self.ff.uu[1, 1],
            array_u[1, :, :, :] * array_u[1, :, :, :],
            err_msg='uy.uy component of stress tensor is wrong.')

        np.testing.assert_almost_equal(
            self.ff.uu[2, 2],
            array_u[2, :, :, :] * array_u[2, :, :, :],
            err_msg='uz.uz component of stress tensor is wrong.')

        np.testing.assert_almost_equal(
            self.ff.uu[1, 2],
            array_u[1, :, :, :] * array_u[2, :, :, :],
            err_msg='uy.uz component of stress tensor is wrong.')

    def test_grad_stress_tensor(self):
        array_u = np.ndarray(shape=(3, self.Nx, self.Ny, self.Nz))
        for i in range(array_u.shape[0]):
            for nx in range(array_u.shape[1]):
                for ny in range(array_u.shape[2]):
                    for nz in range(array_u.shape[3]):
                        array_u[i, nx, ny, nz] = 1 * \
                            i + 2 * nx + 3 * ny + 4 * nz
        self.ff.u = array_u

        # print(self.ff.u[0,:,:,:])
        # print(self.ff.uu[0,0,:,:,:])
        # print(self.ff.dop.grad_stress_tensor()[0,0,2,:,:,:])

    def test_attribute(self):
        file2 = h5py.File('database/test_files/test_attrs.h5', 'r')
        for a in file2.attrs:
            print(a)
        file2.close()
        print('---')
        ff3 = FlowField()
        ff3.load('database/test_files/test_attrs.h5')
        for a in ff3.attributes:
            print(a)
        print(' -> ', ff3.attributes['a'])

# TESTS SUCCESSFUL WITH : Ny = 100, degree_interp = 50

test = unittest.TestSuite()
# test.addTest(ChannelTest("test_init"))

test.addTest(ChannelTest("test_save_load"))
test.addTest(ChannelTest("test_copy"))

test.addTest(ChannelTest("test_interpolation"))
test.addTest(ChannelTest("test_interpolation_u_graphic"))
test.addTest(ChannelTest("test_interpolation_p_graphic"))
test.addTest(ChannelTest("test_dataset_chebyshev2uniform_u_graphic"))
test.addTest(ChannelTest("test_dataset_chebyshev2uniform_p_graphic"))

test.addTest(ChannelTest("test_deriv"))
test.addTest(ChannelTest("test_deriv_u_p"))
test.addTest(ChannelTest("test_deriv_u_graphic"))
test.addTest(ChannelTest("test_deriv_p_graphic"))
test.addTest(ChannelTest("test_deriv_cross_partial"))

test.addTest(ChannelTest("test_div_u"))
test.addTest(ChannelTest("test_grad_u_order1"))
test.addTest(ChannelTest("test_grad_u_order2"))
test.addTest(ChannelTest("test_grad_u_order4"))
test.addTest(ChannelTest("test_grad_p_order1"))
test.addTest(ChannelTest("test_grad_p_order2"))
test.addTest(ChannelTest("test_grad_p_order4"))

test.addTest(ChannelTest("test_curl_u"))
test.addTest(ChannelTest("test_laplacian_u"))

test.addTest(ChannelTest("test_generate_stress_tensor"))
test.addTest(ChannelTest("test_grad_stress_tensor"))

# test.addTest(ChannelTest("test_attribute"))
runner = unittest.TextTestRunner()
runner.run(test)
