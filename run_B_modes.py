"""
run_B_modes.py

transform a column of the matrix B, corresponding to a given
mode (kx, kz) for a given variable (v, eta, u00, w00)
into a flowfield and save it.

"""

import numpy as np
from channel import Channel
from operators import Operators as OP
import matplotlib.pyplot as plt
import chebyshev

##########
# PARAMETERS
##########
eq = 1
Re = 400

Nx = 20
Ny = 35
Nz = 20

tau = 10E-2
tau_v = tau
tau_eta = tau
tau_u = tau
tau_w = tau

# MODE OF MATRIX B
variable = 0 # v=0, eta=1, u=2, w=3
# dont forget, eta(0,0) does not exist and for u and w, only mode (0,0)
mode_set = {(2,0), (1,1), (0,0), (0,1), (0,2)}
#mode_set = {(0,2), (1,1), (2,0)}
wall_set = {'up', 'low'} # or 'up' or 'low'

act = True
if act:
    _act = "_act"
else:
    _act = ""

##########
# Loading the flowfield
N_str = str(Nx)+'x'+str(Ny)+'x'+str(Nz)
kind_of_flow = 'eq'+str(eq)+'_'+N_str

# BUILDING FLOWFIELD
ff = Channel('database/eq/'+kind_of_flow+'.h5')
ff.load_baseflow('database/eq/'+kind_of_flow+'.h5')
Mx = ff.Mx
Mz = ff.Mz

# add Couette baseflow
for ny in range(ff.u.shape[2]):
    ff.u0[0, :, ny, :] += ff.y[ny]

# BUILDING OPERATOR
op = OP(ff)
E, A, B = op.op_nonlaminar_baseflow_actuated(ff,
                                             Re,
                                             tau_v,
                                             tau_eta,
                                             tau_u,
                                             tau_w)
B = np.dot(E, B)

for kx, kz in mode_set:
    for wall in wall_set:
        print("\n********************\n B_mode, Variable: ", variable, \
                ", Couple (", kx, ", ", kz, "), wall: ", wall, "\n********************")
        if wall == 'up':
            _wall = 0
        elif wall == 'low':
            _wall = 1

        ##########
        # EXTRACTION OF A GIVEN MODE OF B
        ##########
        if variable == 0:
            mode = (kx*Mz +  kz)*2 + _wall
            _variable = 'v'
        elif variable == 1:
            mode = (Mx*Mz + kx*Mz + kz - 1)*2 + _wall
            _variable = 'eta'
        elif variable == 2:
            mode = (Mx*Mz + Mx*Mz - 1)*2 + _wall
            _variable = 'u'
        elif variable == 3:
            mode = (Mx*Mz + Mx*Mz - 1 + 1)*2 + _wall
            _variable = 'w'

        vector = B[:, mode]

        ##########
        # SAVING HDF5 flowfield of B[mode]
        ##########
        filename_ff = "database/eigenfolder/eq"+str(eq)+"/B_act/B_"+_variable+'_'\
                    +str(kx)+'_'+str(kz)+"_"+wall+"_eq"+str(eq)+"_"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)+_act+".h5"
        op.transform_eigenvector_to_ff(ff, vector, filename_ff, act)

