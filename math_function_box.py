from math import *
import scipy
import scipy.linalg
import numpy as np

def left_null_space(M, eps=1e-15):
    """
    also called 'cokernel', is the ensemble of all vectors x such that
    x.H M = 0.H
    which gives:
    cokernel(M) = kernel(M.H).H

    M = [ U1 U2 ] [ S11 0 ] [ V1 ]
                  [  0  0 ] [ V2 ]
    with SVD decomposition

    left_null_space(M) = M_ = U2.H
    """
    return null_space(M.conj().T, eps).conj().T

def null_space2(M, eps=1e-15):
    """
    """
    # get the SVD decomposition
    u, s, vh = scipy.linalg.svd(M)

    #S = scipy.linalg.diagsvd(s, M.shape[0], M.shape[1])
    #print('u :\n', u)
    #print('s :\n', s)
    #print('S :\n', S)
    #print('vh :\n', vh)

    # get the near-zero eigenvalues
    null_mask = (s <= eps)
    # extract the corresponding eigenvectors
    null_space = scipy.compress(null_mask, vh, axis=0)

    return scipy.transpose(null_space)

def null_space(A, eps=1e-13, rtol=0):
    A = np.atleast_2d(A)
    u, s, vh = np.linalg.svd(A)
    tol = max(eps, rtol * s[0])
    nnz = (s >= tol).sum()
    ns = vh[nnz:].conj().T
    return ns
