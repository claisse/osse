"""
run_modal_controllabilty.py

This file is a method in order to test the modal controllability
of the actuated system for the extended OSS model
for a nonlaminar baseflow.

Firstly, we set-up the FlowField and the baseflow,
then calculate the eigenvalues for the actuated model,
and finally build the modal controllability matrix
for the biggest real-part eigenvalues.
"""
import numpy as np
from channel import Channel
from operators import Operators as OP
import matplotlib.pyplot as plt
import chebyshev

# Constant definition
eq = 1
filename = 'database/eigenfolder/eq'+str(eq)+'/EIGS_act/'
Re = 400
k = 20 # nb of eigenvalues to find
N_eigenvectors = 3 # nb of eigenvalues to use for modal control matrix

Nx = 4
Ny = 15
Nz = 4

tau_v = 10E-2
tau_eta = 10E-2
tau_u = 10E-2
tau_w = 10E-2

# Loading the flowfield
N_str = str(Nx)+'x'+str(Ny)+'x'+str(Nz)
kind_of_flow = 'eq'+str(eq)+'_'+N_str

# BUILDING FLOWFIELD
ff = Channel('database/eq/'+kind_of_flow+'.h5')
ff.load_baseflow('database/eq/'+kind_of_flow+'.h5')

# add Couette baseflow
for ny in range(ff.u.shape[2]):
    ff.u0[0, :, ny, :] += ff.y[ny]

# BUILDING OPERATOR
op = OP(ff)
E, A, B = op.op_nonlaminar_baseflow_actuated(ff=ff,
                                             Re=Re,
                                             lifting_function=None,
                                             tau_v=tau_v,
                                             tau_eta=tau_eta,
                                             tau_u=tau_u,
                                             tau_w=tau_w)

# EIGENVALUES CALCULATION
if True:
    eigs, left_vecs = op.eigen(
                    filename=filename+kind_of_flow,
                    EA=np.dot(E, A),
                    EB=np.dot(E, B),
                    left=True,
                    k=k)

# ... or directly load the eigenvalues
else:
    eigs = np.load(filename+kind_of_flow+'_EIG_act.npy')
    left_vecs = np.load(filename+kind_of_flow+'_VEC_left_act.npy')

WB = op.modal_controllability(ff,
                                 E, A, B,
                                 eigs, left_vecs,
                                 N=N_eigenvectors,
                                 controllability_measure=True)
                                 #graph='hinton')
