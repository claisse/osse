#!/usr/bin/env python3
"""
utility to copy attributes from one h5 file to another.
A Sharma 2016
"""
import sys
import h5py
from optparse import OptionParser

def main(argv):
    usage = "copy_attributes template.hd5 flowfield1.hd5 [flowfield2.hd5 [.....]]"
    parser=OptionParser(usage)
    (options, filenames) = parser.parse_args(sys.argv)
    template = h5py.File(filenames[1], 'r')
    for filename in filenames[2:]:
        dest = h5py.File(filename, 'a')
        for a in template.attrs:
            dest.attrs[a] = template.attrs[a]
        dest.close()
    template.close()

if __name__=="__main__":
    sys.exit(main(sys.argv))

