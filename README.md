The project "osse" consist in a spectral method project for a FlowField object based on the same model as Channelflow.org.

It is composed of the following files:

-----------
Geometries:
-----------
flowfield.py:
    main geometry files to define a FlowField representing fluid flow.
box.py:
    inherits from FlowField, with Fourier x Fourier x Fourier discretisation
channel.py:
    inherits from FlowField, as in Channelflow.org,
    with Fourier x Chebyshev x Fourier discretisation
boundary_layer.py --
    inherits from FlowField, not really developped,
    with Fourier x Boundary_Layer x Fourier discretisation 

-----------
Mathematical Operators and Models
-----------
operators.py:
    main method containign the different models...
        OSS: Orr-Sommerfeld Squire Model,
        LNSE: Linear Navier Stokes Eq. model,
        OSSE: Orr-Sommerfeld Squire model Extended around a non-laminar solution
    ...and many methods to calculate the eigenvalues, the eigenvectors and
    transforms them from a vector to a FlowField object,
    establish the controllability of the model, etc...

-----------
Executable:
-----------
run_eigen.py:
    Calculate the eigenvalues and eigenvectors for an EQ,
    then print and plot them.
run_modal_controllability.py:
    calculate the modal controllability for some equilibria
run_eigenvector_to_ff.py:
    read previously calculated eigenvectors saved in .npy
    and save them into a .h5 file in a FlowField form.
run_B_modes.py:
    tale a column of the matrix B and transform it into a FlowField hdf5.
run_martinelli.py:
    not finished, calculate the passivity of a model

-----------
Tools:
-----------
math_function_box.py:
    mathematical functions
chebyshev.pyi:
    Chebyshev library for differentiation matrices,
    translated from Weidemann and Reddy
hdf5tovtk.py:
    convert a HDF5 file into a VTR file for Paraview
plotfield.py:
    plot a slice of a given hdf5 flowfield file.
chebt.py
copy_attributes.py

-----------
Testing files:
-----------
test_flowfield.py
test_channel.py
test_bl.py
test_operators.py
test_convergence_eigs_oss_extended.py
test_change_basis.py
test_eigenvector_to_ff.py

-----------
OLD:
-----------
svd_dmd.py

###############
FOLDER:
###############

deprecated:
    old files not updated and not working, but useful for methods insights
database:
    library of equilibria, eigenvectors, eigenmodes, etc...
pyevtk:
    library necessary for hd5tovtk.py
figures
streaming
modes
