"""
linear_validation_comp.py

Method compares :
    - CF 1.4.2
        _> cf
    - CF 1.5.1
        -> cf142
    - OSSE with change of basis, so [u, v, w] exported
        -> osse_uvw
    - OSSE-dmd without change of basis, so [v, eta] exported
        -> osse_veta
    - OSS

It compares the dataset :
    - u
    - v
    - w
    - eta

And also check the relation between
[v, eta] and [u,v,w] for CF and osse.

########################################################
Calculations made,
all for actuation (0,1), (1,0) and (1,1),
resolution 10x65x10:
-------------------------|-----------------------|
Reynolds                 |   400        10000    |
-------------------------|-----------------------|
OSS COUETTE              |    x           x      |
OSSE COUETTE             |    x           x      |
CF linear COUETTE        |                x      |
CF skewsymmetric COUETTE |    x           x      |
CF 142 linear COUETTE    |                x      |
-------------------------|-----------------------|
OSSE EQ1                 |    x                  |
CF skewsymmetric EQ1     |    x                  |

Forcing 0.001
OSSE EQ1 Re=400
10, 11
CF EQ1 skewsymmetrix Re=400
01, 10, 11

########################################################

"""
import numpy as np
import scipy as sp
import h5py
import chebyshev
import itertools

import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
font = {'size':20}
matplotlib.rc('font', **font)

from channel import Channel
from operators import Operators as OP

home = "/home/gcpc1m15/"

NORM_1 = False
NORM_2 = False
PLOT = 0
timestep_plot = [40, 100, 150]#[0, 50, 100]
PLOT_NORM = True
LOG = 1

difference = False # difference not working when delta_T
CF = True
CF2 = False # NO DIFFERNECE / INTERP
CF142 = False
osse_uvw = True
osse_veta = False
OSS = False
continuity = False

#nonlinearity_cf = "linearaboutprofile"
nonlinearity_cf = "skewsymmetric"
#baseflow = "couette"
baseflow = "eq1"

Re = 400
T = 150
Nt = T + 1
# major difference with 18 x Ny x 20 for (1,1)
Nx = 20
Ny = 65
Ny_interp = 35 # value to which CF data is interpolated for the difference calculation
Nz = 20
Nx_osse = Nx
Ny_osse = 35
Nz_osse = Nz

# REM : data for (1,0) is produced with actuation x2
kx = +1 #01, 10, 11
kz = +1
forcing_str = "0.0005"
forcing_type = "Exp" # "Sin"
wd_cf = "WD"

# Number of unactuated modes to display for norm
Nxm = 4
Nzm = 4

# if comparison is made with Fourier coefficients
spectral = '_spectral' # ""

# just use to evaluate manually the expected result
Lx = 2 * np.pi / 1.14
Lz = 2 * np.pi / 2.5

# modes to be displayed if not the ones actuated
x = kx
z = kz
alpha = 2 * np.pi * x / Lx
beta = 2 * np.pi * z / Lz

# time to skip at the beginning
# 11 : min_t = 30, max_t = 150, t_shift = -6
# 10 : min_t = 40, max_t = 150, t_shift = -9
# 01 : min_t = 35, max_t = 150, t_shift = -30 # does not really fit or only at the end
delta_T = 0 # 10
min_t = 0
max_t = 150 # = T
t_shift_cf_cf2 = -0

t_saved = np.linspace(min_t, max_t, max_t - min_t + 1)
t_saved2 = np.linspace(min_t + delta_T, max_t + delta_T, max_t - min_t + 1)
if NORM_1 or NORM_2 or PLOT_NORM:
    timestep = t_saved
    timestep2 = t_saved2
elif PLOT:
    timestep = timestep_plot
print(timestep)

sign_x = '+' if np.sign(kx) == 1 else ''
sign_z = '+' if np.sign(kz) == 1 else ''

# differentiation matrices
I = np.eye(Ny_osse)
nodes, DM = chebyshev.chebdiff(Ny_osse, 1)
D_osse = DM[0, :, :]
if difference:
    nodes, DM_cf = chebyshev.chebdiff(Ny_interp, 1)
else:
    nodes, DM_cf = chebyshev.chebdiff(Ny, 1)
D_cf = DM_cf[0, :, :]

# Norms
if NORM_1:
    # norm of all the modes except the actuated one
    norm_osse_except = np.zeros((3))
    norm_cf142_except = np.zeros((3))
    norm_cf_except = np.zeros((3))

    # norm of the actuated mode
    norm_delta_osse = np.zeros((3))
    norm_delta_cf142 = np.zeros((3))
    norm_delta_cf = np.zeros((3))
    norm_delta_cf2 = np.zeros((3))

if NORM_2:
    norm2_delta_osse = np.zeros((3))
    norm2_delta_cf142 = np.zeros((3))
    norm2_delta_cf = np.zeros((3))
    norm2_delta_cf2 = np.zeros((3))
    
    nodes, weights = chebyshev.clencurt(Ny)
    W = np.diag(weights)

if PLOT_NORM:
    # norm array for the actuated mode
    norm_LN_osse = np.zeros((3, Nt))
    norm_LN_cf = np.zeros((3, Nt))
    norm_LN_cf2 = np.zeros((3, Nt))
    norm_LN_diff_cf = np.zeros((3, Nt))
    norm_LN_diff_cf2 = np.zeros((3, Nt))

    # norm array for the UN actuated modes
    norm_LN_osse_harm = np.zeros((Nxm, Nzm, 3, Nt))
    norm_LN_cf_harm = np.zeros((Nxm, Nzm, 3, Nt))
    norm_LN_cf2_harm = np.zeros((Nxm, Nzm, 3, Nt))
    norm_LN_diff_cf_harm = np.zeros((Nxm, Nzm, 3, Nt))
    norm_LN_diff_cf2_harm = np.zeros((Nxm, Nzm, 3, Nt))

    #nodes, weights = chebyshev.clencurt(Ny)
    if difference:
        W = np.eye(Ny_interp) # np.diag(weights)
    else:
        W = np.eye(Ny) # np.diag(weights)

    #nodes_osse, weights_osse = chebyshev.clencurt(Ny_osse)
    W_osse = np.eye(Ny_osse) # np.diag(weights_osse)
    
#---------------------------------------
#---------------------------------------
# LOOP OVER ALL TIMESTEPS
# as we add pi/2, our t=30 correspond to Heins 60s
for it in timestep:

    #---------------------------------------
    # CREATE FILENAMES
    basename_cf = home+"channelflow/database/PHD/CFBC_validation/"\
            +nonlinearity_cf+"/"+baseflow+"/Re="+str(Re)+"/"
    basename_cf2 = home+"channelflow/database/PHD/CFBC_validation/"\
            +nonlinearity_cf+"/"+baseflow+"/Re="+str(Re)+"/"
    basename_cf142 = home+"channelflow-1.4.2/database/PHD/"\
            +"linear_validation/Re="+str(Re)+"/"
    basename_osse = home+"osse/database/CFBC_validation/osse/"\
            +baseflow+"/Re="+str(Re)+"/"
    basename_oss = home+"osse/database/CFBC_validation/oss/Re="\
            +str(Re)+"/"

    if difference:
        compname_cf = str(Nx) + "x" + str(Ny) + "x" + str(Nz) \
            + "_f=" + forcing_str + "_"+forcing_type+"_t=0..150" \
            + "/kx=" + sign_x + str(kx) + "_kz=" + sign_z + str(kz) +"/Ny" + str(Ny_interp) + "/"
    else:
        compname_cf = str(Nx) + "x" + str(Ny) + "x" + str(Nz) \
            + "_f=" + forcing_str + "_"+forcing_type+"_t=0..150" \
            + "/kx=" + sign_x + str(kx) + "_kz=" + sign_z + str(kz) +"/"

    if difference:
        compname_cf2 = str(Nx) + "x" + str(Ny) + "x" + str(Nz) \
            + "_f=" + forcing_str + "_"+forcing_type+"_t=10..150_" + wd_cf \
            + "/kx=" + sign_x + str(kx) + "_kz=" + sign_z + str(kz) +"/Ny" + str(Ny_interp) + "/"
    else:
        compname_cf2 = str(Nx) + "x" + str(Ny) + "x" + str(Nz) \
            + "_f=" + forcing_str + "_"+forcing_type+"_t=10..150_" + wd_cf \
            + "/kx=" + sign_x + str(kx) + "_kz=" + sign_z + str(kz) +"/"

    compname_osse = str(Nx_osse) + "x" + str(Ny_osse) + "x" + str(Nz_osse) \
        + "_f=" + forcing_str + "_"+forcing_type+"_t=0..150" \
        + "/kx=" + sign_x + str(kx) + "_kz=" + sign_z + str(kz) + spectral + "/"
    compname_oss = str(Nx_osse) + "x" + str(Ny_osse) + "x" + str(Nz_osse) \
        + "/kx=" + sign_x + str(kx) + "_kz=" + sign_z + str(kz) + "/"

    filename_cf = "u"+str(int(it))+".h5"
    filename_cf2 = "u"+str(int(it+t_shift_cf_cf2))+".h5"
    filename_osse_uvw = "x_"+str(Nx_osse)+"x"+str(Ny_osse)\
            +"x"+str(Nz_osse)+"_Re="+str(Re) \
            + "_T=" + str(T) + "_t=" + str(int(it)) + "_act.h5"
    filename_osse_veta = "x_"+str(Nx_osse)+"x"+str(Ny_osse)\
            +"x"+str(Nz_osse)+"_Re="+str(Re) \
            + "_T=" + str(T) + "_t=" + str(int(it)) + "_veta.h5"
    filename_oss = "x_"+str(Nx_osse)+"x"+str(Ny_osse)\
            +"x"+str(Nz_osse)+"_Re="+str(Re) \
            + "_T=" + str(T) + "_t=" + str(int(it)) + "_oss.h5"

    name_cf = basename_cf + compname_cf + filename_cf
    name_cf2 = basename_cf2 + compname_cf2 + filename_cf2
    name_cf142 = basename_cf142 + compname_cf + filename_cf
    name_osse_uvw = basename_osse + compname_osse + filename_osse_uvw
    name_osse_veta = basename_osse + compname_osse + filename_osse_veta
    name_oss = basename_oss + compname_oss + filename_oss
    #print("CF : ", name_cf)
    #print("CF142 : ", name_cf142)
    #print("osse_uvw : ", name_osse_uvw) 
    #print("osse_veta : ", name_osse_veta) 
    #print("osse_oss : ", name_oss) 

    #---------------------------------------
    # LOADING FILES
    # COMPARE RESULT IN SPECTRAL AND NOT IN PHYSICAL

    # load EQ1 to substract it from the channelflow flowfield
    if difference:
        kind_of_flow = 'eq1_'+str(Nx)+'x'+str(Ny_interp)+'x'+str(Nz)
    else:
        kind_of_flow = 'eq1_'+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
    ff1 = Channel('database/eq/'+kind_of_flow+'.h5')
    ff1.make_spectral('xz')

    if CF:
        channel_cf = Channel(name_cf)
        # make spectral for cf if spetral is on
        if spectral == '_spectral':
            channel_cf.make_spectral()
        y = channel_cf.y
        data_cf = channel_cf.u[:, 0:Nx, :, 0:Nz] - ff1.u
    
    if CF2:
        channel_cf2 = Channel(name_cf2)
        # make spectral for cf if spetral is on
        if spectral == '_spectral':
            channel_cf2.make_spectral()
        y = channel_cf2.y
        data_cf2 = channel_cf2.u[:, 0:Nx, :, 0:Nz] - ff1.u

    if CF142:
        channel_cf142 = Channel(name_cf142)
        # make spectral for cf if spetral is on
        if spectral == '_spectral':
            channel_cf142.make_spectral()
        y = channel_cf142.y
        data_cf142 = channel_cf142.u[:, 0:Nx, :, 0:Nz] - ff1.u

    if osse_uvw:
        channel_osse_uvw = Channel(name_osse_uvw)
        y_osse = channel_osse_uvw.y
        data_osse_uvw = channel_osse_uvw.u

    if difference:
        data_diff_cf = data_osse_uvw - data_cf
        data_diff_cf2 = data_osse_uvw - data_cf2
    
    if osse_veta:
        channel_osse_veta = Channel(name_osse_veta)
        data_osse_veta = channel_osse_veta.u

        # building eta for osse_uvw and cf for comparison with eta for osse_veta
        eta_osse_uvw = 1j * beta * data_osse_uvw[0, :, :, :] \
                - 1j * alpha *  data_osse_uvw[2, :, :, :]
        if CF:
            eta_cf = 1j * beta * data_cf[0, :, :, :] \
                - 1j * alpha *  data_cf[2, :, :, :]
        if CF142:
            eta_cf142 = 1j * beta * data_cf142[0, :, :, :] \
                - 1j * alpha *  data_cf142[2, :, :, :]

    if OSS:
        channel_oss = Channel(name_oss)
        data_oss = channel_oss.u
        
        d_dy = D_osse * 1j * alpha / (alpha**2 + beta**2)
        d_di = - I * 1j * beta / (alpha**2 + beta**2)
        dv_dy = np.dot(d_dy, data_oss[1, x, :, z])
        deta_di = np.dot(d_di, data_oss[2, x, :, z])
        #dv_dy = np.dot(d_dy, data_osse_uvw_veta[1, x, :, z])
        #deta_di = np.dot(d_di, data_osse_uvw_veta[2, x, :, z])
        u = dv_dy + deta_di

        d_dy_w = D_osse * 1j * beta / (alpha**2 + beta**2)
        d_di_w = I * 1j * alpha / (alpha**2 + beta**2)
        dv_dy_w = np.dot(d_dy_w, data_oss[1, x, :, z])
        deta_di_w = np.dot(d_di_w, data_oss[2, x, :, z])
        #dv_dy = np.dot(d_dy, data_osse_uvw_veta[1, x, :, z])
        #deta_di = np.dot(d_di, data_osse_uvw_veta[2, x, :, z])
        w = dv_dy_w + deta_di_w

    #---------------------------------------
    # NORM 1 DELTA
    if NORM_1:
        # calculation of the norm of all the modes except the one actuated
        #for ix, iz, ii in itertools.product(range(Nx), range(Nz), range(3)):
        #    if ix == kx and iz == kz:
        #        continue
        #    norm_osse_except[ii] += np.linalg.norm(data_osse_uvw[ii, ix, :, iz])/Ny 
        #    norm_cf_except[ii] += np.linalg.norm(data_cf[ii, ix, :, iz])/Ny 
        #    if CF142:
        #        norm_cf142_except[ii] += np.linalg.norm(data_cf142[ii, ix, :, iz])/Ny 
           
        # calculation of the delta in norm for the actuated mode only
        ref1 = [data_cf[0, kx, :, kz], data_cf[1, kx, :, kz], data_cf[2, kx, :, kz]]

        for ii in range(3):
            # OSSE
            if osse_uvw:
                norm_delta_osse[ii] += np.linalg.norm(data_osse_uvw[ii, kx, :, kz] - ref1[ii])/Ny
            # CF 142
            if CF142:
                norm_delta_cf142[ii] += np.linalg.norm(data_cf142[ii, kx, :, kz] - ref1[ii])/Ny
            # CF
            if CF:
                norm_delta_cf[ii] += np.linalg.norm(data_cf[ii, kx, :, kz] - ref1[ii])/Ny
            # CF 2
            if CF2:
                norm_delta_cf2[ii] += np.linalg.norm(data_cf2[ii, kx, :, kz] - ref1[ii])/Ny
    # END OF IF NORM 1

    #---------------------------------------
    # NORM DELTA
    if NORM_2:
        # calculation of the delta in norm for the actuated mode only
        ref2 = [data_cf[0, kx, :, kz], data_cf[1, kx, :, kz], data_cf[2, kx, :, kz]]

        for ii in range(3):
            # OSSE
            if osse_uvw:
                norm2_delta_osse[ii] += np.linalg.multi_dot(
                        [np.conjugate(data_osse_uvw[ii, kx, :, kz] - ref2[ii]).T,
                         np.conjugate(W).T,
                         W,
                         data_osse_uvw[ii, kx, :, kz] - ref2[ii]])
            # CF 142
            if CF142:
                norm2_delta_cf142[ii] += np.linalg.multi_dot(
                        [np.conjugate(data_cf142[ii, kx, :, kz] - ref2[ii]).T,
                         np.conjugate(W).T,
                         W,
                         data_cf142[ii, kx, :, kz] - ref2[ii]])
            # CF
            if CF:
                norm2_delta_cf[ii] += np.linalg.multi_dot(
                        [np.conjugate(data_cf[ii, kx, :, kz] - ref2[ii]).T,
                         np.conjugate(W).T,
                         W,
                         data_cf[ii, kx, :, kz] - ref2[ii]])
            # CF2
            if CF2:
                norm2_delta_cf2[ii] += np.linalg.multi_dot(
                        [np.conjugate(data_cf2[ii, kx, :, kz] - ref2[ii]).T,
                         np.conjugate(W).T,
                         W,
                         data_cf2[ii, kx, :, kz] - ref2[ii]])
    # END OF IF NORM 2

    #---------------------------------------
    # NORM OVER TIME
    if PLOT_NORM:
        for ix, iz, ii in itertools.product(range(Nxm), range(Nzm), range(3)):
            # NORM OF ACTUATED VECTOR
            if ix == kx and iz == kz:
                if difference:
                    if CF:
                        norm_LN_diff_cf[ii, int(it)] = np.sqrt(np.linalg.multi_dot(
                            [np.conjugate(data_diff_cf[ii, ix, :, iz]).T,
                             np.conjugate(W).T,
                             W, 
                             data_diff_cf[ii, ix, :, iz]]).real/Ny_interp)
                    if CF2:
                        norm_LN_diff_cf2[ii, int(it)] = np.sqrt(np.linalg.multi_dot(
                            [np.conjugate(data_diff_cf2[ii, ix, :, iz]).T,
                             np.conjugate(W).T,
                             W, 
                             data_diff_cf2[ii, ix, :, iz]]).real/Ny_interp)

                else:
                    # FOR OSSE
                    if osse_uvw:
                        norm_LN_osse[ii, int(it)] = np.sqrt(np.linalg.multi_dot(
                                    [np.conjugate(data_osse_uvw[ii, ix, :, iz]).T,
                                     np.conjugate(W_osse).T,
                                     W_osse, 
                                     data_osse_uvw[ii, ix, :, iz]]).real/Ny_osse)
                    # FOR CF2
                    if CF2 :
                        norm_LN_cf2[ii, int(it)] = np.sqrt(np.linalg.multi_dot(
                            [np.conjugate(data_cf2[ii, ix, :, iz]).T,
                             np.conjugate(W).T,
                             W, 
                             data_cf2[ii, ix, :, iz]]).real/Ny)
                    # FOR CF
                    if CF :
                        norm_LN_cf[ii, int(it)] = np.sqrt(np.linalg.multi_dot(
                            [np.conjugate(data_cf[ii, ix, :, iz]).T,
                             np.conjugate(W).T,
                             W, 
                             data_cf[ii, ix, :, iz]]).real/Ny)

            else: 
                # NORM FOR HARMONIC
                if difference:
                    if CF:
                        norm_LN_diff_cf_harm[ix, iz, ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                            [np.conjugate(data_diff_cf[ii, ix, :, iz]).T,
                             np.conjugate(W).T,
                             W, 
                             data_diff_cf[ii, ix, :, iz]]).real/Ny_interp)
                    if CF2:
                        norm_LN_diff_cf2_harm[ix, iz, ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                            [np.conjugate(data_diff_cf2[ii, ix, :, iz]).T,
                             np.conjugate(W).T,
                             W, 
                             data_diff_cf2[ii, ix, :, iz]]).real/Ny_interp)
                else:
                    # FOR OSSE
                    if osse_uvw:
                        norm_LN_osse_harm[ix, iz, ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                                [np.conjugate(data_osse_uvw[ii, ix, :, iz]).T,
                                 np.conjugate(W_osse).T,
                                 W_osse,
                                 data_osse_uvw[ii, ix, :, iz]]).real/Ny_osse)

                    # FOR CF
                    if CF2:
                        norm_LN_cf2_harm[ix, iz, ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                            [np.conjugate(data_cf2[ii, ix, :, iz]).T,
                             np.conjugate(W).T,
                             W, 
                             data_cf2[ii, ix, :, iz]]).real/Ny)

                    # FOR CF
                    if CF:
                        norm_LN_cf_harm[ix, iz, ii, int(it)] += np.sqrt(np.linalg.multi_dot(
                            [np.conjugate(data_cf[ii, ix, :, iz]).T,
                             np.conjugate(W).T,
                             W, 
                             data_cf[ii, ix, :, iz]]).real/Ny)

    # END OF IF PLOT_NORM
        
    #---------------------------------------
    # PLOT PROFILES AT TIME IT
    marker_style = dict(markersize = 25)
    if PLOT and it in timestep_plot:
        if True:
            if osse_veta:
                fig0, ((ax0, ax1, ax2, ax3)) = plt.subplots(ncols=4)
                #fig0, ((ax1, ax3)) = plt.subplots(ncols=2)
            else:
                fig0, ((ax0, ax1, ax2)) = plt.subplots(ncols=3)
                #fig0, ((ax0, ax1, ax2)) = plt.subplots(ncols=3, figsize =(25,15))
            plt.set_cmap('bwr')

            ###################################
            if True:
                # u
                ax0.set_title('u')
                if CF:
                    if baseflow == "eq1":
                        ax0.plot(abs(data_cf[0, x, :, z]), y, '1-', color='b', label='cf real', **marker_style)
                    else:
                        ax0.plot(data_cf[0, x, :, z].real, y, '1-', color='b', label='cf real', **marker_style)
                        ax0.plot(data_cf[0, x, :, z].imag, y, '1:', color='b', label='cf imag', **marker_style)

                if CF2:
                    if baseflow == "eq1":
                        ax0.plot(abs(data_cf2[0, x, :, z]), y, '1-', color='r', label='cf2 real', **marker_style)
                    else:
                        ax0.plot(data_cf2[0, x, :, z].real, y, '1-', color='r', label='cf2 real', **marker_style)
                        ax0.plot(data_cf2[0, x, :, z].imag, y, '1:', color='r', label='cf2 imag', **marker_style)

                if CF142:
                    ax0.plot(data_cf142[0, x, :, z].real, y, '2', color='g', label='cf 1.4.2 real', **marker_style)
                    ax0.plot(data_cf142[0, x, :, z].imag, y, '2', color='g', label='cf 1.4.2 imag', **marker_style)

                if osse_uvw:
                    if baseflow == "eq1":
                        ax0.plot(abs(data_osse_uvw[0, x, :, z]), y_osse, '3-', color='g', label='osse uvw real', **marker_style)
                    else:
                        ax0.plot(data_osse_uvw[0, x, :, z].real, y_osse, '3-', color='g', label='osse uvw real', **marker_style)
                        ax0.plot(data_osse_uvw[0, x, :, z].imag, y_osse, '3:', color='g', label='osse uvw imag', **marker_style)

                if OSS:
                    ax0.plot(u.real, y_osse, '4-', color='k', label='OSS real',
                            **marker_style)
                    ax0.plot(u.imag, y_osse, '4:', color='k', label='OSS imag',
                            **marker_style)

            ###################################
            # v
            ax1.set_title('v')
            if CF:
                if baseflow == "eq1":
                    ax1.plot(abs(data_cf[1, x, :, z]), y, '1-',color='b', label='cf real', **marker_style)
                else:
                    ax1.plot(data_cf[1, x, :, z].real, y, '1-',color='b', label='cf real', **marker_style)
                    ax1.plot(data_cf[1, x, :, z].imag, y, '1:',color='b', label='cf imag', **marker_style)

            if CF2:
                if baseflow == "eq1":
                    ax1.plot(abs(data_cf2[1, x, :, z]), y, '1-',color='r', label='cf2 real', **marker_style)
                else:
                    ax1.plot(data_cf2[1, x, :, z].real, y, '1-',color='r', label='cf2 real', **marker_style)
                    ax1.plot(data_cf2[1, x, :, z].imag, y, '1:',color='r', label='cf2 imag', **marker_style)

            if CF142:
                ax1.plot(data_cf142[1, x, :, z].real, y, '2',color='g', label='cf 1.4.2 real', **marker_style)
                ax1.plot(data_cf142[1, x, :, z].imag, y, '2',color='g', label='cf 1.4.2 imag', **marker_style)

            if osse_uvw:
                if baseflow == "eq1":
                    ax1.plot(abs(data_osse_uvw[1, x, :, z]), y_osse, '3-', color='g', label='osse uvw real', **marker_style)
                else:
                    ax1.plot(data_osse_uvw[1, x, :, z].real, y_osse, '3-', color='g', label='osse uvw real', **marker_style)
                    ax1.plot(data_osse_uvw[1, x, :, z].imag, y_osse, '3:', color='g', label='osse uvw imag', **marker_style)

            if osse_veta:
                ax1.plot(data_osse_veta[1, x, :, z].real, y_osse, 'x', color='darkviolet', label='osse eta real', **marker_style)
                ax1.plot(data_osse_veta[1, x, :, z].imag, y_osse, 'x', color='darkviolet', label='osse eta imag', **marker_style)

            if OSS:
                ax1.plot(data_oss[1, x, :, z].real, y_osse, '4-', color='k', label='oss real', **marker_style)
                ax1.plot(data_oss[1, x, :, z].imag, y_osse, '4:', color='k', label='oss imag', **marker_style)

            ###################################
            # w
            if True:
                ax2.set_title('w')
                #ax2.set_xlim([-0.01, 0.01])
                if CF:
                    if baseflow == "eq1":
                        ax2.plot(abs(data_cf[2, x, :, z]), y, '1-', color='b', label='Channelflow v447', **marker_style)
                    else:
                        ax2.plot(data_cf[2, x, :, z].real, y, '1-', color='b', label='Channelflow v447', **marker_style)
                        ax2.plot(data_cf[2, x, :, z].imag, y, '1:', color='b', **marker_style)

                if CF2:
                    if baseflow == "eq1":
                        ax2.plot(abs(data_cf2[2, x, :, z]), y, '1-', color='r', label='Cf 2', **marker_style)
                    else:
                        ax2.plot(data_cf2[2, x, :, z].real, y, '1-', color='r', label='Cf 2', **marker_style)
                        ax2.plot(data_cf2[2, x, :, z].imag, y, '1:', color='r', **marker_style)

                if CF142:
                    ax2.plot(data_cf142[2, x, :, z].real, y, '2', color='g', label='Channelflow 1.4.2 real', **marker_style)
                    ax2.plot(data_cf142[2, x, :, z].imag, y, '2', color='g', **marker_style)

                if osse_uvw:
                    if baseflow == "eq1":
                        ax2.plot(abs(data_osse_uvw[2, x, :, z]), y_osse, '3-', color='g', label='OSSE model', **marker_style)
                    else:
                        ax2.plot(data_osse_uvw[2, x, :, z].real, y_osse, '3-', color='g', label='OSSE model', **marker_style)
                        ax2.plot(data_osse_uvw[2, x, :, z].imag, y_osse, '3:', color='g', **marker_style)

                if OSS:
                    ax2.plot(w.real, y_osse, '4-', color='k', label='OSS model', **marker_style)
                    ax2.plot(w.imag, y_osse, '4:', color='k', **marker_style)

            ###################################
            # eta
            if osse_veta:
                ax3.set_title('eta')
                if CF:
                    # veta from eta_cf
                    ax3.plot(eta_cf[x, :, z].real, y_osse, '1', color='b', label='cf real', **marker_style)
                    ax3.plot(eta_cf[x, :, z].imag, y_osse, '1', color='b', label='cf imag', **marker_style)

                if CF142:
                    # veta from eta_cf142
                    ax3.plot(eta_cf142[x, :, z].real, y_osse, '2', color='g', label='cf 1.4.2 real', **marker_style)
                    ax3.plot(eta_cf142[x, :, z].imag, y_osse, '2', color='g', label='cf 1.4.2 imag', **marker_style)

                # veta from data_osse_uvw
                ax3.plot(eta_osse_uvw[x, :, z].real, y_osse, '+', color='r', label='osse uvw real', **marker_style)
                ax3.plot(eta_osse_uvw[x, :, z].imag, y_osse, '+', color='r', label='osse uvw imag', **marker_style)

                # veta from data_osse_veta
                ax3.plot(data_osse_veta[2, x, :, z].real, y_osse, 'x', color='darkviolet', label='osse eta real', **marker_style)
                ax3.plot(data_osse_veta[2, x, :, z].imag, y_osse, 'x', color='darkviolet', label='osse eta imag', **marker_style)
                
                if OSS:
                    # veta from data_oss
                    ax3.plot(data_oss[2, x, :, z].real, y_osse, '4-', color='k', label='oss real', **marker_style)
                    ax3.plot(data_oss[2, x, :, z].imag, y_osse, '4:', color='k', label='oss imag', **marker_style)
                
                ax3.legend()

            ###################################
            # continuity
            if continuity:
                # check if equal to 0

                if CF:
                    conti_cf = 1j * alpha * data_cf[0, x, :, z] \
                        + np.dot(D_cd, data_cf[1, x, :, z]) \
                        + 1j * beta * data_cf[2, x, :, z]
                    ax1.plot(abs(conti_cf), y, 'o', color='c', label='conti cf')

                conti_osse = 1j * alpha * data_osse_uvw[0, x, :, z] \
                        + np.dot(D_osse, data_osse_uvw[1, x, :, z]) \
                        + 1j * beta * data_osse_uvw[2, x, :, z]
                ax1.plot(abs(conti_osse), y_osse, '-', color='fuchsia', label='conti osse')

            ###################################

            ax0.set_ylabel('wall-normal coordinate y')
            #ax1.set_ylabel('wall-normal y')
            #ax2.legend(loc='best')

            fig0.suptitle("Re = "+str(Re)+" / "+str(Nx)+'x'+str(Ny)+'x'+str(Nz)+' / Modes ('+str(kx)+', '+str(kz)+') / Time : ' + str(it) + 's')
            #fig0.subplots_adjust(hspace=0.4)
            #fig0.subplots_adjust(bottom=0.05, top=0.93, left=0.05, right=0.99)
            plt.show()

# END OF LOOP FOR IT IN TIMESTEP 

if NORM_1:
    norm_osse_except /= (max_t - min_t + 1)
    norm_cf_except /= (max_t - min_t + 1)
    norm_cf142_except /= (max_t - min_t + 1)

    norm_delta_osse /= (max_t - min_t + 1)
    norm_delta_cf142 /= (max_t - min_t + 1)
    norm_delta_cf /= (max_t - min_t + 1)
    norm_delta_cf2 /= (max_t - min_t + 1)

if NORM_2:
    norm2_delta_osse /= (max_t - min_t + 1)
    norm2_delta_cf142 /= (max_t - min_t + 1)
    norm2_delta_cf /= (max_t - min_t + 1)
    norm2_delta_cf2 /= (max_t - min_t + 1)

#---------------------------------------
#---------------------------------------
if False:
    # Exponentiel curve fitting between t1 and t2 
    t1, t2 = 50, 150
    polynom = np.polyfit(x = timestep[t1:t2],
                         y = np.vstack((np.log(norm_LN_osse_harm[0, t1:t2]),
                                        np.log(norm_LN_osse_harm[1, t1:t2]),
                                        np.log(norm_LN_osse_harm[2, t1:t2]))).T, 
                         deg = 1)
    y_p = [np.exp(polynom[1, 0] + timestep * polynom[0, 0]),
           np.exp(polynom[1, 1] + timestep * polynom[0, 1]),
           np.exp(polynom[1, 2] + timestep * polynom[0, 2])]

#---------------------------------------
#---------------------------------------
# PLOT NORM OVER TIME
if PLOT_NORM:
    marker_size = 3#10
    style_osse = dict(color = 'g',
                         markerfacecolor = 'r',
                         markersize = marker_size,
                         markeredgewidth = 2)
    style_cf = dict(color = 'b',
                         markerfacecolor = "None",
                         markersize = marker_size,
                         markeredgewidth = 2)
    style_cf2 = dict(color = 'r',
                         markerfacecolor = "None",
                         markersize = marker_size,
                         markeredgewidth = 2)

    fig_log, ((ax_log0, ax_log1, ax_log2)) = plt.subplots(nrows=3)
    ax_log = [ax_log0, ax_log1, ax_log2]

    for ii in range(3):
        if LOG:
            ax_log[ii].set_yscale('log')
            #ax_log[ii].set_ylim(ymin = 1E-5)
            ax_log[ii].set_ylim([1E-6, 1E-1])
            #ax_log[2].set_ylim([1E-14, 1E-1])

        for ix, iz in itertools.product(range(Nxm), range(Nzm)):
            if ix == 0 and iz == 0:
                continue
            elif ix == kx and iz == kz:
                if difference:
                    if CF2:
                        ax_log[ii].plot(timestep2, norm_LN_diff_cf2[ii, min_t:max_t+1], 'd:', label='Diff CF2, actuated mode.', **style_cf2)
                    if CF:
                        ax_log[ii].plot(timestep, norm_LN_diff_cf[ii, min_t:max_t+1], 'd:', label='Diff CF, actuated mode.', **style_cf)
                else:
                    if osse_uvw:
                        ax_log[ii].plot(timestep, norm_LN_osse[ii, min_t:max_t+1], 'd-', **style_osse, label='OSSE actuated mode.')
                    if CF2:
                        ax_log[ii].plot(timestep2, norm_LN_cf2[ii, min_t:max_t+1], 'd-', label='Channelflow actuated mode, started from OSSE.', **style_cf2)
                    if CF:
                        ax_log[ii].plot(timestep, norm_LN_cf[ii, min_t:max_t+1], 'd-', label='Channelflow actuated mode.', **style_cf)
            elif ix < 2 and iz < 2:
                if difference:
                    if CF2:
                        ax_log[ii].plot(timestep2, norm_LN_diff_cf2_harm[ix, iz, ii, min_t:max_t+1], ':', **style_cf2)
                    if CF:
                        ax_log[ii].plot(timestep, norm_LN_diff_cf_harm[ix, iz, ii, min_t:max_t+1], ':', **style_cf)
                else:
                    if osse_uvw:
                        ax_log[ii].plot(timestep, norm_LN_osse_harm[ix, iz, ii, min_t:max_t+1], ':', **style_osse)
                    if CF2:
                        ax_log[ii].plot(timestep2, norm_LN_cf2_harm[ix, iz, ii, min_t:max_t+1], ':', **style_cf2)
                    if CF:
                        ax_log[ii].plot(timestep, norm_LN_cf_harm[ix, iz, ii, min_t:max_t+1], ':', **style_cf)
        ax_log[ii].plot(0,0, ':', **style_osse, label='OSSE non-actuated modes.')
        ax_log[ii].plot(0,0, ':', **style_cf, label='Channelflow non-actuated modes, started from OSSE.')
        ax_log[ii].plot(0,0, ':', **style_cf2, label='Channelflow non-actuated modes.')
        
        #ax_log[ii].plot(timestep, 1E-5 * np.exp(0.0501205 * timestep), '-', color='fuchsia')
    
    ax_log0.set_title('u')
    ax_log1.set_title('v')
    ax_log2.set_title('w')
    ax_log0.set_ylabel('norm2(u)')
    ax_log1.set_ylabel('norm2(v)')
    ax_log2.set_ylabel('norm2(w)')
    ax_log2.set_xlabel('time')
    fig_log.suptitle("Re = "+str(Re)+" / "+str(Nx)+'x'+str(Ny)+'x'+str(Nz)+' / Modes ('+str(kx)+', '+str(kz)+')')
    ax_log2.legend(loc='best')
    fig_log.subplots_adjust(bottom=0.05, top=0.93, left=0.05, right=0.99)
    plt.show()
# END OF PLOT NORM OVER TIME

#---------------------------------------
# PRINT NORM
if NORM_1:
    """
    print("NORM OSSE")
    print("norm_u_except : ", norm_osse_except[0])
    print("norm_v_except : ", norm_osse_except[1])
    print("norm_w_except : ", norm_osse_except[2])

    if CF142:
        print("NORM CF142")
        print("norm_u_cf142_except : ", norm_cf142_except[0])
        print("norm_v_cf142_except : ", norm_cf142_except[1])
        print("norm_w_cf142_except : ", norm_cf142_except[2])

    print("NORM CF")
    print("norm_u_cf_except : ", norm_cf_except[0])
    print("norm_v_cf_except : ", norm_cf_except[1])
    print("norm_w_cf_except : ", norm_cf_except[2])
    """
    if osse_uvw:
        print("NORM DELTA OSSE")
        print("norm_delta_osse_u : ", norm_delta_osse[0])
        print("norm_delta_osse_v : ", norm_delta_osse[1])
        print("norm_delta_osse_w : ", norm_delta_osse[2])

    if CF142:
        print("NORM DELTA CF142")
        print("norm_delta_cf142_u : ", norm_delta_cf142[0])
        print("norm_delta_cf142_v : ", norm_delta_cf142[1])
        print("norm_delta_cf142_w : ", norm_delta_cf142[2])
    
    if CF:
        print("NORM DELTA CF")
        print("norm_delta_cf_u : ", norm_delta_cf[0])
        print("norm_delta_cf_v : ", norm_delta_cf[1])
        print("norm_delta_cf_w : ", norm_delta_cf[2])

    if CF2:
        print("NORM DELTA CF2")
        print("norm_delta_cf2_u : ", norm_delta_cf2[0])
        print("norm_delta_cf2_v : ", norm_delta_cf2[1])
        print("norm_delta_cf2_w : ", norm_delta_cf2[2])

if NORM_2:
    if osse_uvw:
        print("NORM 2 DELTA OSSE")
        print("norm2_delta_osse_u : ", np.sqrt(norm2_delta_osse[0]))
        print("norm2_delta_osse_v : ", np.sqrt(norm2_delta_osse[1]))
        print("norm2_delta_osse_w : ", np.sqrt(norm2_delta_osse[2]))

    if CF142:
        print("NORM 2 DELTA CF142")
        print("norm2_delta_cf142_u : ", np.sqrt(norm2_delta_cf142[0]))
        print("norm2_delta_cf142_v : ", np.sqrt(norm2_delta_cf142[1]))
        print("norm2_delta_cf142_w : ", np.sqrt(norm2_delta_cf142[2]))

    if CF:
        print("NORM 2 DELTA CF")
        print("norm2_delta_cf_u : ", np.sqrt(norm2_delta_cf[0]))
        print("norm2_delta_cf_v : ", np.sqrt(norm2_delta_cf[1]))
        print("norm2_delta_cf_w : ", np.sqrt(norm2_delta_cf[2]))
    
    if CF2:
        print("NORM 2 DELTA CF2")
        print("norm2_delta_cf2_u : ", np.sqrt(norm2_delta_cf2[0]))
        print("norm2_delta_cf2_v : ", np.sqrt(norm2_delta_cf2[1]))
        print("norm2_delta_cf2_w : ", np.sqrt(norm2_delta_cf2[2]))
