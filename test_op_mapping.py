"""
test_op_mapping.py

test the matrix C and Cinv formed by the method
op_mapping
from operators.py

"""
import numpy as np
import h5py
from flowfield import FlowField
from plotfield import plot_slice
from operators import Operators
import itertools

# parameters 
eq = 1
Nx = 4
Ny = 15
Nz = 4
N = Ny - 2

# 0. loading the equilibrium
filename_eq = "./database/eq/"+\
        "eq"+str(eq)+"_"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)+".h5"
ff_eq = FlowField(filename_eq)
Mx = ff_eq.Mx
Mz = ff_eq.Mz

# 1.building matrices
op = Operators(ff_eq)
C, Cinv = op.op_mapping(ff_eq)
W, Winv = op.op_weighting(ff_eq)

# 2. check if Cinv * Winv * W * C is the identity
P = np.linalg.multi_dot([Cinv, Winv, W, C])
#np.savetxt('P.real', P.real, fmt='%+.1f')
#np.savetxt('P.imag', P.imag, fmt='%+.1f')
# -> YES

# 3 check each block of the matrix C
# choose block to check :
beta = 1
alpha = 0
# modes alpha : 0 1.14 -2.28 -1.14
alpha_vect = [0, 1.14, -2.28, -1.14]
# modes beta : 0 2.5 -5 -2.5
beta_vect = [0, 2.5, -5, -2.5]

a = 0*2*Mx*Mz*N + (alpha*Mx + beta) * N
b = 0*2*Mx*Mz*N + (alpha*Mx + beta + 1) * N
c = Mx*Mz*N + (alpha*Mx + beta) * N - N
d = Mx*Mz*N + (alpha*Mx + beta + 1) * N - N

k2 = alpha_vect[alpha]**2 + beta_vect[beta]**2
#print(np.round(k2 * C[a:b, c:d].imag / alpha_vect[alpha],2))
print(np.round(k2 * C[a:b, c:d].imag / beta_vect[beta],2))
# the result should be either D or I matric depending of the chosen block.

#np.savetxt('C.real', C.real, fmt='%+.1f')
#np.savetxt('C.imag', C.imag, fmt='%+.1f')
