import numpy as np
import scipy as sp
import datetime
import scipy.linalg

import matplotlib
import matplotlib.pyplot as plt

from channel import Channel
from operators import Operators as OP

omega = 1

# SYSTEM DEFINITION
n = 300 # x
m = 3 # u
q = 2 # y
l = 2 # w
I = np.eye(n)

# dx/dt = A  x + B1 q
#   y   = C2 x +  0 q
#   z   = [ y q ]^T

A = 1 * np.random.rand(n, n)
#A = np.array([[1,2,3,4,5],
#     [2,3,4,5,6],
#     [7,8,9,10,11],
#     [8,9,10,11,12],
#     [13,14,15,17,16]])
B1 = 10 * np.random.rand(n, l)
#B1 = np.array([[1,0],
#               [1,0],
#               [1,0],
#               [1,0],
#               [1,1]])
B2 = 10 * np.random.rand(n, m)
#B2 = np.array([[1,2,3],
#               [4,5,6],
#               [7,8,9],
#               [8,9,11],
#               [1,7,9]])
#C1 = np.eye(n)
C2 = 10 * np.random.rand(n, n)
C1 = C2
#C2 = np.array([[1,0,0,0,0],
#               [0,1,0,0,0]])
D12 = 10 * np.random.rand(n, m)
#D12 = np.array([[1,0,0],
#                [0,1,0],
#                [0,0,1],
#                [0,0,0],
#                [0,0,0]])
#print(np.dot(np.transpose(D12), D12))
#D21 = np.array([[1,0],
#                [0,1]])
#print(np.dot(D21, np.transpose(D21)))

# EIGENVALUES OF A
eigs, eigenvectors = np.linalg.eig(A)
plt.figure()
plt.plot(eigs.real, eigs.imag, 'rx', ms='5', mew='1')
plt.grid(b=True, which='Major')
plt.xlabel('real')
plt.ylabel('imag')
#plt.show()
#print(eigs)

# CHECKING THE STANDARD ASSUMPTION

M = np.concatenate((np.concatenate((A - 1j * omega*I, B2), axis=1),
                    np.concatenate((C1, D12), axis=1)), axis=0)
print('Should be : ', n+m, ' and it is : ', np.linalg.matrix_rank(M))
# should be n + m

#N = np.concatenate((np.concatenate((A - 1j * omega*I, B1), axis=1),
#                    np.concatenate((C2, D21), axis=1)), axis=0)
#print('Should be : ', n+q, ' and it is : ', np.linalg.matrix_rank(N))
# should be n + q
                    
# DEFINING THE RICCATI EQ
# SOLVING THE RICCATI EQ

begin = np.datetime64(datetime.datetime.now(), 'ms')

P = sp.linalg.solve_continuous_are(A,
                                   B2,
                                   np.dot(np.transpose(C1), C1),
                                   np.eye(m))

end = np.datetime64(datetime.datetime.now(), 'ms')
print('Time elapsed : ', end - begin)

# BUILDING THE CONTROLLED SYSTEM
K1 = - np.dot(B2, np.dot(np.transpose(B2), P))
K2 = 0

# EIGENVALUES OF A_act
eigs2, eigenvectors2 = np.linalg.eig(A + K1)
plt.figure()
plt.plot(eigs2.real, eigs2.imag, 'rx', ms='5', mew='1')
plt.grid(b=True, which='Major')
plt.xlabel('real')
plt.ylabel('imag')
plt.show()
#print(eigs2)
