"""
riccati.py
"""
import numpy as np
import scipy as sp
import scipy.integrate as spi
import datetime
import h5py
import resource
import os, errno

import sys
import argparse

import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt

#import control
#import control.matlab

from channel import Channel
from operators import Operators as OP

### RICCATI FUNCTION
def riccati(Nx, Ny, Nz):
    ###########
    # PARAMETERS
    ###########

    basename = '/home/gcpc1m15/osse/'
    baseflow_type = 'eq1'
    filename = 'database/eigenfolder/'
    Re = 400
    tau = 1E-5
    tau_v = tau
    tau_eta = tau
    tau_u = tau
    tau_w = tau
    kx_v = int(100)
    kz_v = int(100)
    kx_eta = -1
    kz_eta = -1
    k_uw = False

    T0 = 0
    T1 = 200

    N_eq = 1
    act = True
    if act:
        N = (Ny) * (Nx*Nz + Nx * Nz - 1 + 2)
    else:
        N = (Ny-2) * (Nx*Nz + Nx * Nz - 1 + 2)
        N_act = 2 * (Nx*Nz + Nx * Nz - 1 + 2)

    memory = True

    print('\n##############################################################################')
    kind_of_flow = 'eq'+str(N_eq)+'_'+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
    print(filename+kind_of_flow)
    print('Nx x Ny x Nz : ', Nx, ' x ', Ny, ' x ', Nz, '\n')
    print('EQ : ', N_eq)
    print('Re : ', Re)
    print('tau_v : ', tau_v)
    print('tau_eta : ', tau_eta)
    print('tau_u : ', tau_u)
    print('tau_w : ', tau_w)
    print('actuation : ', act)
    print('kx_v : ', kx_v)
    print('kz_v : ', kz_v)
    print('kx_eta : ', kx_eta)
    print('kz_eta : ', kz_eta)
    print('k_uw : ', k_uw)
    print('\n##############################################################################')

    ###########
    # BUILDING FLOWFIELD
    ###########
    ff = Channel('database/eq/'+kind_of_flow+'.h5')
    ff.load_baseflow('database/eq/'+kind_of_flow+'.h5')

    Mx = ff.Mx
    Mz = ff.Mz
    print(Mx)
    print(Mz)
    if act:
        M = (Ny) * (Mx*Mz + Mx*Mz - 1 + 2)
    else:
        M = (Ny-2) * (Mx*Mz + Mx*Mz - 1 + 2)
        M_act = 2 * (Mx*Mz + Mx*Mz - 1 + 2)

    # add a Couette baseflow U(y) = y
    for ny in range(ff.u.shape[2]):
        ff.u0[0, :, ny, :] += ff.y[ny]
        #ff.u0[0, :, ny, :] = 1 - ff.y[ny]**2
        #ff.u0[1, :, ny, :] = 0 #ff.y[ny]
        #ff.u0[2, :, ny, :] = 0 #ff.y[ny]

    # BUILDING OPERATOR AND OBJECTIVES
    oss_op = OP(ff)
    if False:
        if act:
            # VALIDATE ACTUATED SYSTEM ?
            E, A, B = oss_op.op_nonlaminar_baseflow_actuated(ff, Re,
                                                             tau_v=tau_v,
                                                             tau_eta=tau_eta,
                                                             tau_u=tau_u,
                                                             tau_w=tau_w)
            C, Cinv = oss_op.op_mapping_actuated(ff)
            act_str = '_act'
        else:
            E, A = oss_op.op_nonlaminar_baseflow(ff, Re)
            B = np.zeros((M, M_act))
            # might require truncation=False ?
            C, Cinv = oss_op.op_mapping(ff, truncation=True)
            act_str = ''

        A = np.dot(E,A)
        B = np.dot(E,B)
        #BH = np.transpose(B).conjugate()
        # B and BH are real

        Qx, Qq = oss_op.op_nonlaminar_baseflow_objectives(ff, cholesky=False)

        # Qx, Qq and BH are pure real
        Qx = Qx.real
        Qq = Qq.real
        #if np.linalg.norm(Qx.imag) == 0:
        #    print("Qx real")
        #    Qx = Qx.real
        #    if np.linalg.norm(Qq.imag) == 0:
        #        print("Qq real")
        #        Qq = Qq.real
        #        #if np.linalg.norm(BH.imag) == 0:
        #        #    BH = BH.real

    # BASIC OPERATORS FUNCTIONS
    elif False:
        M_x = 4
        M_q = 1
        A  = (+1 + 0j) * np.zeros((M_x, M_x))
        B  = (+1 + 0j) * np.zeros((M_x, M_q))
        Qx = (+1 + 0j) * np.zeros((M_x, M_x))
        Qq = (+1 + 0j) * np.zeros((M_q, M_q))

        A[0, 1] = 1.0
        A[1, 1] = -15
        A[1, 2] = 10
        A[2, 3] = 1
        A[3, 3] = -15

        B[1, 0] = 10.0
        B[3, 0] = 1.0

        #BH = np.transpose(B).conjugate()

        Qx[0, 0] = 1
        Qx[2, 2] = 1
        Qx[3, 3] = 2

        Qq[0, 0] = 1

        print("A : \n", A)
        print("B : \n", B)
        print("Qx: \n", Qx)
        print("Qq: \n", Qq)

    # LOADING OPERATORS AND OBJECTIVES
    else:
        A, A_N1, A_N2 = oss_op.load_mat_hdf5(
            basename+"database/OSSE/"+baseflow_type
            +"/Re="+str(Re)+"/"
            +str(Nx)+"x"+str(Ny)+"x"+str(Nz)+"/K_all_modes_v/A.h5")

        B, B_N1, B_N2 = oss_op.load_mat_hdf5(
            basename+"database/OSSE/"+baseflow_type
            +"/Re="+str(Re)+"/"
            +str(Nx)+'x'+str(Ny)+"x"+str(Nz)+"/K_all_modes_v/B.h5")

        Qx, Qx_N1, Qx_N2 = oss_op.load_mat_hdf5(
            basename+"database/OSSE/"+baseflow_type
            +"/Re="+str(Re)+"/"
            +str(Nx)+"x"+str(Ny)+"x"+str(Nz)+"/K_all_modes_v/Qx.h5")

        Qq, Qq_N1, Qq_N2 = oss_op.load_mat_hdf5(
            basename+"database/OSSE/"+baseflow_type
            +"/Re="+str(Re)+"/"
            +str(Nx)+"x"+str(Ny)+"x"+str(Nz)+"/K_all_modes_v/Qq.h5")

        #BH = np.transpose(B).conjugate()
        C, Cinv = oss_op.op_mapping_actuated(ff)
        act_str = '_act'

    # SAVING MATRICES
    if False:
        print("SAVING MATRICES")
        OP.save_mat_cmplx_hdf5( A,
                         basename+"database/OSSE/eq"+str(N_eq)+"/Re="+str(Re)+"/"
                         +str(Nx)+'x'+str(Ny)+'x'+str(Nz)+"/",
                         "A.h5")
        OP.save_mat_cmplx_hdf5( B,
                         basename+"database/OSSE/eq"+str(N_eq)+"/Re="+str(Re)+"/"
                         +str(Nx)+'x'+str(Ny)+'x'+str(Nz)+"/",
                         "B.h5")
        OP.save_mat_real_hdf5( Qx,
                         basename+"database/OSSE/eq"+str(N_eq)+"/Re="+str(Re)+"/"
                         +str(Nx)+'x'+str(Ny)+'x'+str(Nz)+"/",
                         "Qx.h5")
        OP.save_mat_real_hdf5( Qq,
                         basename+"database/OSSE/eq"+str(N_eq)+"/Re="+str(Re)+"/"
                         +str(Nx)+'x'+str(Ny)+'x'+str(Nz)+"/",
                         "Qq.h5")

    # SPARSITY OF MATRIX A
    if False:
        (_, _, V) = sp.sparse.find(A)
        print("Non zero elements of A : ", V.size)

    # REDUCE B TO ONLY THE MAIN EIGENMODES
    if True:
        # check modal controllability for 8 x 35 x 18
        # remove control of wall normal vorticity
        # keep the central modes for the wall normal velocity

        N_aq, B, Qq = oss_op.op_reduce_actuation_modes(ff, B, Qq,
                                                       kx_v = kx_v,
                                                       kz_v = kz_v,
                                                       kx_eta = kx_eta,
                                                       kz_eta = kz_eta,
                                                       uw = k_uw,
                                                       cholesky= False)
        pass

    # EIGENVALUES
    if False:
        filename0 = filename+'eq'+str(N_eq)+'/EIGS'+act_str+'/'+kind_of_flow
        vals, vecs = oss_op.eigen(
            filename=filename0,
            EA=A,
            EB=B,
            timer=False,
            memory=False,
            tolerance=1E-8)

        oss_op.eigen_print(10, filename0+'_EIG'+act_str+'.npy')
        #oss_op.eigen_plot(filename0+'_EIG'+act_str+'.npy')

    if False:
        ff.plot_major_modes(baseflow=True)

    # MAJOR MODES IN EQ
    if False:
        ff.make_spectral('xz', baseflow='only')

        # Reordering the Fourier modes for streamwise and spanwise direction
        K = 2 # if full set of modes needed, set K=2
        freq_kx_0 = np.fft.fftfreq(Nx, 1/Nx).astype(np.integer, copy=False)
        freq_kz_0 = np.fft.rfftfreq(Nz, 1/Nz).astype(np.integer, copy=False)
        freq_kx = np.concatenate((freq_kx_0[-int(Mx/K):], freq_kx_0[:int(Mx/K+1)]))
        freq_kz = freq_kz_0[:int(Mz/K+1)]
        #freq_kz = np.concatenate((freq_kz_0[-int(Mz/K):], freq_kz_0[:int(Mz/K+1)]))

        table_modes_u_0 = abs(ff.u0[0, :, 4, :])
        table_modes_v_0 = abs(ff.u0[1, :, 4, :])
        table_modes_w_0 = abs(ff.u0[2, :, 4, :])

        # Reordering the Fourier modes for streamwise and spanwise direction
        table_modes_u_1 = table_modes_u_0[:int(Mz/K+1), :]
        #table_modes_u_1 = np.concatenate((table_modes_u_0[-int(Mz/K):, :],
        #                                  table_modes_u_0[:int(Mz/K+1), :]))
        table_modes_u = np.concatenate((table_modes_u_1[:, -int(Mx/K):],
                                        table_modes_u_1[:, :int(Mx/K+1)]), axis=1)

        table_modes_v_1 = table_modes_v_0[:int(Mz/K+1), :]
        #table_modes_v_1 = np.concatenate((table_modes_v_0[-int(Mz/K):, :],
        #                                  table_modes_v_0[:int(Mz/K+1), :]))
        table_modes_v = np.concatenate((table_modes_v_1[:, -int(Mx/K):],
                                        table_modes_v_1[:, :int(Mx/K+1)]), axis=1)

        table_modes_w_1 = table_modes_w_0[:int(Mz/K+1), :]
        #table_modes_w_1 = np.concatenate((table_modes_w_0[-int(Mz/K):, :],
        #                                  table_modes_w_0[:int(Mz/K+1), :]))
        table_modes_w = np.concatenate((table_modes_w_1[:, -int(Mx/K):],
                                        table_modes_w_1[:, :int(Mx/K+1)]), axis=1)

        # figure
        fig, (ax0, ax1, ax2) = plt.subplots(ncols=3)
        plt.set_cmap('Greys')

        ax0.set_title('Modes of $u$')
        ax0.set_ylabel('Alpha')
        ax0.set_xlabel('Beta')
        ax0.autoscale_view()
        ax0.set_xticks(np.arange(0.5, Mz))
        ax0.set_xticklabels(freq_kz)
        ax0.set_yticks(np.arange(0.5, Mx))
        ax0.set_yticklabels(freq_kx)
        ax0.pcolormesh(table_modes_u,
                       edgecolor='k',
                       lw=0.01)

        ax1.set_title('Modes of $v$')
        #ax1.set_ylabel('Alpha')
        ax1.set_xlabel('Beta')
        ax1.autoscale_view()
        ax1.set_xticks(np.arange(0.5, Mz))
        ax1.set_xticklabels(freq_kz)
        ax1.set_yticks(np.arange(0.5, Mx))
        ax1.set_yticklabels(freq_kx)
        ax1.pcolormesh(table_modes_v,
                       edgecolor='k',
                       lw=0.01)

        ax2.set_title('Modes of $w$')
        #ax2.set_ylabel('Alpha')
        ax2.set_xlabel('Beta')
        ax2.autoscale_view()
        ax2.set_xticks(np.arange(0.5, Mz))
        ax2.set_xticklabels(freq_kz)
        ax2.set_yticks(np.arange(0.5, Mx))
        ax2.set_yticklabels(freq_kx)
        ax2.pcolormesh(table_modes_w,
                       edgecolor='k',
                       lw=0.01)
        plt.show()

        ff.make_physical('xz', baseflow='only')

    # RICCATI EQ
    if True:
        print("\nRICCATI EQ")

        #if False:
        #    # cause error and abortion of the code routine
        #    # or ask for Qx to bw symmetric (which it is...)
        #    system_ss = control.ss(A, B, np.zeros((10, N)), np.zeros((10, N_act)))
        #    K, S, E = control.lqr(system_ss, Qx, Qq)
        #    #P = control.care(A.real, B.real, np.eye(N), np.eye(N_act))
        if False:
            print("\nSOLVING EQ")
            begin = np.datetime64(datetime.datetime.now(), 'ms')
            P = sp.linalg.solve_continuous_are(A,
                                               B,
                                               Qx,
                                               Qq)
            #np.eye(N_act)

            end = np.datetime64(datetime.datetime.now(), 'ms')
            print('Time elapsed : ', end - begin)
            #print("P : \n", P)

            if memory:
                print('MEMORY used for eigenvalues-vectors calculation :',
                      resource.getrusage(resource.RUSAGE_SELF).ru_maxrss /1000, 'MB')

            # BUILDING THE CONTROLLED SYSTEM
            K = - np.dot(np.transpose(B).conjugate(), P)
            print("KIMAG: ", np.linalg.norm(K.imag))
            BK = np.dot(B, K)

            print("SAVING K")
            print(K.shape)
            print(type(K))
            print(type(K[0, 0]))

            OP.save_mat_cmplx_hdf5(K,
                             basename+"database/OSSE/"+baseflow_type
                             +"/Re="+str(Re)+"/"
                             +str(Nx)+'x'+str(Ny)+'x'+str(Nz)+"/",
                             "K_veta.h5")

            # changing base
            K_uvw = np.dot(K, Cinv)
            OP.save_mat_cmplx_hdf5(K_uvw,
                             basename+"database/OSSE/"+baseflow_type
                             +"/Re="+str(Re)+"/"
                             +str(Nx)+'x'+str(Ny)+'x'+str(Nz)+"/",
                             "K_uvw.h5")

        else:
            K, K_N1, K_N2 = oss_op.load_mat_hdf5(
                basename+"database/OSSE/"+baseflow_type
                +"/Re="+str(Re)+"/"
                +str(Nx)+"x"+str(Ny)+"x"+str(Nz)+"/K_all_modes_v/K_veta.h5")
            BK = np.dot(B, K)

            K_uvw, K_uvw_N1, K_uvw_N2 = oss_op.load_mat_hdf5(
                basename+"database/OSSE/"+baseflow_type
                +"/Re="+str(Re)+"/"
                +str(Nx)+"x"+str(Ny)+"x"+str(Nz)+"/K_all_modes_v/K_uvw.h5")

        # EIGENVALUES
        if False:
            filename_ctrl = filename+'eq'+str(N_eq)+'/EIGS_ctrl/'+kind_of_flow

            vals_ctrl, vecs_ctrl = oss_op.eigen(
                filename=filename_ctrl,
                EA=A + BK,
                EB=B,
                timer=False,
                memory=False,
                tolerance=1E-8)

            oss_op.op_eigen_print(10, filename_ctrl+'_EIG'+act_str+'.npy')
            #oss_op.op_eigen_plot(filename_ctrl+'_EIG'+act_str+'.npy')

        # EIGENVALUES OF A_act
        if False:
            vals_ctrl2, vecs_ctrl2 = np.linalg.eig(A + BK)
            print('Vals 2 :\n', vals_ctrl2[:10])

            vals_ctrl3, vecs_ctrl3 = sp.sparse.linalg.eigs(A + BK,
                                                           k=20,
                                                           which='LR',
                                                           tol=1E-4)#,
            #sigma=0.2)
            print('Vals 3 :\n', vals_ctrl3[:10])
            plt.figure()
            plt.plot(vals_ctrl2.real, vals_ctrl2.imag, 'rx', ms='5', mew='1')
            plt.grid(b=True, which='Major')
            plt.xlabel('real')
            plt.ylabel('imag')
            plt.show()
            #print(eigs2)

        # INTEGRATION
        if True:
            print('\nINTEGRATION')
            Nt = (T1-T0)+1
            t_saved = np.linspace(T0, T1, Nt)

            x_0 = (0.05 + 0j) * np.ones(M)#* np.random.rand(N)

            #x_0 = np.zeros((A.shape[0]), dtype=complex)

            def direct_eq(t, x, A):
                return np.dot(A, x) #+ np.dot(B, q_interp(t))

            def direct_eq_ctrl(t, x, A, BK):
                return np.dot(A + BK, x) #+ np.dot(B, q_interp(t))

            begin = np.datetime64(datetime.datetime.now(), 'm')
            list_call = [T0]
            call_period = 1
            def f_call(t, x):
                t1 = np.floor(t)
                if t1 % call_period == 0 and t1 > list_call[-1]:
                    print(">>> ", t1, " / elapsed : ", np.datetime64(datetime.datetime.now(), 'm') - begin)
                    list_call.append(t1)
                return t % call_period

            print('\nSolving basic system')
            sol = spi.solve_ivp(fun = lambda t, y: direct_eq(t, y, A),
                                t_span = (T0,T1),
                                y0 = x_0,
                                method = 'BDF',
                                t_eval = t_saved,
                                events = f_call,
                                rtol = 1e-8,
                                atol = 1e-8,
                                jac = A)
            x = sol.y.T # shape [Nt, N]
            print('Done')

            print('\nSolving controlled system')
            list_call = [T0]
            sol_ctrl = spi.solve_ivp(fun = lambda t, y: direct_eq_ctrl(t, y, A, BK),
                                     t_span = (T0,T1),
                                     y0 = x_0,
                                     method = 'BDF',
                                     t_eval = t_saved,
                                     events = f_call,
                                     rtol = 1e-8,
                                     atol = 1e-8,
                                     jac = A + BK)
            x_ctrl = sol_ctrl.y.T # shape [Nt, N]
            print('Done')

            plt.figure()
            plt.plot(t_saved, np.linalg.norm(x, axis=1), 'x-', color='r', label='x')
            plt.plot(t_saved, np.linalg.norm(x_ctrl, axis=1), 'x-', color='b', label='x ctrl')
            plt.legend(loc='best')
            plt.show()

            ###########
            # SAVING
            ###########
            print('\nSaving')
            f_u = OP.f_lift(ff.y, 'up', 'clamped')
            f_l = OP.f_lift(ff.y, 'low', 'clamped')
            #save_spectral = True
            if T0 == 0 :
                T_str = str(T1)
            else:
                T_str = str(T0)+"-"+str(T1)

            for it in t_saved:
                if it%10 == 0:
                    print(">>>>>>>>> it : ", str(it))

                x_t1 = x[int(it),:]
                x_ctrl_t1 = x_ctrl[int(it),:]

                x_1 = oss_op.transform_spectral_vector_veta_to_array_uvw_actuated(
                    ff,
                    x_t1,
                    f_u, f_l)
                x_ctrl_1 = oss_op.transform_spectral_vector_veta_to_array_uvw_actuated(
                    ff,
                    x_ctrl_t1,
                    f_u, f_l)
                x_1 = ff.make_physical_dataset(x_1, nz=Nz, index='xz')
                x_ctrl_1 = ff.make_physical_dataset(x_ctrl_1, nz=Nz, index='xz')

                try:
                    os.makedirs(basename+"database/OSSE/"
                                +baseflow_type
                                +"/Re="+str(Re)
                                +"/"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)
                                +"/integration-non-actuated/")
                    os.makedirs(basename+"database/OSSE/"
                                +baseflow_type
                                +"/Re="+str(Re)
                                +"/"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)
                                +"/integration-actuated/")
                except OSError as e:
                    if e.errno != errno.EEXIST:
                        raise

                oss_op.save_h5_real(x_1,
                               filename = basename+"database/OSSE/"
                               +baseflow_type
                               +"/Re="+str(Re)
                               +"/"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)
                               +"/integration-non-actuated/"
                               +"/x_"+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
                               +"_Re="+str(Re)+"_T="+T_str+"_t="+str(int(it))+".h5")
                oss_op.save_h5_real(x_ctrl_1,
                               filename = basename+"database/OSSE/"
                               +baseflow_type
                               +"/Re="+str(Re)
                               +"/"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)
                               +"/integration-actuated/"
                               +"/x_"+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
                               +"_Re="+str(Re)+"_T="+T_str+"_t="+str(int(it))+".h5")

                if False:
                    x_1_phys = ff.make_physical_dataset(x_1, nz=Nz, index='xz')
                    x_ctrl_1_phys = ff.make_physical_dataset(x_ctrl_1, nz=Nz, index='xz')

                    oss_op.save_h5_real(x_1_phys,
                                        filename = basename+"database/OSSE/"
                                        +baseflow_type
                                        +"/Re="+str(Re)
                                        +"/"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)
                                        +"/integration-non-actuated/"
                                        +"/x_phys_"+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
                                        +"_Re="+str(Re)+"_T="+T_str+"_t="+str(int(it))+".h5")
                    oss_op.save_h5_real(x_ctrl_1_phys,
                                        filename = basename+"database/OSSE/"
                                        +baseflow_type
                                        +"/Re="+str(Re)
                                        +"/"+str(Nx)+"x"+str(Ny)+"x"+str(Nz)
                                        +"/integration-actuated/"
                                        +"/x_phys_"+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
                                        +"_Re="+str(Re)+"_T="+T_str+"_t="+str(int(it))+".h5")

    if False:
        # RESHAPE FROM [v, eta, u00, w00] to [u, v, w]
        K, K_N1, K_N2 = oss_op.load_mat_hdf5(
            basename+"database/OSSE/eq"+str(N_eq)+"/Re="+str(Re)+"/"
            +str(Nx)+'x'+str(Ny)+'x'+str(Nz)+"/K_veta.h5")

        K_uvw = np.dot(K, Cinv)
        OP.save_mat_cmplx_hdf5(K_uvw,
                         basename+"database/OSSE/eq"+str(N_eq)+"/Re="+str(Re)+"/"
                         +str(Nx)+'x'+str(Ny)+'x'+str(Nz)+"/",
                         "K_uvw.h5")

    return 0

def main():
    parser = argparse.ArgumentParser(description='Solve Riccati equation for the OSSE model')
    parser.add_argument("Nx", type=int,
                        help="Streamwise resolution")
    parser.add_argument("Ny", type=int,
                        help="Wall-normal resolution")
    parser.add_argument("Nz", type=int,
                        help="Spanwise resolution")
    #parser.add_argument("-Nx", "--Nx",
    #        type=int,
    #        help='Nx input')

    args = parser.parse_args()

    riccati(args.Nx, args.Ny, args.Nz)

    return 0

if __name__=="__main__":
    sys.exit(main())
