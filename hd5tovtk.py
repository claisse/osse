#!/usr/bin/env python3
"""
Converts hd5 channelflow files to vtk files readable by paraview.
Requires pyevtk
A Sharma 2016
"""
import sys
import os
from optparse import OptionParser
from flowfield import FlowField
from pyevtk.hl import gridToVTK
from numpy import real
"""
Requires pyevtk in the same folder:
    parent/
    .......hd5tovtk.py
    .......pyevtk/
    ..............__init__.py
    ..............evtk.py
    ..............hl.py
    ..............vtk.py
    ..............xml.py
"""

def hdfToVTK(ff, fname):
    """
    write ff object u,v,w to .vtr file
    """
    fname, ext = os.path.splitext(fname)
    #ff.u = real(ff.u).transpose(0, 3, 2, 1)
    ff.u = real(ff.u)
    gridToVTK(fname, ff.x, ff.y, ff.z, pointData = {"u" : ff.u[0,:,:,:], "v" : ff.u[1,:,:,:], "w" : ff.u[2,:,:,:], "V": (ff.u[0,:,:,:], ff.u[1,:,:,:], ff.u[2,:,:,:])})


def main(argv):
    """ 
    program to convert channelflow hdf5 file to vtk format for (e.g.) paraview
    usage:
        hd5tovtk [--mean=mean.asc] flowfield.hd5
    """

    usage = "hd5tovtk [--mean=mean.asc] flowfield1.hd5 [flowfield2.hd5 [.....]]"
    parser=OptionParser(usage)
    parser.add_option("--mean", "-m")
    (options, filenames) = parser.parse_args(sys.argv)
    
    for filename in filenames[1:]:
        ff = FlowField()
        ff.load(filename)
        if options.mean:
            try:
                ff.addMean(options.mean)
            except:
                print("Adding mean failed for " + filename)
                raise

        hdfToVTK(ff, filename)

    return 0


if __name__=="__main__":
    sys.exit(main(sys.argv))
