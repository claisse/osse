import numpy as np
import scipy as sp
import scipy.integrate as spi
import itertools

from channel import Channel
from operators import Operators as OP
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
import matplotlib.pyplot as plt



#filename = 'database/eigenfolder/'
basename = '/home/gcpc1m15/osse/'
#basename = '/home/gcls/osse/'
Re = 5000
tau = 0.05
tau_v = tau
tau_eta = tau

Nx = 4
Ny = 200
Nz = 4
act = True
if act:
    N = (Ny) * 2
#else:
if True:
    N_original = (Ny-2) * 2
N_act = 2 * 2

alpha = 0
beta = 2.044

print('\n###########################################################################\n')
print('Nx       = ', Nx)
print('Ny       = ', Ny)
print('Nz       = ', Nz)
print('tau      = ', tau)
print('Re       = ', Re)
print('alpha    = ', alpha)
print('beta     = ', beta)
print('\n###########################################################################\n')

###########
# BUILDING FLOWFIELD
###########

#just to get the structure
N_eq = 1
kind_of_flow = 'eq'+str(N_eq)+'_'+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
ff = Channel('database/eq/'+kind_of_flow+'.h5')
#sff.mean.apply_mean_flow('couette')
ff.mean.apply_mean_flow('poiseuille')
ff.u.fill(0)
#ff.mean.apply_mean_flow('couette')

Lx = ff.attributes['Lx']
Lz = ff.attributes['Lz']
kx = alpha * Lx / (2 * np.pi)
kz = beta * Lz / (2 * np.pi)

f_u = OP.f_lift(ff.y, 'up', 'clamped')
f_l = OP.f_lift(ff.y, 'low', 'clamped')

# BUILDING OPERATOR
#oss_op = OP(ff)
#E0, A0, B0 = oss_op.oss_operator_actuated(
#        ff,
#        Re,
#        kx, kz,
#        lifting_function = OP.f_lift,
#        tau_v=tau_v,
#        tau_eta=tau_eta)

#A0 = np.dot(E0, A0)
#print(A0.shape)
#B0 = np.dot(E0, B0)
#print(B0.shape)

oss_op = OP(ff)
vals, vecs, vals2, vecs2 = oss_op.oss_eigen(ff,
                                            Re,
                                            ff.u0,
                                            kx,
                                            kz,
                                            omega=-1)

print('\nEigenvalues (scipy.sparse.linalg) : \n', np.round(np.sort(vals), 6))
print("-------")
print('\nEigenvalues (scipy.sparse.linalg) : \n', np.round(np.sort(vals2), 6))

plt.figure()
plt.plot(vals.real, np.absolute(vals.imag),
         'rx', ms='5', mew='1',
         label='Eigenvalues Orr-Sommerfeld')
plt.plot(vals2.real, np.absolute(vals2.imag), 'bx', ms='5', mew='1',
         label='Eigenvalues Squire')

plt.legend(fontsize='small')
plt.title('Eigenvalues of the Orr-Sommerfeld-Squire equation.')
plt.grid(b=True, which='Major')
plt.xlim(-0.5, 0.1)
plt.ylim(0, 1)
plt.xlabel('real')
plt.ylabel('imag')
plt.show()


