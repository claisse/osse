"""
...
"""
import h5py
import numpy as np
from math import *
import unittest
import warnings

try:
    import sympy as sp
    import_sympy = True
except:
    import_sympy = False

from flowfield import FlowField
from flowfield import DiffOperator_FlowField
from channel import Channel
from channel import DiffOperator_Channel
from boundary_layer import Boundary_Layer
from boundary_layer import DiffOperator_Boundary_Layer
import chebyshev

# import noise
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt


class BLTest(unittest.TestCase):
    """ Test of methods implements in boundary_layer."""

    def setUp(self, filename=None):
        # delete warning due to code in chebyshev.chebdiff (see details in code)
        #warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)

        filename = 'database/test_files/bl_sample.h5'

        # Creation of a new h5-file similar to a ChannelFlow flowfield
        file = h5py.File(filename, 'w')
        data = file.create_group('data')
        geom = file.create_group('geom')

        file.attrs['Lx'] = 1 * pi
        file.attrs['Lz'] = 0.5 * pi
        file.attrs['a'] = 0
        file.attrs['b'] = np.inf
        a = file.attrs['a']
        b = file.attrs['b']

        # BE CAREFUL, VALUES BELOW 10 DOES NOT WORK
        file.attrs['Nx'] = 2
        file.attrs['Ny'] = 51
        file.attrs['Nz'] = 4
        self.Nx = file.attrs['Nx']
        self.Ny = file.attrs['Ny']
        self.Nz = file.attrs['Nz']

        # stretching factor
        # stretching factor influence the convergence for y->inf
        # example function :
        # u(y) = exp(-b * y)
        # OR : u(n) = n^(b*Y) transformed
        # d^a u(n) /dn^a = (b*Y)! / (b*Y-n)! * n^(b*Y-a)
        # if b*Y-a > 0 : convergence when y->inf or n->0
        # if b*Y-a =< 0 : divergence when y->inf or n->0
        # CCL: b*Y > a to prevent problem with infinite boundary condition
        # as max(a) = 4 and b = 1 => min(Y) = 5
        self.Y = 5

        # Creation of a basis (x, y, z) of dim (Nx, Ny, Nz)
        array_x = np.arange(0, file.attrs['Lx'], file.attrs['Lx'] / self.Nx)
        array_y_temp = chebyshev.chebnodes(2 * self.Ny)
        array_y = -self.Y * np.log(array_y_temp[:self.Ny])
        # array_y = np.zeros((self.Ny))
        # for index in range(self.Ny):
        #    if array_y_temp[index] > 0:
        #        array_y[index] = -self.Y*np.log(array_y_temp[index])
        #    else:
        #        array_y[index] = 0
        # array_y[index] = -self.Y*np.log(array_y_temp[:self.Ny])
        # array_y = np.linspace(a, b, num = self.Ny, endpoint=True)
        array_z = np.arange(0, file.attrs['Lz'], file.attrs['Lz'] / self.Nz)

        x = geom.create_dataset('x', data=array_x)
        y = geom.create_dataset('y', data=array_y)
        z = geom.create_dataset('z', data=array_z)

        # Creation of a dataset u(x,y,z)
        coef_cos_Z = 3
        coef_sin_Z = 2
        offset = np.power(np.tan(array_y[0] * (np.pi / 2 - 0.5)), 2)
        array_u = np.ndarray(shape=(3, self.Nx, self.Ny, self.Nz))

        # BOUNDARY CONDITION :
        # u(x,y->infinite,z,t) = 0
        # div u (x,y->infinite,z,t) = 0

        for i in range(array_u.shape[0]):
            for nx in range(array_u.shape[1]):
                for ny in range(array_u.shape[2]):
                    for nz in range(array_u.shape[3]):
                        array_u[i, nx, ny, nz] = 0 * i \
                            - 0 * (np.power(np.tan(array_y[ny] * (np.pi / 2 - 0.5)), 2) - offset) \
                            + 0 * np.power(2 * (ny - (self.Ny - 1) / 2) / (self.Ny - 1), 4) \
                            + 0 * np.sin(2 * np.pi * 4 * array_x[nx] / file.attrs['Lx']) \
                            + 0 * 1 / (array_y[ny] + 1) \
                            + 0 * np.cos(+array_y[ny]) * np.exp(-array_y[ny]) \
                            + 1 * np.exp(-1 * array_y[ny])
                        #+ 0 * np.cos(2*np.pi * coef_cos_Z*array_z[nz]/file.attrs['Lz']) * np.sin(2*np.pi * coef_sin_Z*array_z[nz]/file.attrs['Lz'])
                        #+ 1 * np.cos(2*np.pi * 4*array_y[ny]/(b-a)) \

        u = data.create_dataset('u', data=array_u)

        file.close()

        # LOad the saved h5-file to a new flowfield
        self.ff = Boundary_Layer(filename)

    def tearDown(self):
        pass

    def test_interpolation(self):
        print('config y-axis', self.ff.conf_y_axis)

        plt.figure()
        # print('\nbl y : \n', np.round(self.ff.y,2))
        # print('bl data: \n', np.round(self.ff.u[0,0,:,0],2))
        # print('Ny : \n', self.ff.attributes['Ny'])
        plt.plot(self.ff.y, self.ff.u[0, 0, :, 0], 'o')

        self.ff.u *= 0.5
        self.ff.chebyshev2boundarylayer()
        self.ff.u *= 0.5
        self.ff.chebyshev2boundarylayer()

        self.ff.u *= 2
        self.ff.boundarylayer2chebyshev()
        self.ff.u *= 2
        self.ff.boundarylayer2chebyshev()
        # print('\ncheb y : \n', np.round(self.ff.y,2))
        # print('cheb data: \n', np.round(self.ff.u[0,0,:,0],2))
        # print('Ny : \n', self.ff.attributes['Ny'])

        plt.plot(self.ff.y, self.ff.u[0, 0, :, 0], 'o-')

        self.ff.chebyshev2boundarylayer()
        self.ff.chebyshev2boundarylayer()
        # print('\nbl y 2 : \n', np.round(self.ff.y,2))
        # print('bl 2 data: \n', np.round(self.ff.u[0,0,:,0],2))
        # print('Ny : \n', self.ff.attributes['Ny'])
        plt.plot(self.ff.y, self.ff.u[0, 0, :, 0], 'x-')
        plt.savefig('figures/test_bl_interp.png',
                    bbox_inches='tight')
        plt.show()

    def test_deriv(self):
        deriv_ux = self.ff.dop.deriv_u(1, 0, 0)
        deriv_uy = self.ff.dop.deriv_u(0, 1, 0)
        deriv_uz = self.ff.dop.deriv_u(0, 0, 1)
        deriv_px = self.ff.dop.deriv_p(1, 0, 0)
        deriv_py = self.ff.dop.deriv_p(0, 1, 0)
        deriv_pz = self.ff.dop.deriv_p(0, 0, 1)

        deriv_dat_ux = self.ff.dop.deriv(self.ff.u, 1, 0, 0)
        deriv_dat_uy = self.ff.dop.deriv(self.ff.u, 0, 1, 0)
        deriv_dat_uz = self.ff.dop.deriv(self.ff.u, 0, 0, 1)
        deriv_dat_px = self.ff.dop.deriv(self.ff.p, 1, 0, 0)
        deriv_dat_py = self.ff.dop.deriv(self.ff.p, 0, 1, 0)
        deriv_dat_pz = self.ff.dop.deriv(self.ff.p, 0, 0, 1)

        np.testing.assert_almost_equal(
            deriv_ux,
            deriv_dat_ux,
            err_msg="deriv_u('x') != deriv(self.ff.u, 'x')")
        np.testing.assert_almost_equal(
            deriv_uy,
            deriv_dat_uy,
            err_msg="deriv_u('y') != deriv(self.ff.u, 'y')")
        np.testing.assert_almost_equal(
            deriv_uz,
            deriv_dat_uz,
            err_msg="deriv_u('z') != deriv(self.ff.u, 'z')")

        np.testing.assert_almost_equal(
            deriv_px,
            deriv_dat_px,
            err_msg="deriv_p('x') != deriv(self.ff.p, 'x')")
        np.testing.assert_almost_equal(
            deriv_py,
            deriv_dat_py,
            err_msg="deriv_p('y') != deriv(self.ff.p, 'y')")
        np.testing.assert_almost_equal(
            deriv_pz,
            deriv_dat_pz,
            err_msg="deriv_p('z') != deriv(self.ff.p, 'z')")

    def test_deriv_u(self):
        order = 2  # < (self.Y -1)
        plt.figure()

        # ORIGINAL DATA
        plt.plot(self.ff.y, self.ff.u[0, 0, :, 0], 'bo-')

        if import_sympy:
            # NUMPY DERIVATIVE
            x = sp.symbols('x')
            # l = 1/(x+1)
            l = sp.exp(-x)
            # l = sp.cos(x)*sp.exp(-x)
            lprime = l.diff(x, order)
            fct = sp.lambdify(x, lprime, 'numpy')
            plt.plot(self.ff.y, fct(self.ff.y[:]), 'r--')

        # DERIV_U
        diff = self.ff.dop.deriv_u(0, order, 0)
        # print(diff[0,0,:,0])

        # print('diff: \n', np.round(diff[0,1,:,5],2))
        # plt.plot(self.ff.y, 3*self.ff.y*self.ff.y)
        # plt.plot(self.ff.y, -np.sin(self.ff.y))
        # np.diff(self.ff.y[0,1,:,5])
        # plt.plot(self.ff.y[2:], np.diff(self.ff.u[0,1,:,5], order), 'ro')
        plt.plot(self.ff.y, diff[0, 0, :, 0], 'ro')

        # CHECKK IF DATA IS NOT CHANGED
        plt.plot(self.ff.y, self.ff.u[0, 0, :, 0], 'rx')
        plt.savefig('figures/test_deriv_u.png',
                    bbox_inches='tight')

        # plt.ylim([-100,100])
        plt.show()

    def test_deriv_p(self):
        order = 2  # < (self.Y -1)
        plt.figure()

        # ORIGINAL DATA
        plt.plot(self.ff.y, self.ff.p[0, :, 0], 'bo-')

        if import_sympy:
            # NUMPY DERIVATIVE
            x = sp.symbols('x')
            # l = 1/(x+1)
            l = sp.exp(-x)
            # l = sp.cos(x)*sp.exp(-x)
            lprime = l.diff(x, order)
            fct = sp.lambdify(x, lprime, 'numpy')
            plt.plot(self.ff.y, fct(self.ff.y[:]), 'r--')

        # DERIV_P
        diff = self.ff.dop.deriv_p(0, order, 0)
        # print(diff[0,0,:,0])

        # print('diff: \n', np.round(diff[1,:,5],2))
        # plt.plot(self.ff.y, 3*self.ff.y*self.ff.y)
        # plt.plot(self.ff.y, -np.sin(self.ff.y))
        # np.diff(self.ff.y[1,:,5])
        # plt.plot(self.ff.y[2:], np.diff(self.ff.p[1,:,5], order), 'ro')
        plt.plot(self.ff.y, diff[0, :, 0], 'ro')

        # CHECKK IF DATA IS NOT CHANGED
        plt.plot(self.ff.y, self.ff.p[0, :, 0], 'rx')
        plt.savefig('figures/test_deriv_p.png',
                    bbox_inches='tight')

        # plt.ylim([-100,100])
        plt.show()

test = unittest.TestSuite()

# test.addTest(BLTest("test_interpolation"))
test.addTest(BLTest("test_deriv"))
test.addTest(BLTest("test_deriv_u"))
test.addTest(BLTest("test_deriv_p"))

runner = unittest.TextTestRunner()
runner.run(test)
