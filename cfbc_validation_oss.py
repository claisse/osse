"""
run_linear_validation_oss.py

create the matrices of the OSS model actuated by :
- either using only the OSS method
- or by extraction the right block from the OSSE method
"""
import numpy as np
import scipy as sp
import scipy.integrate as spi
import itertools

from channel import Channel
from operators import Operators as OP

###########
# PARAMETERS
###########

# OSS: either true if OSS method
# or False if extracted from OSSE method.
OSS = True
OSSE = False

#filename = 'database/eigenfolder/'
basename = '/home/gcpc1m15/osse/'
#basename = '/home/gcls/osse/'
T = 100
dt = 0.01
Re = 400
tau = 0.05
tau_v = tau
tau_eta = tau

# initial function coefficients
e = +5
f = -3
g = -1

def forcing(t):
    return 1 * np.cos(0.05 * t + np.pi/2) * (0.05 - 0.01j) # np.cos(radians)

# time to save the dataset
Nt = T+1
t_saved = np.linspace(0, T, Nt)

# Lx = 5.51156605893 # 2*pi / 1.14
# Lz = 2.51327412287 # 2*pi / 2.5
Lx = 2 * np.pi / 1.14
Lz = 2 * np.pi / 2.5
Nx = 10
Ny = 65
Nz = 10
act = True
if act:
    N = (Ny) * 2
#else:
if True:
    N_original = (Ny-2) * 2
N_act = 2 * 2

kx = 1
kz = 1

sign_x = '+' if np.sign(kx) == 1 else ''
sign_z = '+' if np.sign(kz) == 1 else ''

print('\n###########################################################################\n')
print('Nx       = ', Nx)
print('Ny       = ', Ny)
print('Nz       = ', Nz)
print('T        = ', T)
print('dt       = ', dt)
print('tau      = ', tau)
print('Re       = ', Re)
print('t_saved  = ', t_saved)
print('kx       = ', kx)
print('kz       = ', kz)
print('\n###########################################################################\n')

###########
# BUILDING FLOWFIELD
###########

#just to get the structure
N_eq = 1
kind_of_flow = 'eq'+str(N_eq)+'_'+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
ff = Channel('database/eq/'+kind_of_flow+'.h5')
ff.u.fill(0)
ff.mean.apply_mean_flow('couette')

f_u = OP.f_lift(ff.y, 'up', 'clamped')
f_l = OP.f_lift(ff.y, 'low', 'clamped')

# BUILDING OPERATOR
if OSS:
    oss_op = OP(ff)
    E0, A0, B0 = oss_op.oss_operator_actuated(
            ff,
            Re,
            kx, kz,
            lifting_function = OP.f_lift,
            tau_v=tau_v,
            tau_eta=tau_eta)

    A0 = np.dot(E0, A0)
    print(A0.shape)
    B0 = np.dot(E0, B0)
    print(B0.shape)

if OSSE:
    E2_, A2_, B2_ = oss_op.op_nonlaminar_baseflow_actuated(
                ff,
                Re,
                lifting_function = OP.f_lift,
                tau_v=tau_v,
                tau_eta=tau_eta,
                tau_u=tau_v,
                tau_w=tau_v)
    A2_ = np.dot(E2_, A2_)
    B2_ = np.dot(E2_, B2_)

    if False:
        # check which block of the matrix A is not zero:
        print("A NONZERO")
        for i_k, i_k2 \
            in itertools.product(range(Nx*Nz + Nx*Nz - 1 + 2),
                                 range(Nx*Nz + Nx*Nz - 1 + 2)):


            X = A2_[(i_k )*Ny: (i_k  + 1)*Ny,
                    (i_k2)*Ny: (i_k2 + 1)*Ny]

            if np.linalg.norm(X, axis=(0,1)) > 1E-8:
                print("(", i_k, " / ", i_k2, ")")

    if False:
        # check which block of the matrix B is not zero:
        print("B NONZERO")
        for i_k, i_k2 \
            in itertools.product(range(Nx*Nz + Nx*Nz - 1 + 2),
                                 range(Nx*Nz + Nx*Nz - 1 + 2)):
            X2 = B2_[(i_k )*Ny: (i_k  + 1)*Ny,
                     (i_k2)*2 : (i_k2 + 1)*2 ]
            if np.linalg.norm(X2, axis=(0,1)) > 1E-8:
                print("(", i_k, " / ", i_k2, ")")

    # build the OSS matrix by extracting
    # the right block matrices of the OSSE model
    A2_1 = A2_[(kx*Nz + kz)*Ny: (kx*Nz + kz + 1)*Ny,
               (kx*Nz + kz)*Ny: (kx*Nz + kz + 1)*Ny]
    A2_2 = np.zeros(shape=(Ny, Ny))
    A2_3 = A2_[(Nx*Nz + kx*Nz + kz - 1)*Ny: (Nx*Nz + kx*Nz + kz - 1 + 1)*Ny,
               (kx*Nz + kz)*Ny: (kx*Nz + kz + 1)*Ny]
    A2_4 = A2_[(Nx*Nz + kx*Nz + kz - 1)*Ny: (Nx*Nz + kx*Nz + kz - 1 + 1)*Ny,
               (Nx*Nz + kx*Nz + kz - 1)*Ny: (Nx*Nz + kx*Nz + kz - 1 + 1)*Ny]
    A2 = np.concatenate((np.concatenate((A2_1, A2_2), axis=1),
                         np.concatenate((A2_3, A2_4), axis=1)), axis=0)

    B2_1 = B2_[(kx*Nz + kz)*Ny: (kx*Nz + kz + 1)*Ny,
               (kx*Nz + kz)*2: (kx*Nz + kz + 1)*2]
    B2_2 = np.zeros(shape=(Ny, 2))
    B2_3 = np.zeros(shape=(Ny, 2))
    B2_4 = B2_[(Nx*Nz + kx*Nz + kz - 1)*Ny: (Nx*Nz + kx*Nz + kz - 1 + 1)*Ny,
               (Nx*Nz + kx*Nz + kz - 1)*2: (Nx*Nz + kx*Nz + kz - 1 + 1)*2]
    B2 = np.concatenate((np.concatenate((B2_1, B2_2), axis=1),
                         np.concatenate((B2_3, B2_4), axis=1)), axis=0)

    if OSS:
        # compare matrices from OSS and OSSE models
        print("delta_A : ", np.linalg.norm(A0-A2, axis=(0,1)))
        print("delta_B : ", np.linalg.norm(B0-B2, axis=(0,1)))

if OSS:
    A = A0
    B = B0
elif OSSE:
    A = A2
    B = B2

###########
# INTEGRATION
###########
N_step = T/dt

#factor = np.array([1, 1]) #(1 + 1j) * np.random.rand((2))
#x_0 = factor[:, np.newaxis] * OP.f_initial(ff.y, e, f, g)
#x_0 = x_0.flatten()
x_0 = np.zeros((A.shape[0]), dtype=complex)
vect = [1, 1, 0, 0]

def direct_eq(t, x, A, B, vect, forcing):
    return np.dot(A, x) + forcing(t) * np.dot(B, vect)

print('\nINTEGRATION ACTUATED')
sol = spi.solve_ivp(fun = lambda t, y: direct_eq(t, y, A, B, vect, forcing),
                    t_span = (0,T),
                    y0 = x_0,
                    method = 'BDF',
                    t_eval = t_saved)
print("success ?! -> ", sol.success)
x = sol.y.T # shape [N_step, N]
print("Shape of solution : ", x.shape)

###########
# SAVING
###########
for it in t_saved:
    if it%10 == 0:
        print(">>>>>>>>> it : ", str(it))
    x_t1 = x[int(it), :]

    # save in velocity / vorticity array
    if True:
        #array_veta = oss_op.transform_spectral_veta_vector_to_array(
        #    ff, x_t1, truncation=False)

        array_veta = np.zeros(shape=(3, Nx, Ny, Nz), dtype=complex)
        array_veta[1, kx, :, kz] = x_t1[0:Ny]
        array_veta[2, kx, :, kz] = x_t1[Ny:2*Ny]

        if np.linalg.norm(array_veta[2, :, 0, :], axis=(0,1)) > 1E-4:
            print("UP  / it : ", str(it))
            print(array_veta[2, :, 0, :])
        if np.linalg.norm(array_veta[2, :, -1, :], axis=(0,1)) > 1E-4:
            print("LOW  / it : ", str(it))
            print(array_veta[2, :, -1, :])

        # APPLY WALL ACTUATION
        array_veta = oss_op.apply_wall_actuation_on_v0_veta(
            array_veta, f_u, f_l)

        array_veta_phys = ff.make_physical_dataset(array_veta, nz=ff.Nz, index='xz')
        oss_op.save_h5_real(
            array_veta_phys,
            filename = basename+"database/CFBC_validation/oss/"
                +"Re="+str(Re)+"/"+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
                +"/kx="+sign_x+str(kx)+"_kz="+sign_z+str(kz)
                +"/x_"+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
                +"_Re="+str(Re)+"_T="+str(T)+"_t="+str(int(it))+"_oss.h5")
