import pstats
pstats.Stats('prof').strip_dirs().sort_stats("cumulative").print_stats()
""" use like
    python -m cProfile -o prof testDMD.py
    python dumpprof.py > stats.txt
from http://cyrille.rossant.net/profiling-and-optimizing-python-code/ """
