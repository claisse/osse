"""
DRAFT
martinelli.py:

Python3 file containing the program calling methods
from operators.py in order to
verify the passitivity of the actuated system for OSS extended.

We do:
    Building the operator system for an actuated OSS model
        around an invariant solution.
    Check that B.H B > 0 so B has full column rank.
    Check that B B.H > 0 is not possible so B has not full row rank.
    Check the negative-definiteness of null(B) (A + A.H) null(B).H
        by calculating its biggest eigenvalue.

G Claisse July 2017
"""
import numpy as np
import scipy.io
from math import *
import itertools

#matplotlib.rcParams['text.usetex'] = True
#matplotlib.rcParams['text.latex.unicode'] = True
#import matplotlib
#matplotlib.use('TkAgg')
#from matplotlib import rc
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
#rc('text', usetex=True)
#import matplotlib.pyplot as plt

import chebyshev
from flowfield import FlowField
from flowfield import DiffOperator_FlowField
from flowfield import BoundaryCondition_FlowField
from flowfield import MeanFlow_FlowField
from channel import Channel
from channel import DiffOperator_Channel
from channel import BoundaryCondition_Channel
from channel import MeanFlow_Channel
from boundary_layer import Boundary_Layer
from boundary_layer import DiffOperator_Boundary_Layer
from boundary_layer import BoundaryCondition_Boundary_Layer
from boundary_layer import MeanFlow_Boundary_Layer
from operators import Operators as OP
from math_function_box import left_null_space, null_space

# PARAMETERS
printing = True
filename='database/martinelli/nonlaminar_baseflow/EIGS/'
Re = 10 # from Gibson 2008
tau = 10E-5
tau_v = tau
tau_eta = tau
tau_u = tau
tau_w = tau
nx = 0
nz = 0

#Lx = 2*np.pi#/1.14 #5.51156605893 from Gibson 2008
#Lz = 2*np.pi#*2/5 #2.51327412287 from Gibson 2008
#Lx = 2*np.pi #2*np.pi/1.14 #5.51156605893
#Lz = 2*np.pi #4*np.pi/5 #2.51327412287

list_Nx = {4}#{4, 8, 12, 16, 20}#, 24, 28, 32}
list_Ny = {15} #20
list_Nz = {4} #10
N_eq = {1} #{'SYM', 'ASYM'} #{2, 5, 9, 11}

for n_eq in N_eq:
    for Nx, Ny, Nz in itertools.product(list_Nx, list_Ny, list_Nz):
        kind_of_flow = 'eq'+str(n_eq)+'_'+str(Nx)+'x'+str(Ny)+'x'+str(Nz)

        print('\n##############################################################################')
        print(filename+kind_of_flow)
        print('Nx x Ny x Nz : ', Nx, ' x ', Ny, ' x ', Nz, '\n')

        # BUILDING FLOWFIELD
        ff = Channel('database/eq/'+kind_of_flow+'.h5')
        ff.load_baseflow('database/eq/'+kind_of_flow+'.h5')

        # add a Couette baseflow U(y) = y
        for ny in range(ff.u.shape[2]):
            ff.u0[0, :, ny, :] += ff.y[ny]
            #ff.u0[0, :, ny, :] = 1 - ff.y[ny]**2
            #ff.u0[1, :, ny, :] = 0 #ff.y[ny]
            #ff.u0[2, :, ny, :] = 0 #ff.y[ny]

        # BUILDING OPERATOR
        oss_op = OP(ff)

        E, A, B = oss_op.op_nonlaminar_baseflow_actuated(ff, Re,
                                            tau_v=tau_v,
                                            tau_eta=tau_eta,
                                            tau_u=tau_u,
                                            tau_w=tau_w)

        EA = np.dot(E, A)
        EB = np.dot(E, B)

        # EIGENVALUES
        #vals, vecs = oss_op.op_eigen(
        #                filename=filename+kind_of_flow,
        #                EA=EA,
        #                EB=EB,
        #                timer=True,
        #                memory=True,
        #                tolerance=10E-8)
        #
        #oss_op.op_eigen_print(10, filename+kind_of_flow+'_EIG'+act_str+'.npy')
        #oss_op.op_eigen_plot(filename+kind_of_flow+'_EIG'+act_str+'.npy')

        ##########
        # Definition of the weight matrix Q = W.T W
        W = []
        for n in range(int((Mx*Mz + Mx*Mz - 1 +2))):
            W = np.concatenate((W, ff.w))
        W = W[np.newaxis, :]
        Q = np.dot(W.T, W)
        print(Q.shape)

        QB = np.dot(Q, B)
        QA = np.dot(Q, A)

        ##########
        #TODO
        # APPLY QB INSTEAD OF B ?

        if False:
            # Definition of the matrix B.H B
            BHB = np.dot(B.conj().T, B)

            w_BHB, v_BHB = np.linalg.eig(BHB)
            if printing:
                print('--------------')
                print('-- B.H B --')
                print('shape : ', BHB.shape)
                print('rank : ', np.linalg.matrix_rank(BHB))
                print('Eigenvalues :\n', min(w_BHB))

            # The assumption B.T.B > 0 is true as B has full column rank

        ##########
        #TODO
        # APPLY QB INSTEAD OF B ?

        if False:
            # Testing criteria B B.H > 0 (as Martinelli)
            # Definition of the matrix B (B).H
            BBH = np.dot(B, B.conj().T)

            w_BBH, v_BBH = np.linalg.eig(BBH)
            if printing:
                print('--------------')
                print('-- B B.H --')
                print('shape : ', BBH.shape)
                print('rank : ', np.linalg.matrix_rank(BBH))
                print('min(Eigenvalues) :\n', min(w_BBH))

            # BBT is not positive definite, as its eigenvalues are not all positive
            # it implies that condition B.B.T > 0 is not fullfilled.

        ##########
        #TODO
        # APPLY QB INSTEAD OF B ?

        #TODO
        # CHECH HOW THE TWO INEQUALITIES OF MARTINELLI
        # ARE DEFINED IN ORDER TO CHECK HOW TO INSERT Q
        # BUT IT SHOUDL BE DIRECTLY B = Q.B AND A = Q.A

        # Testing criteria null(Q.B) (Q.A + (Q.A).H) null(Q.B).H < 0 (as Martinelli
        print('-- QB_ (QA + (QA).H) (QB_).H --')

        scipy.io.savemat('QB.mat', mdict={'QB':QB})

        # Left null space of weighted B
        B_ = left_null_space(B)
        print('left null space : \n', B_.shape)

        # null_space
        B_n = null_space(B)
        print('null_space : \n', B_n.shape)

        #print('QB_ :\n', QB_.shape)

        if B_.size == 0:
            B_ = 1
            B_H = 1
            print('Left null space of QB is empty.')
        else:
            B_H = B_.conj().T

        M = np.dot(B_, np.dot((A + A.conj().T), B_H))

        w_M, v_M = np.linalg.eig(M)
        print('max(Eigenvalues) :\n', max(w_M))
        scipy.io.savemat('M.mat', mdict={'M':M})
        print('M shape : ', M.shape)

    """
    output for 12 x 35 x 12

    BHB shape (578, 578)
    rank 578
    eigs (1139062500)

    BBH
    shape (10115, 10115)
    rank 578
    eigs (-0.021189)

    QB_ (QA + QA.H) QB_.h
    left null space of QB is empty.
    """
