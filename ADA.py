"""
ADA.py
"""

import numpy as np
from scipy.integrate import odeint
import scipy.integrate as spi
import scipy.interpolate
import scipy.optimize
import h5py

from odeintw import odeintw

import matplotlib.pyplot as plt

from channel import Channel
from operators import Operators as OP

import interpolate
import sys

import time

###########
# PARAMETERS
###########

filename = 'database/eigenfolder/'
Re = 400
tau = 1E-5
tau_v = tau
tau_eta = tau
tau_u = tau
tau_w = tau

N_eq = 1
Nx = 4
Ny = 15
Nz = 4
act = True
if act:
    N = (Ny) * (Nx*Nz + Nx * Nz - 1 + 2)
else:
    N = (Ny-2) * (Nx*Nz + Nx * Nz - 1 + 2)
N_act = 2 * (Nx*Nz + Nx * Nz - 1 + 2)

t0 = 0
T = 10
# consider N_step fixed for now
N_step =  100#1*T+1

# 1x2x1 : L = 1E+4, factor = 1E+0
L = 0#1E+0
factor = 1E+0 #-4

tol = 1E-5
p_max = 1E+15
T_max = 100
T_factor = 1.05

print('\n##############################################################################')
kind_of_flow = 'eq'+str(N_eq)+'_'+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
print(filename+kind_of_flow)
print('Nx x Ny x Nz : ', Nx, ' x ', Ny, ' x ', Nz, '\n')

filename_storage = "/home/gcpc1m15/osse/database/ADA/"+str(Nx)+'x'+str(Ny)+'x'+str(Nz)+"/"

###########
# BUILDING FLOWFIELD
###########

if True:
    ff = Channel('database/eq/'+kind_of_flow+'.h5')
    ff.load_baseflow('database/eq/'+kind_of_flow+'.h5')

    # add a Couette baseflow U(y) = y
    for ny in range(ff.u.shape[2]):
        ff.u0[0, :, ny, :] += ff.y[ny]
        #ff.u0[0, :, ny, :] = 1 - ff.y[ny]**2
        #ff.u0[1, :, ny, :] = 0 #ff.y[ny]
        #ff.u0[2, :, ny, :] = 0 #ff.y[ny]

    # BUILDING OPERATOR AND OBJECTIVES
    oss_op = OP(ff)
    if act:
        # VALIDATE ACTUATED SYSTEM ?
        E, A, B = oss_op.op_nonlaminar_baseflow_actuated(ff, Re,
                                                tau_v=tau_v,
                                                tau_eta=tau_eta,
                                                tau_u=tau_u,
                                                tau_w=tau_w)
    else:
        E, A = oss_op.op_nonlaminar_baseflow(ff, Re)
        B = np.zeros((N, N_act))
    A = np.dot(E,A)
    B = np.dot(E,B)
    BH = np.transpose(B).conjugate()
    # B and BH are real

    Qx, Qq = oss_op.op_nonlaminar_baseflow_objectives(ff, cholesky=False)

else:
    A = (-1 + 0j) * np.eye(N)
    A[0, 0] = +0.05
    A[1, 1] = +1E-6
    A[2, 2] = +1E-6
    A[-1, -1] = -10
    B = (1 + 0j) * np.ones(shape=(N, N_act))
    #B[0,0] = 1
    #B[0,0] = 1
    BH = np.transpose(B).conjugate()

    Qx = np.eye(N)
    Qq = np.eye(N_act)

if np.linalg.norm(Qx.imag) == 0:
    Qx = Qx.real
if np.linalg.norm(Qq.imag) == 0:
    Qq = Qq.real
if np.linalg.norm(BH.imag) == 0:
    BH = BH.real

# Qx, Qq and BH are pure real

###########
# ALGORITHM
###########

# INIT
X_0 = 2 * np.ones(shape=(1, N))
#X_0 = - np.dot(np.linalg.inv(Qq), np.transpose(B).conjugate()) / l**2
print('X_0 shape : ', X_0.shape)

t_forward_0 = np.linspace(t0, T, N_step)
t_backward_0 = np.linspace(T, t0, N_step)


# need to loop x_0 on all the row of - Qq^(-1) B* / l^2 to get the full matrix K
for row in [0]: #np.arange(X_0.shape[0]):
    x_0 = X_0[row, :]
    print(np.linalg.norm(x_0))

    # initial iteration control signal for all time iteration
    q = (-1 + 1j) * np.random.rand(N_step , 2*(Nx*Nz+Nx*Nz-1+2))
    #q = (1 + 1j) * np.zeros((N_step , 2*(Nx*Nz+Nx*Nz-1+2))) 
    q_previous = (-1 + 1j) * np.zeros(shape=(N_step , 2*(Nx*Nz+Nx*Nz-1+2)))
    print(np.linalg.norm(q))
    
    # serie of the cost function at each iteration
    J = []

    # to replace with a while loop and condition on J
    max_iter = 50
    for iteration in np.arange(0, max_iter):
        print('\nITERATION ', iteration)
        print("T : ", T)

        t_forward = np.linspace(t0, T, N_step)
        t_backward = np.linspace(T, t0, N_step)
        
        if False:
            q_interp = scipy.interpolate.interp1d(t_forward,
                            q,
                            kind='cubic',
                            axis=0,
                            bounds_error=False,
                            fill_value="extrapolate")
        else:
            # saving q into files is slower (2x for 4x15x4 and q random),
            # as it always requires to read file
            # however, it might solve the RAM limitation problem on big data
            # but q is relatively small, so it is perhaps not necessary

            # ALSO: would it be more interesting to save q2 = Bq instead of q ?
            # and operate the calculate for q2 instead ?
            # it saves a dot product at each iteration
            q_interp = interpolate.interpolate(filename_storage,
                                               var = 'q',
                                               var_data = q,
                                               timesteps = t_forward)

        ###########################################################################
        # 1. FORWARD DIRECT SOLUTION
        #    SOLVE dx/dt = A x + B q, x(0) = x_0
        #
        # Notes:
        # - break the integration into different steps if x(t) for t [0, T]
        #   is too big, and save step by step eahc x(t)
        #
        # solve_ivp:
        # - Using a matrix for jac input is faster with BDF.
        #   This is not supported with LSODA solver.
        # - For stiff problems, either LSODA, Radau or BDF
        #   BUT: LSODA, Radau can not cast complex numbers
        #   CCL: use BDF for stiff problems
        #
        # Odeint, ode:
        # - for complex : odeint -> odeintw, and ode -> complex_ode
        # - complex_ode has a bug in passing arguments.
        # - TO use odeintw with complex, make y0 also complex (* (1 + 1j) ).
            
        ###########################################################################
        #print("--------------- DIRECT FORWARD ---------------")
        
        def direct_eq(t, x):
            return np.dot(A, x) + np.dot(B, q_interp(t))
        
        sol = spi.solve_ivp(fun = direct_eq,
                            t_span = (0,T),
                            y0 = x_0,
                            method = 'BDF',
                            t_eval = t_forward,
                            jac = A)
        x = sol.y.T # shape [N_step, N]

        if iteration == 0:
            x_iteration0 = x.copy()
            q_iteration0 = q.copy()

        if False:
            # ODEINTW solver
            def direct_eq_bis(x, t, A, B, q_interp):
                return np.dot(A, x) + np.dot(B, q_interp(t))

            def direct_jac_bis(x, t, A, B, q_interp):
                return A

            x, infodict1 = odeintw(func=direct_eq_bis,
                                   y0=x_0,
                                   t=t_forward,
                                   args=(A, B, q_interp),
                                   Dfun=direct_jac_bis,
                                   full_output=True,
                                   mxstep=5000)
            # x is an array of the shape: [timestep, state]
      
        x_interp = interpolate.interpolate(filename_storage,
                                       var = 'x',
                                       var_data = x,
                                       timesteps = t_forward)
        
        ###########################################################################
        # 2. COMPUTE COST FUNCTION
        #    J = int(0 to T) x* Qx x + l**2 q* Qq q
        ###########################################################################

        def inner_product_period(a, b, M=None):
            """
            a : R  -> R^N
                t |-> a(t)
            b : R -> R^N
                t |-> b(t)

            In discrete time, a, b are arrays of size (N, N_t)
            with N_t the number of tiem discretisation steps.

            < a, b >_M = int(0, T) a*(t) M b(t) dt
                       = sum(tk = 0:N_steps) a*(t_k) M b(t_k)
            where * is the conjugate transpose operator.
            """
            Nt = a.shape[0]
            J = 0
            for tk in np.arange(Nt):
                J += inner_product(a[tk,:], b[tk,:], M)
            return J / Nt # * T

        def inner_product(a, b, M=None):
            """
            return a* M b. or a* b is M is None
            """
            if M is None:
                return np.dot(np.conjugate(a).T, b)
            else:
                return np.dot(np.conjugate(a).T, np.dot(M, b))

        def norm_period(a, M=None):
            """
            Return a 1D array of the norm of vector a at each timestep.
            """
            Nt = a.shape[0]
            norm_array = np.zeros((Nt), dtype=complex)
            for tk in np.arange(Nt):
                norm_array[tk] = norm(a[tk,:], M)
            return norm_array.real

        def norm(a, M=None):
            return np.sqrt(inner_product(a, a, M)).real

        J_temp = np.sum(norm_period(x, Qx)) / 2 + L * np.sum(norm_period(q, Qq)) / 2
        J.append(J_temp)

        # LIMIT T SUCH THAT X IS FINITE:
        # T given such that T = 0 000 or x(T) = 1e+8 for example

        ###########################################################################
        # 3. CONDITION OF J
        ###########################################################################

        if iteration > 0:
            print("Cost : ", J_temp, " / variation : ", (J[-1] - J[-2]), " / % : ", (J[-1] - J[-2]) / J[-2] )
            
            if False:
                # Increase the period T
                if np.abs((J[-1] - J[-2]) / J[-2]) < tol and T < T_max:
                    print("J stagnates. Increase T.")
                
                    # get a better estimation to increase T ?
                    T = T_factor * T

                    # update N_step and q
                    N_step0 = N_step
                    N_step = int(T_factor * N_step)

                    # USE CONCATENATE
                    q_new = (-1 + 1j) * np.random.rand(N_step , 2*(Nx*Nz+Nx*Nz-1+2))
                    q_previous_new = (-1 + 1j) * np.zeros(shape=(N_step , 2*(Nx*Nz+Nx*Nz-1+2)))
                    q_new[0:N_step0, :] = q.copy()
                    q_previous_new[0:N_step0, :] = q_previous.copy()
                    q = q_new
                    q_previous = q_previous_new

                    #factor /= factor_coef
                    #print("factor : ", factor)

                    continue

            # stop the loop
            elif np.abs((J[-1] - J[-2]) / J[-2]) < tol and T > T_max:
                print("J stagnates. T > T_max.")
                break
        
        ###########################################################################
        # 3. ADJOINT BACKWARD SOLUTION
        #    SOLVE dp/dt = - A* p - Qx x, p(T) = 0
        ###########################################################################
        #print("--------------- ADJOINT BACKWARD ---------------")
        AH = np.transpose(A).conjugate()
        p_final = (0 + 0j) * np.zeros(N)

        def adjoint_eq(t, p):
            return - np.dot(AH, p) - np.dot(Qx, x_interp(t))

        sol_adjoint = spi.solve_ivp(fun = adjoint_eq,
                                       t_span = (T, 0),
                                       y0 = p_final,
                                       method = 'BDF',
                                       t_eval = t_backward,
                                       jac = - AH)
        p = sol_adjoint.y.T

        if False:
            # ODEINTW solver
            def adjoint_eq_bis(p, t, AH, Qx, x_interp):
                return - np.dot(AH, p) - np.dot(Qx, x_interp(t))

            def adjoint_jac_bis(p, t, AH, Qx, x_interp_scipy):
                return - AH

            p_odeintw, infodict3 = odeintw(func=adjoint_eq_bis,
                                 y0=p_final,
                                 t=t_backward,
                                 args=(AH, Qx, x_interp),
                                 Dfun=adjoint_jac_bis,
                                 full_output=True,
                                 mxstep=5000)

        p_interp = interpolate.interpolate(filename_storage,
                                       var = 'p',
                                       var_data = p,
                                       timesteps = t_backward)
        if True:
            indices = np.where ( np.linalg.norm(p, axis=1) > p_max)
            if indices[0].size != 0:
                print("p_max reached")
        if False:
            # Restrict the period T if p diverge over p_max
            indices = np.where ( np.linalg.norm(p, axis=1) > p_max)
            if indices[0].size != 0:
                print("index at which p > p_max : ", indices[0][0])
                print("time at which p > p_max : ", t_backward[indices[0][0]])
                #print(np.linalg.norm(p, axis=1))
                #print(np.linalg.norm(p[indices], axis=1))
                # decreae T
                extra_t = t_backward[indices[0][0]]
                T = T - extra_t

                # update N_step and q
                N_step0 = N_step
                N_step = indices[0][0]
                print("N_step0 : ", N_step0)
                print("N_step : ", N_step)

                # USE CONCATENATE
                q = q[0:N_step, :]
                q_previous = q_previous[0:N_step, :]

                #factor *= factor_coef
                print("factor : ", factor)

                continue

        if True:
            ###########################################################################
            # 5. COMPUTE dJ/dq 
            #    dJ/dq = l^2 Qq q + B* p
            ###########################################################################

            # 4x15x4 : 0.000001
            # replace the factor by using kappa in the Qq matrix instead
            dJdq = L * np.dot(Qq, q.T).T + factor * np.dot(BH, p.T).T # shape = (N_step, N_act)
            #dJdq = factor * np.dot(BH, p.T)).T # shape = (N_step, N_act)

            ###########################################################################
            # 6. CONJUGATE GRADIENT METHOD WITH KNOWN GRADIENT
            #    to define the next value of q (gradient based or other method)
            #   
            #    Semeraro 2013 used a conjugate-gradient method
            #    Pralits 2010 used a steepest descent method
            ###########################################################################
            if True: 
                #print("------------- STEEPEST DESCENT -------------")
                #if iteration == 0:
                r0 = -dJdq

                for tk in np.arange(N_step):
                    #print('tk : ', tk)
                    if tk == 0:
                        continue
                    if tk == 1:
                        print('r0 norm : ', np.linalg.norm(r0[tk, :]))
                    #print('d0 norm : ', np.linalg.norm(d0[tk, :]))
                    #alpha0 = inner_product(r0[tk, :], r0[tk, :]) / inner_product(r0[tk, :], r0[tk, :], L * Qq)
                    alpha0 = inner_product(r0[tk, :], r0[tk, :]) / inner_product(r0[tk, :], r0[tk, :], Qq)
                    alpha0 = 1
                    if alpha0 == 0:
                        print("alpha0 is 0.")
                        alpha0 = 0.01
                    #print("alpha0 : ", alpha0)

                    q_previous[tk, :] = q[tk, :]
                    q[tk, :] = q[tk, :] + alpha0 * r0[tk, :]
                    
                    # for next iteration
                    #r0[tk, :] = r0[tk, :] - alpha0 * np.dot(Qq, r0[tk, :])


            if False:
                print("------------- CONJUGATE GRADIENT -------------")

                if iteration == 0:
                    #r0 = (1 + 1j) * np.random.rand(N_step , 2*(Nx*Nz+Nx*Nz-1+2))
                    #r0 = l**2 * np.dot(Qq, q.T).T
                    r0 = -dJdq
                    d0 = r0.copy()

                    r1 = np.zeros(q.shape, dtype=complex)
                    d1 = np.zeros(q.shape, dtype=complex)

                for tk in np.arange(N_step):
                    #print('tk : ', tk)
                    if tk == 0:
                        continue
                    #print('r0 norm : ', np.linalg.norm(r0[tk, :]))
                    #print('d0 norm : ', np.linalg.norm(d0[tk, :]))
                    alpha0 = inner_product(r0[tk, :], r0[tk, :]) / inner_product(d0[tk, :], d0[tk, :], Qq)
                    #print("alpha0 : ", alpha0)

                    q[tk, :] = q[tk, :] + alpha0 * d0[tk, :]

                    # CG for r1
                    #r1[tk, :] = r0[tk, :] - alpha0 * np.dot(Qq, d0[tk, :])
                    # should be the same with the gradient
                    r1[tk, :] = -dJdq[tk, :] 
                    #print("r1 norm : ", np.linalg.norm(r1[tk,:]))

                    # Fletcher Reeves
                    beta1 = inner_product(r1[tk, :], r1[tk, :]) / inner_product(r0[tk, :], r0[tk, :])
                    # Polak Ribiere
                    #beta1 = inner_product(r1[tk, :] - r0[tk, :], r1[tk, :]) / inner_product(r0[tk, :], r0[tk, :])
                    #print("beta1 : ", beta1)

                    d1[tk, :] = r1[tk, :] + beta1 * d0[tk, :]
                    #print("d1 norm: ", np.linalg.norm(d1[tk, :]))

                    r0[tk, :] = r1[tk, yy:].copy()
                    d0[tk, :] = d1[tk, :].copy()
                
            #print("d0 norm : ", np.linalg.norm(d0[1, :]))
            #print("d1 norm : ", np.linalg.norm(d1[1, :]))
            #print("r1 norm : ", np.linalg.norm(r1[1, :]))
            #print("r0 norm : ", np.linalg.norm(r0[1, :]))

    ###########################################################################
    # PLOT
    ###########################################################################
        
    # DIRECT
    if True:
        print("--------------- DIRECT PRINT ---------------")
        plt.figure()
        #plt.plot(x[:, :].real, x[:, :].imag, ':', label='forward')
        #plt.plot(x[-1, :].real, x[-1, :].imag, '+') # final position
        #plt.plot(x_back[:, :].real, x_back[:, :].imag, '--', label='backward')
        #plt.plot(x_back[-1, :].real, x_back[-1, :].imag, 'o') # final position
        
        plt.plot(t_forward, np.linalg.norm(x, axis=1), '+--', color='r', label='x')
        plt.plot(t_forward_0, np.linalg.norm(x_iteration0, axis=1), 'x-', color='r', label='x iteration0')
        #plt.plot(t_backward, np.linalg.norm(p, axis=1), '+--', color='k', label='p')
        plt.plot(t_forward, np.linalg.norm(q, axis=1), '+--', color='g', label='q')
        plt.plot(t_forward_0, np.linalg.norm(q_iteration0, axis=1), 'x-', color='g', label='q iteratiom0')
        #plt.plot(t_forward, np.linalg.norm(q_previous, axis=1), '+--', color='g', label='q_previous')
        
        #plt.plot(ts1, x[:, 0].imag, '+--', color='b', label='direct forward imag')
        #plt.plot(ts2, x_back[:, :].real, 'x', color='b', label='direct backward real')
        #plt.plot(ts2, x_back[:, 0].imag, 'x', color='b', label='direct backward imag')
        plt.legend(loc='best')
        plt.show()

    # ADJOINT
    if False:
        print("--------------- ADJOINT PRINT ---------------")
        plt.figure()
        ax = plt.gca()
        #ax.set_ylim([-10, +10])
        plt.plot(ts3, p[:, :].real, '+--', color='r', label='adjoint backward real')
        #plt.plot(ts3, p[:, :].imag, '+--', color='b', label='adjoint backward imag')
        #plt.plot(ts4, p_for[:, :].real, 'x', color='b', label='adjoint forward real')
        #plt.plot(ts4, p_for[:, :].imag, 'x', color='b', label='adjoint forward imag')
        plt.show()
        
    #print(J)
    
    plt.figure()
    plt.plot(np.arange(max_iter), J, 'x-')
    #plt.plot(t_forward, norm_period(x))
    plt.show()
