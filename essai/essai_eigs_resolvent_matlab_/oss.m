% State-space formulation of Orr-Sommerfeld-Squire model
% for Poisseulle channel flow
% dx/dt = Ax
% Energy density E = y1'y1
% [u' v' w' p']'=Cuvw*x
%
% Uses Weiderman & Reddy BCs / diff. matrices
%
% A Sharma 2004

function [A, Ce, Cuvw, y, W] = oss(N,Re,kx,kz,Lx,Lz)

% a = af * exp(i*kx*2*pi/Lx*x) transformed to easier a = af*exp(i*kx*x)
kx = kx*2*pi/Lx;
kz = kz*2*pi/Lz;

% Incorporate homogeneous Neumann boundary conditions on D
% for first derivative - when applied to v
[~, DM] = chebdif(N,2);
% 2nd deriv matrix; +bc
D1 = DM(2:N-1,2:N-1,1);
D2 = DM(2:N-1,2:N-1,2);
% 4th deriv matrix; clamped boundary conditions
[y, D4] = cheb4c(N);
% returns y without endpoints

% Mean flow U(y) (rescaled to 1 at centreline)
U = diag(1-y.*y);
dUdy = diag(-2*y);
d2Udy2 = -2;
% update to turbulent mean

m=N-2;
I=eye(m);
Z=zeros(m);
k2 = (kx*kx + kz*kz);

% form OSS operator
Lap = D2 - k2*I;
L2 = D4 + k2*k2*I - 2*k2*D2;

Mos = -i*kx*U*Lap + i*kx*d2Udy2*I + L2/Re;
Mc = -i*kz*dUdy;
Msq = -i*kx*U + (D2 - k2*I)/Re;
A = [Lap\Mos zeros(m); Mc Msq];

% [u' v' w']'=Cuvw*x
Cuvw = [ (i/k2)*[(kx*D1) (-kz*I)]  ; [I Z]; (i/k2)*[(kz*D1) (kx*I)] ];

%-------
% define energy norm
% Omega
% Not looking at wall here
% Energy density, x'Qx
[~,dy] = clencurt(N);
W = diag(sqrt(dy(2:end-1)));
W = [W Z Z; Z W Z; Z Z W];
Ce = W*Cuvw;
