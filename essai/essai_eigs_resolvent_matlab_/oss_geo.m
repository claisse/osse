clear

import chebdif.*
import cheb4c.*

% Constants
Re = 10000;
N = 50;
m = N-2;

% DIFFERENCE WITH BETA WITH ATI TO INVESTIGATE
alpha = 1;
beta = 0;

% DIFFERENTIATION MATRICES
[~, DM] =  chebdif(N, 4);
D1 = DM(2:N-1,2:N-1,1);
D2 = DM(2:N-1,2:N-1,2);
%D4 = DM(2:N-1,2:N-1,4);
[y, D4] = cheb4c(N);

% DEFINITION OF LAMINAR SOLUTION AND DERIVATIVES
%U = (ones(N,1) - y.^2)';
%dUdy = (-2 * y)';
%d2Udy2 = (-2*ones(N,1))';
U = diag(1-y.*y);
dUdy = diag(-2*y);
d2Udy2 = -2 * eye(m);

% LAPLACIAN and Los
k2 = (alpha*alpha + beta*beta);

Lap = D2 - k2 * eye(m);
Lap2 = D4 - 2*k2*D2 + k2*k2*eye(m);

Los = + 1i * alpha * d2Udy2 * eye(m)...
      + Lap2 / Re...
      - 1i * alpha * U * Lap;
  
% EIGENVALUES
eig_mat = eig(Los, Lap);
%w = 1;
%eig = eig(Los, - i * w * Lap);
       
% MATRICES WITH FUNCTION FROM ATI
% AND EIGENVALUES
[A_ati, Los_ati, Lap_ati] = OSS_ati(N, Re, alpha, beta);
eig_ati = eig(Los_ati, Lap_ati);

% EIGENVALUES FROM BOOK OF SHMID HENNINGSON
%eigenvalues = eigen(alpha, beta);

%%% matrices calculated with python
%L = load(strcat('L', num2str(N), '.mat'));
%Los_2 = L.Los;
%Lap_2 = L.Lap;
%OS2 = L.OS;

%eig2 = eig(Los2);
%eigOS2 = eig(OS);

%%% Values calculated with scipy
%L2 = load(strcat('eig', num2str(N), '.mat'));
%eigs = L2.scipy;

figure
plot(real(eig_mat), imag(eig_mat), '+', 'DisplayName', 'eig Geoffroy');
hold on;
%plot(real(eigenvalues), imag(eigenvalues), 'o', 'DisplayName', 'schmid book');
plot(real(eig_ati), imag(eig_ati), '.', 'DisplayName', 'eig Ati');
%plot(real(eig2), imag(eig2), 'x', 'DisplayName', 'eig python matrices');
%plot(real(eigs), imag(eigs), 'o', 'DisplayName','eig scipy');
xlabel('real')
ylabel('imag')
axis([-0.5 0.1 -1 0]) 

grid on
legend('show')

function [A, Mos, Lap] = OSS_ati(N,Re,alpha,beta)
% Function from Ati Sharma to solve OSS equation

% a = af * exp(i*kx*2*pi/Lx*x) transformed to easier a = af*exp(i*kx*x)
%kx = kx*2*pi/Lx;
%kz = kz*2*pi/Lz;
kx = alpha;
kz = beta;

% Incorporate homogeneous Neumann boundary conditions on D
% for first derivative - when applied to v
[~, DM] = chebdif(N,4);
% 2nd deriv matrix; +bc
D1 = DM(2:N-1,2:N-1,1);
D2 = DM(2:N-1,2:N-1,2);

% 4th deriv matrix; clamped boundary conditions
[y, D4] = cheb4c(N);
%D4 = DM(2:N-1,2:N-1,4);
%[y, ~] = cheb4c(N);

% returns y without endpoints

% Mean flow U(y) (rescaled to 1 at centreline)
U = diag(1-y.*y);
dUdy = diag(-2*y);
d2Udy2 = -2;
% update to turbulent mean

m=N-2;
I=eye(m);
Z=zeros(m);
k2 = (kx*kx + kz*kz);

% form OSS operator
Lap = D2 - k2*I;
L2 = D4 + k2*k2*I - 2*k2*D2;

Mos = -1i*kx*U*Lap + 1i*kx*d2Udy2*I + L2/Re;
Mc = -1i*kz*dUdy;
Msq = -1i*kx*U + (D2 - k2*I)/Re;
A = [Lap\Mos zeros(m); Mc Msq];
end

function [eigenvalues] = eigen(alpha, beta)
% Just the eigenvalues found by Schmid and Henningson in their book
if alpha == 1 && beta == 0
    eigenvalues =  [0.3121-1i*0.01979;
                0.42418-1i*0.0767;
                0.9207-1i*0.07804;
                0.9209-1i*0.0782;
                0.8571-1i*0.1399;
                0.8575-1i*0.1403;
                0.7939-1i*0.2019;
                0.7941-1i*0.2023;
                0.6392-1i*0.22134;
                0.5344-1i*0.22356];
end
if alpha == 0.25 && beta == 3
    eigenvalues =  [0.5639-1i*0.08548;
                0.8379-1i*0.14010;
                0.8449-1i*0.14965;
                0.5871-1i*0.15289;
                0.7273-1i*0.22753;
                0.7333-1i*0.28917;
                0.6354-1i*0.36625;
                0.6225-1i*0.41560;
                0.6467-1i*0.44988;
                0.6626-1i*0.56491];
end
if alpha == 0.5 && beta ==1
    eigenvalues =  [0.3722-1i*0.03737;
                    0.4993-1i*0.0992;
                    0.8877-1i*0.1094;
                    0.8880-1i*0.1096;
                    0.7953-1i*0.1933;
                    0.7983-1i*0.1965;
                    0.7264-1i*0.2609;
                    0.6477-1i*0.2697;
                    0.7047-1i*0.2987;
                    0.4332-1i*0.3065];
end
end
