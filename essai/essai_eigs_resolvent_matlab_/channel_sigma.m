% Singular values at particular kx, kz, Re, c, with resn N
% USV' = G(jw, kx, kz, Re)
% function [U,S,V] = channel_sigma(kx,kz,Re,N,c);
% A S Sharma 2006
function [U_phys, S , V_phys, y] = channel_sigma(kx,kz,Re,N,c);

[A, Ce, Cuvw, y, W] = oss(N,Re,kx,kz,2*pi,2*pi);

I = eye(size(A));

[U,S,V] = svd( Ce*inv(-i*kx*c*I - A)*pinv(Ce) );
S=diag(S);

% velocities recovered by
U_phys = W'\U;
V_phys = W'\V;
% [u' v' w']'=Cuvw*x
