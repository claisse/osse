import numpy as np
from scipy.sparse.linalg import svds
from scipy.sparse.linalg import LinearOperator

from matplotlib import pyplot as plt
import chebyshev

N = 50
rank = N-2
j = np.complex(0,1)
I = np.eye(N)
kx = 1

# TEST 1
y, DM = chebyshev.chebdiff(N, 2)
D = DM[0,:,:]

# TEST 2
# WHY DOESNT IT WORK ?
#D = I

#TEST 3
#D = np.random.rand(N, N)

D = D #+ 1*j*D

D2 = D * j * kx #+ j * kx * I

#######
def deriv(x):
    # np.dot(x.T, D.T) works for x (N) and (N,1)
    # np.dot(x, D.T) works only for x (N) as x = x.T
    #return np.dot(x.T, D.T)
    return np.dot(D,x)

def deriv_H(x):
    # TEST WITH D + i.KX TO CHECK HOW TO OPERATE THE CONJUGATE
    # IF conjugate(D + i.kx) the same as conjugate(D) - i.kx
    # HOW DOES THE TRANSPOSE AFFECT THAT ? it shouldnt 
    return np.dot(np.conjugate(D.T), x)
    # OR :
    #return np.dot(x.T, np.conjugate(D))

######

def fct(v):
    return deriv(v) * j * kx #+ np.dot(j * kx * I, v)

def fct_adjoint(v):
    return deriv_H(v) * (-j) * kx #- np.dot(j * kx * I, v)
    #return deriv(v)

lo = LinearOperator((N,N),
                    matvec = fct,
                    rmatvec = fct_adjoint,
                    dtype = complex)

# If only one of the following parameter is complex
# dtype or D or vector
# the svds produces some errors
# in the imaginary part
# of order 1E-11 (N=50) or 1E-9 (N=100)
# if all three parameters are complex,
# errors does not increase

############################
# SVDS MATRIX

u,s,v = svds(D2, rank)
print(s)

ss = np.diagflat(s)
M = np.dot(u, np.dot(ss, v))

#print('-----')
#print(M.real)
#print('-----')
#print(M.imag)

vector_real = np.linspace(0,N-1, N)
vector_imag = 2 * np.linspace(0,N-1, N)
vector = vector_real + j * vector_imag

x1 = np.dot(D2, vector)
x2 = np.dot(M, vector)

############################
# SVDS LINEAR OPERATOR

u_lo, s_lo, v_lo = svds(lo, rank)
print(s_lo)

ss_lo = np.diagflat(s_lo)
M_lo = np.dot(u_lo, np.dot(ss_lo, v_lo))

#print('-----')
#print(M_lo.real)
#print('-----')
#print(M_lo.imag)

x1_lo = lo.matvec(vector)
x2_lo = np.dot(M_lo, vector)

############################

f, axarr = plt.subplots(2, sharex=True)

axarr[0].plot(vector.real, x1.real, 'r+-')
axarr[0].plot(vector.real, x2.real, 'r+--')
axarr[0].plot(vector.real, x1_lo.real, 'gx-')
axarr[0].plot(vector.real, x2_lo.real, 'gx--')
axarr[0].set_title('Real part (matrix in red, LO in green, - direct, -- SVDS)')

axarr[1].plot(vector.real, x1.imag, 'r+-')
axarr[1].plot(vector.real, x2.imag, 'r+--')
axarr[1].plot(vector.real, x1_lo.imag, 'gx-')
axarr[1].plot(vector.real, x2_lo.imag, 'gx--')
axarr[1].set_title('Imaginary part')

plt.tight_layout()
plt.show()
