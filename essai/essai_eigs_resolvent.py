import numpy as np
import scipy
import sympy
import scipy.io

from scipy.sparse.linalg import aslinearoperator
from scipy.sparse.linalg import eigs
from scipy.sparse.linalg import LinearOperator

import chebyshev

N = 10 # should be 4, 20, 50, 100, 300
max_order = 4

alpha = 1
beta = 1
k2 = (alpha**2 + beta**2)

j = np.complex(0,1)

option = 1
#option 1: building the differentiation matrix DM for a given order
if option == 1:
    if 1:
        # usage of package chebyshev, but I add a file with the matrix inside
        nodes, _DM = chebyshev.chebdiff(N, max_order)
        order = 1
        _DM[:,0,:] = 0
        _DM[:,N-1,:] = 0
        _DM[:,:,0] = 0
        _DM[:,:,N-1] = 0
        DM = _DM[order-1,:,:]

        laplacian_mat = (_DM[1,:,:] - k2 * np.eye(N))
        #laplacian_mat[0,:] = 0
        #laplacian_mat[N-1,:] = 0
        #laplacian_mat[:,0] = 0
        #laplacian_mat[:,N-1] = 0
        
        #laplacian_mat = np.eye(N)
        
        print('det ', np.linalg.det(laplacian_mat))

        #outfile = TemporaryFile()
        #np.save('DM4', DM)

        scipy.io.savemat('files_essai_eigs_resolvent/DM10.mat', mdict={'DM':DM})
        scipy.io.savemat('files_essai_eigs_resolvent/LAP10.mat', mdict={'LAP':laplacian_mat})
    if 0:
        # loading the matrix from the file
        # uncomment depending on N
        DM = np.load('files_essai_eigs_resolvent/DM4.npy')
        #DM = np.load('files_essai_eigs_resolvent/DM20.npy')
        #DM = np.load('files_essai_eigs_resolvent/DM50.npy')
        #DM = np.load('files_essai_eigs_resolvent/DM100.npy')
        #DM = np.load('files_essai_eigs_resolvent/DM300.npy')
        print('\n', DM.shape,'\n')
        print(DM)
        print(DM.flags)
        print(DM.itemsize)
        print(DM.nbytes)
        print(DM[0,2].dtype)
    if 0 and N == 4:
        DM = np.array( [[ 3.16666666667, -4.           ,  1.33333333333, -0.5          ],
                        [ 1.            , -0.33333333333, -1.           ,  0.33333333333],
                        [-0.33333333333 ,  1.           ,  0.33333333333, -1.           ],
                        [ 0.5           , -1.33333333333,  4.           , -3.16666666667]])
        print(DM)
        print(DM.dtype)
        print(DM.flags)
        print(DM.itemsize)
        print(DM.nbytes)
        print(DM[0,2].dtype)
    if 0 and N == 20:
        DM = np.array([[  1.20500000e+02,  -1.46641579e+02,   3.69121112e+01,  -1.65938957e+01,
            9.48498924e+00,  -6.19735294e+00,   4.41450584e+00,  -3.34277905e+00,
            2.65071111e+00,  -2.18002504e+00,   1.84743964e+00,  -1.60579952e+00,
            1.42684350e+00,  -1.29286815e+00,   1.19240564e+00,  -1.11785519e+00,
            1.06412766e+00,  -1.02784576e+00,   1.00686617e+00,  -5.00000000e-01],
         [  3.66603947e+01,  -1.82043391e+01,  -2.46645244e+01,   9.35562635e+00,
           -5.07045925e+00,   3.23541112e+00,  -2.27576260e+00,   1.71037859e+00,
           -1.34975387e+00,   1.10646157e+00,  -9.35505635e-01,   8.11789264e-01,
           -7.20431651e-01,   6.52184067e-01,  -6.01090544e-01,   5.63221053e-01,
           -5.35953056e-01,   5.17550514e-01,  -5.06913641e-01,   2.51716543e-01],
         [ -9.22802780e+00,   2.46645244e+01,  -4.48553318e+00,  -1.50730689e+01,
            6.38256865e+00,  -3.72390007e+00,   2.50708827e+00,  -1.83782375e+00,
            1.42789470e+00,  -1.15842922e+00,   9.72387513e-01,  -8.39417199e-01,
            7.42108052e-01,  -6.69897628e-01,   6.16105435e-01,  -5.76382912e-01,
            5.47857851e-01,  -5.28643355e-01,   5.17550514e-01,  -2.56961440e-01],
         [  4.14847392e+00,  -9.35562635e+00,   1.50730689e+01,  -1.94122100e+00,
           -1.10701219e+01,   4.94578970e+00,  -3.00728732e+00,   2.09302085e+00,
           -1.57731626e+00,   1.25487140e+00,  -1.03944367e+00,   8.88921097e-01,
           -7.80537043e-01,   7.01054852e-01,  -6.42361703e-01,   5.99299685e-01,
           -5.68521800e-01,   5.47857851e-01,  -5.35953056e-01,   2.66031914e-01],
         [ -2.37124731e+00,   5.07045925e+00,  -6.38256865e+00,   1.10701219e+01,
           -1.04589176e+00,  -8.93983100e+00,   4.12894955e+00,  -2.58101093e+00,
            1.83940175e+00,  -1.41530564e+00,   1.14715754e+00,  -9.66532837e-01,
            8.39746242e-01,  -7.48453321e-01,   6.81931903e-01,  -6.33600726e-01,
            5.99299685e-01,  -5.76382912e-01,   5.63221053e-01,  -2.79463797e-01],
         [  1.54933823e+00,  -3.23541112e+00,   3.72390007e+00,  -4.94578970e+00,
            8.93983100e+00,  -6.25618413e-01,  -7.67262955e+00,   3.62862942e+00,
           -2.31590799e+00,   1.68151380e+00,  -1.31603031e+00,   1.08369712e+00,
           -9.26803818e-01,   8.16840153e-01,  -7.38245393e-01,   6.81931903e-01,
           -6.42361703e-01,   6.16105435e-01,  -6.01090544e-01,   2.98101409e-01],
         [ -1.10362646e+00,   2.27576260e+00,  -2.50708827e+00,   3.00728732e+00,
           -4.12894955e+00,   7.67262955e+00,  -3.90204711e-01,  -6.88455202e+00,
            3.31716029e+00,  -2.15346073e+00,   1.58849295e+00,  -1.26193531e+00,
            1.05413668e+00,  -9.14163422e-01,   8.16840153e-01,  -7.48453321e-01,
            7.01054852e-01,  -6.69897628e-01,   6.52184067e-01,  -3.23217037e-01],
         [  8.35694763e-01,  -1.71037859e+00,   1.83782375e+00,  -2.09302085e+00,
            2.58101093e+00,  -3.62862942e+00,   6.88455202e+00,  -2.39491944e-01,
           -6.40164138e+00,   3.13365595e+00,  -2.06494342e+00,   1.54516300e+00,
           -1.24472416e+00,   1.05413668e+00,  -9.26803818e-01,   8.39746242e-01,
           -7.80537043e-01,   7.42108052e-01,  -7.20431651e-01,   3.56710874e-01],
         [ -6.62677777e-01,   1.34975387e+00,  -1.42789470e+00,   1.57731626e+00,
           -1.83940175e+00,   2.31590799e+00,  -3.31716029e+00,   6.40164138e+00,
           -1.30613948e-01,  -6.13850399e+00,   3.04817798e+00,  -2.03678028e+00,
            1.54516300e+00,  -1.26193531e+00,   1.08369712e+00,  -9.66532837e-01,
            8.88921097e-01,  -8.39417199e-01,   8.11789264e-01,  -4.01449881e-01],
         [  5.45006260e-01,  -1.10646157e+00,   1.15842922e+00,  -1.25487140e+00,
            1.41530564e+00,  -1.68151380e+00,   2.15346073e+00,  -3.13365595e+00,
            6.13850399e+00,  -4.15731747e-02,  -6.05478279e+00,   3.04817798e+00,
           -2.06494342e+00,   1.58849295e+00,  -1.31603031e+00,   1.14715754e+00,
           -1.03944367e+00,   9.72387513e-01,  -9.35505635e-01,   4.61859911e-01],
         [ -4.61859911e-01,   9.35505635e-01,  -9.72387513e-01,   1.03944367e+00,
           -1.14715754e+00,   1.31603031e+00,  -1.58849295e+00,   2.06494342e+00,
           -3.04817798e+00,   6.05478279e+00,   4.15731747e-02,  -6.13850399e+00,
            3.13365595e+00,  -2.15346073e+00,   1.68151380e+00,  -1.41530564e+00,
            1.25487140e+00,  -1.15842922e+00,   1.10646157e+00,  -5.45006260e-01],
         [  4.01449881e-01,  -8.11789264e-01,   8.39417199e-01,  -8.88921097e-01,
            9.66532837e-01,  -1.08369712e+00,   1.26193531e+00,  -1.54516300e+00,
            2.03678028e+00,  -3.04817798e+00,   6.13850399e+00,   1.30613948e-01,
           -6.40164138e+00,   3.31716029e+00,  -2.31590799e+00,   1.83940175e+00,
           -1.57731626e+00,   1.42789470e+00,  -1.34975387e+00,   6.62677777e-01],
         [ -3.56710874e-01,   7.20431651e-01,  -7.42108052e-01,   7.80537043e-01,
           -8.39746242e-01,   9.26803818e-01,  -1.05413668e+00,   1.24472416e+00,
           -1.54516300e+00,   2.06494342e+00,  -3.13365595e+00,   6.40164138e+00,
            2.39491944e-01,  -6.88455202e+00,   3.62862942e+00,  -2.58101093e+00,
            2.09302085e+00,  -1.83782375e+00,   1.71037859e+00,  -8.35694763e-01],
         [  3.23217037e-01,  -6.52184067e-01,   6.69897628e-01,  -7.01054852e-01,
            7.48453321e-01,  -8.16840153e-01,   9.14163422e-01,  -1.05413668e+00,
            1.26193531e+00,  -1.58849295e+00,   2.15346073e+00,  -3.31716029e+00,
            6.88455202e+00,   3.90204711e-01,  -7.67262955e+00,   4.12894955e+00,
           -3.00728732e+00,   2.50708827e+00,  -2.27576260e+00,   1.10362646e+00],
         [ -2.98101409e-01,   6.01090544e-01,  -6.16105435e-01,   6.42361703e-01,
           -6.81931903e-01,   7.38245393e-01,  -8.16840153e-01,   9.26803818e-01,
           -1.08369712e+00,   1.31603031e+00,  -1.68151380e+00,   2.31590799e+00,
           -3.62862942e+00,   7.67262955e+00,   6.25618413e-01,  -8.93983100e+00,
            4.94578970e+00,  -3.72390007e+00,   3.23541112e+00,  -1.54933823e+00],
         [  2.79463797e-01,  -5.63221053e-01,   5.76382912e-01,  -5.99299685e-01,
            6.33600726e-01,  -6.81931903e-01,   7.48453321e-01,  -8.39746242e-01,
            9.66532837e-01,  -1.14715754e+00,   1.41530564e+00,  -1.83940175e+00,
            2.58101093e+00,  -4.12894955e+00,   8.93983100e+00,   1.04589176e+00,
           -1.10701219e+01,   6.38256865e+00,  -5.07045925e+00,   2.37124731e+00],
         [ -2.66031914e-01,   5.35953056e-01,  -5.47857851e-01,   5.68521800e-01,
           -5.99299685e-01,   6.42361703e-01,  -7.01054852e-01,   7.80537043e-01,
           -8.88921097e-01,   1.03944367e+00,  -1.25487140e+00,   1.57731626e+00,
           -2.09302085e+00,   3.00728732e+00,  -4.94578970e+00,   1.10701219e+01,
            1.94122100e+00,  -1.50730689e+01,   9.35562635e+00,  -4.14847392e+00],
         [  2.56961440e-01,  -5.17550514e-01,   5.28643355e-01,  -5.47857851e-01,
            5.76382912e-01,  -6.16105435e-01,   6.69897628e-01,  -7.42108052e-01,
            8.39417199e-01,  -9.72387513e-01,   1.15842922e+00,  -1.42789470e+00,
            1.83782375e+00,  -2.50708827e+00,   3.72390007e+00,  -6.38256865e+00,
            1.50730689e+01,   4.48553318e+00,  -2.46645244e+01,   9.22802780e+00],
         [ -2.51716543e-01,   5.06913641e-01,  -5.17550514e-01,   5.35953056e-01,
           -5.63221053e-01,   6.01090544e-01,  -6.52184067e-01,   7.20431651e-01,
           -8.11789264e-01,   9.35505635e-01,  -1.10646157e+00,   1.34975387e+00,
           -1.71037859e+00,   2.27576260e+00,  -3.23541112e+00,   5.07045925e+00,
           -9.35562635e+00,   2.46645244e+01,   1.82043391e+01,  -3.66603947e+01],
         [  5.00000000e-01,  -1.00686617e+00,   1.02784576e+00,  -1.06412766e+00,
            1.11785519e+00,  -1.19240564e+00,   1.29286815e+00,  -1.42684350e+00,
            1.60579952e+00,  -1.84743964e+00,   2.18002504e+00,  -2.65071111e+00,
            3.34277905e+00,  -4.41450584e+00,   6.19735294e+00,  -9.48498924e+00,
            1.65938957e+01,  -3.69121112e+01,   1.46641579e+02,  -1.20500000e+02]])

#option 2: building a random matrix
elif option == 2:
    np.random.seed(0)
    Real = np.random.random((N, N)) - 0.5
    Im = np.random.random((N,N)) - 0.5

    # If I want DM symmetric:
    #Real = np.dot(Real, Real.T)
    #Im = np.dot(Im, Im.T)

    DM = Real
    for i in range(N):
        DM[i,i] = 0

    # If I want DM singular:
    #DM[0,:] = DM[1,:]

    laplacian_mat = np.eye(N)

print(np.round(DM, 3), '\n')
print(np.round(laplacian_mat, 1))

# Test DM symmetric
print('Is DM symmetric ? \n', (DM.transpose() == DM).all() )        
# Test DM Hermitian
print('Is DM hermitian ? \n', (DM.transpose().real == DM.real).all() and
                                        (DM.transpose().imag == -DM.imag).all() )  

# building a linear operator which wrap matrix DM
laplacian_inv = scipy.sparse.linalg.factorized(laplacian_mat)
def DM_LAP1(v):
    retour = np.dot(DM,v)
    return laplacian_inv(retour)
linop_DM_LAP1 = LinearOperator( (N, N), matvec = DM_LAP1)

#-------
def derivative(v):
    return np.dot(DM, v)
linop_DM = LinearOperator( (N,N), matvec = derivative)

def laplacian(v):
    return np.dot(laplacian_mat, v)
linop_LAP = LinearOperator( (N, N), matvec = laplacian)

def DM_LAP2(v):
    retour = linop_DM.matvec(v)
    sol, info = scipy.sparse.linalg.gmres(linop_LAP, retour)
    return sol
linop_DM_LAP2 = LinearOperator( (N, N), matvec = DM_LAP2)

# building a linear operator directly from a matrix DM with asLinearOperator
aslinop_DM = aslinearoperator(DM)

# comparison of LinearOperator and direct Dot Product
V = np.random.random((N))
diff_lo = linop_DM.matvec(V)
diff_mat = np.dot(DM, V)
# diff_lo and diff_mat are equals

# FINDING EIGENVALUES

#number of eigenvalues to find
k = 3#N-2
if 1:
    # SCIPY SPARSE LINALG LINEAR OPERATOR
    vals_sparse1, vecs1 = scipy.sparse.linalg.eigs(linop_DM_LAP2, k, which='SR',
                            maxiter = 100000,
                            tol = 1E-3)
                            #M = linop_LAP)
    #vals_sparse1 = np.sort(vals_sparse1)
    print('\nEigenvalues (scipy.sparse.linalg Linear Operator) : \n', np.round(np.sort(vals_sparse1),5))
    for index in range(k):
        print('Norm :',
            np.linalg.norm(np.dot(DM, vecs1[:,index]) \
                - vals_sparse1[index] * np.dot(laplacian_mat, vecs1[:,index])))
        #print('Norm :', np.linalg.norm(DM - vals_sparse1[index] * np.eye(N)))

if 1:
    # SCIPY SPARSE ARRAY
    vals_sparse2, vecs2 = scipy.sparse.linalg.eigs(DM, k, which='SR',
                            maxiter = 100000,
                            tol = 1E-9,
                            M = laplacian_mat)
    #vals_sparse2 = np.sort(vals_sparse2)
    print('\nEigenvalues (scipy.sparse.linalg with matrix DM) : \n', np.round(np.sort(vals_sparse2),5))
    for index in range(k):
        print('Norm :',
            np.linalg.norm(np.dot(DM, vecs2[:,index]) \
                - vals_sparse2[index] * np.dot(laplacian_mat, vecs2[:,index])))
        #print('Norm :', np.linalg.norm(DM - vals_sparse2[index] * np.eye(N)))

if 1:
    # SCIPY SPARSE AS LINEAR OPERATOR
    vals_sparse3, vecs3 = scipy.sparse.linalg.eigs(aslinop_DM, k, which='SR',
                            maxiter = 100000,
                            tol = 1E-9,
                            M = laplacian_mat)
    #vals_sparse3 = np.sort(vals_sparse3)
    print('\nEigenvalues (scipy.sparse.linalg AS linear Operator) : \n', np.round(np.sort(vals_sparse3),5))
    for index in range(k):
        print('Norm :',
            np.linalg.norm(np.dot(DM, vecs3[:,index]) \
                - vals_sparse3[index] * np.dot(laplacian_mat, vecs3[:,index])))
        #print('Norm :', np.linalg.norm(DM - vals_sparse3[index] * np.eye(N)))

if 0:
    # NUMPY LINALG / SAME RESULT AS SCIPY LINALG
    vals_np = np.linalg.eigvals(DM)
    #vals_np = np.sort(vals_np)
    print('\nEigenvalues (numpy.linalg) : \n', vals_np)

if 1:
    # SCIPY LINALG
    vals_sp, vecs_sp = scipy.linalg.eig(DM, b=laplacian_mat)
    #vals_sp = np.sort(vals_sp)
    print('\nEigenvalues (scipy.linalg.eig) : \n', np.round(np.sort(vals_sp),5))
    for index in range(vals_sp.shape[0]):
        print('Norm :',
            np.linalg.norm(np.dot(DM, vecs_sp[:,index]) \
                - vals_sp[index] * np.dot(laplacian_mat, vecs_sp[:,index])))
        #print('Norm :', np.linalg.norm(DM - vals_sp[index] * np.eye(N)))
        
if 0:
    x = sympy.Symbol('x')
    D = sympy.Matrix(DM)
    print('\ndet D (sympy):', D.det() )
    E = D - x*np.eye(DM.shape[0])
    eig_sympy = sympy.solve(E.det(), x)
    print('\nEigenvalues (sympy) : \n', eig_sympy)
