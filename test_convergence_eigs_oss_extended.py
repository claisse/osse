"""
test_convergence_eigs_oss_extended.py

Code to check the eigenvalues of the OSS operator for a nonlaminar baseflow.

We derive the time-evolution of the nonlaminar baseflow-OSS model
with an Euler time-stepping scheme, with an initial condition
given by an eigenvector.
This time evolution (in log scale) is a function f(x) = a.x + b
where a is the eigenvalue associated to this eigenvector.

We can then compare it with the eigenvalue found by scipy.linalg.sparse.eigs,
to determine the convergence if this eigenvalue.
"""
import numpy as np
from channel import Channel
from operators import Operators as OP
import matplotlib.pyplot as plt
import chebyshev

# Constant definition
j = np.complex(0,1)
Re = 400
Nx = 20
Ny = 35
Nz = 20
size = Nx*Nz + Nx*Nz - 1 + 2

#TODO
# Limit to positive-real eigs
N_eq = {1}#{'SYM', 'ASYM'} #{1, 2, 5, 9, 11, 24}

for n_eq in N_eq:
    # Loading the flowfield
    N_str = str(Nx)+'x'+str(Ny)+'x'+str(Nz)
    print("###############################")
    print('Case : eq', n_eq, ' - ', N_str)
    kind_of_flow = 'eq'+str(n_eq)+'_'+N_str
    filename = 'database/eigenfolder/eq'+str(n_eq)+'/EIGS/'

    ff = Channel('database/eq/'+kind_of_flow+'.h5')
    ff.load_baseflow('database/eq/'+kind_of_flow+'.h5')
    for ny in range(ff.u.shape[2]):
        ff.u0[0, :, ny, :] += ff.y[ny]

    # Loading the eigenvalues calculated by scipy.sparse
    vals = np.load(filename+kind_of_flow+'_EIG.npy')
    vecs = np.load(filename+kind_of_flow+'_VEC.npy')

    # Building the operator correspond to the flowfield
    # du/dt = A . B . u
    op = OP(ff)
    A, B = op.op_nonlaminar_baseflow(ff, Re)
    M_real = np.dot(A, B)
    M_comp = np.dot(A, B)*(-j)

    ##########################
    # TIME STEPPING
    ###########################

    # Constant definition for time-stepping
    dt = 0.001 # resolution
    N_step = 50 # number of tiem steps
    N_eig = 10 # number of maximal real part eigenvalues we want check

    #index_eig = np.argsort(vals.real)[::-1]
    index_eig = range(vals.shape[0])

    for index in index_eig:
        # chosen eigenvalue
        l0 = vals[index]
        # chosen eigenvector
        u0 = vecs[:, index]

        t = np.zeros(shape=(N_step))
        normU_log_real = np.zeros(shape=(N_step))
        normU_log_comp = np.zeros(shape=(N_step))

        # time-stepping loop
        for n in range(N_step):
            if n == 0:
                u_real = u0
                u_comp = u0*(-j)
            else:
                # Euler scheme
                u_real = u_real + dt * np.dot(M_real, u_real)
                u_comp = u_comp + dt * np.dot(M_comp, u_comp)
                t[n] = t[n-1] + dt

            normU_log_real[n] = np.log(op.op_norm(u_real, ff.u.shape[2]))
            normU_log_comp[n] = np.log(op.op_norm(u_comp, ff.u.shape[2]))

        # Fitting curve to get the eigenvalue
        result_real = np.polyfit(t, normU_log_real, 1)
        result_comp = np.polyfit(t, normU_log_comp, 1)

        print('----------')
        print('Eig.real:', l0.real)
        print('Real(t) =', result_real[0], '. t + ...')#, result[1])
        print('Eig.imag:', l0.imag)
        print('Comp(t) =', result_comp[0], '. t + ...')#, result[1])

        # Plot
        #plt.figure()
        #plt.plot(t, normU_log_real, 'r')
        #plt.plot(t, normU_log_comp, 'b')
        #plt.show()
