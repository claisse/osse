"""
operators.py:

Python3 file containing methods
for the OSS to
    build the operator,
    change basis,
    calculate its eigenvalues,
    build its resolvent
for the OSS operator for a nonlaminar baseflow
and
the OSS operator for a nonlaminar baseflow with actuation to
    build the operator,
    calculate its eigenvalues and eigenvectors,
    print and plot the eigenvalues,
    change basis.
"""

import numpy as np
import scipy
import warnings
import itertools
import scipy.io
import datetime
import resource
#from memory_profiler import profile # add @profile to use profiler.
from scipy.sparse.linalg import eigs
import h5py
import os, errno

import matplotlib
matplotlib.use('TkAgg')
from matplotlib import rc
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
#rc('text', usetex=True)
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker #import FuncFormatter, MaxNLocator

from flowfield import FlowField
from flowfield import DiffOperator_FlowField
from channel import Channel
from channel import DiffOperator_Channel
from boundary_layer import Boundary_Layer
from boundary_layer import DiffOperator_Boundary_Layer
import chebyshev

class Operators:

    def __init__(self, ff: FlowField):
        # I should write self.ff = ff
        # and reference to self.ff instead of calling parameter ff
        # in each method.
        self.ff = ff
        pass

    ###############################
    # for ORR-SOMMERFELD SQUIRE MODEL
    ###############################

    def oss_operator(self,
                     ff: FlowField,
                     Re,
                     baseflow,
                     kx, kz,
                     orr=True, squire=True,
                     omega=-1):
        """
        Returns Orr-Sommerfeld-Squire equation operator:

        K_freq . [ Lap  0 ] d [ v ]     = [ Los   0  ] . [ v ]
                 [  0   I ]   [ n ] /dt   [ Lc   Lsq ]   [ n ]

        OR:

        K_freq . d [ v ]     = [ OS   0  ] . [ v ]
                   [ n ] /dt   [ Lc  Lsq ]   [ n ]

        with :
        - Lap     Laplacian operator
        - I       Identity
        - Los     Orr-Sommerfeld operator
        - Lc      Coupling operator
        - Lsq     Squire operator
        - OS      inv(Laplacian)*Orr-Sommerfeld operator
        - K_freq  frequency constant:
                    if omega != -1:
                        K_freq = - 1j . omega = - 1j . alpha . c
                            with omega  frequency
                                 alpha  wave mode
                                 c      wave speed
                    elif omaga = -1:
                        K_freq = 1

        ----------
        Keyword arguments:
        ff:FlowField -- FlowField flow
        Re -- Reynolds number
        baseflow -- U = U(y) = 1 - y**2, V = 0, W = 0
        kx -- mode streamwise (alpha = 2.pi.kx/Lx)
        kz -- mode spanwise (beta = 2.pi.kz/Lz)
        orr -- boolean
            if 'True' : return LinearOperators for Orr-Sommerfeld equation.
            if 'False': Los_lo, Lap_lo, OS_lo are None.
        squire -- boolean
            if 'True' : return LinearOperators for Squire equation.
            if 'False': Lsq_lo, Lc_lo are None.
        omega -- frequency use in the OSS model.
            set to omega = -1 (default) if not in use.

        ----------
        Return:
        Los -- Matrix,
            for Orr-Sommerfeld operator
        Lap -- Matrix,
            for Laplacian operator
        OS -- None, but uncomment code to compute the matrix.
            Matrix, corresponding to
            OS = inv(-1*Lap) . (-1)*Los
        Lsq -- Matrix,
            for Squire operator
        Lc -- Matrix,
            for Coupling operator

        ----------
        REMARKS:
        - Method scipy.sparse.linalg.eigs() returns some errors
            if used with Los_lo and Lap_lo, but works fine with OS_lo.

        - Method scipy.sparse.linalg.eigs() requires
            a generalized eigenvalues problem:
                    A.x = lambda.B.x
            with matrix/LinearOperator B being positive-definite
            (all eigenvalues positive).
            However, as Lap_lo is negative definite,
            we solved that problem by using:
                    (-A).x = lambda.(-B).x

        - Omitting first and last column and row let matrix B
            be non-singular.

        ----------
        VALIDATION:
        - Eigenvalues validated for Orr-Sommerfeld and Squire equations
            in a Channel, with omega = -1
            with table found in:
                Optimal and robust control and estimation
                    of linear paths to transition
                Thomas BEWLEY and Sharon LIU
                J. Fluid Mech 1998, vol 365, pp. 305-349
                Table 1 for alpha=1, beta=0, Re=10000
                Table 2 for alpha=0, beta=2.044, Re=5000
        - Eigenvalue for omaga = +1 validated for Orr-SOmmerfeld equations
            in a Channel
            with table 1 and figure 2 in:
                The eigenvalue spectrum of the Orr-Sommereld problem
                Basil N. ANTAR
                NASA Technical Report NASA TR R-469, 1976
        - No validation has been made for a boundary layer, as the
            differentiation matrices should be changed.
        """
        # Constant definition
        alpha = 2 * np.pi * kx / ff.attributes['Lx']
        beta = 2 * np.pi * kz / ff.attributes['Lz']
        k2 = (alpha**2 + beta**2)
        N = ff.Ny
        I = np.eye(N-2)
        I2 = np.eye(N)
        Z = np.zeros((N-2, N-2))
        if omega == -1:
            K_freq = 1
        else:
            K_freq = -1j * omega

        # Differentiation matrices
        # with Dirichlet BCs on D1 and D2
        # and clamped BCs on D4
        nodes, DM = chebyshev.chebdiff(N, 4)
        D1 = DM[0, :, :]
        D2 = DM[1, :, :]
        #D4 = DM[3, 1:N-1, 1:N-1]
        D4 = chebyshev.cheb4c(N)[1]

        # Baseflow and differentiates
        U = baseflow[0, 0, :, 0]

        # Differentiate U in y-direction with D not reduced,
        # as we dont want to apply boundary condition on the baseflow.
        dUdy = np.dot(D1, U)
        d2Udy2 = np.dot(D2, U)

        if False:
            # this loop imposes Dirichlet BC on D4
            # and clamped BCs on D1 and D2
            # just to compare with the usual BCs
            nodes2, DM2 = chebyshev.chebxc(N)
            D1 = DM2[0,:,:]
            D2 = DM2[1,:,:]
            D4 = DM[3, 1:N-1, 1:N-1]
        else:
            D1 = D1[1:N-1, 1:N-1]
            D2 = D2[1:N-1, 1:N-1]

        U = U[1:N-1]
        dUdy = dUdy[1:N-1]
        d2Udy2 = d2Udy2[1:N-1]

        # Laplacian
        L1 = D2 - k2*I
        L2 = D4 + k2*k2*I - 2*k2*D2

        # For Boundary layer, use baseflow-1 for derivative method
        # as boundaryLayer derivative methods requires data to
        # tend to zero at inifity.

        ##########################################
        # DEFINITION OF ORR-SOMMERFELD OPERATORS #
        ##########################################
        if orr:
            Los = (-1j*alpha*np.einsum('i,ij->ij',U,L1) + 1j*alpha*d2Udy2*I + L2/Re) / K_freq
            Lap = L1
            OS = None  # np.dot(np.linalg.inv(Lap), Los)
        else:
            Los = None
            Lap = L1
            OS = None

        ########################################
        #  DEFINITION OF THE SQUIRE OPERATORS  #
        ########################################
        if squire:
            Lsq = (-1j*alpha*U*I + (D2 - k2*I)/Re) / K_freq
            Lc = (-1j*beta*dUdy*I) / K_freq
        else:
            Lsq = None
            Lc = None

        return Los, Lap, OS, Lsq, Lc

    def oss_operator_actuated(self,
                     ff: FlowField,
                     Re,
                     kx, kz,
                     lifting_function = None,
                     tau_v=1, tau_eta=1):
        """
        Returns actuated Orr-Sommerfeld-Squire equation operator:

        dx/dt = E_f . A_f . x + E_f . B_f . u
        wher E_f and A_f corresponds to :
        [ Lap  0 ] d [ v ]     = [ Los   0  ] . [ v ] + B_f . u
        [  0   I ]   [ n ] /dt   [ Lc   Lsq ]   [ n ]

        where v = [v+, v0, v-] and eta = [eta+, eta0, eta-]

        with :
        - Lap     Laplacian operator
        - I       Identity
        - Los     Orr-Sommerfeld operator
        - Lc      Coupling operator
        - Lsq     Squire operator

        ----------
        Keyword arguments:
        ff:FlowField -- FlowField flow
        Re -- Reynolds number
        baseflow -- U = U(y) = 1 - y**2, V = 0, W = 0
        kx -- mode streamwise (alpha = 2.pi.kx/Lx)
        kz -- mode spanwise (beta = 2.pi.kz/Lz)
        orr -- boolean
            if 'True' : return LinearOperators for Orr-Sommerfeld equation.
            if 'False': Los_lo, Lap_lo, OS_lo are None.
        squire -- boolean
            if 'True' : return LinearOperators for Squire equation.
            if 'False': Lsq_lo, Lc_lo are None.

        ----------
        Return:
        Los -- Matrix,
            for Orr-Sommerfeld operator
        Lap -- Matrix,
            for Laplacian operator
        OS -- None, but uncomment code to compute the matrix.
            Matrix, corresponding to
            OS = inv(-1*Lap) . (-1)*Los
        Lsq -- Matrix,
            for Squire operator
        Lc -- Matrix,
            for Coupling operator

        """
        # Constant definition
        N = ff.Ny
        I = np.eye(N)
        Z = np.zeros((N, N))

        alpha = 2 * np.pi * kx / ff.attributes['Lx']
        beta = 2 * np.pi * kz / ff.attributes['Lz']
        #k2 = (alpha**2 + beta**2)

        # Differentiation matrices...
        # ... for v
        y, DM = chebyshev.chebdiff(N, 4)
        D1_v = np.copy(DM[0, :, :])
        D2_v = np.copy(DM[1, :, :])
        D4_v = np.copy(DM[3, :, :])
        D4_v_clamped = chebyshev.cheb4c(N)[1]

        # ... for eta
        D2_eta = np.copy(DM[1, :, :])

        # ... for baseflow
        D1_0 = np.copy(DM[0, :, :])
        D2_0 = np.copy(DM[1, :, :])

        # Baseflow and differentiates
        #ff.make_spectral('xz', baseflow='only')
        #U = ff.u0[0, kx, :, kz]
        #ff.make_physical('xz', baseflow='only')
        U = y # np.zeros(N)
        dUdy = np.dot(D1_0, U)
        d2Udy2 = np.dot(D2_0, U)

        # nabla functions
        nabla2_v = -1 * (alpha**2 + beta**2) * I + D2_v
        nabla2_eta = -1 * (alpha**2 + beta**2) * I + D2_eta

        nabla4_v_clamped = np.zeros((N, N), dtype=complex)
        nabla4_v_clamped[1:-1, 1:-1] = D4_v_clamped \
                    + (alpha**2 + beta**2)**2 * I[1:N-1, 1:N-1] \
                    - 2 * (alpha**2 + beta**2) * D2_v[1:N-1, 1:N-1]

        nabla4_v =  D4_v + (alpha**2 + beta**2)**2 * I \
                    - 2 * (alpha**2 + beta**2) * D2_v

        # lifting function
        if lifting_function is None:
            f_u = Operators.f_lift(y, 'up', 'clamped')
            f_l = Operators.f_lift(y, 'low', 'clamped')
            f_u_eta = Operators.f_lift(y, 'up', 'dirichlet')
            f_l_eta = Operators.f_lift(y, 'low', 'dirichlet')
        else:
            f_u = lifting_function(y, 'up', 'clamped')
            f_l = lifting_function(y, 'low', 'clamped')
            f_u_eta = lifting_function(y, 'up', 'dirichlet')
            f_l_eta = lifting_function(y, 'low', 'dirichlet')

        ##########################################
        # DEFINITION OF ORR-SOMMERFELD OPERATORS #
        ##########################################
        A = np.zeros((N, N), dtype=complex) # v interacts with v
        L2_v = np.zeros((N, N), dtype=complex) # v interacts with v
        L1_v_inv = np.zeros((N, N), dtype=complex) # v interacts with v

        # --------------------------------------------------------------
        # Los: A + L2_v -> v convolved with v
        A_ = -1j * alpha * np.einsum('i,ij->ij', U, nabla2_v, dtype=complex) \
                + 1j * alpha * np.diag(d2Udy2)

        # interaction of dv/dt (0:-2) with v (0:-2)
        A[1:-1, 1:-1] = A_[1:-1, 1:-1]
        # interaction of dv/dt (0:-2) with v+ (-2)
        A[1:-1, 0] = np.dot(A_, f_u)[1:-1]
        # interaction of dv/dt (0:-2) with v- (-1)
        A[1:-1, -1] = np.dot(A_, f_l)[1:-1]
        # interaction of dv+/dt (-2) with [v, v+, v-] (:)
        #A[0, :] = 0 # already the case
        # interaction of dv-/dt (-1) with [v, v+, v-] (:)
        #A[-1, :] = 0 # already the case

        # interaction of dv/dt (0:-2) with v (1:-1)
        L2_v[1:-1, 1:-1] = 1 / Re * nabla4_v_clamped[1:-1, 1:-1]
        # interaction of dv/dt (1:-1) with v+ (0)
        L2_v[1:-1,  0] = 1 / tau_v * np.dot(nabla2_v, f_u)[1:-1] \
                        + 1 / Re * np.dot(nabla4_v, f_u)[1:-1]
        # interaction of dv/dt (1:-1) with v- (-1)
        L2_v[1:-1, -1] = 1 / tau_v * np.dot(nabla2_v, f_l)[1:-1] \
                        + 1 / Re * np.dot(nabla4_v, f_l)[1:-1]
        # interaction of dv+/dt (0) with v+ (0)
        L2_v[ 0, 0] = - 1 / tau_v
        # interaction of dv-/dt (-1) with v- (-1)
        L2_v[-1, -1] = - 1 / tau_v
        # already set to 0:
        # L2_v[ 0, 1:-1] = L2_v[-1, 1:-1] = L2_v[ 0, -1] = L2_v[-1, 0] = 0

        Los = A + L2_v

        # --------------------------------------------------------------
        # L1_v inverse
        L1_v_inv[1:-1, 1:-1] = np.linalg.inv(nabla2_v[1:-1, 1:-1])
                               #scipy.linalg.inv(nabla2_v[1:N-1, 1:N-1])
        # already the case:
        # L1_v_inv[1:-1, 0] =  L1_v_inv[1:-1, -1] = L1_v_inv[0, 1:-1]
        #       = L1_v_inv[-1, 1:-1] = 0
        # considering the interaction of dv+/dt (0) with v+ (0)
        L1_v_inv[ 0,  0] = 1
        # considering the interaction of dv-/dt (0) with v- (0)
        L1_v_inv[-1, -1] = 1

        ########################################
        #  DEFINITION OF THE SQUIRE OPERATORS  #
        ########################################
        HJ = np.zeros((N, N), dtype=complex) # eta interacts with eta
        L1_eta = np.zeros((N, N), dtype=complex) # eta interacts with eta
        Lc  = np.zeros((N, N), dtype=complex) # eta interacts with v

        # --------------------------------------------------------------
        # Lsq = L1_eta + B -> eta convolved with eta
        HJ_ = np.diag(-1j * alpha * U)

        # interaction of deta/dt (1:-1) with eta (1:-)
        HJ[1:-1, 1:-1] = HJ_[1:N-1, 1:-1]
        # interaction of deta/dt (1:-1) with eta+ (0)
        HJ[1:-1, 0] = np.dot(HJ_, f_u_eta)[1:-1]
        # interaction of dv/dt (1:-1) with eta- (-1)
        HJ[1:-1, -1] = np.dot(HJ_, f_l_eta)[1:-1]
        # interaction of deta+/dt (0) with [eta+, eta, eta-] (:)
        #HJ[0, :] = 0 # already the case
        # interaction of deta-/dt (-1) with [eta+, eta, eta-] (:)
        #HJ[-1, :] = 0 # already the case

        # interaction of deta/dt (0:-2) with eta (1:-1)
        L1_eta[1:-1, 1:-1] = 1 / Re * nabla2_eta[1:-1, 1:-1]
        # interaction of deta/dt (1:-1) with eta+ (0)
        L1_eta[1:-1,  0] = 1 / tau_eta * f_u_eta[1:-1] \
                        + 1 / Re * np.dot(nabla2_eta, f_u_eta)[1:-1]
        # interaction of deta/dt (1:-1) with eta- (-1)
        L1_eta[1:-1, -1] = 1 / tau_eta * f_l_eta[1:-1] \
                        + 1 / Re * np.dot(nabla2_eta, f_l_eta)[1:-1]
        # interaction of deta+/dt (0) with eta+ (0)
        L1_eta[ 0,  0] = - 1 / tau_eta
        # interaction of deta-/dt (-1) with eta- (-1)
        L1_eta[-1, -1] = - 1 / tau_eta

        Lsq = HJ + L1_eta

        # --------------------------------------------------------------
        # Lc: eta convolved with v
        Lc_ = -1j * beta * dUdy * I

        # interaction of deta/dt (1:-1) with v (1:-1)
        Lc[1:-1, 1:-1] = Lc_[1:-1, 1:-1]
        # interaction of deta/dt (1:-1) with v+ (0)
        Lc[1:-1,  0] = np.dot(Lc_, f_u)[1:-1]
        # interaction of deta/dt (1:-1) with v- (-1)
        Lc[1:-1, -1] = np.dot(Lc_, f_l)[1:-1]
        # interaction of deta+/dt (0) with [v+, v, v-] (:)
        #Lc[0, :] = 0 # already the case
        # interaction of deta-/dt (-1) with [v+, v, v-] (:)
        #Lc[-1, :] = 0 # already the case

        ########################################
        # Actuation matrices
        ########################################
        B_v_act = np.zeros((N, 2), dtype=complex) # v interacts with q_v
        B_eta_act = np.zeros((N, 2), dtype=complex) # eta interacts with q_eta

        B_v_act[1:-1, 0] = - 1 / tau_v * np.dot(nabla2_v, f_u)[1:-1]
        B_v_act[1:-1, 1] = - 1 / tau_v * np.dot(nabla2_v, f_l)[1:-1]
        B_v_act[  0 , 0] = 1 / tau_v
        B_v_act[ -1 , 1] = 1 / tau_v
        # already set to 0:
        #B_v_act[0, 1] = B_v_act[-1, 0] = 0

        B_eta_act[1:-1, 0] = - 1 / tau_eta * f_u_eta[1:-1]
        B_eta_act[1:-1, 1] = - 1 / tau_eta * f_l_eta[1:-1]
        B_eta_act[  0 , 0] = 1 / tau_eta
        B_eta_act[ -1 , 1] = 1 / tau_eta
        # already set to 0:
        #B_eta_act[0, 1] = B_eta_act[-1, 0] = 0

        ########################################################################
        # FINAL CONCATENATION FOR OPERATOR MATRICES
        ########################################################################

        # Building inverse matrix E_f
        E_f = np.eye(2 * N, dtype=complex)
        E_f[0:N, 0:N] = L1_v_inv

        # Building operator matrix A_f
        A_f = np.concatenate((np.concatenate((Los,  Z ), axis=1),
                              np.concatenate((Lc , Lsq), axis=1)), axis=0)

        # Building operator matrix B_f
        B_f = np.zeros((2 * N, 2 * 2), dtype=complex)
        B_f[0 :   N  , 0 :   2  ] = B_v_act
        B_f[N : 2 * N, 2 : 2 * 2] = B_eta_act

        return E_f, A_f, B_f

    def oss_mapping(self, ff: FlowField, kx, kz):
        """
        Function to map the state vector [v, eta] into
        a velocity vector [u, v, w] for the OSS equation:

        [ u ]              [ i.kx.d/dy  -i.kz ]   [  v  ]
        [ v ] =  1/k2 * [    k2        0   ] . [ eta ]
        [ w ]              [ i.kz.d/dy   i.kx ]
        where W is the energy grid streching factor

        Keyword argument:
        ff:FlowField -- FlowField flow
        kx -- mode streamwise (alpha = 2.pi.kx/Lx)
        kz -- mode spanwise (beta = 2.pi.kz/Lz)

        Return:
        Ce -- Energy-weighted transformation matrix.

        Remark:
        - Use numpy.linalg.pinv() to invert Ce.
        """
        N = ff.Ny
        alpha = 2 * np.pi * kx / ff.attributes['Lx']
        beta = 2 * np.pi * kz / ff.attributes['Lz']
        k2 = (alpha**2 + beta**2)
        zeros = np.zeros(shape=(N-2, N-2))
        I = np.eye(N-2)

        DM = chebyshev.chebdiff(N, 1)[1]
        D1 = DM[0, 1:N-1, 1:N-1]

        # factor for energy norm
        #W = np.sqrt(ff.w[1:N-1])
        #W3 = np.diag(np.hstack((W, W, W)))
        # Winv = np.linalg.inv(W)

        a = np.hstack((1j*alpha*D1, -1j*beta*I)) / k2
        b = np.hstack((I, zeros))
        c = np.hstack((1j*beta*D1, 1j*alpha*I)) / k2
        Cuvw = np.vstack((a, b, c))
        return Cuvw

    def oss_eigen(
            self, ff: Channel,
            Re, baseflow, kx, kz,
            orr=True, squire=True,
            omega=-1, tol=1E-5, maxiter=10000, which='LR'):
        """
        Wrapper of mathod scipy.linalg.eig to determine eigenvalues
        of the Orr-Sommerfeld equation with LinearOperator only.
        (can also use scipy.sparse.linalg.eigs)

        Keyword Arguments:
        ff:Channel -- Channel(FlowField) flow to access differential operators.
        Re -- Reynolds number
        baseflow -- U = U(y), V = 0, W = 0
        kx -- mode streamwise (alpha = 2.pi.kx/Lx)
        kz -- mode spanwise (beta = 2.pi.kz/Lz)
        orr -- boolean, return values for Orr-Sommerfeld equation,
            default 'True'.
        squire -- boolean, return values for Squire equation,
            default 'True'.
        omega -- see method oss_operator()

        Keyword Arguments for scipy.sparse.linalg.eigs (see documentation):
        tol -- tolerance of scipy.sparse.linalg.eigs,
            default 1E-5
        maxtir -- maximal number of iteration of scipy.sparse.linalg.eigs,
            default 10000
        which -- orientation of research of eigenvalues
            of scipy.sparse.linalg.eigs, default 'LR'.

        Return:
        eigenvalue_os -- eigenvalues of the Orr-Sommerfeld eq.
        eigenvector_os -- eigenvectors of the Orr-Sommerfeld eq.
        eigenvalue_sq -- eigenvalues of the Squire eq.
        eigenvector_sq -- eigenvectors of the Squire eq.

        REMARK:
        eigenvector_os and eigenvector_sq are respectively eigenvectors of the
        Orr-Sommerfeld eq. and Squire eq. but are no eigenvectors
        for the whole Orr-Sommerfeld-Squire system.
        """
        Los, Lap, OS, Lsq, Lc = self.oss_operator(
                                                ff,
                                                Re,
                                                baseflow,
                                                kx,
                                                kz,
                                                orr=orr,
                                                squire=squire,
                                                omega=omega)

        k = Los.shape[0]-2

        if orr:
            eigenvalue_os, eigenvector_os = scipy.linalg.eig(Los, b=Lap)

            # Equivalent to :
            # eigenvalue_os, eigenvector_os = scipy.linalg.eig(OS)
            # eigenvalue_os, eigenvector_os = scipy.sparse.linalg.eigs(
            #                                                OS,
            #                                                k,
            #                                                which=which,
            #                                                maxiter=maxiter,
            #                                                tol=tol)

            # Next lines are not working fine:
            # eigenvalue_os, eigenvector_os = scipy.sparse.linalg.eigs(
            #                                               Los,
            #                                               k,
            #                                               which=which,
            #                                               maxiter=maxiter,
            #                                               tol=tol,
            #                                               M=Lap)
        else:
            eigenvalue_os = None
            eigenvector_os = None

        if squire:
            eigenvalue_sq, eigenvector_sq = scipy.linalg.eig(Lsq)

            # Equivalent to:
            # eigenvalue_sq, eigenvector_sq = scipy.sparse.linalg.eigs(
            #                                                Lsq,
            #                                                k,
            #                                                which=which,
            #                                                maxiter=maxiter,
            #                                                tol=tol)
        else:
            eigenvalue_sq = None
            eigenvector_sq = None

        return eigenvalue_os, eigenvector_os, eigenvalue_sq, eigenvector_sq

    def oss_resolvent(self,
                      ff: FlowField,
                      Re, baseflow, kx, kz,
                      omega=1, rank=1):
        """
        Returns Orr-Sommerfeld Squire Resolvent Operator
        for couple (alpha, beta)
        and its singular value decomposition (SVD).

                  -------------------RESOLVENT---------------------
        [  u  ] = [ - j * omega * [ Lap  0 ] - [ Los   0  ]  ]^(-1) . [  Fu  ]
        [ eta ]   [               [  0   1 ]   [ Lc   Lsq ]  ]        [ Feta ]

        with weightings and transformation (u, eta) <-> [u,v,w]
        of method oss_mapping():
        [u,v,w].T = W . C . R . C^(-1) . W^(-1) . [Fu, Fv, Fw].T

        Keyword arguments:
        ff:FlowField -- FlowField flow to access differential operators.
        Re -- Reynolds number
        baseflow -- U = U(y), V = 0, W = 0
        kx -- mode streamwise (alpha = 2.pi.kx/Lx)
        kz -- mode spanwise (beta = 2.pi.kz/Lz)
        omega -- frequency
        rank -- number of singular values in the SVD.
            if rank = -1, SVD is not calculated.
            rank need to be inferior to size Ny-1.
        """
        # 0: SETTING CONSTANTS, OPERATORS #
        N = ff.Ny
        alpha = 2 * np.pi * kx / ff.attributes['Lx']
        beta = 2 * np.pi * kz / ff.attributes['Lz']
        k2 = (alpha**2 + beta**2)
        zeros = np.zeros(shape=(N-2, N-2))
        I = np.eye(N-2)

        Los, Lap, OS, Lsq, Lc = self.oss_operator(ff,
                                                  Re,
                                                  baseflow,
                                                  kx, kz,
                                                  orr=True,
                                                  squire=True,
                                                  omega=-1)

        # 1: DEFINITION OF THE UNMAPPED RESOLVENT
        A = np.concatenate(
                (np.concatenate((Los, zeros), axis=1),
                 np.concatenate((Lc,  Lsq), axis=1)),
                axis=0)
        B = np.concatenate(
                (np.concatenate((Lap, zeros), axis=1),
                 np.concatenate((zeros,  I), axis=1)),
                axis=0)
        R = np.linalg.inv(-1j*omega*B - A)

        # 2: DEFINITION OF THE MAPPED RESOLVENT
        Ce = self.oss_mapping(ff, kx, kz)
        H = np.dot(Ce, np.dot(R, np.linalg.pinv(Ce)))

        # 3: SVD OPERATION
        U, S, V = np.linalg.svd(H)
        # U, S, D = scipy.sparse.linalg.svds(H, rank)
        return H, U, S, V

    ###############################
    # for LINEARIZED NAVIER-STOKES EQUATION
    ###############################

    def lnse_operator(self, ff: FlowField, Re, baseflow, kx, kz):
        """
        Returns Linearized Navier-Stokes equation operator
        for couple (alpha, beta):

        d [ u ]       [ A11   A12    0    -d/dx ]   [ u ]
          [ v ]     = [  0    A22    0    -d/dy ] . [ v ]
          [ w ]       [  0     0    A33   -d/dz ]   [ w ]
          [ p ] /dt   [ d/dx  d/dy  d/dz    0   ]   [ p ]

        Keyword arguments:
        ff:FlowField -- FlowField flow to access differential operators.
        Re -- Reynolds number
        baseflow -- U = U(y), V = 0, W = 0
        kx -- mode streamwise (alpha = 2.pi.kx/Lx)
        kz -- mode spanwise (beta = 2.pi.kz/Lz)

        Return:
        A -- Matrix for LNSE operator
        """
        # Constant definition
        alpha = 2 * np.pi * kx / ff.attributes['Lx']
        beta = 2 * np.pi * kz / ff.attributes['Lz']
        k2 = (alpha**2 + beta**2)
        N = ff.Ny
        I = np.eye(N)
        Z = np.zeros((N, N))

        # Differentiation matrices
        nodes, DM = chebyshev.chebdiff(N, 4)
        D1 = DM[0, :, :]
        D2 = DM[1, :, :]

        # Baseflow and differentiates
        U = baseflow[0, 0, :, 0]
        dUdy = np.dot(D1, U)

        # Laplacian
        L1 = D2 - k2*I

        A11 = (1 / Re) * L1 - 1j * alpha * U * I
        A12 = - dUdy * I
        A22 = (1 / Re) * L1 - 1j * alpha * U * I
        A33 = (1 / Re) * L1 - 1j * alpha * U * I

        A1 = np.hstack((A11, A12, Z, -1j*alpha*I))
        A2 = np.hstack((Z, A22, Z, -D1))
        A3 = np.hstack((Z, Z, A33, -1j*beta*I))
        A4 = np.hstack((1j*alpha*I, D1, 1j*beta*I, Z))

        A = np.vstack((A1, A2, A3, A4))
        return A

    def lnse_resolvent(self,
                       ff: FlowField,
                       Re, baseflow, kx, kz,
                       omega, rank=-1):
        """
        Returns Linear Navier-Stokes inverse Resolvent Operator
        for couple (alpha, beta)
        and its singular value decomposition (SVD).

        ------------------RESOLVENT------------------
        [               [ 1 0 0 0 ]   [          ]  ]   [ u ]   [   ]
        [ - 1j * omega * [ 0 1 0 0 ] - [   LNSE   ]  ] . [ v ] = [ F ]
        [               [ 0 0 1 0 ]   [ OPERATOR ]  ]   [ w ]   [   ]
        [               [ 0 0 0 0 ]   [          ]  ]   [ p ]   [   ]

        Keyword arguments:
        ff:FlowField -- FlowField flow to access differential operators.
        Re -- Reynolds number
        baseflow -- U = U(y), V = 0, W = 0
        kx -- mode streamwise (alpha = 2.pi.kx/Lx)
        kz -- mode spanwise (beta = 2.pi.kz/Lz)
        omega -- frequency
        rank -- number of singular values in the SVD.
            if rank = -1, SVD is not calculated.
            rank need to be inferior to size Ny-1.

        Return:
        resolvent_lnse -- Matrix for the resolvent of the LNSE.
        u -- SVD matrix U.
        s -- SVD matrix of singular value of given rank.
        vt -- SVD matrix V.

        References:
        - Resolvent definition:
            Experimental manipulation of wall turbulence: a systems approach
            McKeon, Sharma, Jacobi
            Phys. FLuids 25, 031301 (2013)
            doi:10.1063/1.4793444
        - Resolvent definition with pressure:
            Opposition control within the resolvent analysis framework
            Luhar, Sharma, McKeon,
            JFM 2014, vol 749, pp 597-626
            doi:10.101/jfm.2014.209
        """
        N = ff.Ny

        A = self.lnse_operator(ff,
                               Re,
                               baseflow,
                               kx, kz)

        diag = np.concatenate((np.ones(3*N), np.zeros(N)))
        Ibis = np.diagflat(diag)

        R = - 1j * omega * Ibis - A
        H = R  # np.linalg.inv(R)

        U, S, V = np.linalg.svd(H)
        # U, S, D = scipy.sparse.linalg.svds(H, rank)

        return H, U, S, V

    ###############################
    # for OSS EXTENDED FOR A NONLAMINAR BASEFLOW
    ###############################

    # OPERATORS CREATION
    def op_nonlaminar_baseflow(self,
                     ff: FlowField,
                     Re,
                     MX_max=0, MZ_max=0,
                     memory=True):
        """
        Returns Orr-Sommerfeld-Squire equation operator extended
        for the case of a time-invariant nonlaminar baseflow (U,V,W)(x,y,z).

        dx/dt = E_f . A_f . x

        also written as

          [ v ]          [Lap 0 0 0 ]   [ Lap2/Re + A + B       C      D  E ]   [ v ]
        d [eta] /dt = INV[ 0  I 0 0 ] . [        F+G       Lap/Re+H+J  K  L ] . [eta]
          [u00]          [ 0  0 I 0 ]   [        M+N            O      S  0 ]   [u00]
          [w00]          [ 0  0 0 I ]   [        P+Q            R      0  S ]   [w00]

        where:
        - v are the modes of the wall-normal velocity v from couple (0,0) to (Mx-1, Mz-1),
            of size (Mx * Mz) * (Ny - 2).
        - eta are the modes of the wall-normal velocity eta
            from couple (0,0)[EXCLUDED] to (Mx-1, Mz-1),
            of size (Mx * Mz - 1 ) * (Ny - 2).
        - u00 is the first mode (0,0) of the streamwise (x) velocity,
            of size 1 * (Ny - 2).
        - w00 is the first mode (0,0) of the spanwise (z) velocity,
            of size 1 * (Ny - 2).
        - A..S are matrices of differents sizes representing differential operators
            of the interaction between different modes.

        ----------
        Keyword arguments:
        ff:FlowField -- FlowField flow
        Re -- Reynolds number
        MX_max, MZ_max -- (not implemented)
            maximal number for modes in x and z to calculate the interaction.
            > THIS CODE HAS NOT BEEN TESTED FOR MX_max != 0 or MZ_max != 0,
            > which means all the modes and their interactions are calculated,
            > and the interaction between high frequencies is not set to zero.
        memory -- boolean, if memory used by the method is necessary

        ----------
        Return:
        E_f -- 2D square matrix, of width ((Mx * Mz) + Mx * Mz - 1 + 2)
            for Laplacian (extended) operator.
        A_f -- 2D square matrix, of width ((Mx * Mz) + Mx * Mz - 1 + 2)
            for Orr-Sommerfeld (extended) operator.
        """
        ########################################################################
        # Definition of Constants
        ########################################################################
        N = ff.Ny # Wall normal resolution, which is equal to My
        Nx = ff.Nx # Streamwise resolution
        Nz = ff.Nz # Spanwise resolution
        Mx = ff.Mx
        Mz = ff.Mz
        I = np.eye(N)
        Z = np.zeros((N-2, N-2))

        # Baseflow
        ff.make_spectral('xz', baseflow='only')
        U = ff.u0[0, :, :, :]
        V = ff.u0[1, :, :, :]
        W = ff.u0[2, :, :, :]
        ff.make_physical('xz', baseflow='only')

        ########################################################################
        # Definition of Differentiation matrices
        ########################################################################
        # REMARK:
        # DO NOT REDUCE DM to DM[:, 1:N-1, 1:N-]
        # The differentiation needs to be operated with the entire DM matrices.
        nodes, DM = chebyshev.chebdiff(N, 4)

        # Differentiation matrices for baseflow (U, V, W)
        # No boundary condition applied to the baseflow.
        D1_0 = np.copy(DM[0, :, :])
        D2_0 = np.copy(DM[1, :, :])

        # Differnetiation matrices for perturbation velocity v
        # Boundary conditions:
        # For 1st, 2nd and 3th order: Dirichlet
        # For 4th order: Dirichlet + Neumann (clamped)
        # Use two different boundary condition for D1, D2, D3 and for D4 as made
        # by Weideman & Reddy 2000 and Huang & Sloan 1994 to eliminate spurious modes.
        D1_v = np.copy(DM[0, :, :])
        D2_v = np.copy(DM[1, :, :])
        D3_v = np.copy(DM[2, :, :])
        #D4_v = DM[3, 1:N-1, 1:N-1] # if only DIrichlet needed for D4.
        D4_v_clamped = chebyshev.cheb4c(N)[1]

        # if clamped boundary condition also necessary for D1_v, D2_v, D3_v, uncomment :
        # nodes2, DM_xc = chebyshev.chebxc(N)
        # D1_v = DM_xc[0]
        # D2_v = DM_xc[1]
        # D3_v = DM_xc[2]
        # D4_v_clamped = DM_xc[3]
        # Remark: chebxc return size (N-2, N-2) instead of (N, N)

        # Differentiation matrices for perturbation vorticity eta
        # Boundary conditions: Dirichlet
        D1_eta = np.copy(DM[0, :, :])
        D2_eta = np.copy(DM[1, :, :])

        def nabla2_0(kx, kz):
            """
            Return Laplacian operator matrix of order 1 (nabla2) of size [N, N]
            for the baseflow (U, V, W).

            ----------
            Keyword arguments:
            kx -- streamwise mode (not alpha !).
            kz -- spanwisewise mode (not beta !).
            """
            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            return (-1 * (alpha**2 + beta**2)*I + D2_0)

        def nabla2_v(kx, kz):
            """
            Return Laplacian operator matrix of order 1 (nabla2) of size [N, N]
            for the wall-normal velocity (v).

            ----------
            Keyword arguments:
            kx -- streamwise mode (not alpha !).
            kz -- spanwisewise mode (not beta !).
            """
            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            return (-1 * (alpha**2 + beta**2)*I + D2_v)

        def nabla2_eta(kx, kz):
            """
            Return Laplacian operator matrix of order 1 (nabla2) of size [N, N]
            for the wall-normal vorticity (eta).

            ----------
            Keyword arguments:
            kx -- streamwise mode (not alpha !).
            kz -- spanwisewise mode (not beta !).
            """
            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            return (-1 * (alpha**2 + beta**2)*I + D2_eta)

        def nabla4_v_clamped(kx, kz):
            """
            Return Laplacian operator matrix of order 2 (nabla1) of size [1:N-1, 1:N-1] [!]
            for the wall-normal velocity (v).

            ----------
            Keyword arguments:
            kx -- streamwise mode (not alpha !).
            kz -- spanwisewise mode (not beta !).
            """
            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            return  D4_v_clamped + (alpha**2 + beta**2)**2*I[1:N-1, 1:N-1] - 2*(alpha**2 + beta**2)*D2_v[1:N-1, 1:N-1]

        ########################################################################
        # FILLING MATRICES
        # Each function returns 1 matriX of size [N-2, N-2],
        # which are filling the operator matrices for each mode interaction.
        ########################################################################

        # A_ = fct(kx, kz, kx2, kz2, U, V, W) applied on v
        def A_(kx, kz, kx2, kz2):
            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            # REMARK:
            # to differentiate U, use the entire U[:] and D_0[:,:]
            # then limit it to U[1:N-1] before multiplying with D_v, L1_v... as
            # np.dot(D_0, U[kx2, :, kz2])[1:N-1]

            return + 1j * (alpha - alpha2) * (
                        np.diag(
                            + 2*np.dot(D2_0, U[kx2, :, kz2])[1:N-1]
                            - np.dot(nabla2_0(kx2, kz2), U[kx2, :, kz2])[1:N-1]
                            + 2 * (alpha2 * (alpha - alpha2)
                                + beta2 * (beta - beta2)) * U[kx2, 1:N-1, kz2])
                        - np.einsum('i,ij->ij',
                            U[kx2, :, kz2],
                            nabla2_v(kx - kx2, kz - kz2),
                            dtype=complex)[1:N-1, 1:N-1]) \
                    - np.einsum('i,ij->ij',
                        np.dot(D1_0, V[kx2, :, kz2]),
                        nabla2_v(kx - kx2, kz - kz2), dtype=complex)[1:N-1, 1:N-1] \
                    - np.einsum('i,ij->ij',
                        V[kx2, :, kz2],
                        np.dot(nabla2_v(kx - kx2, kz - kz2), D1_v),
                        dtype=complex)[1:N-1, 1:N-1] \
                    + 2 * (alpha2 * (alpha - alpha2) + beta2 * (beta - beta2)) * \
                        (np.diag(np.dot(D1_0, V[kx2, :, kz2])[1:N-1]) \
                         + np.einsum('i,ij->ij',
                                             V[kx2, :, kz2],
                                             D1_v, dtype=complex)[1:N-1, 1:N-1]) \
                    - np.diag(np.dot(nabla2_0(kx2, kz2),
                                     np.dot(D1_0, V[kx2, :, kz2]))[1:N-1]) \
                    - np.einsum('i,ij->ij',
                        np.dot(nabla2_0(kx2, kz2), V[kx2, :, kz2]),
                        D1_v, dtype=complex)[1:N-1, 1:N-1] \
                    + 1j * (beta - beta2) * (
                        np.diag(
                            + 2 * np.dot(D2_0, W[kx2, :, kz2])[1:N-1]
                            - np.dot(nabla2_0(kx2, kz2), W[kx2, :, kz2])[1:N-1]
                            + 2 * (alpha2 * (alpha - alpha2) + beta2 * (beta - beta2))
                                * W[kx2, 1:N-1, kz2])
                        - np.einsum('i,ij->ij',
                            W[kx2, :, kz2],
                            nabla2_v(kx - kx2, kz - kz2), dtype=complex)[1:N-1, 1:N-1])

        # B_ = fct(kx, kz, kx2, kz2, U, V, W) applied on v
        def B_(kx, kz, kx2, kz2):
            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            B = (alpha2 * (alpha - alpha2) + beta2 * (beta - beta2)) * (
                - 2 * 1j * (alpha - alpha2) *
                    (np.einsum('i,ij->ij',
                               np.dot(D1_0, U[kx2, :, kz2]),
                               D1_v,
                               dtype=complex)[1:N-1, 1:N-1]
                     + np.einsum('i,ij->ij',
                                 U[kx2, :, kz2],
                                 D2_v,
                                 dtype=complex)[1:N-1, 1:N-1])
                - 2 * 1j * (beta - beta2) *
                    (np.einsum('i,ij->ij',
                               np.dot(D1_0, W[kx2, :, kz2]),
                               D1_v,
                               dtype=complex)[1:N-1, 1:N-1]
                     + np.einsum('i,ij->ij',
                                 W[kx2, :, kz2],
                                 D2_v,
                                 dtype=complex)[1:N-1, 1:N-1])
                + np.einsum('i,ij->ij',
                            V[kx2, :, kz2],
                            np.dot(nabla2_v(kx - kx2, kz - kz2), D1_v),
                            dtype=complex)[1:N-1, 1:N-1]
                + np.einsum('i,ij->ij',
                            np.dot(nabla2_0(kx2, kz2), V[kx2, :, kz2]),
                            D1_v,
                            dtype=complex)[1:N-1, 1:N-1]
                - 2 * (alpha2 * (alpha - alpha2) + beta2 * (beta - beta2))
                    * np.einsum('i,ij->ij',
                                V[kx2, :, kz2],
                                D1_v,
                                dtype=complex)[1:N-1, 1:N-1]
                - 2 * np.einsum('i,ij->ij',
                                V[kx2, :, kz2],
                                D3_v,
                                dtype=complex)[1:N-1, 1:N-1]
                    ) / ((alpha-alpha2)**2 + (beta-beta2)**2)
            return B

        # C_ = fct(kx, kz, kx2, kz2, U, V, W) applied on eta
        def C_(kx, kz, kx2, kz2):
            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            C = (alpha2 * beta - beta2 * alpha) * (
                + 2 * 1j * (alpha - alpha2) *
                    (np.diag(np.dot(D1_0, U[kx2, :, kz2])[1:N-1])
                     + np.einsum('i,ij->ij',
                                 U[kx2, :, kz2],
                                 D1_eta,
                                 dtype=complex)[1:N-1, 1:N-1])
                + 2 * 1j * (beta - beta2) *
                    (np.diag(np.dot(D1_0, W[kx2, :, kz2])[1:N-1])
                     + np.einsum('i,ij->ij',
                                 W[kx2, :, kz2],
                                 D1_eta,
                                 dtype=complex)[1:N-1, 1:N-1])
                - np.einsum('i,ij->ij',
                            V[kx2, :, kz2],
                            nabla2_eta(kx - kx2, kz - kz2),
                            dtype=complex)[1:N-1, 1:N-1]
                - np.diag(np.dot(nabla2_0(kx2, kz2), V[kx2, :, kz2])[1:N-1])
                + np.diag(2 * (alpha2 * (alpha - alpha2) + beta2 * (beta - beta2))
                    * V[kx2, 1:N-1, kz2])
                + 2 * np.einsum('i,ij->ij',
                                V[kx2, :, kz2],
                                D2_eta,
                                dtype=complex)[1:N-1, 1:N-1]
                    ) / ((alpha-alpha2)**2 + (beta-beta2)**2)
            return C

        # D_ = fct(kx, kz, kx2=0, kz2=0, U, V, W) applied on u_00
        def D_(kx, kz, kx2, kz2):
            if kx2 != 0 or kz2 != 0:
                print('Should be kx2=0 and kz2=0 for D_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']

            D = 1j * alpha * np.einsum('i,ij->ij',
                                V[kx, :, kz],
                                D2_v,
                                dtype=complex)[1:N-1, 1:N-1] \
                - 1j * alpha * np.diag(np.dot(nabla2_0(kx, kz),
                                      V[kx, :, kz]))[1:N-1, 1:N-1]

            return D

        # E_ = fct(kx, kz, kx2=0, kz2=0, U, V, W) applied on w_00
        def E_(kx, kz, kx2, kz2):
            if kx2 != 0 or kz2 != 0:
                print('Should be kx2=0 and kz2=0 for E_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']

            E = 1j * beta * np.einsum('i,ij->ij',
                                V[kx, :, kz],
                                D2_v,
                                dtype=complex)[1:N-1, 1:N-1] \
                - 1j * beta * np.diag(np.dot(nabla2_0(kx, kz),
                                             V[kx, :, kz]))[1:N-1, 1:N-1]
            return E

        # F_ = fct(kx, kz, kx2, kz2, U, V, W) applied on v
        def F_(kx, kz, kx2, kz2):
            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            F = np.diag(- 1j * beta * np.dot(D1_0, U[kx2, :, kz2])[1:N-1]
                        + 1j * alpha * np.dot(D1_0, W[kx2, :, kz2])[1:N-1])
            return F

        # G_ = fct(kx, kz, kx2, kz2, U, V, W) applied on v
        def G_(kx, kz, kx2, kz2):
            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
               return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            #if kx == kx2 and kz == kz2:
            #    G = Z[1:N-1, 1:N-1]
            #else:
            G = (- (alpha2 * beta - beta2 * alpha) *
                    np.einsum('i,ij->ij',
                              V[kx2, :, kz2],
                              D2_v,
                              dtype=complex)[1:N-1, 1:N-1] \
                + 1j * (alpha * (alpha - alpha2) + beta * (beta - beta2)) *
                    np.einsum('i,ij->ij',
                              (beta2 * U[kx2, :, kz2]
                                  - alpha2 * W[kx2, :, kz2]),
                              D1_v,
                              dtype=complex)[1:N-1, 1:N-1]
                )/ ((alpha-alpha2)**2 + (beta-beta2)**2)
            return G

        # H_ = fct(kx, kz, kx2, kz2, U, V, W) applied on eta
        def H_(kx, kz, kx2, kz2):
            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            H = np.diag(- 1j * alpha * U[kx2, 1:N-1, kz2]
                        - 1j * beta * W[kx2, 1:N-1, kz2]) \
                   + np.einsum('i,ij->ij',
                               - V[kx2, :, kz2],
                               D1_eta,
                               dtype=complex)[1:N-1, 1:N-1]
            return H

        # J_ = fct(kx, kz, kx2, kz2, U, V, W) applied on eta
        def J_(kx, kz, kx2, kz2):
            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            J = (- 1j * (alpha2 * beta - beta2 * alpha)
                    * np.diag(beta2 * U[kx2, 1:N-1, kz2]
                           - alpha2 * W[kx2, 1:N-1, kz2])
                - (alpha2 * (alpha - alpha2) + beta2 * (beta - beta2))
                    * np.einsum('i,ij->ij',
                                V[kx2, :, kz2],
                                D1_eta,
                                dtype=complex)[1:N-1, 1:N-1]
                ) / ((alpha-alpha2)**2 + (beta-beta2)**2)
            return J

        # K_ = fct(kx, kz, kx2=0, kz2=0, U, V, W) applied on u_00
        def K_(kx, kz, kx2, kz2):
            if kx2 != 0 or kz2 != 0:
                print('Should be kx2=0 and kz2=0 for K_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']

            K = np.diag(
                    + alpha * beta * U[kx, 1:N-1, kz]
                    - alpha**2 * W[kx, 1:N-1, kz]) \
                - 1j * beta * np.einsum('i,ij->ij',
                                V[kx, :, kz],
                                D1_v,
                                dtype=complex)[1:N-1, 1:N-1]
            return K

        # L_ = fct(kx, kz, kx2=0, kz2=0, U, V, W) applied on w_00
        def L_(kx, kz, kx2, kz2):
            if kx2 != 0 or kz2 != 0:
                print('Should be kx2=0 and kz2=0 for L_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']

            L = np.diag(
                    + beta**2 * U[kx, 1:N-1, kz]
                    - alpha * beta * W[kx, 1:N-1, kz]) \
                + 1j * alpha * np.einsum('i,ij->ij',
                                V[kx, :, kz],
                                D1_v,
                                dtype=complex)[1:N-1, 1:N-1]
            return L

        # M_ = fct(kx=0, kz=0, kx2, kz2, U, V, W) applied on v
        def M_(kx, kz, kx2, kz2):
            if kx != 0 or kz != 0:
                print('Should be kx=0 and kz=0 for M_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            M = - np.diag(np.dot(D1_0, U[kx2, :, kz2])[1:N-1])
            return M

        # N_ = fct(kx=0, kz=0, kx2, kz2, U, V, W) applied on v
        def N_(kx, kz, kx2, kz2):
            if kx != 0 or kz != 0:
                print('Should be kx=0 and kz=0 for N_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            NN = (1j * alpha2 * np.einsum('i,ij->ij',
                                       V[kx2, :, kz2],
                                       D2_v,
                                       dtype=complex)[1:N-1, 1:N-1]
                 - beta2**2 * np.einsum('i,ij->ij',
                                       U[kx2, :, kz2],
                                       D1_v,
                                       dtype=complex)[1:N-1, 1:N-1]
                 + alpha2 * beta2 * np.einsum('i,ij->ij',
                                       W[kx2, :, kz2],
                                       D1_v,
                                       dtype=complex)[1:N-1, 1:N-1]
                ) / (alpha2**2 + beta2**2)
            return NN

        # O_ = fct(kx=0, kz=0, kx2, kz2, U, V, W) applied on eta
        def O_(kx, kz, kx2, kz2):
            if kx != 0 or kz != 0:
                print('Should be kx=0 and kz=0 for O_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            O = (- 1j * beta2 * np.einsum('i,ij->ij',
                                       V[kx2, :, kz2],
                                       D1_eta,
                                       dtype=complex)[1:N-1, 1:N-1]
                 + np.diag(- alpha2 * beta2 * U[kx2, 1:N-1, kz2]
                          - beta2**2 * W[kx2, 1:N-1, kz2])
                ) / (alpha2**2 + beta2**2)
            return O

        # P_ = fct(kx=0, kz=0, kx2, kz2, U, V, W) applied on v
        def P_(kx, kz, kx2, kz2):
            if kx != 0 or kz != 0:
                print('Should be kx=0 and kz=0 for p_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            P = - np.diag(np.dot(D1_0, W[kx2, :, kz2])[1:N-1])
            return P

        # Q_ = fct(kx=0, kz=0, kx2, kz2, U, V, W) applied on v
        def Q_(kx, kz, kx2, kz2):
            if kx != 0 or kz != 0:
                print('Should be kx=0 and kz=0 for Q_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            Q = (1j * beta2 * np.einsum('i,ij->ij',
                                       V[kx2, :, kz2],
                                       D2_v,
                                       dtype=complex)[1:N-1, 1:N-1]
                 - alpha2**2 * np.einsum('i,ij->ij',
                                       W[kx2, :, kz2],
                                       D1_v,
                                       dtype=complex)[1:N-1, 1:N-1]
                 + alpha2 * beta2 * np.einsum('i,ij->ij',
                                       U[kx2, :, kz2],
                                       D1_v,
                                       dtype=complex)[1:N-1, 1:N-1]
                ) / (alpha2**2 + beta2**2)
            return Q

        # R_ = fct(kx=0, kz=0, kx2, kz2, U, V, W) applied on eta
        def R_(kx, kz, kx2, kz2):
            if kx != 0 or kz != 0:
                print('Should be kx=0 and kz=0 for R_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            R = (+ 1j * alpha2 * np.einsum('i,ij->ij',
                                       V[kx2, :, kz2],
                                       D1_eta,
                                       dtype=complex)[1:N-1, 1:N-1]
                 + np.diag(+ alpha2 * beta2 * W[kx2, 1:N-1, kz2]
                           + alpha2**2 * U[kx2, 1:N-1, kz2])
                ) / (alpha2**2 + beta2**2)
            return R

        # Test function
        def BASIC_(kx, kz, kx2, kz2):
            if False and (kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2):
                return 8 + 1j*8
            else:
                if False:
                    if kx2 not in freq_kx2:
                        kx2 = 8
                    if kz2 not in freq_kz2:
                        kz2 = 8

                # return (kx-kx2) + 1j*(kz-kz2)
                # return (kx) + 1j*(kz)
                return (kx2) + 1j*(kz2)

        ########################################################################
        # CREATION OF OPERATOR PARTIAL-MATRICES
        ########################################################################
        print('\nOPERATOR CREATION')

        # Settings the number of modes to use
        Mx2 = Mx
        Mz2 = Mz

        # Setting the maximal index of wavenumbers to take into account
        # so we can skip the high frequency modes of U, V, W
        # NOT IMPLEMENTED LATER so keep MX_max = MZ_max = 0
        Mx2_max = Mx2 - MX_max
        Mz2_max = Mz2 - MZ_max

        # Creation of the frequency array
        freq_kx = np.fft.fftfreq(Nx, 1/Nx).astype(np.integer, copy=False)
        freq_kz = np.fft.rfftfreq(Nz, 1/Nz).astype(np.integer, copy=False)
        freq_kx2 = freq_kx[0:Mx2]
        freq_kz2 = freq_kz[0:Mz2]

        # Create NDARRAY to fill with functiosn A, B, C...
        # for v...
        A = np.zeros((Mx * Mz, Mx * Mz,     N-2,    N-2), dtype=complex)
        B = np.zeros((Mx * Mz, Mx * Mz,     N-2,    N-2), dtype=complex)
        C = np.zeros((Mx * Mz, Mx * Mz - 1, N-2,    N-2), dtype=complex)
        D = np.zeros((Mx * Mz, 1,           N-2,    N-2), dtype=complex)
        E = np.zeros((Mx * Mz, 1,           N-2,    N-2), dtype=complex)

        # for eta...
        F = np.zeros((Mx * Mz -1, Mx * Mz,      N-2, N-2), dtype=complex)
        G = np.zeros((Mx * Mz -1, Mx * Mz,      N-2, N-2), dtype=complex)
        H = np.zeros((Mx * Mz -1, Mx * Mz -1,   N-2, N-2), dtype=complex)
        J = np.zeros((Mx * Mz -1, Mx * Mz -1,   N-2, N-2), dtype=complex)
        K = np.zeros((Mx * Mz -1, 1,            N-2, N-2), dtype=complex)
        L = np.zeros((Mx * Mz -1, 1,            N-2, N-2), dtype=complex)

        # for u00...
        M = np.zeros( (1, Mx * Mz,      N-2, N-2), dtype=complex)
        NN = np.zeros((1, Mx * Mz,      N-2, N-2), dtype=complex)
        O = np.zeros( (1, Mx * Mz - 1,  N-2, N-2), dtype=complex)
        # S

        # for W00...
        P = np.zeros((1, Mx * Mz,   N-2, N-2), dtype=complex)
        Q = np.zeros((1, Mx * Mz,   N-2, N-2), dtype=complex)
        R = np.zeros((1, Mx * Mz-1, N-2, N-2), dtype=complex)
        # S

        # Laplacians...
        L1_v_inv =  np.zeros((Mx * Mz, Mx * Mz,         N-2, N-2), dtype=complex) # Laplacian
        L2_v =      np.zeros((Mx * Mz, Mx * Mz,         N-2, N-2), dtype=complex) # Laplacian**2
        L1_eta =    np.zeros((Mx * Mz -1, Mx * Mz -1,   N-2, N-2), dtype=complex) # Laplacian

        # Test matrix
        #T = np.zeros((Mx * Mz, Mx2 * Mz2, 1, 1), dtype=complex)

        ########################################################################
        # FILLING OPERATOR PARTIAL-MATRICES
        ########################################################################
        # i_kx = i_kx
        # i_kx2 = i_kx'
        # i_kx3 = i_kx - i_kx'
        # freq_kx[...] contains the actual modes number from np.fft.fftfreq and np.fft.rfftfreq

        print(' --> V')
        # ---------------------------------------------------------------------
        # for v modes (INcluded mode 0,0) ...
        # ---------------------------------------------------------------------
        for i_kx, i_kz, i_kx2, i_kz2 \
            in itertools.product(range(Mx), range(Mz), range(Mx), range(Mz)):

            i_KX3 = freq_kx[i_kx-i_kx2]%Mx
            i_KZ3 = freq_kz[i_kz-i_kz2]%Mz

            kx = freq_kx[i_kx]
            kz = freq_kz[i_kz]
            kx2 = freq_kx2[i_kx2]
            kz2 = freq_kz2[i_kz2]

            # --------------------------------------------------------------
            # ... convolved with v (INcluded mode 0,0)
            # --------------------------------------------------------------
            A[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, :, :] = \
                    A_(kx, kz, kx2, kz2)

            #T[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, :, :] = \
            #        BASIC_(kx, kz, kx2, kz2)

            if ((i_kx*Mz) + i_kz) == ((i_KX3*Mz2) + i_KZ3):
                L1_v_inv[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, :, :] = \
                        np.linalg.inv(nabla2_v(kx, kz)[1:N-1, 1:N-1])
                        #scipy.linalg.inv(nabla2_v(kx, kz)[1:N-1, 1:N-1])

                L2_v[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, :, :] = \
                        1 / Re * nabla4_v_clamped(kx, kz)[:,:]

            # --------------------------------------------------------------
            # ... convolved with eta (EXcluded mode 0,0)
            # --------------------------------------------------------------
            if not (i_KX3 == 0 and i_KZ3 == 0):
                B[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, :, :] = \
                        B_(kx, kz, kx2, kz2)

                # the index " -1" is due to the fact that matrix C excludes
                # the interaction with eta(0,0), which corresponds to the colum
                # (i_KX3*Mz2) + i_KZ3 = 0,
                # but we dont want C to have an empty column.
                C[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, :, :] = \
                        C_(kx, kz, kx2, kz2)

            # --------------------------------------------------------------
            # ... convolved with u00 and w00 (ONLY mode 0,0)
            # --------------------------------------------------------------
            if i_kx2 == 0 and i_kz2 ==0:
                D[(i_kx*Mz) + i_kz, 0, :, :] = \
                        D_(kx, kz, kx2, kz2)

                E[(i_kx*Mz) + i_kz, 0, :, :] = \
                        E_(kx, kz, kx2, kz2)

        # ---------------------------------------------------------------------
        # for eta modes (EXcluded mode 0,0) ...
        # ---------------------------------------------------------------------
        print(' --> ETA')
        for i_kx, i_kz, i_kx2, i_kz2 \
            in itertools.product(range(Mx), range(Mz), range(Mx), range(Mz)):

            if i_kx == 0 and i_kz == 0:
                continue

            i_KX3 = freq_kx[i_kx-i_kx2]%Mx
            i_KZ3 = freq_kz[i_kz-i_kz2]%Mz

            kx = freq_kx[i_kx]
            kz = freq_kz[i_kz]
            kx2 = freq_kx2[i_kx2]
            kz2 = freq_kz2[i_kz2]

            # --------------------------------------------------------------
            # ... convolved with v (INcluded mode 0,0)
            # --------------------------------------------------------------
            # the index " -1" is due to the fact that matrix F excludes the case
            # eta(0,0), which does not exist and which corresponds to the colum
            # (i_kx*Mz) + i_kz = 0,
            # but we dont want F to have an empty row.
            F[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3, :, :] = \
                        F_(kx, kz, kx2, kz2)

            # --------------------------------------------------------------
            # ... convolved with eta (EXcluded mode 0,0)
            # --------------------------------------------------------------
            if not (i_KX3 == 0 and i_KZ3 == 0):
                G[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3, :, :] = \
                        G_(kx, kz, kx2, kz2)

                H[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, :, :] = \
                        H_(kx, kz, kx2, kz2)

                J[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, :, :] = \
                        J_(kx, kz, kx2, kz2)

                if ((i_kx*Mz) + i_kz) == ((i_KX3*Mz2) + i_KZ3):
                    L1_eta[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, :, :] = \
                        1 / Re * nabla2_eta(kx, kz)[1:N-1, 1:N-1]

            # --------------------------------------------------------------
            # ... convolved with u00 and w00 (ONLY mode 0,0)
            # --------------------------------------------------------------
            if i_kx2 == 0 and i_kz2 ==0:
                K[(i_kx*Mz) + i_kz -1, 0, :, :] = \
                        K_(kx, kz, kx2, kz2)

                L[(i_kx*Mz) + i_kz -1, 0, :, :] = \
                        L_(kx, kz, kx2, kz2)

        # ---------------------------------------------------------------------
        # for u00 and w00 (ONLY mode 0,0) ...
        # ---------------------------------------------------------------------
        print(' --> U00 W00')
        for i_kx, i_kz, i_kx2, i_kz2 \
            in itertools.product({0}, {0}, range(Mx), range(Mz)):

            i_KX3 = freq_kx[i_kx-i_kx2]%Mx
            i_KZ3 = freq_kz[i_kz-i_kz2]%Mz

            kx = freq_kx[i_kx]
            kz = freq_kz[i_kz]
            kx2 = freq_kx2[i_kx2]
            kz2 = freq_kz2[i_kz2]

            # --------------------------------------------------------------
            # ... convolved with v (INcluded mode 0,0)
            # --------------------------------------------------------------
            M[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, :, :] = \
                    M_(kx, kz, kx2, kz2)

            P[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, :, :] = \
                    P_(kx, kz, kx2, kz2)

            # --------------------------------------------------------------
            # ... convolved with eta (EXcluded mode 0,0)
            # --------------------------------------------------------------
            if not (i_KX3 == 0 and i_KZ3 == 0):
                NN[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, :, :] = \
                        N_(kx, kz, kx2, kz2)

                O[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, :, :] = \
                        O_(kx, kz, kx2, kz2)

                Q[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, :, :] = \
                        Q_(kx, kz, kx2, kz2)

                R[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, :, :] = \
                        R_(kx, kz, kx2, kz2)

            # --------------------------------------------------------------
            # ... convolved with u00 and w00 (ONLY mode 0,0)
            # --------------------------------------------------------------
            if i_kx2 == 0 and i_kz2 == 0:
                S = 1 / Re * nabla2_v(0,0)[1:N-1, 1:N-1] \
                        - np.einsum('i,ij->ij', V[0, :, 0],
                                            D1_v, dtype=complex)[1:N-1, 1:N-1]

        ########################################################################
        # FLATTEN OPERATOR PARTIAL-MATRICES
        ########################################################################
        print(' --> STACKING')

        # TO FLATTEN THE NDARRAY INTO A 2D-MATRIX
        A = np.hstack(np.hstack(A))
        B = np.hstack(np.hstack(B))
        C = np.hstack(np.hstack(C))
        D = np.hstack(np.hstack(D))
        E = np.hstack(np.hstack(E))
        F = np.hstack(np.hstack(F))
        G = np.hstack(np.hstack(G))
        H = np.hstack(np.hstack(H))
        J = np.hstack(np.hstack(J))
        K = np.hstack(np.hstack(K))
        L = np.hstack(np.hstack(L))
        M = np.hstack(np.hstack(M))
        NN = np.hstack(np.hstack(NN))
        O = np.hstack(np.hstack(O))
        P = np.hstack(np.hstack(P))
        Q = np.hstack(np.hstack(Q))
        R = np.hstack(np.hstack(R))
        # S

        L1_v_inv = np.hstack(np.hstack(L1_v_inv))
        L2_v = np.hstack(np.hstack(L2_v))
        L1_eta = np.hstack(np.hstack(L1_eta))

        #T = np.hstack(np.hstack(T))

        ########################################################################
        # FINAL CONCATENATION FOR OPERATOR MATRICES
        ########################################################################
        # Building inverse matrix A_f
        E_f = np.eye((Mx * Mz + Mx * Mz - 1 + 2) * (N-2), dtype=complex)
        E_f[0:Mx*Mz*(N-2), 0:Mx*Mz*(N-2)] = L1_v_inv

        # Building operator matrix B_f
        A_f = np.concatenate((np.concatenate((1 * L2_v + 1 * A + 1 * B,
                             1*C,
                             1*D,
                             1*E), axis=1),
                          np.concatenate((F + G,
                             L1_eta + H + J,
                             K,
                             L), axis=1),
                          np.concatenate((M + NN,
                             O,
                             S,
                             Z), axis=1),
                          np.concatenate((P + Q,
                             R,
                             Z,
                             S), axis=1)
                          ), axis=0)

        # Save test matrix if necessary for dev.
        #np.savetxt('T.real', T.real, fmt='%+.0f')
        #np.savetxt('T.imag', T.imag, fmt='%+.0f')

        # if needed to save matrices in matlab
        #scipy.io.savemat(filename+'E.mat', mdict={'E':E_f})
        #scipy.io.savemat(filename+'A.mat', mdict={'A':A_f})

        print(' --> DONE')

        if memory:
            print('MEMORY used for operator creation :',
                    resource.getrusage(resource.RUSAGE_SELF).ru_maxrss /1000, 'MB')

        return E_f, A_f

    def op_nonlaminar_baseflow_actuated(self,
                     ff: FlowField,
                     Re,
                     lifting_function = None,
                     tau_v=1, tau_eta=1, tau_u=1, tau_w=1,
                     MX_max=0, MZ_max=0,
                     memory=True):
        """
        Returns Orr-Sommerfeld-Squire equation operator extended
        for the case of a time-invariant nonlaminar baseflow (U,V,W)(x,y,z).

        dx/dt = E_f . A_f . x + E_f . B_f . u

        also written as

          [ v ]          [Lap 0 0 0 ]   [ Lap2/Re + A + B       C      D  E ]   [ v ]
        d [eta] /dt = INV[ 0  I 0 0 ] . [        F+G       Lap/Re+H+J  K  L ] . [eta]
          [u00]          [ 0  0 I 0 ]   [        M+N            O      S  0 ]   [u00]
          [w00]          [ 0  0 0 I ]   [        P+Q            R      0  S ]   [w00]
                                                               [  q_v+  ]
                                                               [  q_v-  ]
                          [Lap 0 0 0 ]   [ B_1  0   0   0  ]   [ q_eta+ ]
                     + INV[ 0  I 0 0 ] . [  0  B_2  0   0  ] . [ q_eta- ]
                          [ 0  0 I 0 ]   [  0   0  B_3  0  ]   [ q_u00+ ]
                          [ 0  0 0 I ]   [  0   0   0  B_4 ]   [ q_u00- ]
                                                               [ q_w00+ ]
                                                               [ q_w00- ]

        where:
        - v are the modes of the wall-normal velocity v from couple (0,0) to (Mx-1, Mz-1),
            of size (Mx * Mz) * (Ny - 2).
        - eta are the modes of the wall-normal velocity eta
            from couple (0,0)[EXCLUDED] to (Mx-1, Mz-1),
            of size (Mx * Mz - 1 ) * (Ny - 2).
        - u00 is the first mode (0,0) of the streamwise (x) velocity,
            of size 1 * (Ny - 2).
        - w00 is the first mode (0,0) of the spanwise (z) velocity,
            of size 1 * (Ny - 2).
        - A..S are matrices of differents sizes representing differential operators
            of the interaction between different modes.

        ----------
        Keyword arguments:
        ff:FlowField -- FlowField flow
        Re -- Reynolds number
        f_u -- upper lifting function
        f_l -- lower lifting function
            if f_u or f_l is None, use by defaut op_lifting_f_u/l
        tau_... -- Actuation time for v, eta, u00 and w00.
            Either all of them scaler, or array of dimension:
                tau_v: [Mx, Mz]
                tau_eta: [Mx, Mz] but [0, 0] is not used
                tau_u: 1
                tau_w: 1
            tau_X[i, j] correspond to the actuation time
                for mode (i, 1j) of the control of X
        MX_max, MZ_max -- (not implemented)
            maximal number for modes in x and z to calculate the interaction.
            > THIS CODE HAS NOT BEEN TESTED FOR MX_max != 0 or MZ_max != 0,
            > which means all the modes and their interactions are calculated,
            > and the interaction between high frequencies is not set to zero.
        memory -- boolean, if memory used by the method is necessary

        ----------
        Return:
        E_f -- 2D square matrix, of width ((Mx * Mz) + Mx * Mz - 1 + 2)
            for Laplacian (extended) operator.
        A_f -- 2D square matrix, of width ((Mx * Mz) + Mx * Mz - 1 + 2)
            for Orr-Sommerfeld (extended) operator.
        B_f -- 2D square matrix, of width ((Mx * Mz) + Mx * Mz - 1 + 2)
            for actuation operator.
        """
        ########################################################################
        # Definition of Constants
        ########################################################################

        N = ff.Ny # Wall normal resolution, equal to My
        Nx = ff.Nx # Streamwise resolution
        Nz = ff.Nz # Spanwise resolution
        Mx = ff.Mx
        Mz = ff.Mz
        I = np.eye(N)
        Z = np.zeros((N, N))

        # Baseflow
        ff.make_spectral('xz', baseflow='only')
        U = ff.u0[0, :, :, :]
        V = ff.u0[1, :, :, :]
        W = ff.u0[2, :, :, :]
        ff.make_physical('xz', baseflow='only')

        # tau definition
        if np.shape(tau_v) == ():
            tau_v = np.full(shape=(Mx, Mz), fill_value=tau_v)
        if np.shape(tau_eta) == ():
            tau_eta = np.full(shape=(Mx, Mz), fill_value=tau_eta)
        # tau_u and tau_w always shape () as dimension 1.

        if np.shape(tau_v) != (Mx,Mz) \
                or np.shape(tau_eta) != (Mx,Mz) \
                or np.shape(tau_u) != () \
                or np.shape(tau_w) != ():
            warnings.warm(
                """Shape of tau not as required, either () or :
                    tau_v = (Mx,Mz), tau_eta = (Mx,Mz), tau_u = (), tau_w = ().
                """)

        #tau_eta = np.ndarray((Mx, Mz))
        #tau_eta[:,:] = tau_ETA
        #tau_u00 = np.ndarray((Mx, Mz))
        #tau_u00[:,:] = tau_U00
        #tau_w00 = np.ndarray((Mx, Mz))
        #tau_w00[:,:] = tau_W00

        ########################################################################
        # Definition of function f_upper and f_lower
        ########################################################################
        y, weight = chebyshev.clencurt(N)

        # CHECK MCKERNAN PAPADAKIS WHIDBORNE

        # If all the f_upper and f_lower are set to zero, we get the same eigenvalues
        # as the unactuated system. The implementation of the actuation does not seem
        # to disturb the original system.
        if lifting_function is None:
            f_u = Operators.f_lift(y, 'up', 'clamped')
            f_l = Operators.f_lift(y, 'low', 'clamped')
            f_u_eta = Operators.f_lift(y, 'up', 'dirichlet')
            f_l_eta = Operators.f_lift(y, 'low', 'dirichlet')
        else:
            f_u = lifting_function(y, 'up', 'clamped')
            f_l = lifting_function(y, 'low', 'clamped')
            f_u_eta = lifting_function(y, 'up', 'dirichlet')
            f_l_eta = lifting_function(y, 'low', 'dirichlet')

        #def dirichlet(matrix_in):
        #    matrix = np.copy(matrix_in)
        #    matrix[0,:] = 0
        #    matrix[-1, :] = 0
        #    matrix[:, 0] = 0
        #    matrix[:, -1] = 0
        #    return matrix

        ########################################################################
        # Definition of Differentiation matrices
        ########################################################################
        # REMARK:
        # DO NOT REDUCE DM to DM[:, 1:N-1, 1:N-1]
        # The differentiation needs to be operated with the entire DM matrices.
        nodes, DM = chebyshev.chebdiff(N, 4)

        # Differentiation matrices for baseflow (U, V, W)
        # No boundary condition applied to the baseflow.
        D1_0 = np.copy(DM[0, :, :])
        D2_0 = np.copy(DM[1, :, :])
        #D3_0 = np.copy(DM[2, :, :])
        #D4_0 = np.copy(DM[3, :, :])

        # Differnetiation matrices for perturbation velocity v
        # Boundary conditions:
        # For 1st, 2nd and 3th order: Dirichlet
        # For 4th order: Dirichlet + Neumann (clamped)
        # Use two different boundary condition for D1, D2, D3 and for D4 as made
        # by Weideman & Reddy 2000 and Huang & Sloan 1994 to eliminate spurious modes.
        D1_v = np.copy(DM[0, :, :])
        D2_v = np.copy(DM[1, :, :])
        D3_v = np.copy(DM[2, :, :])
        D4_v = np.copy(DM[3, :, :]) #DM[3, 1:N-1, 1:N-1] # if only Dirichlet needed for D4.
        D4_v_clamped = chebyshev.cheb4c(N)[1]

        # if clamped boundary condition also necessary for D1_v, D2_v, D3_v, uncomment :
        # nodes2, DM_xc = chebyshev.chebxc(N)
        # D1_v = DM_xc[0]
        # D2_v = DM_xc[1]
        # D3_v = DM_xc[2]
        # D4_v = DM_xc[3]
        # Remark: chebxc return size (N-2, N-2) instead of (N, N)

        # Differentiation matrices for perturbation vorticity eta
        # Boundary conditions: Dirichlet
        D1_eta = np.copy(DM[0, :, :])
        D2_eta = np.copy(DM[1, :, :])

        def nabla2_0(kx, kz):
            """
            Return Laplacian operator matrix of order 1 (nabla2) of size [N, N]
            for the baseflow (U, V, W).

            ----------
            Keyword arguments:
            kx -- streamwise mode (not alpha !).
            kz -- spanwisewise mode (not beta !).
            """
            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            return (-1 * (alpha**2 + beta**2)*I + D2_0)

        def nabla2_v(kx, kz):
            """
            Return Laplacian operator matrix of order 1 (nabla2) of size [N, N]
            for the wall-normal velocity (v).

            ----------
            Keyword arguments:
            kx -- streamwise mode (not alpha !).
            kz -- spanwisewise mode (not beta !).
            """
            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            return (-1 * (alpha**2 + beta**2)*I + D2_v)

        def nabla2_eta(kx, kz):
            """
            Return Laplacian operator matrix of order 1 (nabla2) of size [N, N]
            for the wall-normal vorticity (eta).

            ----------
            Keyword arguments:
            kx -- streamwise mode (not alpha !).
            kz -- spanwisewise mode (not beta !).
            """
            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            return (-1 * (alpha**2 + beta**2)*I + D2_eta)

        def nabla4_v_clamped(kx, kz):
            """
            Return Laplacian operator matrix of order 2 (nabla1) of size [N, N]
            for the wall-normal velocity (v).

            ----------
            Keyword arguments:
            kx -- streamwise mode (not alpha !).
            kz -- spanwisewise mode (not beta !).
            """
            nabla4_clamped = np.zeros((N, N), dtype=complex)
            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            nabla4_clamped[1:-1, 1:-1] = D4_v_clamped \
                    + (alpha**2 + beta**2)**2 * I[1:N-1, 1:N-1] \
                    - 2 * (alpha**2 + beta**2) * D2_v[1:N-1, 1:N-1]
            return nabla4_clamped

        def nabla4_v(kx, kz):
            """
            Return Laplacian operator matrix of order 2 (nabla1) of size [N, N]
            for the wall-normal velocity (v).

            ----------
            Keyword arguments:
            kx -- streamwise mode (not alpha !).
            kz -- spanwisewise mode (not beta !).
            """
            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            return  D4_v \
                    + (alpha**2 + beta**2)**2 * I \
                    - 2 * (alpha**2 + beta**2) * D2_v

        ########################################################################
        # FILLING MATRICES
        # Each function returns 1 matriX of size [N, N],
        # which are filling the operator matrices for each mode interaction.
        ########################################################################

        # A_ = fct(kx, kz, kx2, kz2, U, V, W) applied on v
        def A_(kx, kz, kx2, kz2):
            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            # REMARK:
            # to differentiate U, use the entire U[:] and D_0[:,:]
            # then limit it to U[1:N-1] before multiplying with D_v, L1_v... as
            # np.dot(D_0, U[kx2, :, kz2])[1:N-1]

            A = + 1j * (alpha - alpha2) * (
                        np.diag(
                            + 2*np.dot(D2_0, U[kx2, :, kz2])
                            - np.dot(nabla2_0(kx2, kz2), U[kx2, :, kz2])
                            + 2 * (alpha2 * (alpha - alpha2)
                                + beta2 * (beta - beta2)) * U[kx2, :, kz2])
                        - np.einsum('i,ij->ij',
                            U[kx2, :, kz2],
                            nabla2_v(kx - kx2, kz - kz2),
                            dtype=complex)) \
                    - np.einsum('i,ij->ij',
                        np.dot(D1_0, V[kx2, :, kz2]),
                        nabla2_v(kx - kx2, kz - kz2), dtype=complex) \
                    - np.einsum('i,ij->ij',
                        V[kx2, :, kz2],
                        np.dot(nabla2_v(kx - kx2, kz - kz2), D1_v),
                        dtype=complex) \
                    + 2 * (alpha2 * (alpha - alpha2) + beta2 * (beta - beta2)) * \
                        (np.diag(np.dot(D1_0, V[kx2, :, kz2])) \
                         + np.einsum('i,ij->ij',
                                             V[kx2, :, kz2],
                                             D1_v, dtype=complex)) \
                    - np.diag(np.dot(nabla2_0(kx2, kz2),
                                     np.dot(D1_0, V[kx2, :, kz2]))) \
                    - np.einsum('i,ij->ij',
                        np.dot(nabla2_0(kx2, kz2), V[kx2, :, kz2]),
                        D1_v, dtype=complex) \
                    + 1j * (beta - beta2) * (
                        np.diag(
                            + 2 * np.dot(D2_0, W[kx2, :, kz2])
                            - np.dot(nabla2_0(kx2, kz2), W[kx2, :, kz2])
                            + 2 * (alpha2 * (alpha - alpha2) + beta2 * (beta - beta2))
                                * W[kx2, :, kz2])
                        - np.einsum('i,ij->ij',
                            W[kx2, :, kz2],
                            nabla2_v(kx - kx2, kz - kz2), dtype=complex))
            return A

        # B_ = fct(kx, kz, kx2, kz2, U, V, W) applied on v
        def B_(kx, kz, kx2, kz2):
            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            B = (alpha2 * (alpha - alpha2) + beta2 * (beta - beta2)) * (
                - 2 * 1j * (alpha - alpha2) *
                    (np.einsum('i,ij->ij',
                               np.dot(D1_0, U[kx2, :, kz2]),
                               D1_v,
                               dtype=complex)
                     + np.einsum('i,ij->ij',
                                 U[kx2, :, kz2],
                                 D2_v,
                                 dtype=complex))
                - 2 * 1j * (beta - beta2) *
                    (np.einsum('i,ij->ij',
                               np.dot(D1_0, W[kx2, :, kz2]),
                               D1_v,
                               dtype=complex)
                     + np.einsum('i,ij->ij',
                                 W[kx2, :, kz2],
                                 D2_v,
                                 dtype=complex))
                + np.einsum('i,ij->ij',
                            V[kx2, :, kz2],
                            np.dot(nabla2_v(kx - kx2, kz - kz2), D1_v),
                            dtype=complex)
                + np.einsum('i,ij->ij',
                            np.dot(nabla2_0(kx2, kz2), V[kx2, :, kz2]),
                            D1_v,
                            dtype=complex)
                - 2 * (alpha2 * (alpha - alpha2) + beta2 * (beta - beta2))
                    * np.einsum('i,ij->ij',
                                V[kx2, :, kz2],
                                D1_v,
                                dtype=complex)
                - 2 * np.einsum('i,ij->ij',
                                V[kx2, :, kz2],
                                D3_v,
                                dtype=complex)
                    ) / ((alpha-alpha2)**2 + (beta-beta2)**2)
            return B

        # C_ = fct(kx, kz, kx2, kz2, U, V, W) applied on eta
        def C_(kx, kz, kx2, kz2):
            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            C = (alpha2 * beta - beta2 * alpha) * (
                + 2 * 1j * (alpha - alpha2) *
                    (np.diag(np.dot(D1_0, U[kx2, :, kz2]))
                     + np.einsum('i,ij->ij',
                                 U[kx2, :, kz2],
                                 D1_eta,
                                 dtype=complex))
                + 2 * 1j * (beta - beta2) *
                    (np.diag(np.dot(D1_0, W[kx2, :, kz2]))
                     + np.einsum('i,ij->ij',
                                 W[kx2, :, kz2],
                                 D1_eta,
                                 dtype=complex))
                - np.einsum('i,ij->ij',
                            V[kx2, :, kz2],
                            nabla2_eta(kx - kx2, kz - kz2),
                            dtype=complex)
                - np.diag(np.dot(nabla2_0(kx2, kz2), V[kx2, :, kz2]))
                + np.diag(2 * (alpha2 * (alpha - alpha2) + beta2 * (beta - beta2))
                    * V[kx2, :, kz2])
                + 2 * np.einsum('i,ij->ij',
                                V[kx2, :, kz2],
                                D2_eta,
                                dtype=complex)
                    ) / ((alpha-alpha2)**2 + (beta-beta2)**2)
            return C

        # D_ = fct(kx, kz, kx2=0, kz2=0, U, V, W) applied on u_00
        def D_(kx, kz, kx2, kz2):
            if kx2 != 0 or kz2 != 0:
                print('Should be kx2=0 and kz2=0 for D_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']

            D = 1j * alpha * np.einsum('i,ij->ij',
                                V[kx, :, kz],
                                D2_v,
                                dtype=complex) \
                - 1j * alpha * np.diag(np.dot(nabla2_0(kx, kz),
                                      V[kx, :, kz]))

            return D

        # E_ = fct(kx, kz, kx2=0, kz2=0, U, V, W) applied on w_00
        def E_(kx, kz, kx2, kz2):
            if kx2 != 0 or kz2 != 0:
                print('Should be kx2=0 and kz2=0 for E_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']

            E = 1j * beta * np.einsum('i,ij->ij',
                                V[kx, :, kz],
                                D2_v,
                                dtype=complex) \
                - 1j * beta * np.diag(np.dot(nabla2_0(kx, kz),
                                             V[kx, :, kz]))
            return E

        # F_ = fct(kx, kz, kx2, kz2, U, V, W) applied on v
        def F_(kx, kz, kx2, kz2):
            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            F = np.diag(- 1j * beta * np.dot(D1_0, U[kx2, :, kz2])
                        + 1j * alpha * np.dot(D1_0, W[kx2, :, kz2]))
            return F

        # G_ = fct(kx, kz, kx2, kz2, U, V, W) applied on v
        def G_(kx, kz, kx2, kz2):
            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
               return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            G = (- (alpha2 * beta - beta2 * alpha) *
                    np.einsum('i,ij->ij',
                              V[kx2, :, kz2],
                              D2_v,
                              dtype=complex) \
                + 1j * (alpha * (alpha - alpha2) + beta * (beta - beta2)) *
                    np.einsum('i,ij->ij',
                              (beta2 * U[kx2, :, kz2]
                                  - alpha2 * W[kx2, :, kz2]),
                              D1_v,
                              dtype=complex)
                )/ ((alpha-alpha2)**2 + (beta-beta2)**2)
            return G

        # H_ = fct(kx, kz, kx2, kz2, U, V, W) applied on eta
        def H_(kx, kz, kx2, kz2):
            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            H = np.diag(- 1j * alpha * U[kx2, :, kz2]
                        - 1j * beta * W[kx2, :, kz2]) \
                   + np.einsum('i,ij->ij',
                               - V[kx2, :, kz2],
                               D1_eta,
                               dtype=complex)
            return H

        # J_ = fct(kx, kz, kx2, kz2, U, V, W) applied on eta
        def J_(kx, kz, kx2, kz2):
            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']
            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            J = (- 1j * (alpha2 * beta - beta2 * alpha)
                    * np.diag(beta2 * U[kx2, :, kz2]
                           - alpha2 * W[kx2, :, kz2])
                - (alpha2 * (alpha - alpha2) + beta2 * (beta - beta2))
                    * np.einsum('i,ij->ij',
                                V[kx2, :, kz2],
                                D1_eta,
                                dtype=complex)
                ) / ((alpha-alpha2)**2 + (beta-beta2)**2)
            return J

        # K_ = fct(kx, kz, kx2=0, kz2=0, U, V, W) applied on u_00
        def K_(kx, kz, kx2, kz2):
            if kx2 != 0 or kz2 != 0:
                print('Should be kx2=0 and kz2=0 for K_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']

            K = np.diag(
                    + alpha * beta * U[kx, :, kz]
                    - alpha**2 * W[kx, :, kz]) \
                - 1j * beta * np.einsum('i,ij->ij',
                                V[kx, :, kz],
                                D1_v,
                                dtype=complex)
            return K

        # L_ = fct(kx, kz, kx2=0, kz2=0, U, V, W) applied on w_00
        def L_(kx, kz, kx2, kz2):
            if kx2 != 0 or kz2 != 0:
                print('Should be kx2=0 and kz2=0 for L_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha = 2*np.pi*kx/ff.attributes['Lx']
            beta = 2*np.pi*kz/ff.attributes['Lz']

            L = np.diag(
                    + beta**2 * U[kx, :, kz]
                    - alpha * beta * W[kx, :, kz]) \
                + 1j * alpha * np.einsum('i,ij->ij',
                                V[kx, :, kz],
                                D1_v,
                                dtype=complex)
            return L

        # M_ = fct(kx=0, kz=0, kx2, kz2, U, V, W) applied on v
        def M_(kx, kz, kx2, kz2):
            if kx != 0 or kz != 0:
                print('Should be kx=0 and kz=0 for M_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            M = - np.diag(np.dot(D1_0, U[kx2, :, kz2]))
            return M

        # N_ = fct(kx=0, kz=0, kx2, kz2, U, V, W) applied on v
        def N_(kx, kz, kx2, kz2):
            if kx != 0 or kz != 0:
                print('Should be kx=0 and kz=0 for N_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            NN = (1j * alpha2 * np.einsum('i,ij->ij',
                                       V[kx2, :, kz2],
                                       D2_v,
                                       dtype=complex)
                 - beta2**2 * np.einsum('i,ij->ij',
                                       U[kx2, :, kz2],
                                       D1_v,
                                       dtype=complex)
                 + alpha2 * beta2 * np.einsum('i,ij->ij',
                                       W[kx2, :, kz2],
                                       D1_v,
                                       dtype=complex)
                ) / (alpha2**2 + beta2**2)
            return NN

        # O_ = fct(kx=0, kz=0, kx2, kz2, U, V, W) applied on eta
        def O_(kx, kz, kx2, kz2):
            if kx != 0 or kz != 0:
                print('Should be kx=0 and kz=0 for O_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            O = (- 1j * beta2 * np.einsum('i,ij->ij',
                                       V[kx2, :, kz2],
                                       D1_eta,
                                       dtype=complex)
                 + np.diag(- alpha2 * beta2 * U[kx2, :, kz2]
                          - beta2**2 * W[kx2, :, kz2])
                ) / (alpha2**2 + beta2**2)
            return O

        # P_ = fct(kx=0, kz=0, kx2, kz2, U, V, W) applied on v
        def P_(kx, kz, kx2, kz2):
            if kx != 0 or kz != 0:
                print('Should be kx=0 and kz=0 for p_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            P = - np.diag(np.dot(D1_0, W[kx2, :, kz2]))
            return P

        # Q_ = fct(kx=0, kz=0, kx2, kz2, U, V, W) applied on v
        def Q_(kx, kz, kx2, kz2):
            if kx != 0 or kz != 0:
                print('Should be kx=0 and kz=0 for Q_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            Q = (1j * beta2 * np.einsum('i,ij->ij',
                                       V[kx2, :, kz2],
                                       D2_v,
                                       dtype=complex)
                 - alpha2**2 * np.einsum('i,ij->ij',
                                       W[kx2, :, kz2],
                                       D1_v,
                                       dtype=complex)
                 + alpha2 * beta2 * np.einsum('i,ij->ij',
                                       U[kx2, :, kz2],
                                       D1_v,
                                       dtype=complex)
                ) / (alpha2**2 + beta2**2)
            return Q

        # R_ = fct(kx=0, kz=0, kx2, kz2, U, V, W) applied on eta
        def R_(kx, kz, kx2, kz2):
            if kx != 0 or kz != 0:
                print('Should be kx=0 and kz=0 for R_.')

            if kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2:
                return Z

            alpha2 = 2*np.pi*kx2/ff.attributes['Lx']
            beta2 = 2*np.pi*kz2/ff.attributes['Lz']

            R = (+ 1j * alpha2 * np.einsum('i,ij->ij',
                                       V[kx2, :, kz2],
                                       D1_eta,
                                       dtype=complex)
                 + np.diag(+ alpha2 * beta2 * W[kx2, :, kz2]
                           + alpha2**2 * U[kx2, :, kz2])
                ) / (alpha2**2 + beta2**2)
            return R

        def S_(Re):
            S = 1 / Re * nabla2_v(0,0) \
                - np.einsum('i,ij->ij',
                            V[0, :, 0],
                            D1_v, dtype=complex)
            return S

        # Test function
        def BASIC_(kx, kz, kx2, kz2):
            if False and (kx-kx2 not in freq_kx2 or kz-kz2 not in freq_kz2):
                return 8 + 1j*8
            else:
                if False:
                    if kx2 not in freq_kx2:
                        kx2 = 8
                    if kz2 not in freq_kz2:
                        kz2 = 8

                # return (kx-kx2) + 1j*(kz-kz2)
                # return (kx) + 1j*(kz)
                return (kx2) + 1j*(kz2)

        ########################################################################
        # CREATION OF OPERATOR PARTIAL-MATRICES
        ########################################################################
        print('\nOPERATOR CREATION with actuation')

        # Settings the number of modes to use
        Mx2 = Mx
        Mz2 = Mz

        # Setting the maximal index of wavenumbers to take into account
        # so we can skip the high frequency modes of U, V, W
        # NOT IMPLEMENTED so keep MX_max = MZ_max = 0
        Mx2_max = Mx2 - MX_max
        Mz2_max = Mz2 - MZ_max

        # Creation of the frequency array
        freq_kx = np.fft.fftfreq(Nx, 1/Nx).astype(np.integer, copy=False)
        freq_kz = np.fft.rfftfreq(Nz, 1/Nz).astype(np.integer, copy=False)
        freq_kx2 = freq_kx[0:Mx2]
        freq_kz2 = freq_kz[0:Mz2]

        # Create NDARRAY to fill with functiosn A, B, C...
        # for v...
        A = np.zeros((Mx * Mz, Mx * Mz,     N,    N), dtype=complex) # interacts with v
        B = np.zeros((Mx * Mz, Mx * Mz,     N,    N), dtype=complex) # interacts with v
        C = np.zeros((Mx * Mz, Mx * Mz - 1, N,    N), dtype=complex) # interacts with eta
        D = np.zeros((Mx * Mz, 1,           N,    N), dtype=complex) # interacts with u00
        E = np.zeros((Mx * Mz, 1,           N,    N), dtype=complex) # interacts with w00

        # for eta...
        F = np.zeros((Mx * Mz -1, Mx * Mz,      N, N), dtype=complex) # interacts with v
        G = np.zeros((Mx * Mz -1, Mx * Mz,      N, N), dtype=complex) # interacts with v
        H = np.zeros((Mx * Mz -1, Mx * Mz -1,   N, N), dtype=complex) # interacts with eta
        J = np.zeros((Mx * Mz -1, Mx * Mz -1,   N, N), dtype=complex) # interacts with eta
        K = np.zeros((Mx * Mz -1, 1,            N, N), dtype=complex) # interacts with u00
        L = np.zeros((Mx * Mz -1, 1,            N, N), dtype=complex) # interacts with w00

        # for u00...
        M = np.zeros( (1, Mx * Mz,      N, N), dtype=complex) # interacts with v
        NN = np.zeros((1, Mx * Mz,      N, N), dtype=complex) # interacts with v
        O = np.zeros( (1, Mx * Mz - 1,  N, N), dtype=complex) # interacts with eta
        # S

        # for W00...
        P = np.zeros((1, Mx * Mz,   N, N), dtype=complex) # interacts with v
        Q = np.zeros((1, Mx * Mz,   N, N), dtype=complex) # interacts with v
        R = np.zeros((1, Mx * Mz-1, N, N), dtype=complex) # interacts with eta
        S_u = np.zeros((1, 1, N, N), dtype=complex) # interacts with u00
        S_w = np.zeros((1, 1, N, N), dtype=complex) # interacts with w00

        # Laplacians...
        L1_v_inv =  np.zeros((Mx * Mz, Mx * Mz,         N, N), dtype=complex) # Laplacian
        L2_v =      np.zeros((Mx * Mz, Mx * Mz,         N, N), dtype=complex) # Laplacian**2
        L1_eta =    np.zeros((Mx * Mz -1, Mx * Mz -1,   N, N), dtype=complex) # Laplacian

        # actuation
        B_1_act = np.zeros((Mx * Mz, Mx *Mz, N, 2), dtype=complex) # v interacts with v
        B_2_act = np.zeros((Mx * Mz -1, Mx * Mz -1, N, 2), dtype=complex) # eta interacts with eta
        B_3_act = np.zeros((1, 1, N, 2), dtype=complex) # u00 interacts with u00
        B_4_act = np.zeros((1, 1, N, 2), dtype=complex) # w00 interacts with w00

        # Test matrix
        #T = np.zeros((Mx * Mz, Mx2 * Mz2, 1, 1), dtype=complex)

        ########################################################################
        # FILLING OPERATOR PARTIAL-MATRICES
        ########################################################################
        # i_kx = i_kx
        # i_kx2 = i_kx'
        # i_kx3 = i_kx - i_kx'
        # freq_kx[...] contains the actual modes number from np.fft.fftfreq

        print(' --> V')
        # ---------------------------------------------------------------------
        # for v modes (INcluded mode 0,0) ...
        # ---------------------------------------------------------------------
        for i_kx, i_kz, i_kx2, i_kz2 \
            in itertools.product(range(Mx), range(Mz), range(Mx), range(Mz)):

            i_KX3 = freq_kx[i_kx-i_kx2]%Mx
            i_KZ3 = freq_kz[i_kz-i_kz2]%Mz

            kx = freq_kx[i_kx]
            kz = freq_kz[i_kz]
            kx2 = freq_kx2[i_kx2]
            kz2 = freq_kz2[i_kz2]

            # --------------------------------------------------------------
            # ... convolved with v (INcluded mode 0,0)
            # --------------------------------------------------------------
            # Here are explained the indexes for dimension 3 and 4 for case A.
            # interaction of dv/dt (0:-2) with v (0:-2)
            A[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 1:-1] = \
                    A_(kx, kz, kx2, kz2)[1:N-1, 1:-1]
            # interaction of dv/dt (0:-2) with v+ (-2)
            A[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 0] = \
                    np.dot(A_(kx, kz, kx2, kz2), f_u)[1:-1]
            # interaction of dv/dt (0:-2) with v- (-1)
            A[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, -1] = \
                    np.dot(A_(kx, kz, kx2, kz2), f_l)[1:-1]
            # interaction of dv+/dt (-2) with [v, v+, v-] (:)
            #A[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 0, :] = 0 # already the case
            # interaction of dv-/dt (-1) with [v, v+, v-] (:)
            #A[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, -1, :] = 0 # already the case

            # TEST
            #T[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, :, :] = \
            #        BASIC_(kx, kz, kx2, kz2)

            if ((i_kx*Mz) + i_kz) == ((i_KX3*Mz2) + i_KZ3):
                L1_v_inv[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 1:-1] = \
                        np.linalg.inv(nabla2_v(kx, kz)[1:-1, 1:-1])
                        #scipy.linalg.inv(nabla2_v(kx, kz)[1:N-1, 1:N-1])
                # already the case:
                # L1_v_inv[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3,  1:-1,  0] = 0
                # L1_v_inv[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3,  1:-1, -1] = 0
                # L1_v_inv[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3,  0, 1:-1] = 0
                # L1_v_inv[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, -1, 1:-1] = 0

                # considering the interaction of dv+/dt (-2) with v+ (-2)
                L1_v_inv[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3,  0,  0] = 1
                L1_v_inv[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, -1, -1] = 1

                # interaction of dv/dt (1:-1) with v (1:-1)
                # IMPOSITION OF CLAMPED BCs
                L2_v[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 1:-1] = \
                        1 / Re * nabla4_v_clamped(kx, kz)[1:-1, 1:-1]
                # interaction of dv/dt (1:-1) with v+ (0)
                L2_v[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1,  0] = \
                        1 / tau_v[kx, kz] * np.dot(nabla2_v(kx, kz), f_u)[1:-1] \
                      + 1 / Re * np.dot(nabla4_v(kx, kz), f_u)[1:-1]
                # interaction of dv/dt (1:-1) with v- (-1)
                L2_v[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, -1] = \
                        1 / tau_v[kx, kz] * np.dot(nabla2_v(kx, kz), f_l)[1:-1] \
                      + 1 / Re * np.dot(nabla4_v(kx, kz), f_l)[1:-1]
                # interaction of dv+/dt (0) with v+ (0)
                L2_v[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 0, 0] = \
                      - 1 / tau_v[kx, kz]
                # interaction of dv-/dt (-1) with v- (-1)
                L2_v[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, -1, -1] = \
                      - 1 / tau_v[kx, kz]
                # already set to 0:
                # L2_v[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3,  0, 1:-1] = 0
                # L2_v[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, -1, 1:-1] = 0
                # L2_v[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3,  0, -1] = 0
                # L2_v[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, -1, 0] = 0

                if i_kx == 0 and i_kz == 0:
                    B_1_act[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, :, :] = \
                            np.zeros(shape=(N, 2))
                else:
                    B_1_act[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 0] = \
                            - 1 / tau_v[kx, kz] * np.dot(nabla2_v(kx, kz), f_u)[1:-1]
                    B_1_act[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 1] = \
                            - 1 / tau_v[kx, kz] * np.dot(nabla2_v(kx, kz), f_l)[1:-1]
                    B_1_act[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 0, 0] = \
                            1 / tau_v[kx, kz]
                    B_1_act[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, -1, 1] = \
                            1 / tau_v[kx, kz]
                    # already set to 0:
                    #B_1_act[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3,  0, 1] = 0
                    #B_1_act[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, -1, 0] = 0

            # --------------------------------------------------------------
            # ... convolved with eta (EXcluded mode 0,0)
            # --------------------------------------------------------------
            if not (i_KX3 == 0 and i_KZ3 == 0):
                B[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 1:-1] = \
                        B_(kx, kz, kx2, kz2)[1:-1, 1:-1]
                B[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 0] = \
                        np.dot(B_(kx, kz, kx2, kz2), f_u)[1:-1]
                B[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, -1] = \
                        np.dot(B_(kx, kz, kx2, kz2), f_l)[1:-1]
                #B[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3,  0, :] = 0 # already the case
                #B[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, -1, :] = 0 # already the case

                # the index " -1" is due to the fact that matrix C excludes
                # the interaction with eta(0,0), which corresponds to the colum
                # (i_KX3*Mz2) + i_KZ3 = 0,
                # but we dont want C to have an empty column.
                C[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, 1:-1] = \
                        C_(kx, kz, kx2, kz2)[1:-1, 1:-1]
                C[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, 0] = \
                        np.dot(C_(kx, kz, kx2, kz2), f_u_eta)[1:-1]
                C[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, -1] = \
                        np.dot(C_(kx, kz, kx2, kz2), f_l_eta)[1:-1]
                #C[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1,  0, :] = 0 # already the case
                #C[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, -1, :] = 0 # already the case

            # --------------------------------------------------------------
            # ... convolved with u00 and w00 (ONLY mode 0,0)
            # --------------------------------------------------------------
            if i_kx2 == 0 and i_kz2 ==0:
                D[(i_kx*Mz) + i_kz, 0, 1:-1, 1:-1] = \
                        D_(kx, kz, kx2, kz2)[1:-1, 1:-1]
                D[(i_kx*Mz) + i_kz, 0, 1:-1, 0] = \
                        np.dot(D_(kx, kz, kx2, kz2), f_u)[1:-1]
                D[(i_kx*Mz) + i_kz, 0, 1:-1, -1] = \
                        np.dot(D_(kx, kz, kx2, kz2), f_l)[1:-1]
                #D[(i_kx*Mz) + i_kz, 0,  0, :] = 0 # already the case
                #D[(i_kx*Mz) + i_kz, 0, -1, :] = 0 # already the case

                E[(i_kx*Mz) + i_kz, 0, 1:-1, 1:-1] = \
                        E_(kx, kz, kx2, kz2)[1:-1, 1:-1]
                E[(i_kx*Mz) + i_kz, 0, 1:-1, 0] = \
                        np.dot(E_(kx, kz, kx2, kz2), f_u)[1:-1]
                E[(i_kx*Mz) + i_kz, 0, 1:-1, -1] = \
                        np.dot(E_(kx, kz, kx2, kz2), f_l)[1:-1]
                #E[(i_kx*Mz) + i_kz, 0,  0, :] = 0 # already the case
                #E[(i_kx*Mz) + i_kz, 0, -1, :] = 0 # already the case

        # ---------------------------------------------------------------------
        # for eta modes (EXcluded mode 0,0) ...
        # ---------------------------------------------------------------------
        print(' --> ETA')
        for i_kx, i_kz, i_kx2, i_kz2 \
            in itertools.product(range(Mx), range(Mz), range(Mx), range(Mz)):

            if i_kx == 0 and i_kz == 0:
                continue

            i_KX3 = freq_kx[i_kx-i_kx2]%Mx
            i_KZ3 = freq_kz[i_kz-i_kz2]%Mz

            kx = freq_kx[i_kx]
            kz = freq_kz[i_kz]
            kx2 = freq_kx2[i_kx2]
            kz2 = freq_kz2[i_kz2]

            # --------------------------------------------------------------
            # ... convolved with v (INcluded mode 0,0)
            # --------------------------------------------------------------
            F[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3, 1:-1, 1:-1] = \
                    F_(kx, kz, kx2, kz2)[1:-1, 1:-1]
            F[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3, 1:-1,  0] = \
                    np.dot(F_(kx, kz, kx2, kz2), f_u)[1:-1]
            F[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3, 1:-1, -1] = \
                    np.dot(F_(kx, kz, kx2, kz2), f_l)[1:-1]
            # already set to 0:
            #F[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3,  0, :] = 0
            #F[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3, -1, :] = 0

            # --------------------------------------------------------------
            # ... convolved with eta (EXcluded mode 0,0)
            # --------------------------------------------------------------
            if not (i_KX3 == 0 and i_KZ3 == 0):
                G[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3, 1:-1, 1:-1] = \
                        G_(kx, kz, kx2, kz2)[1:-1, 1:-1]
                G[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3, 1:-1,  0] = \
                        np.dot(G_(kx, kz, kx2, kz2), f_u)[1:-1]
                G[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3, 1:-1, -1] = \
                        np.dot(G_(kx, kz, kx2, kz2), f_l)[1:-1]
                # already set to 0:
                #G[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3,  0, :] = 0
                #G[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3, -1, :] = 0

                H[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, 1:-1] = \
                        H_(kx, kz, kx2, kz2)[1:-1, 1:-1]
                H[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, 0] = \
                        np.dot(H_(kx, kz, kx2, kz2), f_u_eta)[1:-1]
                H[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, -1] = \
                        np.dot(H_(kx, kz, kx2, kz2), f_l_eta)[1:-1]
                # already set to 0:
                #H[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, 0, :] = 0
                #H[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, -1, :] = 0

                J[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, 1:-1] = \
                        J_(kx, kz, kx2, kz2)[1:-1, 1:-1]
                J[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, 0] = \
                        np.dot(J_(kx, kz, kx2, kz2), f_u_eta)[1:-1]
                J[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, -1] = \
                        np.dot(J_(kx, kz, kx2, kz2), f_l_eta)[1:-1]
                # already set to 0:
                #J[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1,  0, :] = 0
                #J[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, -1, :] = 0

                if ((i_kx*Mz) + i_kz) == ((i_KX3*Mz2) + i_KZ3):
                    # interaction of deta/dt (1:-1) with eta (1:-1)
                    # IMPOSITION OF DIRICHLET BCs on nabla2
                    L1_eta[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, 1:-1] = \
                        1 / Re * nabla2_eta(kx, kz)[1:-1, 1:-1]
                    # interaction of deta/dt (1:-1) with eta+ (0)
                    L1_eta[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, 0] = \
                        1 / Re * np.dot(nabla2_eta(kx, kz), f_u_eta)[1:-1] \
                      + 1 / tau_eta[kx, kz] * f_u_eta[1:-1]
                    # interaction of deta/dt (1:-1) with eta- (-1)
                    L1_eta[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, -1] = \
                        1 / Re * np.dot(nabla2_eta(kx, kz), f_l_eta)[1:-1] \
                      + 1 / tau_eta[kx, kz] * f_l_eta[1:-1]
                    # interaction of deta+/dt (0) with eta+ (0)
                    L1_eta[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, 0, 0] = \
                        - 1 / tau_eta[kx, kz]
                    # interaction of deta-/dt (-1) with eta- (-1)
                    L1_eta[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, -1, -1] = \
                        - 1 / tau_eta[kx, kz]
                    # already set to 0:
                    # L1_eta[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1,  0, 1:-1] = 0
                    # L1_eta[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, -1, 1:-1] = 0
                    # L1_eta[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1,  0, -1] = 0
                    # L1_eta[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, -1,  0] = 0

                    B_2_act[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, 0] = \
                        - 1 / tau_eta[kx, kz] * f_u_eta[1:-1]
                        #- 1 / tau_eta[kx, kz] * np.dot(nabla2_eta(kx, kz), f_u_eta)[1:-1]
                    B_2_act[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, 1] = \
                        - 1 / tau_eta[kx, kz] * f_l_eta[1:-1]
                        #- 1 / tau_eta[kx, kz] * np.dot(nabla2_eta(kx, kz), f_l_eta)[1:-1]
                    B_2_act[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, 0, 0] = \
                        1 / tau_eta[kx, kz]
                    B_2_act[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3 -1, -1, 1] = \
                        1 / tau_eta[kx, kz]
                    # already set to 0:
                    #B_2_act[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3, -2, 1] = 0
                    #B_2_act[(i_kx*Mz) + i_kz -1, (i_KX3*Mz2) + i_KZ3, -1, 0] = 0


            # --------------------------------------------------------------
            # ... convolved with u00 and w00 (ONLY mode 0,0)
            # --------------------------------------------------------------
            if i_kx2 == 0 and i_kz2 ==0:
                K[(i_kx*Mz) + i_kz -1, 0, 1:-1, 1:-1] = \
                        K_(kx, kz, kx2, kz2)[1:-1, 1:-1]
                K[(i_kx*Mz) + i_kz -1, 0, 1:-1, 0] = \
                        np.dot(K_(kx, kz, kx2, kz2), f_u)[1:-1]
                K[(i_kx*Mz) + i_kz -1, 0, 1:-1, -1] = \
                        np.dot(K_(kx, kz, kx2, kz2), f_l)[1:-1]
                # already set to 0:
                #K[(i_kx*Mz) + i_kz -1, 0,  0, :] = 0
                #K[(i_kx*Mz) + i_kz -1, 0, -1, :] = 0

                L[(i_kx*Mz) + i_kz -1, 0, 1:-1, 1:-1] = \
                        L_(kx, kz, kx2, kz2)[1:-1, 1:-1]
                L[(i_kx*Mz) + i_kz -1, 0, 1:-1, 0] = \
                        np.dot(L_(kx, kz, kx2, kz2), f_u)[1:-1]
                L[(i_kx*Mz) + i_kz -1, 0, 1:-1, -1] = \
                        np.dot(L_(kx, kz, kx2, kz2), f_l)[1:-1]
                # already set to 0:
                #L[(i_kx*Mz) + i_kz -1, 0,  0, :] = 0
                #L[(i_kx*Mz) + i_kz -1, 0, -1, :] = 0

        # ---------------------------------------------------------------------
        # for u00 and w00 (ONLY mode 0,0) ...
        # ---------------------------------------------------------------------
        print(' --> U00 W00')
        for i_kx, i_kz, i_kx2, i_kz2 \
            in itertools.product({0}, {0}, range(Mx), range(Mz)):

            i_KX3 = freq_kx[i_kx-i_kx2]%Mx
            i_KZ3 = freq_kz[i_kz-i_kz2]%Mz

            kx = freq_kx[i_kx]
            kz = freq_kz[i_kz]
            kx2 = freq_kx2[i_kx2]
            kz2 = freq_kz2[i_kz2]

            # --------------------------------------------------------------
            # ... convolved with v (INcluded mode 0,0)
            # --------------------------------------------------------------
            M[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 1:-1] = \
                    M_(kx, kz, kx2, kz2)[1:-1, 1:-1]
            M[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1,  0] = \
                    np.dot(M_(kx, kz, kx2, kz2), f_u)[1:-1]
            M[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, -1] = \
                    np.dot(M_(kx, kz, kx2, kz2), f_l)[1:-1]
            # already set to 0:
            #M[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3,  0, :] = 0
            #M[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, -1, :] = 0

            P[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 1:-1] = \
                    P_(kx, kz, kx2, kz2)[1:-1, 1:-1]
            P[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 0] = \
                    np.dot(P_(kx, kz, kx2, kz2), f_u)[1:-1]
            P[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, -1] = \
                    np.dot(P_(kx, kz, kx2, kz2), f_l)[1:-1]
            # already set to 0:
            #P[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3,  0, :] = 0
            #P[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, -1, :] = 0

            # --------------------------------------------------------------
            # ... convolved with eta (EXcluded mode 0,0)
            # --------------------------------------------------------------
            if not (i_KX3 == 0 and i_KZ3 == 0):
                NN[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 1:-1] = \
                        N_(kx, kz, kx2, kz2)[1:-1, 1:-1]
                NN[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 0] = \
                        np.dot(N_(kx, kz, kx2, kz2), f_u)[1:-1]
                NN[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, -1] = \
                        np.dot(N_(kx, kz, kx2, kz2), f_l)[1:-1]
                # already set to 0:
                #NN[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3,  0, :] = 0
                #NN[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, -1, :] = 0

                O[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, 1:-1] = \
                        O_(kx, kz, kx2, kz2)[1:-1, 1:-1]
                O[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, 0] = \
                        np.dot(O_(kx, kz, kx2, kz2), f_u_eta)[1:-1]
                O[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, -1] = \
                        np.dot(O_(kx, kz, kx2, kz2), f_l_eta)[1:-1]
                # already set to 0:
                #O[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1,  0, :] = 0
                #O[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, -1, :] = 0

                Q[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 1:-1] = \
                        Q_(kx, kz, kx2, kz2)[1:-1, 1:-1]
                Q[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, 0] = \
                        np.dot(Q_(kx, kz, kx2, kz2), f_u)[1:-1]
                Q[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, 1:-1, -1] = \
                        np.dot(Q_(kx, kz, kx2, kz2), f_l)[1:-1]
                # already set to 0:
                #Q[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3,  0, :] = 0
                #Q[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3, -1, :] = 0

                R[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, 1:-1] = \
                        R_(kx, kz, kx2, kz2)[1:-1, 1:-1]
                R[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, 1:-1,  0] = \
                        np.dot(R_(kx, kz, kx2, kz2), f_u_eta)[1:-1]
                R[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, 1:-1, -1] = \
                        np.dot(R_(kx, kz, kx2, kz2), f_l_eta)[1:-1]
                # already set to 0:
                #R[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1,  0, :] = 0
                #R[(i_kx*Mz) + i_kz, (i_KX3*Mz2) + i_KZ3 -1, -1, :] = 0

            # --------------------------------------------------------------
            # ... convolved with u00 and w00 (ONLY mode 0,0)
            # --------------------------------------------------------------
            if i_kx2 == 0 and i_kz2 == 0:
                # interaction of du0/dt (1:-1) with u0 (1:-1)
                # IMPOSITION OF DIRICHLET BCs on S_
                S_u[0, 0, 1:-1, 1:-1] = S_(Re)[1:-1, 1:-1]
                # interaction of du0/dt (1:-1) with u+ (0)
                S_u[0, 0, 1:-1, 0] = \
                    1 / tau_u * f_u[1:-1] \
                  + np.dot(S_(Re), f_u)[1:-1]
                # interaction of du0/dt (1:-1) with u- (-1)
                S_u[0, 0, 1:-1, -1] = \
                    1 / tau_u * f_l[1:-1] \
                  + np.dot(S_(Re), f_l)[1:-1]
                # interaction of du0+/dt (0) with u0+ (0)
                S_u[0, 0, 0, 0] = \
                    - 1 / tau_u
                # interaction of du0-/dt (-1) with u0- (-1)
                S_u[0, 0, -1, -1] = \
                    - 1 / tau_u
                # already set to 0:
                # S_u[0, 0,  0, 1:-1] = 0
                # S_u[0, 0, -1, 1:-1] = 0
                # S_u[0, 0,  0, -1] = 0
                # S_u[0, 0, -1,  0] = 0

                # interaction of du0/dt (1:-1) with u0 (1:-1)
                # IMPOSITION OF DIRICHLET BCs on S_
                S_w[0, 0, 1:-1, 1:-1] = S_(Re)[1:-1, 1:-1]
                # interaction of du0/dt (0:-2) with u+ (-2)
                S_w[0, 0, 1:-1, 0] = \
                    1 / tau_w * f_u[1:-1] \
                  + np.dot(S_(Re), f_u)[1:-1]
                # interaction of du0/dt (1:-1) with u- (-1)
                S_w[0, 0, 1:-1, -1] = \
                    1 / tau_w * f_l[1:-1] \
                  + np.dot(S_(Re), f_l)[1:-1]
                # interaction of dw0+/dt (0) with w0+ (0)
                S_w[0, 0, 0, 0] = \
                    - 1 / tau_w
                # interaction of dw0-/dt (-1) with w0- (-1)
                S_w[0, 0, -1, -1] = \
                    - 1 / tau_w
                # already set to 0:
                # S_w[0, 0,  0, 1:-1] = 0
                # S_w[0, 0, -1, 1:-1] = 0
                # S_w[0, 0,  0, -1] = 0
                # S_w[0, 0, -1,  0] = 0

                B_3_act[0, 0, 1:-1, 0] = \
                    - 1 / tau_u * f_u[1:-1]
                B_3_act[0, 0, 1:-1, 1] = \
                    - 1 / tau_u * f_l[1:-1]
                B_3_act[0, 0, 0, 0] = \
                    1 / tau_u
                B_3_act[0, 0, -1, 1] = \
                    1 / tau_u
                # already set to 0:
                #B_3_act[0, 0,  0, 1] = 0
                #B_3_act[0, 0, -1, 0] = 0

                B_4_act[0, 0, 1:-1, 0] = \
                    - 1 / tau_w * f_u[1:-1]
                B_4_act[0, 0, 1:-1, 1] = \
                    - 1 / tau_w * f_l[1:-1]
                B_4_act[0, 0, 0, 0] = \
                    1 / tau_w
                B_4_act[0, 0, -1, 1] = \
                    1 / tau_w
                # already set to 0:
                #B_4_act[0, 0,  0, 1] = 0
                #B_4_act[0, 0, -1, 0] = 0

        ########################################################################
        # FLATTEN OPERATOR PARTIAL-MATRICES
        ########################################################################
        print(' --> STACKING')

        # if necessary to investigate each block :
        if False:
            for ii_kx, ii_kz, ii_kx2, ii_kz2 \
                   in itertools.product(range(Mx), range(Mz), range(Mx), range(Mz)):
                if ii_kx == 1 and ii_kz == 1 and ii_kx2 == 1 and ii_kz2 == 1:
                    print("(",ii_kx, " / ", ii_kz, " / ", ii_kx2, " / ", ii_kz2, ")")
                    X1_ = L1_v_inv[(ii_kx*Mz) + ii_kz, (ii_kx2*Mz) + ii_kz2, :, :]
                    if np.linalg.norm(X_, axis=(0,1)) > 1E-4:
                        print("X1 : \n", X1_)
                    X2_ = L2_v[(ii_kx*Mz) + ii_kz, (ii_kx2*Mz) + ii_kz2, :, :]
                    if np.linalg.norm(X_, axis=(0,1)) > 1E-4:
                        print("X2 : \n", X2_)
                    X3_ = A[(ii_kx*Mz) + ii_kz, (ii_kx2*Mz) + ii_kz2, :, :]
                    if np.linalg.norm(X_, axis=(0,1)) > 1E-4:
                        print("X3 : \n", X3_)

        # TO FLATTEN THE NDARRAY INTO A 2D-MATRIX
        A = np.hstack(np.hstack(A))
        B = np.hstack(np.hstack(B))
        C = np.hstack(np.hstack(C))
        D = np.hstack(np.hstack(D))
        E = np.hstack(np.hstack(E))
        F = np.hstack(np.hstack(F))
        G = np.hstack(np.hstack(G))
        H = np.hstack(np.hstack(H))
        J = np.hstack(np.hstack(J))
        K = np.hstack(np.hstack(K))
        L = np.hstack(np.hstack(L))
        M = np.hstack(np.hstack(M))
        NN = np.hstack(np.hstack(NN))
        O = np.hstack(np.hstack(O))
        P = np.hstack(np.hstack(P))
        Q = np.hstack(np.hstack(Q))
        R = np.hstack(np.hstack(R))
        S_u = np.hstack(np.hstack(S_u))
        S_w = np.hstack(np.hstack(S_w))

        B_1_act = np.hstack(np.hstack(B_1_act))
        B_2_act = np.hstack(np.hstack(B_2_act))
        B_3_act = np.hstack(np.hstack(B_3_act))
        B_4_act = np.hstack(np.hstack(B_4_act))

        L1_v_inv = np.hstack(np.hstack(L1_v_inv))
        L2_v = np.hstack(np.hstack(L2_v))
        L1_eta = np.hstack(np.hstack(L1_eta))

        #T = np.hstack(np.hstack(T))

        ########################################################################
        # FINAL CONCATENATION FOR OPERATOR MATRICES
        ########################################################################

        # Building inverse matrix A_f
        E_f = np.eye((Mx * Mz + Mx * Mz - 1 + 2) * (N), dtype=complex)
        E_f[0:Mx*Mz*(N), 0:Mx*Mz*(N)] = L1_v_inv

        # Building operator matrix B_f
        A_f = np.concatenate((np.concatenate((L2_v + A + B,
                             C,
                             D,
                             E), axis=1),
                          np.concatenate((F + G,
                             L1_eta + H + J,
                             K,
                             L), axis=1),
                          np.concatenate((M + NN,
                             O,
                             S_u,
                             Z), axis=1),
                          np.concatenate((P + Q,
                             R,
                             Z,
                             S_w), axis=1)
                          ), axis=0)

        B_f = np.zeros(((Mx * Mz + Mx * Mz - 1 + 2) * (N),
                        (Mx * Mz + Mx * Mz - 1 + 2) * (2)),
                       dtype=complex)
        B_f[0 : Mx*Mz*N,
            0 : Mx*Mz*2] = \
                B_1_act
        B_f[Mx*Mz*N : Mx*Mz*N + (Mx*Mz - 1) * N,
            Mx*Mz*2 : Mx*Mz*2 + (Mx*Mz - 1) * 2] = \
                B_2_act
        B_f[Mx*Mz*N + (Mx*Mz - 1) * N : Mx*Mz*N + (Mx*Mz - 1) * N + N ,
            Mx*Mz*2 + (Mx*Mz - 1) * 2 : Mx*Mz*2 + (Mx*Mz - 1) * 2 + 2] = \
                B_3_act
        B_f[Mx*Mz*N + (Mx*Mz - 1) * N + N : Mx*Mz*N + (Mx*Mz - 1)*N + N + N,
            Mx*Mz*2 + (Mx*Mz - 1) * 2 + 2 : Mx*Mz*2 + (Mx*Mz - 1)*2 + 2 + 2] = \
                B_4_act

        # Save test matrix if necessary for dev.
        #np.savetxt('T.real', T.real, fmt='%+.0f')
        #np.savetxt('T.imag', T.imag, fmt='%+.0f')

        print(' --> DONE')

        if memory:
            print('MEMORY used for operator creation :',
                    resource.getrusage(resource.RUSAGE_SELF).ru_maxrss /1000, 'MB')

        return E_f, A_f, B_f

    def op_nonlaminar_baseflow_objectives(self,
            ff: FlowField,
            cholesky = False):
        """"
        Define the two matrices used to determine the cost function to minimize.
        Considering the objective z = [ C1  x ]
                                      [ D12 q ]
        z* z = x* C1* C1 x + q* D12* D12 q
        C1* C1 is determine from the stored energy.
        D12* D12 = I.

        ----------
        Keyword arguments:
        ff:FlowField -- FlowField flow
        cholesky --- Boolean, True if C1 needed, otherwise False for C1C1
            (C1 calculated from Cholesky decomposition of C1C1)

        Return:
        if cholesky == True: C1, D12
        if cholesky == False: C1C1, D12D12

        C1 or C1C1 (denoted C_1^H C_1) --
            Cost function part related to the state.
        D12D12 -- denoted D_12^H D_12, equal to I,
            Cost function part related to the control (should be multiplied by kappa).
        """
        ########################################################################
        # Definition of Constants
        ########################################################################

        #j = np.complex(0, 1)
        N = ff.Ny # Wall normal resolution, equal to My
        Nx = ff.Nx # Streamwise resolution
        Nz = ff.Nz # Spanwise resolution
        Mx = ff.Mx
        Mz = ff.Mz
        I = np.eye(N)
        Z = np.zeros((N, N))

        # Definition of Weights matrix
        y, weight = chebyshev.clencurt(N)
        W = np.diag(weight[1:N-1])

        # Definition of Differentiation matrix
        nodes, DM = chebyshev.chebdiff(N, 4)
        D1 = DM[0, :, :]

        # Creation of the frequency array
        freq_kx = np.fft.fftfreq(Nx, 1/Nx).astype(np.integer, copy=False)
        freq_kz = np.fft.rfftfreq(Nz, 1/Nz).astype(np.integer, copy=False)

        ########################################################################
        # Definition of Tile matrices Phi1 and Phi2
        ########################################################################
        phi1 = np.zeros((N, N), dtype=complex)
        phi1[0, 0] = 1
        phi1[1:-1, 1:-1] = np.dot(W.T, W) # diagonal matrix, of size [N-1, N-1]
        phi1[-1, -1] = 1
        # rest of phi1 is 0

        phi2 = np.zeros((N, N), dtype=complex)
        D1D1 = np.dot(D1.T, D1)[1:N-1, 1:N-1]
        phi2[1:-1, 1:-1] = np.dot(W.T, np.dot(D1D1, W))
        # rest of phi2 is 0

        ########################################################################
        # Definition by blocks of matrix C_1^H * C_1, denoted as C1C1
        ########################################################################

        # initialization of blocks
        # C1C1 = diag(C1C1_1, C1C1_2, C1C1_3, C1C1_4)
        C1C1_1 = np.zeros((Mx * Mz, Mx * Mz, N, N), dtype=complex)
        C1C1_2 = np.zeros((Mx * Mz -1, Mx * Mz -1, N, N), dtype=complex)
        C1C1_3 = np.zeros((1, 1, N, N), dtype=complex)
        C1C1_4 = np.zeros((1, 1, N, N), dtype=complex)

        # filling each blocks
        for i_kx, i_kz in itertools.product(range(Mx), range(Mz)):
            if i_kx*Mz + i_kz == 0:
                C1C1_1[(i_kx*Mz) + i_kz, (i_kx*Mz) + i_kz, :, :] = phi1 / (4*Mx*Mz)
                C1C1_3[(i_kx*Mz) + i_kz, (i_kx*Mz) + i_kz, :, :] = phi1 / (4*Mx*Mz)
                C1C1_4[(i_kx*Mz) + i_kz, (i_kx*Mz) + i_kz, :, :] = phi1 / (4*Mx*Mz)
            else:
                kx = freq_kx[i_kx]
                kz = freq_kz[i_kz]
                alpha = 2*np.pi*kx/ff.attributes['Lx']
                beta = 2*np.pi*kz/ff.attributes['Lz']

                C1C1_1[(i_kx*Mz) + i_kz, (i_kx*Mz) + i_kz, :, :] = \
                        (phi1 + phi2 / (alpha**2 + beta**2) ) / (4*Mx*Mz)
                C1C1_2[(i_kx*Mz) + i_kz - 1 , (i_kx*Mz) + i_kz -1, :, :] = \
                        phi1 / ((alpha**2 + beta**2) * (4*Mx*Mz))

        # stacking into 2D matrix each block
        C1C1_1 = np.hstack(np.hstack(C1C1_1))
        C1C1_2 = np.hstack(np.hstack(C1C1_2))
        C1C1_3 = np.hstack(np.hstack(C1C1_3))
        C1C1_4 = np.hstack(np.hstack(C1C1_4))

        # final C1C1
        C1C1 = np.zeros(((Mx * Mz + Mx * Mz - 1 + 2) * (N),
                        (Mx * Mz + Mx * Mz - 1 + 2) * (N)),
                        dtype=complex)
        C1C1[0 : Mx*Mz*N,
             0 : Mx*Mz*N] = C1C1_1
        C1C1[Mx*Mz*N : (Mx*Mz + Mx*Mz - 1)*N,
             Mx*Mz*N : (Mx*Mz + Mx*Mz - 1)*N] = C1C1_2
        C1C1[(Mx*Mz + Mx*Mz - 1)*N : (Mx*Mz + Mx*Mz - 1 + 1)*N,
             (Mx*Mz + Mx*Mz - 1)*N : (Mx*Mz + Mx*Mz - 1 + 1)*N] = C1C1_3
        C1C1[(Mx*Mz + Mx*Mz - 1 + 1)*N : (Mx*Mz + Mx*Mz - 1 + 2)*N,
             (Mx*Mz + Mx*Mz - 1 + 1)*N : (Mx*Mz + Mx*Mz - 1 + 2)*N] = C1C1_4

        ########################################################################
        # Definition by blocks of matrix D_12^H * D_12, denoted as D12D12
        # and return
        ########################################################################
        if cholesky:
            # Cholesky decomposition
            C1 = np.linalg.cholesky(C1C1)

            # Cholesky decomposition only returns square matrices, we can define D12 as
            D12 = np.zeros(((Mx*Mz + Mx*Mz - 1 + 2)*N, (Mx*Mz + Mx*Mz - 1 + 2)*2), dtype=complex)
            np.fill_diagonal(D12, 1)

            return C1, D12

        else:
            D12D12 = np.eye((Mx*Mz + Mx*Mz - 1 + 2) *2, dtype=complex)
            return C1C1, D12D12

    def op_reduce_actuation_modes(self,
            ff: FlowField,
            B, D = None,
            kx_v = 1,
            kz_v = 1,
            kx_eta = -1,
            kz_eta = -1,
            uw = True,
            cholesky = False):
        """
        Reduce the actuation matrices into a few actuation modes.
        The new modes are defined as:
        - for the wall-normal velocity, maximal steam/span-wise modes are defined with:
            kx_v, kz_v
        - for the wall-normal vorticity, maximal steam/span-wise modes are defined with:
            kx_eta, kz_eta
        - for the basemode of u and w, we keep them is the bool 'uw' is True.

        if kx_X == -1, no modes is taken
        if kx_X == 0, only the basemode in this direction is taken
        if kx_X == 1, we will take moes [0, 1, -1]

        ----------
        Keyword arguments:
        ff:FlowField -- FlowField flow
        B -- B matrix to reduce (see op_nonlaminar_baseflow_actuated),
            where dimension 0 is untouched and dimension 1 is reduced,
            or vector where dimension 0 is reduced.
        D -- matrix to reduce (see op_nonlaminar_baseflow_objectives)
            if cholesky == True: D12
            if cholesky == False: D12D12
        kx_v -- max streamwise mode for wall-normal velocity
        kz_v -- max spanwise mode for wall-normal velocity
        kx_eta -- max streamwise mode for wall-normal vorticity
        kz_eta -- max spanwise mode for wall-normal vorticity
        uw -- bool, True if we keep actuation on u00 and w00
        cholesky --- Boolean, True if C1 needed, otherwise False for C1C1
            (C1 calculated from Cholesky decomposition of C1C1)

        Return:
        N_aq -- new number of actuation modes
        B -- reduced B
        D12 or D12D12 -- reduced D
            if cholesky == True: D12
            if cholesky == False: D12D12

        where D12 = cholesky(D12D12)
        (see op_nonlaminar_baseflow_objectives)
        """
        # in case of vector given in B:
        if B.ndim == 1:
            vect_bool = True
            B = B[np.newaxis, :]
        else:
            vect_bool = False

        # Definition of Constants
        N = ff.Ny # Wall normal resolution
        Nx = ff.Nx # Streamwise resolution
        Nz = ff.Nz # Spanwise resolution
        Mx = ff.Mx
        Mz = ff.Mz

        # Creation of the frequency array
        freq_kx = np.fft.fftfreq(Nx, 1/Nx).astype(np.integer, copy=False)
        freq_kz = np.fft.rfftfreq(Nz, 1/Nz).astype(np.integer, copy=False)

        # creation of table to recognize the modes to keep
        tab_v = np.ones((Mx * Mz, 2))#, dtype=complex)
        tab_eta = np.ones((Mx * Mz - 1, 2))#, dtype=complex)

        # mode selection
        for i_kx, i_kz in itertools.product(range(Mx), range(Mz)):
            kx = freq_kx[i_kx]
            kz = freq_kz[i_kz]

            if abs(kx) > kx_v or abs(kz) > kz_v:
                tab_v[(i_kx * Mz) + i_kz, :] = 0
            if (abs(kx) > kx_eta or abs(kz) > kz_eta) and not(i_kx == 0 and i_kz == 0):
                tab_eta[(i_kx * Mz) + i_kz - 1, :] = 0

        tab_uw = np.ones((2, 2)) if uw else np.zeros((2,2))

        tab_v = np.hstack(tab_v)
        tab_eta = np.hstack(tab_eta)
        tab_uw = np.hstack(tab_uw)

        modes = np.concatenate((tab_v, tab_eta, tab_uw))

        # truncation of B
        indices = np.where(modes)
        N_aq = indices[0].shape[0]
        B = np.take(B, indices = indices[0], axis=1)
        # if you want to keep the same shape and nullyfy the corresponding row,
        # comment previous and uncomment these two:
        # B_new = np.zeros(shape=B.shape, dtype=complex)
        # B_new[:, indices] = B[:, indices]

        if vect_bool:
            B = B[0, :]

        # truncation of D (actually create a new matrix D)
        if D is not None:
            if cholesky:
                D12 = np.take(D, indices = indices[0], axis=1)
                # or by definition :
                # D12 = np.zeros(((Mx*Mz + Mx*Mz - 1 + 2)*N, (Mx*Mz + Mx*Mz - 1 + 2) * N_aq), dtype=complex)
                # np.fill_diagonal(D12, 1)
                return N_aq, B, D12

            else:
                D = np.take(D, indices = indices[0], axis=1)
                D12D12 = np.take(D, indices = indices[0], axis=0)
                # or by definition :
                # D12D12 = np.eye((Mx*Mz + Mx*Mz - 1 + 2) * N_aq, dtype=complex)
                return N_aq, B, D12D12
        else:
            return N_aq, B, None

    def op_mapping(self, ff: FlowField, truncation=True):
        """
        NOT FOR THE ACTUATED SYSTEM

        [ u v w ] = C [ v eta u00 w00 ]
        [ v eta u00 w00 ].T = Cinv [ u v w].T

        Return:
        C --
        Cinv --
        """
        #u = ff.u

        Nx = ff.Nz
        Ny = ff.Ny
        if truncation:
            N = Ny-2
        else:
            N = Ny
        Nz = ff.Nz
        Mx = ff.Mx
        Mz = ff.Mz

        I = np.eye(N)
        Z = np.zeros(N)
        nodes, DM = chebyshev.chebdiff(Ny, 1)

        if truncation:
            D = DM[0, 1:-1, 1:-1]
        else:
            D = DM[0, :, :]

        ###########
        # Block matrices definition
        ###########

        # vector of alpha and beta
        alpha_vect = np.fft.fftfreq(Nx, 1.0 / Nx) * 1 * 2 * np.pi / ff.attributes['Lx']
        alpha_diag = np.diag(alpha_vect)

        beta_vect = np.fft.rfftfreq(Nz, 1.0 / Nz) * 1 * 2 * np.pi / ff.attributes['Lz']
        beta_diag = np.diag(beta_vect)

        # Matrix of beta*I of size [Mz*N, Mz*N]
        # [ beta0*I    0         0     ]
        # [   0      beta1*I     0     ]
        # [  ...      ...       ...    ]
        # [   0        0      betaMz*I ]
        beta_I0 = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', beta_diag, I)))

        # Matrix of beta*I of size [Mx*Mz*N, Mx*Mz*N]
        # to correspond to beta for each couple (alpha, beta)
        # [ beta0*I   0      0         0       ... ] -> for (alpha0, beta0)
        # [   0      ...     0         0       ... ] -> for (alphaX, beta0)
        # [   0       0    beta0*I     0       ... ] -> for (alphaN, beta0)
        # [   0       0      0       beta1*I   ... ] -> for (alpha0, beta1)
        # [  ...     ...    ...       ...      ... ] -> for (...   , betaX)
        beta_I = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', np.eye(Mx), beta_I0)))

        # Same as beta_I0 and beta_I but for alpha
        alpha_I0 = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', alpha_diag, I)))
        alpha_I = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', alpha_I0, np.eye(Mz))))

        # Same as beta_I0 and beta_I but multiplied by D,
        # first differentiation matrix for Chebyshev discretisation in wall-normal direction,
        # instead of I

        # Old line with problem (probably possible to write it in one line):
        # beta_D0 = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', beta_diag, D)))
        # beta_D = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', np.eye(Mx), beta_D0)))

        # Correction :
        beta_D0 = np.zeros((Mz*N, Mz*N), dtype=complex)
        for kz in range(Mz):
            #print(kz, ' -> ', beta_vect[kx])
            beta_D0[kz*N:(kz+1)*N,
                    kz*N:(kz+1)*N] = \
                np.dot(beta_vect[kz], D)
                #beta_vect[kz] * D

        beta_D = np.zeros((Mx*Mz*N, Mx*Mz*N), dtype=complex)
        for kx in range(Mx):
            for kz in range(Mz):
                #print(kx, ' -> ', alpha_vect[kx])
                #print(kz, ' -> ', beta_vect[kz])
                beta_D[(kx*Mz + kz)*N:(kx*Mz + kz + 1)*N,
                       (kx*Mz + kz)*N:(kx*Mz + kz + 1)*N] = \
                    beta_D0[kz*N:(kz+1)*N, kz*N:(kz+1)*N]

        # Same as beta_D0 and beta_D but for alpha

        # Old line with problem (probably possible to write it in one line):
        #alpha_D0 = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', alpha_diag, D)))
        #alpha_D = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', alpha_D0, np.eye(Mz))))

        # Correction :
        alpha_D0 = np.zeros((Mx*N, Mx*N), dtype=complex)
        for kx in range(Mx):
            #print(kx, ' -> ', alpha_vect[kx])
            alpha_D0[kx*N:(kx+1)*N,
                     kx*N:(kx+1)*N] = \
                np.dot(alpha_vect[kx], D)
                #alpha_vect[kx] * D

        alpha_D = np.zeros((Mx*Mz*N, Mx*Mz*N), dtype=complex)
        for kz in range(Mz):
            for kx in range(Mx):
                #print(kx, ' -> ', alpha_vect[kx])
                #print(kz, ' -> ', beta_vect[kz])
                alpha_D[(kx*Mz + kz)*N:(kx*Mz + kz + 1)*N,
                        (kx*Mz + kz)*N:(kx*Mz + kz + 1)*N] = \
                    alpha_D0[kx*N:(kx+1)*N, kx*N:(kx+1)*N]

        cc_ad = np.zeros(shape=(Mx*Mz, Mx*Mz, N, N), dtype=complex)
        cc_ai = np.zeros(shape=(Mx*Mz, Mx*Mz, N, N), dtype=complex)
        cc_bd = np.zeros(shape=(Mx*Mz, Mx*Mz, N, N), dtype=complex)
        cc_bi = np.zeros(shape=(Mx*Mz, Mx*Mz, N, N), dtype=complex)

        for i_kx, i_kz in itertools.product(range(Mx), range(Mz)):
            alpha = alpha_vect[i_kx]
            beta = beta_vect[i_kz]

            if alpha == 0 and beta == 0:
                continue

            cc_ad[(i_kx*Mz) + i_kz, (i_kx*Mz) + i_kz, :, :] = \
                1j * alpha * D / (alpha**2 + beta**2)
            cc_ai[(i_kx*Mz) + i_kz, (i_kx*Mz) + i_kz, :, :] = \
                1j * alpha * I / (alpha**2 + beta**2)
            cc_bd[(i_kx*Mz) + i_kz, (i_kx*Mz) + i_kz, :, :] = \
                1j * beta * D / (alpha**2 + beta**2)
            cc_bi[(i_kx*Mz) + i_kz, (i_kx*Mz) + i_kz, :, :] = \
                1j * beta * I / (alpha**2 + beta**2)

        cc_ad = np.hstack(np.hstack(cc_ad))
        cc_ai = np.hstack(np.hstack(cc_ai))
        cc_bd = np.hstack(np.hstack(cc_bd))
        cc_bi = np.hstack(np.hstack(cc_bi))

        # matrix 1 / k2
        c_I = beta_I**2 + alpha_I**2
        d_I = np.eye(c_I.shape[0])
        #d_I = np.zeros(c_I.shape)
        #for i in range(N, c_I.shape[0]):
        #    d_I[i, i] = 1. / c_I[i, i]

        ###########
        # C
        ###########
        # [ u v w ] = C [ v eta u00 w00 ]
        #
        # C = [ C1  C2  C3 ]
        #     [ C4   0   0 ]
        #     [ C5  C6  C7 ]

        C = np.zeros(shape=(3*Mx*Mz, Mx*Mz + Mx*Mz - 1 + 2, N, N), dtype=complex)

        # C3
        C[0, -2, :, :] = I
        # C4
        C[Mx*Mz:2*Mx*Mz, 0:Mx*Mz, :, :] = \
            (np.einsum('ij, kl -> ijkl', np.eye(Mx*Mz, Mx*Mz), I))
        # C7
        C[2*Mx*Mz, -1, :, :] = I

        C = np.hstack(np.hstack(C))

        # C1
        C[0:Mx*Mz*N, 0:Mx*Mz*N] = \
            cc_ad[:, :]
            #1j * np.dot(d_I, alpha_D)
        # C2
        C[0:Mx*Mz*N, Mx*Mz*N:(2*Mx*Mz-1)*N] = \
            - cc_bi[:, N::]
            #- 1j * np.dot(d_I, beta_I)[:, N::]
        # C5
        C[2*Mx*Mz*N:3*Mx*Mz*N, 0:Mx*Mz*N] = \
            cc_bd[:, :]
            #1j * np.dot(d_I, beta_D)
        # C6
        C[2*Mx*Mz*N:3*Mx*Mz*N, Mx*Mz*N:(2*Mx*Mz-1)*N] = \
            cc_ai[:, N::]
            #1j * np.dot(d_I, alpha_I)[:, N::]

        #np.savetxt('C_real', C.real, fmt='%+.1f')
        #np.savetxt('C_imag', C.imag, fmt='%+.1f')

        ###########
        # INVERSE C
        ###########
        # [ v eta u00 w00 ].T = Cinv [ u v w].T
        #
        # Cinv = [   0   Cinv1    0   ]
        #        [ Cinv2   0    Cinv3 ]
        #        [ Cinv4   0    Cinv5 ]

        Cinv = np.zeros(shape=((Mx*Mz + Mx*Mz - 1 + 2), 3 * Mx*Mz, N, N),
                        dtype=complex)

        # Cinv1
        Cinv[0:Mx*Mz, Mx*Mz:2*Mx*Mz, :, :] = \
            (np.einsum('ij, kl -> ijkl', np.eye(Mx*Mz, Mx*Mz), I))
        # Cinv4
        Cinv[-2, 0, :, :] = I
        # Cinv5
        Cinv[-1, 2*Mx*Mz, :, :] = I

        Cinv = np.hstack(np.hstack(Cinv))

        # Cinv2
        Cinv[Mx*Mz*N:(2*Mx*Mz-1)*N, 0:Mx*Mz*N] = \
            1j * beta_I[N::, :]
        # Cinv3
        Cinv[Mx*Mz*N:(2*Mx*Mz-1)*N, 2*Mx*Mz*N:3*Mx*Mz*N] = \
            - 1j * alpha_I[N::, :]

        #np.savetxt('Cinv_real', Cinv.real, fmt='%+.0f')
        #np.savetxt('Cinv_imag', Cinv.imag, fmt='%+.0f')

        ###########
        # CHECK
        ###########
        if False:
            P1 = np.dot(W, C)
            P2 = np.dot(Cinv, Winv) #np.dot(np.dot(W, C), np.dot(Cinv, Winv))
            P = np.dot(P2, P1)
            np.savetxt('P_real', P.real, fmt='%+.1f')
            np.savetxt('P_imag', P.imag, fmt='%+.1f')
            # P = I is OK

        return C, Cinv

    def op_mapping_actuated(self, ff: FlowField):
        """
        [ u v w ] = C [ v eta u00 w00 ]
        [ v eta u00 w00 ].T = Cinv [ u v w].T

        Return:
        C --
        Cinv --
        """
        #u = ff.u

        Nx = ff.Nx
        Ny = ff.Ny
        N = Ny
        Nz = ff.Nz
        Mx = ff.Mx
        Mz = ff.Mz

        I = np.eye(N)
        Z = np.zeros(N)
        nodes, DM = chebyshev.chebdiff(Ny, 1)
        D = DM[0, :, :]

        ###########
        # Block matrices definition
        ###########

        # vector of alpha and beta
        alpha_vect = np.fft.fftfreq(Nx, 1.0 / Nx) * 1 * 2 * np.pi / ff.attributes['Lx']
        alpha_diag = np.diag(alpha_vect)

        beta_vect = np.fft.rfftfreq(Nz, 1.0 / Nz) * 1 * 2 * np.pi / ff.attributes['Lz']
        beta_diag = np.diag(beta_vect)

        # Matrix of beta*I of size [Mz*N, Mz*N]
        # [ beta0*I    0         0     ]
        # [   0      beta1*I     0     ]
        # [  ...      ...       ...    ]
        # [   0        0      betaMz*I ]
        beta_I0 = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', beta_diag, I)))

        # Matrix of beta*I of size [Mx*Mz*N, Mx*Mz*N]
        # to correspond to beta for each couple (alpha, beta)
        # [ beta0*I   0      0         0       ... ] -> for (alpha0, beta0)
        # [   0      ...     0         0       ... ] -> for (alphaX, beta0)
        # [   0       0    beta0*I     0       ... ] -> for (alphaN, beta0)
        # [   0       0      0       beta1*I   ... ] -> for (alpha0, beta1)
        # [  ...     ...    ...       ...      ... ] -> for (...   , betaX)
        beta_I = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', np.eye(Mx), beta_I0)))

        # Same as beta_I0 and beta_I but for alpha
        alpha_I0 = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', alpha_diag, I)))
        alpha_I = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', alpha_I0, np.eye(Mz))))

        # Same as beta_I0 and beta_I but multiplied by D,
        # first differentiation matrix for Chebyshev discretisation in wall-normal direction,
        # instead of I

        # Old line with problem (probably possible to write it in one line):
        # beta_D0 = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', beta_diag, D)))
        # beta_D = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', np.eye(Mx), beta_D0)))

        # Correction :
        beta_D0 = np.zeros((Mz*N, Mz*N), dtype=complex)
        for kz in range(Mz):
            #print(kz, ' -> ', beta_vect[kx])
            beta_D0[kz*N:(kz+1)*N,
                    kz*N:(kz+1)*N] = \
                np.dot(beta_vect[kz], D)
                #beta_vect[kz] * D

        beta_D = np.zeros((Mx*Mz*N, Mx*Mz*N), dtype=complex)
        for kx in range(Mx):
            for kz in range(Mz):
                #print(kx, ' -> ', alpha_vect[kx])
                #print(kz, ' -> ', beta_vect[kz])
                beta_D[(kx*Mz + kz)*N:(kx*Mz + kz + 1)*N,
                       (kx*Mz + kz)*N:(kx*Mz + kz + 1)*N] = \
                    beta_D0[kz*N:(kz+1)*N, kz*N:(kz+1)*N]

        # Same as beta_D0 and beta_D but for alpha

        # Old line with problem (probably possible to write it in one line):
        #alpha_D0 = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', alpha_diag, D)))
        #alpha_D = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', alpha_D0, np.eye(Mz))))

        # Correction :
        alpha_D0 = np.zeros((Mx*N, Mx*N), dtype=complex)
        for kx in range(Mx):
            #print(kx, ' -> ', alpha_vect[kx])
            alpha_D0[kx*N:(kx+1)*N,
                     kx*N:(kx+1)*N] = \
                np.dot(alpha_vect[kx], D)
                #alpha_vect[kx] * D

        alpha_D = np.zeros((Mx*Mz*N, Mx*Mz*N), dtype=complex)
        for kz in range(Mz):
            for kx in range(Mx):
                #print(kx, ' -> ', alpha_vect[kx])
                #print(kz, ' -> ', beta_vect[kz])
                alpha_D[(kx*Mz + kz)*N:(kx*Mz + kz + 1)*N,
                        (kx*Mz + kz)*N:(kx*Mz + kz + 1)*N] = \
                    alpha_D0[kx*N:(kx+1)*N, kx*N:(kx+1)*N]

        cc_ad = np.zeros(shape=(Mx*Mz, Mx*Mz, N, N), dtype=complex)
        cc_ai = np.zeros(shape=(Mx*Mz, Mx*Mz, N, N), dtype=complex)
        cc_bd = np.zeros(shape=(Mx*Mz, Mx*Mz, N, N), dtype=complex)
        cc_bi = np.zeros(shape=(Mx*Mz, Mx*Mz, N, N), dtype=complex)

        for i_kx, i_kz in itertools.product(range(Mx), range(Mz)):
            alpha = alpha_vect[i_kx]
            beta = beta_vect[i_kz]

            if alpha == 0 and beta == 0:
                continue

            cc_ad[(i_kx*Mz) + i_kz, (i_kx*Mz) + i_kz, :, :] = \
                1j * alpha * D / (alpha**2 + beta**2)
            cc_ai[(i_kx*Mz) + i_kz, (i_kx*Mz) + i_kz, :, :] = \
                1j * alpha * I / (alpha**2 + beta**2)
            cc_bd[(i_kx*Mz) + i_kz, (i_kx*Mz) + i_kz, :, :] = \
                1j * beta * D / (alpha**2 + beta**2)
            cc_bi[(i_kx*Mz) + i_kz, (i_kx*Mz) + i_kz, :, :] = \
                1j * beta * I / (alpha**2 + beta**2)

        cc_ad = np.hstack(np.hstack(cc_ad))
        cc_ai = np.hstack(np.hstack(cc_ai))
        cc_bd = np.hstack(np.hstack(cc_bd))
        cc_bi = np.hstack(np.hstack(cc_bi))

        # matrix 1 / k2
        c_I = beta_I**2 + alpha_I**2
        d_I = np.eye(c_I.shape[0])
        #d_I = np.zeros(c_I.shape)
        #for i in range(N, c_I.shape[0]):
        #    d_I[i, i] = 1. / c_I[i, i]

        ###########
        # C
        ###########
        # [ u v w ] = C [ v eta u00 w00 ]
        #
        # C = [ C1  C2  C3 ]
        #     [ C4   0   0 ]
        #     [ C5  C6  C7 ]

        C = np.zeros(shape=(3*Mx*Mz, Mx*Mz + Mx*Mz - 1 + 2, N, N), dtype=complex)

        # C3
        C[0, -2, :, :] = I
        # C4
        C[Mx*Mz:2*Mx*Mz, 0:Mx*Mz, :, :] = \
            (np.einsum('ij, kl -> ijkl', np.eye(Mx*Mz, Mx*Mz), I))
        # C7
        C[2*Mx*Mz, -1, :, :] = I

        C = np.hstack(np.hstack(C))

        # C1
        C[0:Mx*Mz*N, 0:Mx*Mz*N] = \
            cc_ad[:, :]
            #1j * np.dot(d_I, alpha_D)
        # C2
        C[0:Mx*Mz*N, Mx*Mz*N:(2*Mx*Mz-1)*N] = \
            - cc_bi[:, N::]
            #- 1j * np.dot(d_I, beta_I)[:, N::]
        # C5
        C[2*Mx*Mz*N:3*Mx*Mz*N, 0:Mx*Mz*N] = \
            cc_bd[:, :]
            #1j * np.dot(d_I, beta_D)
        # C6
        C[2*Mx*Mz*N:3*Mx*Mz*N, Mx*Mz*N:(2*Mx*Mz-1)*N] = \
            cc_ai[:, N::]
            #1j * np.dot(d_I, alpha_I)[:, N::]

        #np.savetxt('C_real', C.real, fmt='%+.1f')
        #np.savetxt('C_imag', C.imag, fmt='%+.1f')

        ###########
        # INVERSE C
        ###########
        # [ v eta u00 w00 ].T = Cinv [ u v w].T
        #
        # Cinv = [   0   Cinv1    0   ]
        #        [ Cinv2   0    Cinv3 ]
        #        [ Cinv4   0    Cinv5 ]

        Cinv = np.zeros(shape=((Mx*Mz + Mx*Mz - 1 + 2), 3 * Mx*Mz, N, N),
                        dtype=complex)

        # Cinv1
        Cinv[0:Mx*Mz, Mx*Mz:2*Mx*Mz, :, :] = \
            (np.einsum('ij, kl -> ijkl', np.eye(Mx*Mz, Mx*Mz), I))
        # Cinv4
        Cinv[-2, 0, :, :] = I
        # Cinv5
        Cinv[-1, 2*Mx*Mz, :, :] = I

        Cinv = np.hstack(np.hstack(Cinv))

        # Cinv2
        Cinv[Mx*Mz*N:(2*Mx*Mz-1)*N, 0:Mx*Mz*N] = \
            1j * beta_I[N::, :]
        # Cinv3
        Cinv[Mx*Mz*N:(2*Mx*Mz-1)*N, 2*Mx*Mz*N:3*Mx*Mz*N] = \
            - 1j * alpha_I[N::, :]

        #np.savetxt('Cinv_real', Cinv.real, fmt='%+.0f')
        #np.savetxt('Cinv_imag', Cinv.imag, fmt='%+.0f')

        ###########
        # CHECK
        ###########
        if False:
            P1 = np.dot(W, C)
            P2 = np.dot(Cinv, Winv) #np.dot(np.dot(W, C), np.dot(Cinv, Winv))
            P = np.dot(P2, P1)
            np.savetxt('P_real', P.real, fmt='%+.1f')
            np.savetxt('P_imag', P.imag, fmt='%+.1f')
            # P = I is OK

        return C, Cinv

    def op_mapping_without_interaction(self, ff: FlowField, truncation=True):
        """
        NOT FOR THE ACTUATED SYSTEM

        [ u v w ] = C [ v eta u00 w00 ]
        [ v eta u00 w00 ].T = Cinv [ u v w].T

        Return:
        C --
        Cinv --
        """
        #u = ff.u

        Nx = ff.Nx
        Ny = ff.Ny
        if truncation:
            N = Ny-2
        else:
            N = Ny
        Nz = ff.Nz
        Mx = ff.Mx
        Mz = ff.Mz

        I = np.eye(N)
        Z = np.zeros(N)
        nodes, DM = chebyshev.chebdiff(Ny, 1)
        if truncation:
            D = DM[0, 1:-1, 1:-1]
        else:
            D = DM[0, :, :]

        ###########
        # Block matrices definition
        ###########

        # vector of alpha and beta
        alpha_vect = np.fft.fftfreq(Nx, 1.0 / Nx) * 1 * 2 * np.pi / ff.attributes['Lx']
        alpha_diag = np.diag(alpha_vect)

        beta_vect = np.fft.rfftfreq(Nz, 1.0 / Nz) * 1 * 2 * np.pi / ff.attributes['Lz']
        beta_diag = np.diag(beta_vect)

        # Matrix of beta*I of size [Mz*N, Mz*N]
        # [ beta0*I    0         0     ]
        # [   0      beta1*I     0     ]
        # [  ...      ...       ...    ]
        # [   0        0      betaMz*I ]
        beta_I0 = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', beta_diag, I)))

        # Matrix of beta*I of size [Mx*Mz*N, Mx*Mz*N]
        # to correspond to beta for each couple (alpha, beta)
        # [ beta0*I   0      0         0       ... ] -> for (alpha0, beta0)
        # [   0      ...     0         0       ... ] -> for (alphaX, beta0)
        # [   0       0    beta0*I     0       ... ] -> for (alphaN, beta0)
        # [   0       0      0       beta1*I   ... ] -> for (alpha0, beta1)
        # [  ...     ...    ...       ...      ... ] -> for (...   , betaX)
        beta_I = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', np.eye(Mx), beta_I0)))

        # Same as beta_I0 and beta_I but for alpha
        alpha_I0 = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', alpha_diag, I)))
        alpha_I = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', alpha_I0, np.eye(Mz))))

        # Same as beta_I0 and beta_I but multiplied by D,
        # first differentiation matrix for Chebyshev discretisation in wall-normal direction,
        # instead of I

        # Old line with problem (probably possible to write it in one line):
        # beta_D0 = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', beta_diag, D)))
        # beta_D = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', np.eye(Mx), beta_D0)))

        # Correction :
        beta_D0 = np.zeros((Mz*N, Mz*N), dtype=complex)
        for kz in range(Mz):
            #print(kz, ' -> ', beta_vect[kx])
            beta_D0[kz*N:(kz+1)*N, kz*N:(kz+1)*N] = beta_vect[kz] * D

        beta_D = np.zeros((Mx*Mz*N, Mx*Mz*N), dtype=complex)
        for kx in range(Mx):
            for kz in range(Mz):
                #print(kx, ' -> ', alpha_vect[kx])
                #print(kz, ' -> ', beta_vect[kz])
                beta_D[(kx*Mz + kz)*N:(kx*Mz + kz + 1)*N, (kx*Mz + kz)*N:(kx*Mz + kz + 1)*N] = \
                    beta_D0[kz*N:(kz+1)*N, kz*N:(kz+1)*N]

        # Same as beta_D0 and beta_D but for alpha

        # Old line with problem (probably possible to write it in one line):
        #alpha_D0 = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', alpha_diag, D)))
        #alpha_D = np.hstack(np.hstack(np.einsum('ij, kl -> ijkl', alpha_D0, np.eye(Mz))))

        # Correction :
        alpha_D0 = np.zeros((Mx*N, Mx*N), dtype=complex)
        for kx in range(Mx):
            #print(kx, ' -> ', alpha_vect[kx])
            alpha_D0[kx*N:(kx+1)*N, kx*N:(kx+1)*N] = alpha_vect[kx] * D

        alpha_D = np.zeros((Mx*Mz*N, Mx*Mz*N), dtype=complex)
        for kz in range(Mz):
            for kx in range(Mx):
                #print(kx, ' -> ', alpha_vect[kx])
                #print(kz, ' -> ', beta_vect[kz])
                alpha_D[(kx*Mz + kz)*N:(kx*Mz + kz + 1)*N, (kx*Mz + kz)*N:(kx*Mz + kz + 1)*N] = \
                    alpha_D0[kx*N:(kx+1)*N, kx*N:(kx+1)*N]

        # matrix 1 / k2
        c_I = beta_I**2 + alpha_I**2
        d_I = np.zeros(c_I.shape)
        for i in range(N, c_I.shape[0]):
            d_I[i, i] = 1. / c_I[i, i]

        ###########
        # C
        ###########
        # [ u v w ] = C [ v eta u00 w00 ]
        #
        # C = [ C1  C2  C3 ]
        #     [ C4   0   0 ]
        #     [ C5  C6  C7 ]

        C = np.zeros(shape=(3*Mx*Mz, Mx*Mz + Mx*Mz - 1 + 2, N, N), dtype=complex)

        # C3
        C[0, -2, :, :] = I
        # C4
        C[Mx*Mz:2*Mx*Mz, 0:Mx*Mz, :, :] = \
            (np.einsum('ij, kl -> ijkl', np.eye(Mx*Mz, Mx*Mz), I))
        # C7
        C[2*Mx*Mz, -1, :, :] = I

        C = np.hstack(np.hstack(C))

        # C1
        C[0:Mx*Mz*N, 0:Mx*Mz*N] = \
            1j * np.dot(d_I, alpha_D)
        # C2
        C[0:Mx*Mz*N, Mx*Mz*N:(2*Mx*Mz-1)*N] = \
            - 1j * np.dot(d_I, beta_I)[:, N::]
        # C5
        C[2*Mx*Mz*N:3*Mx*Mz*N, 0:Mx*Mz*N] = \
            1j * np.dot(d_I, beta_D)
        # C6
        C[2*Mx*Mz*N:3*Mx*Mz*N, Mx*Mz*N:(2*Mx*Mz-1)*N] = \
            1j * np.dot(d_I, alpha_I)[:, N::]

        #np.savetxt('C_real', C.real, fmt='%+.1f')
        #np.savetxt('C_imag', C.imag, fmt='%+.1f')

        ###########
        # INVERSE C
        ###########
        # [ v eta u00 w00 ].T = Cinv [ u v w].T
        #
        # Cinv = [   0   Cinv1    0   ]
        #        [ Cinv2   0    Cinv3 ]
        #        [ Cinv4   0    Cinv5 ]

        Cinv = np.zeros(shape=((Mx*Mz + Mx*Mz - 1 + 2), 3 * Mx*Mz, N, N), dtype=complex)

        # Cinv1
        Cinv[0:Mx*Mz, Mx*Mz:2*Mx*Mz, :, :] = \
            (np.einsum('ij, kl -> ijkl', np.eye(Mx*Mz, Mx*Mz), I))
        # Cinv4
        Cinv[-2, 0, :, :] = I
        # Cinv5
        Cinv[-1, 2*Mx*Mz, :, :] = I

        Cinv = np.hstack(np.hstack(Cinv))

        # Cinv2
        Cinv[Mx*Mz*N:(2*Mx*Mz-1)*N, 0:Mx*Mz*N] = \
            1j * beta_I[N::, :]
        # Cinv3
        Cinv[Mx*Mz*N:(2*Mx*Mz-1)*N, 2*Mx*Mz*N:3*Mx*Mz*N] = \
            - 1j * alpha_I[N::, :]

        #np.savetxt('Cinv_real', Cinv.real, fmt='%+.0f')
        #np.savetxt('Cinv_imag', Cinv.imag, fmt='%+.0f')

        ###########
        # CHECK
        ###########
        if False:
            P1 = np.dot(W, C)
            P2 = np.dot(Cinv, Winv) #np.dot(np.dot(W, C), np.dot(Cinv, Winv))
            P = np.dot(P2, P1)
            np.savetxt('P_real', P.real, fmt='%+.1f')
            np.savetxt('P_imag', P.imag, fmt='%+.1f')
            # P = I is OK

        return C, Cinv

    def op_weighting(self, ff: FlowField, truncation=True):
        """
        NOT FOR THE ACTUATED SYSTEM
        Returns the weighting such that the energy
        E = 0.5 x.H C.H W.H W C x

        Return:
        W --
        Winv --
        """
        Nd = ff.Nd
        Mx = ff.Mx
        Ny = ff.Ny
        Mz = ff.Mz

        nodes, w = chebyshev.clencurt(Ny)
        W = []
        Winv = []
        for n in range(int(Nd * Mx * Mz)):
            if truncation:
                W = np.concatenate((W, w[1:Ny-1]))
                Winv = np.concatenate((Winv, 1./w[1:Ny-1]))
            else:
                W = np.concatenate((W, w[:]))
                Winv = np.concatenate((Winv, 1./w[:]))
        W = np.diag(W)
        Winv = np.diag(Winv)

        return W, Winv

    # CHANGE BASIS AND SHAPE of datasets
    def transform_spectral_uvw_array_to_vector(self, ff:FlowField, array, truncation=True):
        """
        the input array [u, v, w] is spectral in xz and the output vector as well
        return a 1D vector [u, v, w] from an 3D array [u, v, w].
        """
        if truncation:
            N = ff.Ny-2 # Wall normal resolution
        else:
            N = ff.Ny # Wall normal resolution
        Mx = ff.Mx # Streamwise resolution
        Mz = ff.Mz # Spanwise resolution

        vector = np.zeros(shape=(3*Mx*Mz, N), dtype=complex)

        #print('array.shape : ', array.shape)
        #print('array.u[0,0] : ', array[0, 0, :, 0])
        #print('array.u[0,1] : ', array[0, 0, :, 1])
        #print('array.v[0,0] : ', array[1, 0, :, 0])
        #print('array.v[0,1] : ', array[1, 0, :, 1])
        #print('array.w[0,0] : ', array[2, 0, :, 0])
        #print('array.w[0,1] : ', array[2, 0, :, 1])

        for index, i, j in itertools.product(range(3), range(Mx), range(Mz)):
            vector[index * (Mx*Mz) + i*Mz + j, :] = array[index, i, :, j]

        #print(vector.shape)
        #print(vector.shape)

        #vector = np.hstack(vector)
        #print('vector[0 : N] : ', vector[0:N])
        #print('vector[N : 2*N] : ', vector[N:2*N])
        #print('vector[Mx*Mz*N : Mx*Mz*N+N] : ', vector[Mx*Mz*N : Mx*Mz*N+N] )
        #print('vector[Mx*Mz*N+N : Mx*Mz*N+2*N] : ', vector[Mx*Mz*N+N : Mx*Mz*N+2*N])
        #print('vector[2*Mx*Mz*N : 2*Mx*Mz*N + N] : ', vector[2*Mx*Mz*N : 2*Mx*Mz*N + N])
        #print('vector[2*Mx*Mz*N + N : 4*Mx*Mz*N + 2*N] : ', vector[2*Mx*Mz*N + N : 2*Mx*Mz*N + 2*N])

        return np.hstack(vector)

    def transform_spectral_uvw_vector_to_array(self, ff:FlowField, vector, truncation=True):
        """
        return a SPECTRAL 3D array [u, v, w] from a 1D vector [u, v, w].
        """
        if truncation:
            N = ff.Ny-2 # Wall normal resolution
        else:
            N = ff.Ny # Wall normal resolution
        Mx = ff.Mx # Streamwise resolution
        Mz = ff.Mz # Spanwise resolution

        array = np.zeros(shape=(3, Mx, N, Mz), dtype=complex)

        #print('vector.shape : ', vector.shape)
        #print('vector[0 : N] : ', vector[0:N])
        #print('vector[N : 2*N] : ', vector[N:2*N])
        #print('vector[Mx*Mz*N : Mx*Mz*N+N] : ', vector[Mx*Mz*N : Mx*Mz*N+N] )
        #print('vector[Mx*Mz*N+N : Mx*Mz*N+2*N] : ', vector[Mx*Mz*N+N : Mx*Mz*N+2*N])
        #print('vector[2*Mx*Mz*N : 2*Mx*Mz*N + N] : ', vector[2*Mx*Mz*N : 2*Mx*Mz*N + N])
        #print('vector[2*Mx*Mz*N + N : 4*Mx*Mz*N + 2*N] : ', vector[2*Mx*Mz*N + N : 2*Mx*Mz*N + 2*N])

        vector = vector.reshape((3*Mx*Mz, N))

        for index, i, j in itertools.product(range(3), range(Mx), range(Mz)):
            array[index, i, :, j] = vector[index * (Mx*Mz) + i*Mz + j, :]

        #print('array.shape : ', array.shape)
        #print('array.u[0,0] : ', array[0, 0, :, 0])
        #print('array.u[0,1] : ', array[0, 0, :, 1])
        #print('array.v[0,0] : ', array[1, 0, :, 0])
        #print('array.v[0,1] : ', array[1, 0, :, 1])
        #print('array.w[0,0] : ', array[2, 0, :, 0])
        #print('array.w[0,1] : ', array[2, 0, :, 1])

        return array

    def transform_spectral_veta_array_to_vector(self, ff:FlowField, array, truncation=False):
        """
        return a 1D vector [v, eta, u00, w00] from an 4D array [v, eta, u00, w00].
        """
        if truncation:
            N = ff.Ny-2 # Wall normal resolution
        else:
            N = ff.Ny # Wall normal resolution
        Mx = ff.Mx # Streamwise resolution
        Mz = ff.Mz # Spanwise resolution

        vector = np.zeros(shape=(Mx*Mz + Mx*Mz - 1 + 2, N), dtype=complex)

        #print('array.shape : ', array.shape)
        #print('array.u[0,0] : ', array[0, 0, :, 0])
        #print('array.u[0,1] : ', array[0, 0, :, 1])
        #print('array.v[0,0] : ', array[1, 0, :, 0])
        #print('array.v[0,1] : ', array[1, 0, :, 1])
        #print('array.w[0,0] : ', array[2, 0, :, 0])
        #print('array.w[0,1] : ', array[2, 0, :, 1])

        for i, j in itertools.product(range(Mx), range(Mz)):
            vector[0 * (Mx*Mz) + i*Mz + j, :] = array[0, i, :, j]
        for i, j in itertools.product(range(Mx), range(Mz)):
            if i == 0 and j == 0:
                continue
            vector[1 * (Mx*Mz) + i*Mz + j - 1, :] = array[1, i, :, j]

        vector[-2, :] = array[2, 0, :, 0]
        vector[-1, :] = array[3, 0, :, 0]

        #print(vector.shape)
        #print(vector.shape)

        #vector = np.hstack(vector)
        #print('vector[0 : N] : ', vector[0:N])
        #print('vector[N : 2*N] : ', vector[N:2*N])
        #print('vector[Mx*Mz*N : Mx*Mz*N+N] : ', vector[Mx*Mz*N : Mx*Mz*N+N] )
        #print('vector[Mx*Mz*N+N : Mx*Mz*N+2*N] : ', vector[Mx*Mz*N+N : Mx*Mz*N+2*N])
        #print('vector[2*Mx*Mz*N : 2*Mx*Mz*N + N] : ', vector[2*Mx*Mz*N : 2*Mx*Mz*N + N])
        #print('vector[2*Mx*Mz*N + N : 4*Mx*Mz*N + 2*N] : ', vector[2*Mx*Mz*N + N : 2*Mx*Mz*N + 2*N])

        return np.hstack(vector)

    def transform_spectral_veta_vector_to_array(self, ff:FlowField, vector, truncation=False):
        """
        return an 4D array [v, eta, u00, w00] from a 1D vector [v, eta, u00, w00].
        """
        if truncation:
            N = ff.Ny-2 # Wall normal resolution
        else:
            N = ff.Ny # Wall normal resolution
        Mx = ff.Mx # Streamwise resolution
        Mz = ff.Mz # Spanwise resolution

        #array = np.ndarray(shape=(4, Mx, N, Mz), dtype=complex)
        array = np.zeros(shape=(4, Mx, N, Mz), dtype=complex)

        #print('vector.shape : ', vector.shape)
        #print('vector[0 : N] : ', vector[0:N])
        #print('vector[N : 2*N] : ', vector[N:2*N])
        #print('vector[Mx*Mz*N : Mx*Mz*N+N] : ', vector[Mx*Mz*N : Mx*Mz*N+N] )
        #print('vector[Mx*Mz*N+N : Mx*Mz*N+2*N] : ', vector[Mx*Mz*N+N : Mx*Mz*N+2*N])
        #print('vector[2*Mx*Mz*N : 2*Mx*Mz*N + N] : ', vector[2*Mx*Mz*N : 2*Mx*Mz*N + N])
        #print('vector[2*Mx*Mz*N + N : 4*Mx*Mz*N + 2*N] : ', vector[2*Mx*Mz*N + N : 2*Mx*Mz*N + 2*N])

        vector = vector.reshape((Mx*Mz + Mx*Mz - 1 + 2, N))

        for i, j in itertools.product(range(Mx), range(Mz)):
            array[0, i, :, j] = vector[0 * (Mx*Mz) + i*Mz + j, :]
        for i, j in itertools.product(range(Mx), range(Mz)):
            if i == 0 and j == 0:
                continue
            array[1, i, :, j] = vector[1 * (Mx*Mz) + i*Mz + j - 1, :]

        #if index == 2: # u00
            #    if i == 0 and j ==0:
            #        array[index, i, :, j] = vector[-2, :]
            #        #array[index, i, :, j] = vector[index * (Mx*Mz) + i*Mz + j, :]
            #    else:
            #        continue
            #if index == 3: # w00
            #    if i == 0 and j == 0:
            #        array[index, i, :, j] = vector[-1, :]
            #        #array[index, i, :, j] = vector[index * (Mx*Mz) + i*Mz + j, :]
            #    else:
            #        continue

        array[2, 0, :, 0] = vector[-2, :]
        array[3, 0, :, 0] = vector[-1, :]

        #print('array.shape : ', array.shape)
        #print('array.u[0,0] : ', array[0, 0, :, 0])
        #print('array.u[0,1] : ', array[0, 0, :, 1])
        #print('array.v[0,0] : ', array[1, 0, :, 0])
        #print('array.v[0,1] : ', array[1, 0, :, 1])
        #print('array.w[0,0] : ', array[2, 0, :, 0])
        #print('array.w[0,1] : ', array[2, 0, :, 1])

        return array

    def transform_spectral_array_uvw_to_vector_veta_actuated(self,
            ff:FlowField,
            array,
            f_u, f_l,
            filename=None):
            #make_physical=False,
        """
        ONLY for WALL-NORMAL VELOCITY actuation.

        Transform a 3D SPECTRAL FlowField of the form [u, v, w]
        into a SPECTRAL vector of vector form [v, eta, u00, w00]
        and save it under the HDF5 format if 'filename' is given.

        Keyword parameters:
        ff:FlowField -- original FlowField solution linked to the vector
        array -- 3D array [u, v, w] shape
        f_u, f_l -- upper and lower actuation profiles - vector of size Ny
        #make_physical -- if the vector is Physical in xz,
        #    if False, do operate make_physical
        #    if True, do not
        #    to return a physical array uvw.
        filename -- filename for saving the HDF5 file AS SPECTRAL

        Return:
        vector -- SPECTRAL vector in vector [v, eta, u00, w00] shape
        [also save an HDF5 file]

        Remark:
        1. The vector should correspond to a size [Nx, Ny, Nz], which means,
            as it is under the form vector [v, eta, u00, w00],
            shape = (Nx*Nz + Nx*Nz +1)*Ny.
        2. Apply
            ff.state['xz'] = 'Fourier'
            make_physical('xz')
        to the vector FlowField after loading it.

        """
        # 1. make physical ?
        #if make_physical:
        #    array = ff.make_physical_dataset(array, 'xz')

        # 1. Remove wall BCs from the array
        array_uvw = self.remove_wall_actuation_on_v0_veta(array, f_u, f_l)

        # 2. Transform array uvw to vector uvw
        vector_uvw = self.transform_spectral_uvw_array_to_vector(ff, array_uvw, truncation=False)

        # 3. change base to get uvw as vector
        C, Cinv = self.op_mapping_actuated(ff)
        vector_veta = np.dot(Cinv, vector_uvw)

        # 4. save
        if filename is not None:
            self.save_h5_cmplx(vector_veta, filename)

        return vector_veta

    def transform_spectral_vector_veta_to_array_uvw_actuated(self,
            ff:FlowField,
            vector,
            f_u, f_l,
            filename=None):
            #make_physical=False,
        """
        ONLY for WALL-NORMAL VELOCITY actuation.

        Transform a SPECTRAL vector of vector form [v, eta, u00, w00]
        into a 3D SPECTRAL FlowField of the form [u, v, w]
        and save it under the HDF5 format if 'filename' is given.

        Keyword parameters:
        ff:FlowField -- original FlowField solution linked to the vector
        vector -- vector in vector [v, eta, u00, w00] shape
        f_u, f_l -- upper and lower actuation profiles - vector of size Ny
        #make_physical -- if the vector is Physical in xz,
        #    if False, do operate make_physical
        #    if True, do not
        #    to return a physical array uvw.
        filename -- filename for saving the HDF5 file AS PHYSICAL
        act -- boolean,
            True : the vector corresponds to an operator model
                with wall transparition, which implies the variable
                at the upper and lower wall are set by the user,
                and are placed at the end of each wall-normal discretisation,
                position Ny-1 and Ny.
            False : the vector corresponds to a non-actuated state,
                so the wall variable are
                place are position 0 and Ny.

        Return:
        array -- SPECTRAL 3D array [u, v, w] of the eigenvector
        [also save an HDF5 file]

        Remark:
        1. The vector should correspond to a size [Nx, Ny, Nz], which means,
            as it is under the form vector [v, eta, u00, w00],
            shape = (Nx*Nz + Nx*Nz +1)*Ny.
        2. Apply
            ff.state['xz'] = 'Fourier'
            make_physical('xz')
        to the vector FlowField after loading it.
        """
        # 1. transform vector veta to array
        array_veta = self.transform_spectral_veta_vector_to_array(
                ff,
                vector,
                truncation=False)

        # 2. apply wall actuation on v
        x_1 = self.apply_wall_actuation_on_v0_veta(array_veta, f_u, f_l)

        # 3. transform veta back to vector
        vector_veta = self.transform_spectral_veta_array_to_vector(
                ff,
                x_1,
                truncation=False)

        # 4. change base to get uvw as vector
        C, Cinv = self.op_mapping_actuated(ff)
        vector_uvw = np.dot(C, vector_veta)

        # 5. transform vector uvw to array
        array_uvw = self.transform_spectral_uvw_vector_to_array(
                ff,
                vector_uvw,
                truncation=False)

        # 7. save
        if filename is not None:
            array_uvw_phys = ff.make_physical_dataset(array_uvw, nz=ff.Nz, index='xz')
            self.save_h5_real(array_uvw_phys, filename)

        return array_uvw

    def transform_spectral_vector_veta_to_array_uvw(self,
            ff:FlowField,
            vector,
            filename=None, act=False):
            #make_physical=False,
        """
        ONLY for WALL-NORMAL VELOCITY actuation.

        Transform a SPECTRAL vector of vector form [v, eta, u00, w00]
        into a 3D SPECTRAL FlowField of the form [u, v, w]
        and save it under the HDF5 format if 'filename' is given.

        Keyword parameters:
        ff:FlowField -- original FlowField solution linked to the vector
        vector -- vector in vector [v, eta, u00, w00] shape
        #make_physical -- if the vector is Physical in xz,
        #    if False, do operate make_physical
        #    if True, do not
        #    to return a physical array uvw.
        filename -- filename for saving the HDF5 file AS PHYSICAL
        act -- boolean,
            True : the vector corresponds to an operator model
                with wall transparition, which implies the variable
                at the upper and lower wall are set by the user,
                and are placed at the end of each wall-normal discretisation,
                position Ny-1 and Ny.
            False : the vector corresponds to a non-actuated state,
                so the wall variable are
                place are position 0 and Ny.

            if act == True, the values at the wall 0 and Ny
            for the wall-normal velocity actuation
            are stored and untouched during the operation.

        Return:
        array -- 3D array [u, v, w] of the eigenvector
        [also save an HDF5 file]

        Remark:
        1. The vector should correspond to a size [Nx, Ny, Nz], which means,
            as it is under the form vector [v, eta, u00, w00],
            shape = (Nx*Nz + Nx*Nz +1)*Ny.
        2. Apply
            ff.state['xz'] = 'Fourier'
            make_physical('xz')
        to the vector FlowField after loading it.
        """
        N = ff.Ny

        # 1. we operate a truncation to remove the values at the wall
        # and deal with the same matrix in case actuated or not.
        if act:
            # The operation in step 2 can not consider the wall condition,
            # as matrix C is of shape N-2 x N-2.
            # For this reason, we save the BC at the wall for later.
            # The best solution would be to modify C by adding np.eye(2)
            # for each pair (alpha, beta). But I dont want to touch the method.
            array_veta = self.transform_spectral_veta_vector_to_array(ff,
                                                    vector,
                                                    truncation=False)
            # keep in memory the wall-normal velocity actuation at the walls.
            array_BC_upper = array_veta[0, :, 0, :]
            array_BC_lower = array_veta[0, :, N-1, :]
            # remove the last 2 values for y-direction
            array_veta_truncated = array_veta[:, :, 1:N-1, :]
            vector_veta = self.transform_spectral_veta_array_to_vector(ff,
                                                     array_veta_truncated,
                                                     truncation=True)
        else:
            array_veta = self.transform_spectral_veta_vector_to_array(ff,
                                                    vector,
                                                    truncation=False)
            array_veta_truncated = array_veta[:, :, 1:N-1, :]
            vector_veta = self.transform_spectral_veta_array_to_vector(ff,
                                                     array_veta_truncated,
                                                     truncation=True)

        # 2. change of basis,
        C, Cinv = self.op_mapping(ff, truncation=True)

        vector_uvw = np.dot(C, vector_veta)
        array_uvw = self.transform_spectral_uvw_vector_to_array(ff, vector_uvw)

        # 3. expand vector_uvw to add BCs back
        if act:
            array_uvw_1 = np.zeros(shape=(3, ff.Mx, N, ff.Mz),
                                      dtype=complex)
            # I place the both BCs at their respective y-coordinate,
            # and not both at the tail of the y-coordinate.
            array_uvw_1[1, :,  0, :] = array_BC_upper[:, :]
            array_uvw_1[:, :, 1:N-1, :] = array_uvw[:, :, :, :]
            array_uvw_1[1, :, -1, :] = array_BC_lower[:, :]
        else:
            array_uvw_1 = np.zeros(shape=(3, ff.Mx, N, ff.Mz),
                                      dtype=complex)
            array_uvw_1[:, :, 1:N-1, :] = array_uvw[:, :, :, :]

        # 5. save into a FlowField
        if filename is not None:
            array_uvw_1_phys = ff.make_physical_dataset(array_uvw_1, nz=ff.Nz, index='xz')
            self.save_h5_real(array_uvw_1_phys, filename)

        return array_uvw_1

    def transform_eigenvector_to_ff(self, eq:FlowField, eigenvector, filename, act=False):
        """
        Transform a SPECTRAL eigenvector saved as a vector form [v, eta, u00, w00]
        into a 3D PHYSICAL FlowField of the form [u, v, w]
        and save it under the HDF5 format.

        Keyword parameters:
        eq:FlowField -- original FlowField solution linked to the eigenvector
        eigenvector -- eigenvector in vector [v, eta, u00, w00] shape
        filename -- filename for saving the HDF5 file
        act -- boolean,
            True : the eigenvector corresponds to an operator model with wall transparition,
                which implies the variable at the upper and lower wall are set by the user,
                and are placed at position 0 and Ny.
            False : the eigenvector corresponds to a non-actuated state, so the wall variable are
                place are position 0 and Ny.

        Return:
        array -- PHYSICAL 3D array [u, v, w] of the eigenvector
        [also save an HDF5 file]

        Remark:
        1. The eigenvector should correspond to a size [Nx, Ny, Nz], which means,
        as it is under the form vector [v, eta, u00, w00], shape = (Nx*Nz + Nx*Nz +1)*Ny.
        2. Apply
            ff.state['xz'] = 'Fourier'
            make_physical('xz')
        to the eigenvector FlowField after loading it.
        """
        N = eq.u.shape[2]
        op = Operators(eq)

        # 1. we operate a truncation to remove the values at the wall and deal with the same
        # matrix in case actuated or not.
        if act:
            array_veta = op.transform_spectral_veta_vector_to_array(eq,
                                                    eigenvector,
                                                    truncation=False)
            # keep in memory the wall-normal velocity actuation at the walls.
            array_BC_upper = array_veta[0, :, 0, :]
            array_BC_lower = array_veta[0, :, N-1, :]
            # remove the last 2 values for y-direction
            array_veta_truncated = array_veta[:, :, 1:N-1, :]
            vector_veta = op.transform_spectral_veta_array_to_vector(eq,
                                                     array_veta_truncated,
                                                     truncation=True)
        else:
            #array_veta = op.transform_spectral_veta_vector_to_array(eq,
            #                                        eigenvector,
            #                                        truncation=True)
            ## remove the first/last values for y-direction
            #array_veta_truncated = array_veta#[:, :, 1:N-1, :]
            #vector_veta = op.transform_spectral_veta_array_to_vector(eq,
            #                                         array_veta_truncated,
            #                                         truncation=True)
            array_veta = self.transform_spectral_veta_vector_to_array(eq,
                                                    eigenvector,
                                                    truncation=False)
            array_veta_truncated = array_veta[:, :, 1:N-1, :]
            vector_veta = self.transform_spectral_veta_array_to_vector(eq,
                                                     array_veta_truncated,
                                                     truncation=True)


        # 2. change of basis
        C, Cinv = op.op_mapping(eq, truncation=True)

        vector_uvw = np.dot(C, vector_veta)
        array_uvw = op.transform_spectral_uvw_vector_to_array(eq,
                                              vector_uvw)

        # 3. expand vector_uvw to add BCs back
        if act:
            array_uvw_1 = np.zeros(shape=(3, eq.Mx, N, eq.Mz),
                                      dtype=complex)
            # I place the both BCs at their respective y-coordinate,
            # and not both at the tail of the y-coordinate.
            array_uvw_1[1, :,  0, :] = array_BC_upper[:, :]
            array_uvw_1[:, :, 1:N-1, :] = array_uvw[:, :, :, :]
            array_uvw_1[1, :, -1, :] = array_BC_lower[:, :]
        else:
            array_uvw_1 = np.zeros(shape=(3, eq.Mx, N, eq.Mz),
                                      dtype=complex)
            array_uvw_1[:, :, 1:N-1, :] = array_uvw[:, :, :, :]

        # 4. making the array physical
        array_uvw_2 = eq.make_physical_dataset(array_uvw_1, nz=eq.Nz, index='xz')

        # 5. save into a FlowField
        f = h5py.File(filename, 'w')
        f.create_dataset('data/u', data=np.real(array_uvw_2), compression='gzip')
        f.create_dataset('geom/x', data=eq.x)
        f.create_dataset('geom/y', data=eq.y)
        f.create_dataset('geom/z', data=eq.z)
        for a in eq.attributes:
            f.attrs[a] = eq.attributes[a]
        f.close()

        return array_uvw_2

    def apply_wall_actuation_on_v0(self, dataset_v, f_u, f_l):
        """
        Transform a dataset where
        the wall normal direction is given as:
        v_v0vpvm[ 0]   = v+ (=vp)
        v_v0vpvm[1:-1] = v0
        v_v0vpvm[-1]   = v- (=vm)
        into a dataset where
        the wall normal direction is given as:
               [ 0  ]   [     ]      [     ]
        v[:] = [ v0 ] + [ f_u ] v+ + [ f_l ] v-
               [ 0  ]   [     ]      [     ]
        (actually all directions)
        where f_u and f_l are the upper and lower lifting functions of shape [Ny].
        """
        data = np.zeros_like(dataset_v, dtype=complex)
        data[:, :, 1:-1, :] = dataset_v[:, :, 1:-1, :]
        for ii, ix, iz in itertools.product(range(dataset_v.shape[0]),
                                            range(dataset_v.shape[1]),
                                            range(dataset_v.shape[3])):
            data[ii, ix, :, iz] += np.dot(f_u, dataset_v[ii, ix, 0, iz]) \
                                    + np.dot(f_l, dataset_v[ii, ix, -1, iz])
        return data

    def apply_wall_actuation_on_v0_veta(self, dataset_v, f_u, f_l):
        """
        Transform a dataset where
        the wall normal direction is given as:
        v_v0vpvm[ 0]   = v+ (=vp)
        v_v0vpvm[1:-1] = v0
        v_v0vpvm[-1]   = v- (=vm)
        into a dataset where
        the wall normal direction is given as:
               [ 0  ]   [     ]      [     ]
        v[:] = [ v0 ] + [ f_u ] v+ + [ f_l ] v-
               [ 0  ]   [     ]      [     ]
        (actually all directions)
        where f_u and f_l are the upper and lower lifting functions of shape [Ny].
        """
        data = np.zeros_like(dataset_v, dtype=complex)
        data[:, :, 1:-1, :] = dataset_v[:, :, 1:-1, :]
        for ii, ix, iz in itertools.product(range(dataset_v.shape[0]),
                                            range(dataset_v.shape[1]),
                                            range(dataset_v.shape[3])):
            data[ii, ix, :, iz] += np.dot(f_u, dataset_v[ii, ix, 0, iz]) \
                                    + np.dot(f_l, dataset_v[ii, ix, -1, iz])
        return data

    def remove_wall_actuation_on_v0(self, dataset_v, f_u, f_l):
        """
        Transform a dataset where
        the wall normal direction is given as:
               [ 0  ]   [     ]      [     ]
        v[:] = [ v0 ] + [ f_u ] v+ + [ f_l ] v-
               [ 0  ]   [     ]      [     ]
        into a dataset where
        the wall normal direction is given as:
        v_v0vpvm[ 0]   = v+ (=vp)
        v_v0vpvm[1:-1] = v0
        v_v0vpvm[-1]   = v- (=vm)
        (actually all directions)
        where f_u and f_l are the upper and lower lifting functions of shape [Ny].
        """
        data = np.zeros_like(dataset_v, dtype=complex)
        data[:, :, 0, :] = dataset_v[:, :, 0, :]
        data[:, :, -1, :] = dataset_v[:, :, -1, :]
        for ii, ix, iz in itertools.product(
                range(dataset_v.shape[0]),
                range(dataset_v.shape[1]),
                range(dataset_v.shape[3])):
            data[ii, ix, 1:-1, iz] = \
                dataset_v[ii, ix, 1:-1, iz] \
              - np.dot(f_u, dataset_v[ii, ix, 0, iz])[1:-1] \
              - np.dot(f_l, dataset_v[ii, ix, -1, iz])[1:-1]
        return data

    def remove_wall_actuation_on_v0_veta(self, dataset_v, f_u, f_l):
        """
        Transform a dataset where
        the wall normal direction is given as:
               [ 0  ]   [     ]      [     ]
        v[:] = [ v0 ] + [ f_u ] v+ + [ f_l ] v-
               [ 0  ]   [     ]      [     ]
        into a dataset where
        the wall normal direction is given as:
        v_v0vpvm[ 0]   = v+ (=vp)
        v_v0vpvm[1:-1] = v0
        v_v0vpvm[-1]   = v- (=vm)
        (actually all directions)
        where f_u and f_l are the upper and lower lifting functions of shape [Ny].
        """
        data = np.zeros_like(dataset_v, dtype=complex)
        data[:, :, 0, :] = dataset_v[:, :, 0, :]
        data[:, :, -1, :] = dataset_v[:, :, -1, :]
        for ii, ix, iz in itertools.product(
                range(dataset_v.shape[0]),
                range(dataset_v.shape[1]),
                range(dataset_v.shape[3])):
            data[ii, ix, 1:-1, iz] = \
                dataset_v[ii, ix, 1:-1, iz] \
              - np.dot(f_u, dataset_v[ii, ix, 0, iz])[1:-1] \
              - np.dot(f_l, dataset_v[ii, ix, -1, iz])[1:-1]
        return data

    # EIGENVALUES CALCULATION
    def eigen(self,
            filename,
            EA,
            EB=None,
            left=False,
            k=20,
            sigma=0.2,
            tolerance=1E-5, timer=False, memory=False):
        """
        Method to calculate the largest real-part eigenvalues of a matrix EA.
        The method saves the eigenvalues and eigenvalues as the -filename place
            as '..._EIGS.npy" and "..._VECS.npy".
        EB represents the actuation, if EB is not None, the '_act' is added to the filename.

        NOTE: The eigenvectors are SPECTRAL

        ----------
        Keyword arguments:
        filename -- place and name to save the file,
        EA -- matrix to solve,
        EB -- None if not actuated, otherwise means the system is actuated.
        left -- to return the left eigenvectors instead of the right eigenvector
        k -- Number of eigenvalues to solve.
        sigma -- starting value to search the eigenvalues.
        tolerance -- tolerance for the precision of the eigenvalues.
        timer -- if time spent by the method is needed.
        memory -- if memory used is needed.

        ----------
        Returns:
        eigenvalue
        eigenvector

        ----------
        Do not set the value of k to small (minimum 20) or it does not catch
            the right eigenvalues.
        Be careful by setting sigma: the process searches eigenvalues near sigma,
            which implies other eigenvalues may not be caught if they are far
            from sigma even if they have a larger real part.
        """
        if timer:
            begin = np.datetime64(datetime.datetime.now(), 'ms')

        print('\nEIGENVALUES CALCULATION \nTolerance=', tolerance,
                    ', size=', EA.shape,
                    ', actuation=', not(EB is None), '.')

        # If the left-eigenvectors needs to be calculated
        if left:
            EA = np.transpose(EA)
            #EB = EB.T

        # scipy.linalg.eig
        if k == 0:
            print('Method: Scipy.linalg.eig')
            eigenvalue, eigenvector = scipy.linalg.eig(EA)

        # scipy.sparse.linalg.eigs
        elif False:
            eigenvalue, eigenvector = scipy.sparse.linalg.eigs(
                                                        EA,
                                                        k,
                                                        which='LR',
                                                        tol=tolerance,
                                                        sigma=sigma)

        # scipy.sparse.linalg.eigs + shift invert
        else:
            # 'if desired eigenvalues are small, consider using shift inverse for better performances'.
            # As eigenvalues are small, that implies set up : which='SR'
            print('Method: Shift invert eigs, scipy.sparse.linalg.eigs.')
            print('*Calculation*')
            eigenvalue, eigenvector = scipy.sparse.linalg.eigs(
                                                        EA,
                                                        k,
                                                        which='SR',
                                                        tol=tolerance,
                                                        sigma=sigma)
            print('*Done*')

        # setting parameter in case the model searches for left eigenvectors
        if left: # if left eigenvectors were calculated
            eigenvector = np.transpose(eigenvector)
            left_str = '_left'
        else: # if only right eigenvectors
            left_str = ''

        # setting parameter in case the model is actuated of not
        if EB is None: # if not actuated
            act_str = ''
        else: # if actuated
            act_str = '_act'

        ind = np.argsort(eigenvalue.real)[::-1]
        eigenvalue = eigenvalue[ind]
        if left:
            eigenvector = eigenvector[ind, :]
        else:
            eigenvector = eigenvector[:, ind]
        np.save(filename+'_EIG'+act_str+'.npy', eigenvalue)
        np.save(filename+'_VEC'+left_str+act_str+'.npy', eigenvector)

        if timer:
            print('TIME elapsed for eigenvalues-vectors calculation : ',
                    np.datetime64(datetime.datetime.now(), 'ms') - begin)

        if memory:
            print('MEMORY used for eigenvalues-vectors calculation :',
                resource.getrusage(resource.RUSAGE_SELF).ru_maxrss /1000, 'MB')

        return eigenvalue, eigenvector

    def eigen_print(self, N, filename, precision=12):
        """
        N -- Number of eigenvalues to print.
        filename -- storage file .npy
        """
        print(filename)
        vals = np.load(filename)
        print(vals[:N])

        #ind = np.argsort(vals.real)[::-1]
        #print('\nEIGENVALUES PRINT (', N, ' largest real part) : \n',
        #      np.round(vals[ind], precision))

        #ind = np.argpartition(vals.real, -N)[-N:]
        #print('\nEIGENVALUES PRINT (', N, ' largest real part) : \n',
        #      np.round(np.sort(vals[ind])[::-1], precision))

    def eigen_plot(self, filename, x_min=0, x_max=0, y_min=0, y_max=0):
        """
        filename -- storage file .npy
        x_min -- graphic boundaries
        x_max --
        y_min --
        y_max --
        """
        print('\nEIGENVALUES PLOT (all calculated eigenvalues)')
        vals = np.load(filename)

        plt.figure()
        plt.plot(vals.real, vals.imag,
                 'rx', ms='5', mew='1',
                 label='Eigenvalues')

        plt.legend(fontsize='small')
        plt.title('Eigenvalues (real, imag).')
        plt.grid(b=True, which='Major')
        if x_min != 0 and x_max != 0:
            plt.xlim(x_min, x_max)
        if y_min != 0 and y_max != 0:
            plt.ylim(y_min, y_max)
        plt.xlabel('real')
        plt.ylabel('imag')
        plt.show()

    # DIVERSE
    def modal_controllability(self,
            ff,
            E, A, B,
            eigenvalues, left_eigenvectors,
            N=0,
            controllability_measure=False):
        """
        Compute the modal controllability matrix WB
        from the operator matrices and their LEFT eigenvectors
        to determine the effects of an input on each eigenvalues.

        From "Modal controllability and observability of power-system models",
        S M CHAN, 1984:
        "WB(i,j) = w(i).T . b(j) of WB measures
            how much the j-th input affects the i-th modes"

        If Applied to transpoe
        WB.T(i,j) = w(i) . b(j).T of WB.T measures
            how much the i-th input affects the j-th modes

        where B is the actuation matrix
        and W is the matrix where each ROW corresponds to a LEFT-eigenvector

        ----------
        Keyword arguments:
        ff -- flowfield
        E, A, B -- operator for op_nonlaminar_baseflow
        eigenvalues, left_eigenvecors --
        #N -- number of eigenvalues to consider (always take the biggest real-part)
        #controllability_measure -- boolean,
        #    to print the controllability measure of each eigenmode
        #state_reduction -- reduce the controller its state_reduction coefficients
        #    'velocity', 'vorticity', 'uw' or whatever
        #graph -- graphic to return
        #    None
        #    'full'
        #    'pcolormesh'
        #    'square' - for velocity or vorticity only

        ----------
        Return:
        WB -- modal controllability matrix
        """
        #data to output:
        # either the absolute value, the log scale of the absolute value, or real/imag part
        output = 'abs' # 'abs' or 'abs_log' or 'real' or 'imag'
        Nx = ff.Nx
        Ny = ff.Ny
        Nz = ff.Nz
        Mx = ff.Mx
        Mz = ff.Mz

        # consider only the biggest real-part eigenvalues
        N = eigenvalues.shape[0]
        eigs = eigenvalues[:]
        W = left_eigenvectors[:, :]

        # calculate the modal controllability matrix
        _B = np.dot(E, B)
        WB = np.dot(W, _B)
        #np.savetxt('WB.T', WB.T, fmt='%+.3f')

        # calculate the controllability measure (if needed)
        if controllability_measure == True:
            print('\nControllability measures :')
            for index in range(N):
                measure_WB = np.sqrt(
                                np.dot(
                                    np.dot(W[index, :],
                                           _B),
                                    np.dot(np.transpose(_B).conj(),
                                           np.transpose(W[index, :]).conj())))
                print('Eigs ' + str(eigs[index].real) + \
                     ' + j ' +  str(eigs[index].imag)+ ' : '\
                     + str(measure_WB))

        if output == 'abs' or output == 'abs_log':
            WB = np.sqrt(WB.real**2 + WB.imag**2)
        elif output == 'real':
            WB = WB.real
        elif output == 'imag':
            WB = WB.imag

        # Reordering the Fourier modes for streamwise and spanwise direction
        K = 4 # if full set of modes needed, set K=2
        freq_kx_0 = np.fft.fftfreq(Nx, 1/Nx).astype(np.integer, copy=False)
        freq_kz_0 = np.fft.rfftfreq(Nz, 1/Nz).astype(np.integer, copy=False)
        freq_kx = np.concatenate((freq_kx_0[-int(Nx/K):], freq_kx_0[:int(Nx/K+1)]))
        freq_kz = freq_kx_0[:int(Nz/K+1)]
        #freq_kz = np.concatenate((freq_kz_0[-int(Nz/K):], freq_kz_0[:int(Nz/K+1)]))

        ###############
        # FULL GRAPH FOR ONE GIVEN EIGENVALUE
        # without state_reduction
        ###############
        if True:#graph == 'full':
            for eigs_index in range(N):
                fig0 = plt.figure()

                if output == 'abs' or 'abs_log':
                    plt.set_cmap('Greys')
                else:
                    plt.set_cmap('bwr')

                # boundary values
                max_WB = (WB[eigs_index, :]).max()
                min_WB = (WB[eigs_index, :]).min()
                limit = max(abs(min_WB), abs(max_WB))

                # velocity actuation at upper wall +
                ax1 = plt.subplot2grid((42,40), (0,0), rowspan=18, colspan=18)
                ax1.set_title('Velocity actuation, Upper wall')
                ax1.set_ylabel('Alpha')
                ax1.set_xlabel('Beta')
                ax1.autoscale_view()
                ax1.set_xticks(np.arange(0.5, Mz))
                ax1.set_xticklabels(freq_kz)
                ax1.set_yticks(np.arange(0.5, Mx))
                ax1.set_yticklabels(freq_kx)

                table_u_upp_0 = np.ndarray(shape=(Mx, Mz))

                # velocity actuation at lower wall -
                ax2 = plt.subplot2grid((42,40), (0,20), rowspan=18, colspan=18)
                ax2.set_title('Velocity actuation, Lower wall')
                ax2.set_ylabel('Alpha')
                ax2.set_xlabel('Beta')
                ax2.autoscale_view()
                ax2.set_xticks(np.arange(0.5, Mz))
                ax2.set_xticklabels(freq_kz)
                ax2.set_yticks(np.arange(0.5, Mx))
                ax2.set_yticklabels(freq_kx)

                table_u_low_0 = np.ndarray(shape=(Mx, Mz))

                # vorticity actuation at upper wall +
                ax3 = plt.subplot2grid((42,40), (20,0), rowspan=18, colspan=18)
                ax3.set_title('Vorticity actuation, Upper wall')
                ax3.set_ylabel('Alpha')
                ax3.set_xlabel('Beta')
                ax3.autoscale_view()
                ax3.set_xticks(np.arange(0.5, Mz))
                ax3.set_xticklabels(freq_kz)
                ax3.set_yticks(np.arange(0.5, Mx))
                ax3.set_yticklabels(freq_kx)

                table_eta_upp_0 = np.ndarray(shape=(Mx, Mz))

                # vorticity actuation at lower wall -
                ax4 = plt.subplot2grid((42,40), (20,20), rowspan=18, colspan=18)
                ax4.set_title('Vorticity actuation, Lower wall')
                ax4.set_ylabel('Alpha')
                ax4.set_xlabel('Beta')
                ax4.autoscale_view()
                ax4.set_xticks(np.arange(0.5, Mz))
                ax4.set_xticklabels(freq_kz)
                ax4.set_yticks(np.arange(0.5, Mx))
                ax4.set_yticklabels(freq_kx)

                table_eta_low_0 = np.ndarray(shape=(Mx, Mz))

                # u00 and w00 actuation on upper and lower wall
                ax5 = plt.subplot2grid((42,40), (40,10), colspan=20, rowspan=1)
                ax5.set_title(r'$u_{(0,0)}$ and $w_{(0,0)}$ actuation, Upper wall (+) and Lower Wall (-)')
                modes = []
                modes.append('$q^{+}_{u_{(0, 0)}}$')
                modes.append('$q^{-}_{u_{(0, 0)}}$')
                modes.append('$q^{+}_{w_{(0, 0)}}$')
                modes.append('$q^{-}_{w_{(0, 0)}}$')
                ax5.set_xticks(np.arange(0.5, 4))
                ax5.set_xticklabels(modes[0:4], rotation=90)
                ax5.set_aspect('equal', 'box')
                ax5.set_xlabel('Modes')
                ax5.autoscale_view()
                ax5.invert_yaxis()

                # filling the tables
                for i, j in itertools.product(range(Mx), range(Mz)):
                    if i == 0 and j == 0:
                        table_u_upp_0[i,j] = WB[eigs_index, (i*Mz + j)*2]
                        table_u_low_0[i,j] = WB[eigs_index, (i*Mz + j)*2 + 1]
                        # eta does not have the pair (0,0)
                        continue
                    table_u_upp_0[i,j] = WB[eigs_index, (i*Mz + j)*2]
                    table_u_low_0[i,j] = WB[eigs_index, (i*Mz + j)*2 + 1]
                    table_eta_upp_0[i,j] = WB[eigs_index, Mx*Mz*2 + (i*Mz + j -1)*2]
                    table_eta_low_0[i,j] = WB[eigs_index, Mx*Mz*2 + (i*Mz + j -1)*2 + 1]
                table_uw = WB[eigs_index, -4::][np.newaxis, :]

                # Reordering the Fourier modes for streamwise and spanwise direction
                table_u_upp_1 = table_u_upp_0[:int(Mz/K+1), :]
                #table_u_upp_1 = np.concatenate((table_u_upp_0[-int(Mz/K):, :],
                #    table_u_upp_0[:int(Mz/K+1), :]))
                table_u_upp = np.concatenate((table_u_upp_1[:, -int(Mx/K):],
                    table_u_upp_1[:, :int(Mx/K+1)]), axis=1)

                table_u_low_1 = table_u_low_0[:int(Mz/K+1), :]
                #table_u_low_1 = np.concatenate((table_u_low_0[-int(Mz/K):, :],
                #    table_u_low_0[:int(Mz/K+1), :]))
                table_u_low = np.concatenate((table_u_low_1[:, -int(Mx/K):],
                    table_u_low_1[:, :int(Mx/K+1)]), axis=1)

                table_eta_upp_1 = table_eta_upp_0[:int(Mz/K+1), :]
                #table_eta_upp_1 = np.concatenate((table_eta_upp_0[-int(Mz/K):, :],
                #    table_eta_upp_0[:int(Mz/K+1), :]))
                table_eta_upp = np.concatenate((table_eta_upp_1[:, -int(Mx/K):],
                    table_eta_upp_1[:, :int(Mx/K+1)]), axis=1)

                table_eta_low_1 = table_eta_low_0[:int(Mz/K+1), :]
                #table_eta_low_1 = np.concatenate((table_eta_low_0[-int(Mz/K):, :],
                #    table_eta_low_0[:int(Mz/K+1), :]))
                table_eta_low = np.concatenate((table_eta_low_1[:, -int(Mx/K):],
                    table_eta_low_1[:, :int(Mx/K+1)]), axis=1)

                if output == 'abs_log':
                    im1 = ax1.pcolormesh(table_u_upp,
                                     edgecolor='k',
                                     lw=0.01,
                                     norm=matplotlib.colors.LogNorm(vmin=2*10E-2, vmax=+limit))
                    im2 = ax2.pcolormesh(table_u_low,
                                     edgecolor='k',
                                     lw=0.01,
                                     norm=matplotlib.colors.LogNorm(vmin=2*10E-2, vmax=+limit))
                    im3 = ax3.pcolormesh(table_eta_low,
                                     edgecolor='k',
                                     lw=0.01,
                                     norm=matplotlib.colors.LogNorm(vmin=2*10E-2, vmax=+limit))
                    im4 = ax4.pcolormesh(table_eta_low,
                                     edgecolor='k',
                                     lw=0.01,
                                     norm=matplotlib.colors.LogNorm(vmin=2*10E-2, vmax=+limit))
                    im5 = ax5.pcolormesh(table_uw,
                                    edgecolor='k',
                                    lw=0.01,
                                    norm=matplotlib.colors.LogNorm(vmin=2*10E-2, vmax=+limit))

                elif output == 'abs':
                    im1 = ax1.pcolormesh(table_u_upp,
                                edgecolor='k', lw=0.01, vmin=0, vmax=+limit)
                    im2 = ax2.pcolormesh(table_u_low,
                                edgecolor='k', lw=0.01, vmin=0, vmax=+limit)
                    im3 = ax3.pcolormesh(table_eta_upp,
                                edgecolor='k', lw=0.01, vmin=0, vmax=+limit)
                    im4 = ax4.pcolormesh(table_eta_low,
                                edgecolor='k', lw=0.01, vmin=0, vmax=+limit)
                    im5 = ax5.pcolormesh(table_uw,
                                edgecolor='k', lw=0.01, vmin=0, vmax=+limit)

                else:
                    im1 = ax1.pcolormesh(table_u_upp,
                                edgecolor='k', lw=0.01, vmin=-limit, vmax=+limit)
                    im2 = ax2.pcolormesh(table_u_low,
                                edgecolor='k', lw=0.01, vmin=-limit, vmax=+limit)
                    im3 = ax3.pcolormesh(table_eta_upp,
                                edgecolor='k', lw=0.01, vmin=-limit, vmax=+limit)
                    im4 = ax4.pcolormesh(table_eta_low,
                                edgecolor='k', lw=0.01, vmin=-limit, vmax=+limit)
                    im5 = ax5.pcolormesh(table_uw,
                                edgecolor='k', lw=0.01, vmin=-limit, vmax=+limit)

                plt.suptitle('Absolute value of the modal controllability matrix  ' + \
                            'W(i)B(Alpha, Beta) \n' + \
                            'for modes (Alpha, Beta) of the wall-transpiration actuation matrix B \n ' + \
                            'and the i-th eigenvector stored in W, corresponding to the eigenvalue ' +\
                            str(eigs[eigs_index].real) +\
                            ' + j '+ str(eigs[eigs_index].imag) + '.' )

                fig0.subplots_adjust(bottom=0.25, top=0.93, left=0.07, right=0.99)

                # Colorbar: left bottom width height
                cax = fig0.add_axes([0.25, 0.21, 0.5, 0.01])
                cbar = fig0.colorbar(im3, cax, orientation='horizontal')

                plt.show()

        ##############################
        # OLD CODE FOR DIFFERENT GRAPHICAL REPRESENTATIONS
        ##############################
        """
        if state_reduction == 'velocity':
            # Reduce B to the wall-normal velocity control
            range_B = range(0, 2*Nx*Nz)
            _B = np.dot(E, B[:, range_B])
            print('_B: ', _B.shape)
            N1 = int((Nx*Nz - 1) * 1 / 3)*2 # 0 -> N1
            N2 = int((Nx*Nz - 1) * 2 / 3)*2 # N1 -> N2
        elif state_reduction == 'vorticity':
            # Reduce B to the wall-normal vorticity control
            range_B = range(2*Nx*Nz, 2*Nx*Nz + 2*Nx*Nz - 2)
            _B = np.dot(E, B[:, range_B])
            print('_B: ', _B.shape)
            N1 = int(((Nx*Nz - 1) - 1) * 1 / 3)*2 # 0 -> N1
            N2 = int(((Nx*Nz - 1) - 1) * 2 / 3)*2 # N1 -> N2
        elif state_reduction == 'uw':
            # Reduce B to u00 and w00 control
            range_B = range(2*Nx*Nz + 2*Nx*Nz - 2, 2*Nx*Nz + 2*Nx*Nz - 2 + 4)
            _B = np.dot(E, B[:, range_B])
            print('_B: ', _B.shape)
            N1 = int((2 - 1) * 1 / 3)*2 # 0 -> N1
            N2 = int((2 - 1) * 2 / 3)*2 # N1 -> N2
        else:
            _B = np.dot(E, B)
            N1 = int(((Nx*Nz + Nx*Nz + 1) - 1) * 1 / 3)*2 # 0 -> N1
            N2 = int(((Nx*Nz + Nx*Nz + 1) - 1) * 2 / 3)*2 # N1 -> N2

        ###############
        # SQUARE GRAPH
        # for velocity or vorticity only
        # many eigenvalues upper and lower wall-transpiration effect on different rows.
        ###############
        if graph == 'square':
            if state_reduction =='uw':
                warnings.warn("Method 'square' not for 'uw' option")

            fig0 = plt.figure()

            if output == 'abs' or 'abs_log':
                plt.set_cmap('Greys')
            else:
                plt.set_cmap('bwr')

            # filling
            max_WB = (WB[:, :]).max()
            min_WB = (WB[:, :]).min()
            limit = max(abs(min_WB), abs(max_WB))
            print('limit : ', limit)

            # square for each eigenvalue
            for eigs_index in range(N):
                # upper wall +
                ax1 = plt.subplot(N, 2, eigs_index*2 + 1)
                table_upp = np.ndarray(shape=(Nx, Nz))
                # lower wall -
                ax2 = plt.subplot(N, 2, eigs_index*2 + 2)
                table_low = np.ndarray(shape=(Nx, Nz))
                for i, j in itertools.product(range(Nx), range(Nz)):
                    if state_reduction == 'vorticity' and i == 0 and j == 0:
                        # do not forget that eta does not have the pair (0,0)
                        continue
                    table_upp[i,j] = WB[eigs_index, (i*Nz + j -1)*2]
                    table_low[i,j] = WB[eigs_index, (i*Nz + j -1)*2 + 1]

                if output == 'abs_log':
                    im1 = ax1.pcolormesh(table_upp,
                                     edgecolor='k',
                                     lw=0.01,
                                     norm=matplotlib.colors.LogNorm(vmin=2*10E-2, vmax=+limit))
                    im2 = ax2.pcolormesh(table_low,
                                     edgecolor='k',
                                     lw=0.01,
                                     norm=matplotlib.colors.LogNorm(vmin=2*10E-2, vmax=+limit))
                elif output == 'abs':
                    im1 = ax1.pcolormesh(table_upp,
                                edgecolor='k', lw=0.01, vmin=0, vmax=+limit)
                    im2 = ax2.pcolormesh(table_low,
                                edgecolor='k', lw=0.01, vmin=0, vmax=+limit)
                else:
                    im1 = ax1.pcolormesh(table_upp,
                                edgecolor='k', lw=0.01, vmin=-limit, vmax=+limit)
                    im2 = ax2.pcolormesh(table_low,
                                edgecolor='k', lw=0.01, vmin=-limit, vmax=+limit)

                # Axis settings:
                ax1.set_aspect('equal', 'box')
                ax2.set_aspect('equal', 'box')
                #ax.grid(b=True, linestyle='-', linewidth=0.3)

                ax1.set_title('Upper wall transpiration')
                ax2.set_title('Lower wall transpiration')
                ax1.set_xlabel('Beta')
                ax2.set_xlabel('Beta')
                ax1.set_ylabel(r'\textbf{Eig: ' + str(np.trunc(eigs[eigs_index].real*10000)/10000) \
                        + ' + j '+ str(np.trunc(eigs[eigs_index].imag*10000)/10000) + '}\nAlpha')
                ax1.autoscale_view()
                ax2.autoscale_view()
                #ax1.invert_yaxis()
                #ax2.invert_yaxis()

                ax1.set_xticks(np.arange(0.5, Nz))
                ax1.set_xticklabels(np.arange(0, Nz))
                ax1.set_yticks(np.arange(0.5, Nx))
                ax1.set_yticklabels(np.arange(0, Nx))

                ax2.set_xticks(np.arange(0.5, Nz))
                ax2.set_xticklabels(np.arange(0, Nz))
                ax2.set_yticks(np.arange(0.5, Nx))
                ax2.set_yticklabels(np.arange(0, Nx))

            # Title
            plt.suptitle('Absolute value of the modal controllability matrix  ' + \
                        'W(i)B(Alpha, Beta) \n' + \
                        'for all modes (Alpha, Beta) of the wall-normal '+ \
                            state_reduction +' actuation, ' + \
                            'for the ' + str(N) + ' most unstable eigenvalues (i=0,1,2).' )

            fig0.subplots_adjust(bottom=0.12, top=0.94, left=0.08, right=0.98)

            # Colorbar: left bottom width height
            cax = fig0.add_axes([0.25, 0.05, 0.5, 0.03])
            cbar = fig0.colorbar(im1, cax, orientation='horizontal')

            plt.show()

        ###############
        # PCOLORMESH
        ###############
        if graph == 'pcolormesh':

            # TICKS LABEL INITIALIZATION
            # setting the abscissa for the graphics
            modes = []
            if state_reduction == 'velocity':
                for i in range(_B.shape[1]):
                    modes.append('$q^{+}_{v_{'+str(divmod(i, Mz))+'}}$')
                    modes.append('$q^{-}_{v_{'+str(divmod(i, Mz))+'}}$')
            elif state_reduction == 'vorticity':
                for i in range(_B.shape[1]):
                    modes.append('$q^{+}_{\eta_{'+str(divmod(i+1, Mz))+'}}$')
                    modes.append('$q^{-}_{\eta_{'+str(divmod(i+1, Mz))+'}}$')
            elif state_reduction == 'uw':
                modes.append('$q^{+}_{u_{(0, 0)}}$')
                modes.append('$q^{-}_{u_{(0, 0)}}$')
                modes.append('$q^{+}_{w_{(0, 0)}}$')
                modes.append('$q^{-}_{w_{(0, 0)}}$')
            else:
                # In the case where B is not reduced to wall-normal vorticity
                for i in range(_B.shape[1]):
                    if i < Mx*Mz:
                        modes.append('$q^{+}_{v_{'+str(divmod(i, Mz))+'}}$')
                        modes.append('$q^{-}_{v_{'+str(divmod(i, Mz))+'}}$')
                    elif i < Mx*Mz + Mx*Mz - 1:
                        modes.append('$q^{+}_{\eta_{'+str(divmod(i-Mx*Mz+1, Mz))+'}}$')
                        modes.append('$q^{-}_{\eta_{'+str(divmod(i-Mx*Mz+1, Mz))+'}}$')
                    elif i == Mx*Mz + Mx*Mz:
                        modes.append('$q^{+}_{u_{(0, 0)}}$')
                        modes.append('$q^{-}_{u_{(0, 0)}}$')
                    elif i == Mx*Mz + Mx*Mz + 1:
                        modes.append('$q^{+}_{w_{(0, 0)}}$')
                        modes.append('$q^{-}_{w_{(0, 0)}}$')

            fig1 = plt.figure()
            fig1.suptitle(output+' value of WB = [w*(i) b(j)], \n'+\
                'Measure of the impact of the j-th control mode on the i-th eigenvalue.')

            if output == 'abs_log' or output == 'abs':
                # DO NOT CONSIDER THE LOG SCALING FOR THE OTHER METHOD, ONLY abs in that case.
                output = 'abs'
                plt.set_cmap('Greys')
            else:
                plt.set_cmap('bwr')

            ax1 = plt.subplot(3, 1, 1)
            ax2 = plt.subplot(3, 1, 2)
            ax3 = plt.subplot(3, 1, 3)

            # x-axis
            #plt.xticks(rotation='vertical')
            #plt.xticks(np.arange(0.5, WB.shape[1]))
            #ax1.set_xticks(np.arange(0.5, WB.shape[1]))
            #ax1.set_xticklabels(modes)

            ax1.set_xticks(np.arange(0.5, N1))
            ax2.set_xticks(np.arange(0.5, N2))
            ax3.set_xticks(np.arange(0.5, WB.shape[1]))
            ax1.set_xticklabels(modes[0:N1], rotation=90)
            ax2.set_xticklabels(modes[N1:N2], rotation=90)
            ax3.set_xticklabels(modes[N2::], rotation=90)

            # y-axis
            for axis in [ax1.yaxis]:
                axis.set(ticks=np.arange(0.5, len(eigs)),
                         ticklabels=np.trunc(eigs.real*10000)/10000 \
                            + np.complex(0, 1) * np.trunc(eigs.imag*10000)/10000)
            for axis in [ax2.yaxis]:
                axis.set(ticks=np.arange(0.5, len(eigs)),
                         ticklabels=np.trunc(eigs.real*10000)/10000 \
                            + np.complex(0, 1) * np.trunc(eigs.imag*10000)/10000)
            for axis in [ax3.yaxis]:
                axis.set(ticks=np.arange(0.5, len(eigs)),
                         ticklabels=np.trunc(eigs.real*10000)/10000 \
                            + np.complex(0, 1) * np.trunc(eigs.imag*10000)/10000)

            # filling
            max_WB = WB.max()
            min_WB = WB.min()
            limit = max(abs(min_WB), abs(max_WB))

            if output == 'abs':
                im1 = ax1.pcolormesh(WB[:, 0:N1],
                               edgecolor='k',
                               lw=0.01, vmin=0, vmax=+limit)
                im2 = ax2.pcolormesh(WB[:, N1:N2],
                               edgecolor='k',
                               lw=0.01, vmin=0, vmax=+limit)
                im3 = ax3.pcolormesh(WB[:, N2::],
                               edgecolor='k',
                               lw=0.01, vmin=0, vmax=+limit)
            else:
                im1 = ax1.pcolormesh(WB[:, 0:N1],
                               edgecolor='k',
                               lw=0.01, vmin=-limit, vmax=+limit)
                im2 = ax2.pcolormesh(WB[:, N1:N2],
                               edgecolor='k',
                               lw=0.01, vmin=-limit, vmax=+limit)
                im3 = ax3.pcolormesh(WB[:, N2::],
                               edgecolor='k',
                               lw=0.01, vmin=-limit, vmax=+limit)

            #ax.grid(b=True)
            ax1.set_aspect('equal', 'box')
            ax2.set_aspect('equal', 'box')
            ax3.set_aspect('equal', 'box')
            #ax.grid(b=True, linestyle='-', linewidth=0.3)
            #ax1.set_title('WB.'+output)
            ax3.set_xlabel('Modes')
            ax1.set_ylabel('Eigenvalues')
            ax2.set_ylabel('Eigenvalues')
            ax3.set_ylabel('Eigenvalues')
            ax1.autoscale_view()
            ax2.autoscale_view()
            ax3.autoscale_view()
            ax1.invert_yaxis()
            ax2.invert_yaxis()
            ax3.invert_yaxis()
            fig1.subplots_adjust(bottom=0.15, top=0.95, left=0.08, right=0.98)

            # left bottom width height
            cax = fig1.add_axes([0.25, 0.05, 0.5, 0.03])
            cbar = fig1.colorbar(im1, cax, orientation='horizontal')
            #fig1.colorbar(im1, cax=cax)

            plt.show()

        ###############
        # HINTON GRAPHIC
        ###############
        if False:#graph == 'hinton':
            fig = plt.figure()
            ax = plt.subplot(1, 1, 1)

            # x-axis
            plt.xticks(rotation='vertical')
            plt.xticks(np.arange(0.5, WB.shape[1]))
            ax.set_xticklabels(modes)

            # y-axis
            for axis in [ax.yaxis]:
                axis.set(ticks=np.arange(0.5, len(eigs)),
                         ticklabels=np.trunc(eigs.real*10000)/10000 \
                            + np.complex(0, 1) * np.trunc(eigs.imag*10000)/10000)

            # filling
            max_weight=None
            if not max_weight:
                max_weight = 2 ** np.ceil(np.log(np.abs(WB).max()) / np.log(2))

            for (x, y), w in np.ndenumerate(WB):
                color = 'white' if w > 0 else 'black'
                size = np.sqrt(np.abs(w) / max_weight)
                rect = plt.Rectangle([y + 0.5 - size / 2, x + 0.5 - size / 2], size, size,
                                 facecolor=color, edgecolor=color)
                ax.add_patch(rect)

            # settings
            ax.patch.set_facecolor('gray')
            ax.grid(b=True, linestyle='-', linewidth=0.3)
            ax.set_aspect('equal', 'box')
            ax.set_title('WB.'+output)
            ax.set_xlabel('Modes')
            ax.set_ylabel('Eigenvalues')
            ax.autoscale_view()
            ax.invert_yaxis()

            plt.show()
        """

        return WB

    def norm(self, x, Ny):
        """
        Calculate the norm for state vectors of the type
        [   v[alpha=0, beta=0]
            ...
            v[alpha=alpha_max, beta=beta_max],
            {eta[0,0] excluded)}
            eta[alpha=0, beta=1]
            ...
            eta[alpha=alpha_max, beta=beta_max],
            u[alpha=0, beta=0],
            w[alpha=0, beta=0] ]

        with Chebyshev clencurt weights.

        norm = sqrt(x.T W.T W x)
             = sqrt(v.T W.T W v + eta.T W.T W eta + u00.T W.T W u00 + w00 W.T W w00)

        ----------
        Keyword arguments:
        x -- state vector
        Ny -- dimension of 2nd axis

        ----------
        Return:
        norm_x -- norme of x
        """
        #TODO
        # Change this method to directly take the flowfield state vector [u, v, w]
        # and take its norm.
        # or create a method to change the base [u, v, w] <-> [v, eta, u00, w00]
        # and calculate the norm

        # Weight factor vector
        nodes, w = chebyshev.clencurt(Ny)
        W = []
        for n in range(int(x.shape[0] / (Ny-2))):
            W = np.concatenate((W, w[1:Ny-1]))

        norm_sq = np.dot(x.conj().T, np.dot(W.conj().T, np.dot(W, x)))
        return np.sqrt(norm_sq.real)

    def save_h5_cmplx(self, dataset_u, filename):
        """
        Save dataset_u into filename, with attributes
        and x, y, z axis of self.ff/
        """
        try:
            f = h5py.File(filename, 'w')
            f.create_dataset('data/u',
                             data=dataset_u,
                             compression='gzip',
                             dtype=np.complex)
            f.create_dataset('geom/x', data=self.ff.x)
            f.create_dataset('geom/y', data=self.ff.y)
            f.create_dataset('geom/z', data=self.ff.z)
            for a in self.ff.attributes:
                f.attrs[a] = self.ff.attributes[a]
            f.close()
            return True
        except:
            print("Operators.save_h5_cmplx did fail.")
            print(filename)
            return False

    def save_h5_real(self, dataset_u, filename):
        """
        Save dataset_u into filename, with attributes
        and x, y, z axis of self.ff/
        """
        try:
            f = h5py.File(filename, 'w')
            f.create_dataset('data/u',
                             data=np.real(dataset_u),
                             compression='gzip',
                             dtype=np.float)
            f.create_dataset('geom/x', data=self.ff.x)
            f.create_dataset('geom/y', data=self.ff.y)
            f.create_dataset('geom/z', data=self.ff.z)
            for a in self.ff.attributes:
                f.attrs[a] = self.ff.attributes[a]
            f.close()
            return True
        except:
            print("Operators.save_h5_real did fail.")
            print(filename)
            return False

    def f_initial(y, e, f, g):
        """
        Returns a vector of a function, for each point
        of the coordinate vector y,
        which respects both Dirichlet and Neumann BCs.

        Can be used as initial condition to integrate OSS, OSSE models.

        Keyword arguments:
        e, f, g -- int, user-defined

        Return:
        vector of size y.shape
        """
        a = e + 2 * g
        b = f
        c = -2*e -3*g
        d = -2*f
        return a*y**6 + b*y**5 + c*y**4 + d*y**3 + e*y**2 + f*y + g

    def f_lift(y, wall='up', BC='clamped'):
        """
        Return the lifting function for inplementation
        of wall boundary condition, given the wall-normal vector y

        Keyword argument:
        y -- vector of wall-normal coordinate.
        wall -- 'up' or 'low', where the actuation is imposed.
        BC -- 'clamped' or 'dirichlet', kind of boundary condition applied.

        Return :
        vector of shape as y, corresponding to the lifting function.
        """
        if wall == 'up' and BC == 'clamped':
            return 0.25 * (2 * y**4 - y**3 - 4 * y**2 + 3* y + 4)
            #return 0.25 * (- y**3 + 3* y + 2)

        elif wall == 'low' and BC == 'clamped':
            return 0.25 * (2 * y**4 + y**3 - 4 * y**2 - 3* y + 4)
            #return 0.25 * (+ y**3 - 3* y + 2)

        elif wall == 'up' and BC == 'dirichlet':
            return 0.5 * (y + 1)

        elif wall == 'low' and BC == 'dirichlet':
            return 0.5 * (- y + 1)

    def save_mat_binary(folder_path):
        """
        """
        # create folder_path/Mult_Control_Mat folder
        pass

    def save_mat_cmplx_hdf5(matrix, foldername, filename):
        """
        Store the matrix 'matrix' into 'foldername/filename',
        in the dataset "mat", and save its dimension
        N1 = matrix.shape[0] and N2 = matrix.shape[1]
        into the attributes "N1" and "N2".

        matrix -- matrix to store
        foldername -- needs to finish with "/"
        filename -- needs to finish with ".h5"
        """
        # create folder to save matrix
        try:
            os.makedirs(str(foldername))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        # save matrix
        matname = str(foldername) + str(filename)
        print(matname)
        f = h5py.File(matname, 'w')
        f.create_dataset('mat', data=matrix, compression='gzip', dtype=np.complex)
        #f.create_dataset('geom/x', data=self.ff.x)
        #f.create_dataset('geom/y', data=self.ff.y)
        #f.create_dataset('geom/z', data=self.ff.z)
        #for a in self.ff.attributes:
        #    f.attrs[a] = self.ff.attributes[a]

        # save matrix dimensions
        if matrix.ndim == 1:
            N1 = matrix.shape[0]
            N2 = 1
            f.attrs['N1'] = N1
            f.attrs['N2'] = N2
        elif matrix.ndim == 2:
            N1 = matrix.shape[0]
            N2 = matrix.shape[1]
            f.attrs['N1'] = N1
            f.attrs['N2'] = N2
        else:
            print("operators.save_mat_hdf5 received a non 1D or 2D array")

        f.close()

    def save_mat_real_hdf5(matrix, foldername, filename):
        """
        Store the matrix 'matrix' into 'foldername/filename',
        in the dataset "mat", and save its dimension
        N1 = matrix.shape[0] and N2 = matrix.shape[1]
        into the attributes "N1" and "N2".

        matrix -- matrix to store
        foldername -- needs to finish with "/"
        filename -- needs to finish with ".h5"
        """
        # create folder to save matrix
        try:
            os.makedirs(str(foldername))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        # save matrix
        matname = str(foldername) + str(filename)
        print(matname)
        f = h5py.File(matname, 'w')
        f.create_dataset('mat', data=matrix, compression='gzip')
        #f.create_dataset('geom/x', data=self.ff.x)
        #f.create_dataset('geom/y', data=self.ff.y)
        #f.create_dataset('geom/z', data=self.ff.z)
        #for a in self.ff.attributes:
        #    f.attrs[a] = self.ff.attributes[a]

        # save matrix dimensions
        if matrix.ndim == 1:
            N1 = matrix.shape[0]
            N2 = 1
            f.attrs['N1'] = N1
            f.attrs['N2'] = N2
        elif matrix.ndim == 2:
            N1 = matrix.shape[0]
            N2 = matrix.shape[1]
            f.attrs['N1'] = N1
            f.attrs['N2'] = N2
        else:
            print("operators.save_mat_hdf5 received a non 1D or 2D array")

        f.close()

    def load_mat_hdf5(self, filename):
        f = h5py.File(filename, 'r')
        # convert to np.ndarray, so it can be quicker for operation for require to store the entire data:
        matrix = f['mat'][()]
        # otherwise to ge tonly an access to the h5-dataset:
        #matrix = f['mat']
        N1 = f.attrs['N1']
        N2 = f.attrs['N2']

        return matrix, N1, N2
