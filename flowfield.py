"""
flowfield.py

implementing:
FlowField
DiffOperator_FlowField

Flowfield class that loads and saves hdf5 channel data
using conventions compatible with that produced by
John Gibson's channelflow.
generalised to generic rectilinear 3D fields,
with inheriting classes to specialise geometries
A Sharma 2015, 2016
G Claisse 2016
"""
import h5py
import numpy as np
import warnings
import math
import scipy
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt

import chebyshev


class FlowField:
    """Class for loading and holding generic rectilinear
    geometry flowfield snapshots

    Attributes:
    self.u -- 3D velocity field
    self.u0 -- 3D mean velocity field
    self.p -- 1D mean pressure field
    self.uu - 9D Reynolds Stress Tensor
    """

    def __init__(self, filename=None):
        self.version_str = u'FlowField v1.0.1b'  # CHANGE TO CLASS ATTRIBUTE ?
        self.attributes = {}
        self.attributes['version_str'] = self.version_str
        self.attributes['Nd'] = 3
        self.scaled = False
        self.u0 = np.NaN
        self.attributes_baseflow = {}
        self.p = None
        self.uu = None  # reynolds stress tensor, cf generate_stress_tensor()

        if filename is None:
            self.state = {'x': '-', 'y': '-', 'z': '-'}
            self.filename = None
            self.Nd = 0
            self.Nx = 0
            self.Ny = 0
            self.Nz = 0
            self.Mx = 0
            self.My = 0
            self.Mz = 0
        else:
            self.load(filename)
            self.Nd = self.attributes['Nd']
            self.Nx = self.attributes['Nx']
            self.Ny = self.attributes['Ny']
            self.Nz = self.attributes['Nz']

            self.Mx = self.Nx
            self.My = self.Ny
            if (self.Nz & 1) == 0: # Nz is even
                self.Mz = int(self.Nz / 2 + 1)
            else: # Nz is odd
                self.Mz = int((self.Nz + 1) / 2)

        self.dop = DiffOperator_FlowField(self)
        self.mean = MeanFlow_FlowField(self)
        self.bc = BoundaryCondition_FlowField(self)

    def copy(self, ff):
        """
        copy another flowfield to this one
        """
        self.filename = ff.filename
        self.u = np.copy(ff.u)
        self.p = np.copy(ff.p)
        self.u0 = np.copy(ff.u0)
        self.x = np.copy(ff.x)
        self.y = np.copy(ff.y)
        self.z = np.copy(ff.z)
        self.state = ff.state.copy()
        self.attributes = ff.attributes.copy()
        self.Nd = ff.Nd
        self.Nx = ff.Nx
        self.Ny = ff.Ny
        self.Nz = ff.Nz
        self.Mx = ff.Mx
        self.My = ff.My
        self.Mz = ff.Mz

    def load(self, filename):
        """
        loads a flowfield from a specified file
        """
        self.filename = str(filename)
        f = h5py.File(self.filename, 'r')

        for a in f.attrs:
            self.attributes[a] = f.attrs[a]

        # Influence of padding NxPad, NzPad on the following reshape ?
        # There isnt any padding as the H5 are in physical state.
        # [padding could be used during make_spectral()]

        # channelflow.org saves H5 as a line taking dimension in the order
        # D, X, Y, Z but under the shape (Nd, Nz, Ny, Nx).
        # The dataset needs to be reshaped (only) as (Nd, Nx, Ny, Nz),
        # but not transpose as the data is already in the order (D, X, Y, Z).

        # also: if the state is spectral, the type of self.u elements will be
        # complex as the output of np.array is the type of the input.
        self.u = np.reshape(f['data']['u'][()],
                            newshape=(self.attributes['Nd'],
                                      self.attributes['Nx'],
                                      self.attributes['Ny'],
                                      self.attributes['Nz']))
        self.x = np.array(f['geom']['x'])
        self.y = np.array(f['geom']['y'])
        self.z = np.array(f['geom']['z'])

        if(f['data'].get('p')):
            warnings.warn(
                "Dataset p found. But be careful, as it was not reshaped as u.")
            self.p = f['data']['p'][()]#, dtype=complex)
            # Be careful as P could also need to be reshaped.

        # In the case where dataset p is not defined in the HDF5 file,
        # a new dataset composed of zeros will be created.
        else:
            warnings.warn(
                "No dataset p found in the file. A null-dataset was created.")
            self.p = np.zeros(
                (self.u.shape[1], self.u.shape[2], self.u.shape[3]))#,
                #dtype=complex)

        f.close()
        self.state = {'xz': 'Physical', 'y': 'Physical'}
        self.conf_y_axis = self.evaluate_conf_axis('y')

    def load_baseflow(self, filename):
        """
        loads a flowfield baseflow (self.ff.u0)
        from a specified file
        """
        #TODO
        # add a Couette baseflow in case the filename correspond
        # to an equilibria

        self.filename_baseflow = str(filename)
        f = h5py.File(self.filename_baseflow, 'r')
        # Why reshape ? check the method FlowField.load()
        self.u0 = np.reshape(np.array(f['data']['u']),
                            newshape=(f.attrs['Nd'],
                                      f.attrs['Nx'],
                                      f.attrs['Ny'],
                                      f.attrs['Nz']))

        #self.u0 = np.transpose(np.array(f['data']['u']), (0, 3, 2, 1))
        self.x0 = np.array(f['geom']['x'])
        self.y0 = np.array(f['geom']['y'])
        self.z0 = np.array(f['geom']['z'])
        for a in f.attrs:
            self.attributes_baseflow[a] = f.attrs[a]

        self.state0 = {'xz': 'Physical', 'y': 'Physical'}
        self.conf_y0_axis = self.evaluate_conf_axis('y')

        # check if the attributes of the variation and baseflow are the same
        for a in f.attrs:
            if a in self.attributes:
                if self.attributes_baseflow[a] != self.attributes[a]:
                    warnings.warn(
                        "Disturbance and baseflow have different attributes.")
                    print('*** Diff. Attributes: ', a, \
                            ' / Main: ', self.attributes[a], \
                            ' / Baseflow: ', self.attributes_baseflow[a])
            else:
                print('*** Attributes in baseflow but not in main attributes: ', a)

        if self.conf_y_axis != self.conf_y0_axis:
            warnings.warn(
                "Y configuration differs from baseflow.")
        f.close()

    def evaluate_conf_axis(self, index):
        """
        Return the configuration of nodes in the given index direction.

        Keyword arguments:
        index -- 'x', 'y' or 'z', the direction where state will evaluated.

        Return:
        config -- 'Uniform' or 'Chebyshev'.

        REMARK:
        Return always 'uniform' for direction 'x' and 'z'.
        """
        if index in ('x', 'z'):
            return 'Uniform'
        elif index == 'y':
            y_uniform = np.linspace(self.y[0],
                                    self.y[self.y.shape[0] - 1],
                                    num=self.u.shape[2], endpoint=True)
            y_cheb = chebyshev.chebnodes(self.y.shape[0])
            if not hasattr(self, 'Y'):
                self.Y = 1
            y_bl = -self.Y * np.log(chebyshev.chebnodes(2 * self.y.shape[0])
                                    [:self.y.shape[0]])

            if (np.round(self.y, 2) == np.round(y_uniform, 2)).all():
                return 'Uniform'
            elif (np.round(self.y, 2) == np.round(y_cheb, 2)).all():
                return 'Chebyshev'
            else:
                # evaluate the stretching factor and state
                division = np.divide(self.y[1:], y_bl[1:])
                Y = division.mean()
                if (np.round(division - Y, 2)).all() == 0:
                    self.Y = Y
                    return 'Boundary_Layer'
                else:
                    warnings.warn(
                        "The configuaration of nodes in Y direction" +
                        " was not evaluated.")
                    return 'None'
        else:
            raise ValueError(
                "Evaluating the ordinate in this index is not possible:" +
                " only x,y, and z.")

    def calculate_mean(self):
        """

        """
        X_mean = np.mean(np.mean(self.u, axis=3, keepdims=True),
                         axis=2, keepdims=True)
        # gives (3, x_mean) -> ( u_x_mean, v_x_mean, w_x_mean)
        Y_mean = np.mean(np.mean(self.u, axis=3, keepdims=True),
                         axis=1, keepdims=True)
        Z_mean = np.mean(np.mean(self.u, axis=2, keepdims=True),
                         axis=1, keepdims=True)
        # then define mean = (X_mean, Y_mean, Z_mean)
        mean = np.ndarray((self.u.shape[0],
                           self.u.shape[1],
                           self.u.shape[2],
                           self.u.shape[3]))
        return X_mean, Y_mean, Z_mean
        # U_mean =(X_mean[0,:,:,:], Y_mean[0,:,:,:], Z_mean[0,:,:,:])
        # V_mean =(X_mean[1,:,:,:], Y_mean[1,:,:,:], Z_mean[1,:,:,:])
        # W_mean =(X_mean[2,:,:,:], Y_mean[2,:,:,:], Z_mean[2,:,:,:])

    def save(self, filename):
        """
        saves flowfield object to a specified file in hdf5 format.
        the user should ensure that state is physical
        since it is assumed when reading """
        if self.state['xz'] is not 'Physical' \
                or self.state['y'] is not 'Physical' :
            warnings.warn("Saving FlowField instance in non-physical format")
            u = self.u
            p = self.p
            u0 = self.u0
        else:
            u = np.real(self.u)
            p = np.real(self.p)
            u0 = np.real(self.u0)
        f = h5py.File(str(filename), 'w')
        f.create_dataset('data/u', data=u, compression='gzip')#, dtype=np.complex)
        f.create_dataset('data/p', data=p, compression='gzip')#, dtype=np.complex)
        # f.create_dataset('data/u0', data=u0, compression='gzip')
        f.create_dataset('geom/x', data=self.x)
        f.create_dataset('geom/y', data=self.y)
        f.create_dataset('geom/z', data=self.z)
        for a in self.attributes:
            f.attrs[a] = self.attributes[a]
        f.close()

    def flatten(self):
        """
        returns flattened velocity field.
        """
        return self.u.flat

    def unflatten(self, u):
        """
        takes a flattened velocity field and reshapes it
        according to a supplied template, for insertion.
        """
        Nx = self.attributes['Nx']
        Ny = self.attributes['Ny']
        Nz = self.attributes['Nz']
        self.u = u.reshape((3, Nx, Ny, Nz))  # reshape to same as template

    def len(self):
        """
        returns length of flattened velocity field
        """
        return np.prod(self.u.shape)

    def shift(self, shift, axis):
        """
        translates the flowfield along the x, y or z axis by a given amount.
        The shift must be negative.
        """
        # TODO: handle positive shift
        warnings.warn(
            """Function shift() of class FlowField has not been test
                    for scalarfield p.""")
        if axis == 'x':
            i = np.abs(self.x + shift).argmin()
            indexes = np.hstack(
                (i + np.arange(self.x.shape[0] - i), np.arange(i)))
            self.u = self.u[:, indexes, :, :]
            self.p = self.p[indexes, :, :]
        elif axis == 'y':
            i = np.abs(self.y + shift).argmin()
            indexes = np.hstack(
                (i + np.arange(self.y.shape[0] - i), np.arange(i)))
            self.u = self.u[:, :, indexes, :]
            self.p = self.p[:, indexes, :]
        elif axis == 'z':
            i = np.abs(self.z + shift).argmin()
            indexes = np.hstack(
                (i + np.arange(self.z.shape[0] - i), np.arange(i)))
            self.u = self.u[:, :, :, indexes]
            self.p = self.p[:, :, indexes]
        else:
            raise ValueError(
                "Shift in this axis not possible: only x, y and z")

    def reflect(self, axis):
        """
        Reflects the flowfield in the x, y or z axis.
        Changes sign of associated velocity.
        """
        warnings.warn(
            """Fonction reflect() of class FlowField has not been test
                    for scalarfield p.""")
        if axis == 'x':
            self.u = self.u[:, ::-1, :, :]
            self.u[0, :, :, :] *= -1
            self.p = self.p[::-1, :, :]
        elif axis == 'y':
            self.u = self.u[:, :, ::-1, :]
            self.u[1, :, :, :] *= -1
            self.p = self.p[:, ::-1, :]
        elif axis == 'z':
            self.u = self.u[:, :, :, ::-1]
            self.u[2, :, :, :] *= -1
            self.p = self.p[:, :, ::-1]
        else:
            raise ValueError(
                "Reflection by this index not possible: only x, y and z")

    def make_spectral(self, index='xyz', baseflow='no'):
        """
        IF index = 'xyz' necessary, do not do:
            make_spectral(XXX, index= 'y')
            make_spectral(XXX, index= 'xz')
        BUT
            make_spectral(XXX, index= 'xyz')
        AS : transform in Z needs to be first !

        convert flowfield to Fourier space in x-, y- or z- direction
        for u, p and u0

        Keyword argument:
        index -- direction 'xz', 'y' or 'xyz'.
        baseflow -- if the baseflow dataset need to be made spectral.
            'yes': baseflow is made spectral
            'no': baseflow is not made spectral
            'only': baseflow is made spectral but not the main dataset

        REMARK:
        The serie has been normalize with the number of elements
        of the given vector to normalize, which works for simple vectors
        to obtain a unit maximal vector but may not for more complex vectors.
        """

        # TODO
        # CHANGE TO RFFT INSTEAD OF FFT
        # WHICH SHAPE - NUMBER OF MODES - SHALL I CONSIDER ?
        # Mx = Nx
        # Mz = Nz * 2 / 3 ? so that I can use bigger Nz ?
        # What is possible :
        # -> as a basis, rfft transfrom physical N into spectral N/2+1 or (N+1)/2
        # either I keep Mz = Nz, so i save the exact same number of modes
        #     that I already had before
        #     same dimension but perhaps more precise ?
        # either I do Mz = Nz/2 + 1, so my Mz is smaller and I can increase Nz
        #     higher dimension ?
        # either I do Mz = Nz but with padded zero
        #     might be lighter to solve as only zeros, and still precise ?
        #
        # how to put that into practice ?
        # ask for ff.Mx instead of ff.Nx ? but usually I use Nx = ff.u.shape[0]
        # create a private ff.u_phys and ff.u_spec and access to ff.u ?
        # or openly use ff.u_phys and ff.u_spec ?
        # ff.u_phys is a real ndarray
        # ff.u_spec is a complex ndarray
        # shall I create a ff.Nx, ff.Ny, ff.Nz and ff.Mx, ff.My and ff.Mz ?
        # or still use ff.u.shape[0] and change Mx or Nx when it is necessary ?
        #
        # change state 'x' and 'z' into 'xz' only if I do that
        #
        # NO ! instead of divided by /u.shape[0], shall I use rfft(norm="ortho") ?

        u = self.u
        p = self.p
        u0 = self.u0

        if index == 'xz' or index == 'xyz':
                if self.state['xz'] != 'Fourier' and baseflow != 'only':
                    # RFFT for z direction firstly (real to complex)
                    u = np.fft.rfft(u, axis=3)/u.shape[3]
                    p = np.fft.rfft(p, axis=2)/u.shape[3]
                    # FFT for x direction (complex to complex)
                    u = np.fft.fft(u, axis=1)/u.shape[1]
                    p = np.fft.fft(p, axis=0)/u.shape[1]
                    # update state
                    self.state['xz'] = 'Fourier'
                if baseflow != 'no' and self.state0['xz'] != 'Fourier':
                    u0 = np.fft.rfft(u0, axis=3)/u0.shape[3]
                    u0 = np.fft.fft(u0, axis=1)/u0.shape[1]
                    self.state0['xz'] = 'Fourier'

        if index == 'y' or index == 'xyz':
            if self.state['y'] != 'Fourier' and baseflow != 'only':
                u = np.fft.fft(u, axis=2)/u.shape[2]
                p = np.fft.fft(p, axis=1)/u.shape[2]
                self.state['y'] = 'Fourier'
            if baseflow != 'no' and self.state0['y'] != 'Fourier':
                u0 = np.fft.fft(u0, axis=2)/u0.shape[2]
                self.state0['y'] = 'Fourier'

        if index != 'xz' and index != 'y' and index != 'xyz':
            warnings.warn(
                "FFT by this index not possible: only x, y and z")

        self.u = u
        self.p = p
        self.u0 = u0

    def make_spectral_dataset(self, dataset, index='xyz'):
        """
        IF index = 'xyz' necessary, do not do:
            make_spectral(XXX, index= 'y')
            make_spectral(XXX, index= 'xz')
        BUT
            make_spectral(XXX, index= 'xyz')
        AS : transform in Z needs to be first !

        convert 'dataset' to Fourier space in x-, y- or z- direction

        Keyword argument:
        dataset -- dataset of size (3,Nx,Ny,Nz) or (1,Nx,Ny,Nz)
        index -- direction 'xz', 'y', or 'xyz'.

        Return:
        data -- Spectral state of 'dataset'.
        """
        data = dataset.copy()
        if data.ndim == 4:
            n = 0
        elif data.ndim == 3:
            n = 1
        else:
            warnings.warn(
                "make_spectral_dataset() is defined " +
                "to only apply on 1D or 3D dataset.")

        if index == 'xz' or index == 'xyz':
            # if self.state['xz'] != 'Fourier':
            # RFFT for z direction firstly (real to complex)
            data = np.fft.rfft(data, axis=3 - n) / dataset.shape[3-n]
            # FFT for x direction (complex to complex)
            data = np.fft.fft(data, axis=1 - n) / dataset.shape[1-n]
            # self.state['xz'] = 'Fourier'

        if index == 'y' or index == 'xyz':
            # if self.state['y'] != 'Fourier':
            data = np.fft.fft(data, axis=2 - n) / dataset.shape[2-n]
            # self.state['y'] = 'Fourier'

        if index != 'xz' and index != 'y' and index != 'xyz':
            warnings.warn(
                "FFT by this index not possible: only x, y and z")

        return data

    def make_physical(self, index='xyz', baseflow='no'):
        """
        IF index = 'xyz' necessary, do not do:
            make_physical(XXX, index= 'y')
            make_physical(XXX, index= 'xz')
        BUT
            make_physical(XXX, index= 'xyz')
        AS : inverse-transform in Z needs to be last !

        return flowfield to physical space from Fourier space

        Keyword argument:
        index -- direction 'xz', 'y' or 'xyz'.
        baseflow -- if the baseflow dataset need to be made spectral.
            'yes': baseflow is made spectral
            'no': baseflow is not made spectral
            'only': baseflow is made spectral but not the main dataset

        """
        u = self.u
        p = self.p
        u0 = self.u0

        if index == 'y' or index == 'xyz':
            if self.state['y'] != 'Physical' and baseflow != 'only':
                u = np.fft.ifft(u, axis=2)*self.Ny
                p = np.fft.ifft(p, axis=1)*self.Ny
                self.state['y'] = 'Physical'
            if baseflow != 'no' and self.state0['y'] != 'Physical':
                u0 = np.fft.ifft(u0, axis=2)*self.Ny
                self.state0['y'] = 'Physical'

        if index == 'xz' or index == 'xyz':
            if self.state['xz'] != 'Physical' and baseflow != 'only':
                # IFFT for x direction firstly (complex to complex)
                u = np.fft.ifft(u, axis=1)*self.Nx
                p = np.fft.ifft(p, axis=0)*self.Nx
                # IRFFT for z direction LAST (complex to real)
                u = np.fft.irfft(u, n=self.Nz, axis=3)*self.Nz
                p = np.fft.irfft(p, n=self.Nz, axis=2)*self.Nz
                # update state
                self.state['xz'] = 'Physical'

            if baseflow != 'no' and self.state0['xz'] != 'Physical':
                u0 = np.fft.ifft(u0, axis=1)*self.Nx
                u0 = np.fft.irfft(u0, n=self.Nz, axis=3)*self.Nz
                self.state0['xz'] = 'Physical'

        if index != 'xz' and index != 'y' and index != 'xyz':
                warnings.warn(
                    "IFFT by this index not possible: only x, y and z")

        # assert (np.around(u.imag, 10) == 0).all()
        #           or (np.around(p.imag, 10) ==0).all(), \
        #    "Method make_physical() should not result " + \
        #    "in a matrix with a non-null imaginary part."
        # self.u = u.real
        # self.p = p.real
        self.u = u
        self.p = p
        self.u0 = u0

    def make_physical_dataset(self, dataset, nz, index='xyz'):
        """
        IF index = 'xyz' necessary, do not do:
            make_physical(XXX, index= 'y')
            make_physical(XXX, index= 'xz')
        BUT
            make_physical(XXX, index= 'xyz')
        AS : inverse-transform in Z needs to be last !

        return 'dataset' to physical space from Fourier space

        Keyword argument:
        dataset -- dataset of size (3,Nx,Ny,Nz) or (1,Nx,Ny,Nz)
        nz -- the dimension of the returned physical dataset.
            The method rfft does not require Nz, but the inverse
            irfft can not determine if Nz from the original flowfield
            was even or odd, so we need to implement it manually
            as it is not stored anywhere for a random dataset.
            (see numpy.fft.irfft)
        index -- direction 'xz', 'y' or 'xyz'.

        Return:
        data -- Physical state of 'dataset'.
        """
        data = dataset.copy()
        if data.ndim == 4:
            n = 0
        elif data.ndim == 3:
            n = 1
        else:
            warnings.warn(
                "make_physical_dataset() is defined " +
                "to only apply on 1D or 3D dataset.")

        if index == 'y' or index == 'xyz':
            # if self.state['y'] != 'Fourier':
            data = np.fft.ifft(data, axis=2 - n) * dataset.shape[2-n]
            # self.state['y'] = 'Fourier'

        if index == 'xz' or index == 'xyz':
            # if self.state['xz'] != 'Fourier':
            # IFFT for x direction (complex to complex)
            data = np.fft.ifft(data, axis=1 - n) * dataset.shape[1-n]
            # IRFFT for z direction LAST (complex to real)
            data = np.fft.irfft(data, n=nz, axis=3 - n) * nz
            # self.state['xz'] = 'Fourier'

        if index != 'xz' and index != 'y' and index != 'xyz':
            warnings.warn(
                    "IFFT by this index not possible: only x, y and z")

        # assert (np.around(data.imag, 10) == 0).all(), \
        #    "Method make_physical_dataset() should not result " + \
        #    "in a matrix with a non-null imaginary part."
        # return data.real
        return data

    def notch_filter(self, n, index):
        """
        set to zero all Fourier components except at n
        """
        warnings.warn(
            "Fonction notch_filter() of class FlowField " +
            "has not been test for scalarfield p.")

        if index == 'x' or index == 'z':
            self.make_spectral('xz')
        elif index == 'y':
            self.make_spectral('y')

        if index == 'x':
            N = self.u.shape[1]
            ix = np.where(np.fft.fftfreq(N, 1.0 / N) != n)
            self.u[:, ix, :, :] = 0
            self.p[ix, :, :] = 0
        elif index == 'y':
            N = self.u.shape[2]
            ix = np.where(np.fft.fftfreq(N, 1.0 / N) != n)
            self.u[:, :, ix, :] = 0
            self.p[:, ix, :] = 0
        elif index == 'z':
            N = self.Mz
            #N = self.u.shape[3]
            ix = np.where(np.fft.rfftfreq(N, 1.0 / N) != n)
            self.u[:, :, :, ix] = 0
            self.p[:, :, ix] = 0

        if index == 'x' or index == 'z':
            self.make_physical('xyz')
        elif index == 'y':
            self.make_physical('y')

    def extract_fourier_plane(self, n, index):
        """
        return a flattened array over one x-, y- or z-Fourier mode (both parts)
        """
        warnings.warn(
            "Fonction extract_fourier_plane() of class FlowField " +
            "has not been test for scalarfield p.")

        if index == 'x' or index == 'z':
            self.make_spectral('xz')
        elif index == 'y':
            self.make_spectral('y')

        if index == 'x':
            N = self.u.shape[1]
            ix = np.where(np.fft.fftfreq(N, 1.0 / N) == n)
            plane = self.u[:, ix, :, :]
        elif index == 'y':
            N = self.u.shape[2]
            ix = np.where(np.fft.fftfreq(N, 1.0 / N) == n)
            plane = self.u[:, :, ix, :]
        elif index == 'z':
            N = self.Mz
            #N = self.u.shape[3]
            ix = np.where(np.fft.rfftfreq(N, 1.0 / N) == n)
            plane = self.u[:, :, :, ix]

        if index == 'x' or index == 'z':
            self.make_physical('xyz')
        elif index == 'y':
            self.make_physical('y')

        return plane.flat

    def insert_fourier_plane(self, plane, n, index):
        """
        insert a plane generated by .extract_fourier_plane()
        """
        warnings.warn(
            "Fonction insert_fourier_plane() of class FlowField " +
            "has not been test for scalarfield p.")
        plane = np.array(plane)

        if index == 'x' or index == 'z':
            self.make_spectral('xz')
        elif index == 'y':
            self.make_spectral('y')

        if index == 'x':
            N = self.u.shape[1]
            ix = np.where(np.fft.fftfreq(N, 1.0 / N) == n)
            self.u[:, ix, :, :] = plane.reshape(self.u[:, ix, :, :].shape)
        elif index == 'y':
            N = self.u.shape[2]
            ix = np.where(np.fft.fftfreq(N, 1.0 / N) == n)
            self.u[:, :, ix, :] = plane.reshape(self.u[:, :, ix, :].shape)
        elif index == 'z':
            N = self.Mz
            #N = self.u.shape[3]
            ix = np.where(np.fft.rfftfreq(N, 1.0 / N) == n)
            self.u[:, :, :, ix] = plane.reshape(self.u[:, :, :, ix].shape)

        if index == 'x' or index == 'z':
            self.make_physical('xyz')
        elif index == 'y':
            self.make_physical('y')

    def extract_fourier_line(self, k):
        """
        return a flattened array over x- and z-Fourier mode
        """
        warnings.warn(
            "Fonction extract_fourier_line() of class FlowField " +
            "has not been test for scalarfield p.")
        self.make_spectral('xz')
        Nx = self.u.shape[1]
        Nz = self.u.shape[3]
        ix = np.where(np.fft.fftfreq(Nx, 1.0 / Nx) == k[0])
        iz = np.where(np.fft.rfftfreq(Nz, 1.0 / Nz) == k[1])
        line = self.u[:, ix, :, iz]
        self.make_physical('xz')
        return line.flat

    def insert_fourier_line(self, line, k):
        """
        insert a slice generated by .extract_fourier_line()
        """
        warnings.warn(
            "Fonction insert_fourier_line() of class FlowField " +
            "has not been test for scalarfield p.")
        line = np.array(line)
        self.make_spectral('xz')
        Nx = self.u.shape[1]
        Nz = self.u.shape[3]
        ix = np.where(np.fft.fftfreq(Nx, 1.0 / Nx) == k[0])
        iz = np.where(np.fft.rfftfreq(Nz, 1.0 / Nz) == k[1])
        self.u[:, ix, :, iz] = line.reshape(self.u[:, ix, :, iz].shape)
        self.make_physical('xz')

    def decimate(self, axis, stride):
        """
        decimate the flowfield arrays in axis = x, y or z to reduce size.
        """
        warnings.warn(
            "Fonction decimate() of class FlowField " +
            "has not been test for scalarfield p.")
        if stride:
            if axis == 'x':
                self.u = self.u[:, ::stride, :, :]
                self.p = self.p[::stride, :, :]
                self.x = self.x[::stride]
                self.attributes['Nx'] = self.x.shape[0]
            elif axis == 'y':
                self.u = self.u[:, :, ::stride, :]
                self.p = self.p[:, ::stride, :]
                self.y = self.y[::stride]
                self.attributes['Ny'] = self.y.shape[0]
            elif axis == 'z':
                self.u = self.u[:, :, :, ::stride]
                self.p = self.p[:, :, ::stride]
                self.z = self.z[::stride]
                self.attributes['Nz'] = self.z.shape[0]
            else:
                warnings.warn(
                    "decimation in this index not possible: only x, y and z")

    def isEqual(self, flowfield_2):
        """
        Compare two flowfields and return true if they are exactly the same.
        """
        if (self.x == flowfield_2.x).all() and \
            (self.y == flowfield_2.y).all() and \
            (self.z == flowfield_2.z).all() and \
            (np.round(self.u, 12) == np.round(flowfield_2.u, 12)).all() and \
                (np.round(self.p, 12) == np.round(flowfield_2.p, 12)).all():
            return True
        else:
            return False

    def pressure_solve(self):
        """ find the pressure field from the velocity field """
        pass

    def generate_stress_tensor(self):
        """Generate the Reynolds Stress Tensor of the dataset u.

        This method generates the stress tensor
        of the dataset u of the flowfield.
        It does not return the stress tensor array,
        but create a new attribute for the flowfield.
        You can then call 'self.uu' to access.

        Considering a FlowField V of dataset u whose shape is (3, Nx, Ny, Nz):
        tensor(u) = [ [ux.ux, ux.uy, ux,uz],
                      [uy.ux, uy.uy, uy.uz],
                      [uz.ux, uz.uy, uz.uz]  ]
        ...of shape (3, 3, Nx, Ny, Nz)

        WARNING: Only apply on 3D dataset
        """
        if isinstance(self.uu, np.ndarray):
            return

        assert(self.u.shape[0] == 3), 'Stress tensor only apply on 3D dataset.'

        print("flowflied.generate_stress_tensor : should be dot product instead of * ?")
        xx = self.u[0, :, :, :] * self.u[0, :, :, :]
        yy = self.u[1, :, :, :] * self.u[1, :, :, :]
        zz = self.u[2, :, :, :] * self.u[2, :, :, :]
        xy = self.u[0, :, :, :] * self.u[1, :, :, :]
        xz = self.u[0, :, :, :] * self.u[2, :, :, :]
        yz = self.u[1, :, :, :] * self.u[2, :, :, :]

        self.uu = np.ndarray((3, 3, self.u.shape[1],
                              self.u.shape[2],
                              self.u.shape[3]))
        self.uu[:, :, :, :, :] = [[xx[:, :, :], xy[:, :, :], xz[:, :, :]],
                                  [xy[:, :, :], yy[:, :, :], yz[:, :, :]],
                                  [xz[:, :, :], yz[:, :, :], zz[:, :, :]]]

    def plot_major_modes(self, baseflow=True):
        """
        FONCTION flowfield.plot_major_modes WAS NOT UPDATE TO BE USED WITH RFFT INSTEAD OF FFT

        Plot the major modes of the data self.u or of the baseflow self.u0.

        Remark:
        Test only for baseflow.
        """
        print("FONCTION flowfield.plot_major_modes WAS NOT UPDATE TO BE USED WITH RFFT INSTEAD OF FFT")

        print("Should use Mx and Mz instead")
        Nx = self.Nx
        Nz = self.Nz
        Mx = self.Mx
        Mz = self.Mz
        #Nx = self.x.shape[0]
        #Nz = self.z.shape[0]
        self.make_spectral('xz', baseflow='yes')

        # Reordering the Fourier modes for streamwise and spanwise direction
        K = 2 # if full set of modes needed, set K=2
        freq_kx_0 = np.fft.fftfreq(Nx, 1/Nx).astype(np.integer, copy=False)
        freq_kz_0 = np.fft.rfftfreq(Nz, 1/Nz).astype(np.integer, copy=False)
        freq_kx = np.concatenate((freq_kx_0[-int(Mx/K):], freq_kx_0[:int(Mx/K+1)]))
        freq_kz = freq_kz_0[-int(Mz/K):]
        #freq_kz = np.concatenate((freq_kz_0[-int(Mz/K):], freq_kz_0[:int(Mz/K+1)]))

        if baseflow:
            table_modes_u_0 = abs(self.u0[0, :, int(self.y.shape[0]/2), :])
            table_modes_v_0 = abs(self.u0[1, :, int(self.y.shape[0]/2), :])
            table_modes_w_0 = abs(self.u0[2, :, int(self.y.shape[0]/2), :])
        else:
            table_modes_u_0 = abs(self.u[0, :, int(self.y.shape[0])/2, :])
            table_modes_v_0 = abs(self.u[1, :, int(self.y.shape[0])/2, :])
            table_modes_w_0 = abs(self.u[2, :, int(self.y.shape[0])/2, :])

        # Reordering the Fourier modes for streamwise and spanwise direction
        table_modes_u_1 = table_modes_u_0[:int(Mz/K+1), :]
        #table_modes_u_1 = np.concatenate((table_modes_u_0[-int(Mz/K):, :],
        #                                  table_modes_u_0[:int(Mz/K+1), :]))
        table_modes_u = np.concatenate((table_modes_u_1[:, -int(Mx/K):],
                                        table_modes_u_1[:, :int(Mx/K+1)]), axis=1)

        table_modes_v_1 = table_modes_v_0[:int(Mz/K+1), :]
        #table_modes_v_1 = np.concatenate((table_modes_v_0[-int(Mz/K):, :],
        #                                  table_modes_v_0[:int(Mz/K+1), :]))
        table_modes_v = np.concatenate((table_modes_v_1[:, -int(Mx/K):],
                                        table_modes_v_1[:, :int(Mx/K+1)]), axis=1)

        table_modes_w_1 = table_modes_w_0[:int(Mz/K+1), :]
        #table_modes_w_1 = np.concatenate((table_modes_w_0[-int(Mz/K):, :],
        #    table_modes_w_0[:int(Mz/K+1), :]))
        table_modes_w = np.concatenate((table_modes_w_1[:, -int(Mx/K):],
            table_modes_w_1[:, :int(Mx/K+1)]), axis=1)

        # figure
        fig, (ax0, ax1, ax2) = plt.subplots(ncols=3)
        plt.set_cmap('Greys')

        ax0.set_title('Modes of $u$')
        ax0.set_ylabel('Alpha')
        ax0.set_xlabel('Beta')
        ax0.autoscale_view()
        ax0.set_xticks(np.arange(0.5, Mz))
        ax0.set_xticklabels(freq_kz)
        ax0.set_yticks(np.arange(0.5, Mx))
        ax0.set_yticklabels(freq_kx)
        ax0.pcolormesh(table_modes_u,
                      edgecolor='k',
                      lw=0.01)

        ax1.set_title('Modes of $v$')
        #ax1.set_ylabel('Alpha')
        ax1.set_xlabel('Beta')
        ax1.autoscale_view()
        ax1.set_xticks(np.arange(0.5, Mz))
        ax1.set_xticklabels(freq_kz)
        ax1.set_yticks(np.arange(0.5, Mx))
        ax1.set_yticklabels(freq_kx)
        ax1.pcolormesh(table_modes_v,
                      edgecolor='k',
                      lw=0.01)

        ax2.set_title('Modes of $w$')
        #ax2.set_ylabel('Alpha')
        ax2.set_xlabel('Beta')
        ax2.autoscale_view()
        ax2.set_xticks(np.arange(0.5, Mz))
        ax2.set_xticklabels(freq_kz)
        ax2.set_yticks(np.arange(0.5, Mx))
        ax2.set_yticklabels(freq_kx)
        ax2.pcolormesh(table_modes_w,
                      edgecolor='k',
                      lw=0.01)
        plt.show()

        self.make_physical('xz', baseflow='yes')


class DiffOperator_FlowField:
    """
    Class to manage differential operation on the Flowfield object.
    DiffOperator_FLowField contains operator like derivative,
        divergence, gradient, curl and laplacian.
    Operators can be applied though the usage of a FlowField
        to a 3D velocity dataset or to a 1D pressure dataset
        (derivative and gradient only) or to a Reynolds Stress Tensor.

    Attributes :
    self.ff -- flowfield on which apply the operations.
    """

    def __init__(self, flowfield):
        """
        """
        self.ff = flowfield

    def __deriv(self, dataset, x_order=0, y_order=0, z_order=0,
            bc=False):
        """
        Derivative of the dataset 'dataset' of the flowfield 'self.ff'
        in direction 'index' and order 'order'.

        Considering the flowfield V whose shape of dataset u
        is (3, Nx, Ny, Nz):
        __deriv(V.u, 'x', n) = (dn_V.ux/dx**n,
                                dn_V.uy/dx**n,
                                dn_V.uz/dx**n) (x,y,z)
        __deriv(V.u, 'y', n) = (dn_V.ux/dy**n,
                                dn_V.uy/dy**n,
                                dn_V.uz/dy**n) (x,y,z)
        __deriv(V.u, 'z', n) = (dn_V.ux/dz**n,
                                dn_V.uy/dz**n,
                                dn_V.uz/dz**n) (x,y,z)

        WARNING:
        Apply method make_spectral() to the flowfield
        BEFORE using method __deriv().

        Keyword arguments:
        dataset -- dataset self.ff.u or self.ff.p
                    on which apply the derivative.
        x_order -- order of differentiation in direction 'x'.
        y_order -- order of differentiation in direction 'y'.
        z_order -- order of differentiation in direction 'z'.
        bc -- kind of boundary condition,
            see class BoundaryCondition_FlowField

        Return:
        diff -- (Physical) Array (3, Nx, Ny, Nz) for dataset (3,Nx,Ny,Nz).
                (Physical) Array (1, Nx, Ny, Nz) for dataset (1,Nx,Ny,Nz).

        """
        diff = np.ndarray((dataset.shape[0], dataset.shape[1],
                           dataset.shape[2],
                           dataset.shape[3]))#, dtype=complex)
        Nz = diff.shape[3]

        if y_order == 0:
            dataset = self.ff.make_spectral_dataset(dataset, index='xz')
            diff = self.ff.make_spectral_dataset(diff, index='xz')
        else:
            dataset = self.ff.make_spectral_dataset(dataset, index='xyz')
            diff = self.ff.make_spectral_dataset(diff, index='xyz')

        kx = np.ones(dataset.shape[1])
        ky = np.ones(dataset.shape[2])
        kz = np.ones(dataset.shape[3])

        if x_order > 0:
            # assert self.ff.state['x'] == 'Fourier', \
            #    "Flowfield state needs to be spectral in direction 'x'" + \
            #    " before applying method __deriv()"
            kx = (
                np.fft.fftfreq(
                    dataset.shape[1],
                    1.0 /
                    dataset.shape[1]) *
                np.complex(
                    0,
                    1) *
                2 *
                np.pi /
                self.ff.attributes['Lx'])**x_order

        if y_order > 0:
            # assert self.ff.state['y'] == 'Fourier', \
            #    "Flowfield state needs to be spectral in direction 'y'" + \
            #    " before applying method __deriv()"
            ky = (
                np.fft.fftfreq(
                    dataset.shape[2],
                    1.0 /
                    dataset.shape[2]) *
                np.complex(
                    0,
                    1) *
                2 *
                np.pi /
                self.ff.attributes['Ly'])**y_order

        if z_order > 0:
            # assert self.ff.state['z'] == 'Fourier', \
            #    "Flowfield state needs to be spectral in direction 'z'" + \
            #    " before applying method __deriv()"
            kz = (
                np.fft.rfftfreq(
                    Nz,
                    1.0 /
                    Nz) *
                np.complex(
                    0,
                    1) *
                2 *
                np.pi /
                self.ff.attributes['Lz'])**z_order

        k = np.einsum('i,j,k -> ijk', kx, ky, kz)
        # print(' K :  \n', np.round(k, 2), '\n --------------------------\n')

        for i in range(dataset.shape[0]):
            diff[i, :, :, :] = k * dataset[i, :, :, :]

        # for inverse transform, always Z direction (RFFT) last
        #if y_order > 0:
        #    diff = np.fft.ifft(diff, axis=2)*dataset.shape[2]
        #if x_order > 0 or z_order > 0:
        #    diff = np.fft.ifft(diff, axis=1)*dataset.shape[1]
        #    diff = np.fft.irfft(diff, axis=3)*dataset.shape[3]

        if y_order == 0:
            diff = self.ff.make_physical_dataset(diff, nz=Nz, index='xz')
            dataset = self.ff.make_physical_dataset(dataset, nz=Nz, index='xz')
        else:
            diff = self.ff.make_physical_dataset(diff, nz=Nz, index='xyz')
            dataset = self.ff.make_physical_dataset(dataset, nz=Nz, index='xyz')

        # assert (np.around(diff.imag, 5) == 0).all(), \
        #    "Method __deriv() should not result " + \
        #   "in a matrix with a non-null imaginary part."
        return diff.real

    def deriv(self, dataset, x_order=0, y_order=0, z_order=0, bc=False, state=None):
        """
        Wrapper of method __deriv() for any dataset.
        Derivative of the dataset of the flowfield 'self.ff'
        in direction 'index' and order 'order'.

        Considering the flowfield V whose shape of dataset D
        is (3, Nx, Ny, Nz):
        deriv(D, 'x', n) = (dn_Dx/dx**n, dn_Dy/dx**n, dn_Dz/dx**n) (x,y,z)
        deriv(D, 'y', n) = (dn_Dx/dy**n, dn_Dy/dy**n, dn_Dz/dy**n) (x,y,z)
        deriv(D, 'z', n) = (dn_Dx/dz**n, dn_Dy/dz**n, dn_Dz/dz**n) (x,y,z)

        The dataset D can be of size (3, Nx, Ny, Nz) or (Nx, Ny, Nz).

        Keyword arguments:
        dataset -- dataset to differentiate
        x_order -- order of differentiation in direction 'x'.
        y_order -- order of differentiation in direction 'y'.
        z_order -- order of differentiation in direction 'z'.
        bc -- see docstring on __deriv().

        Do not use or correct the method:
        state -- if the state of the dataset is given
            state= {'xz': state_xz, 'y': state_y}
            with state varialbe as 'Physical' or 'Fourier'.
                This option can prevent calling FFT and IFFT many time.
                However, if x_order = 0, and state['x'] == 'Fourier',
            the returned differential will still be Fourier
            transformed in the X direction.

        Return:
        deriv -- (Physical) Array (3, Nx, Ny, Nz) for dataset (3,Nx,Ny,Nz)
                 (Physical) Array (Nx, Ny, Nz) for dataset (Nx,Ny,Nz)
        """
        assert isinstance(x_order, int) \
            and isinstance(y_order, int) \
            and isinstance(z_order, int), \
            "Order of derivation must be integer."

        if x_order == 0 and y_order == 0 and z_order == 0:
            return dataset.copy()

        if state == None:
            state = {'xz': 'Physical', 'y':'Physical'}

        data = dataset.copy()
        #if (x_order > 0 or z_order > 0) and state['xz'] == 'Physical':
        #    data = self.ff.make_spectral_dataset(data, 'xz')
        #if y_order > 0 and state['y'] == 'Physical':
        #    data = self.ff.make_spectral_dataset(data, 'y')

        if data.ndim == 4:
            Nz = dataset.shape[3]
            deriv = self.__deriv(data, x_order, y_order, z_order)
        elif data.ndim == 3:
            Nz = dataset.shape[2]
            deriv = self.__deriv(
                data[
                    np.newaxis, :, :, :], x_order, y_order, z_order
                    )[0, :, :, :]
        else:
            warnings.warn(
                "Derivative method deriv is defined " +
                "to only apply on 1D or 3D dataset.")

        # for inverse transform, always Z direction (RFFT) last
        # if y_order > 0:
        #    data = self.ff.make_physical_dataset(data, nz=Nz, 'y')
        # if z_order > 0 or x_order > 0:
        #    data = self.ff.make_physical_dataset(data, nz=Nz, 'xz')

        return deriv

    def deriv_u(self, x_order=0, y_order=0, z_order=0, bc=False):
        """ Wrapper of method __deriv() dedicated to dataset u.
        Derivative of the dataset 'u' of the flowfield 'self.ff'
        in direction 'index' and order 'order'.

        Considering the flowfield V whose shape of dataset u
        is (3, Nx, Ny, Nz):
        deriv_u('x', n) = (dn_V.ux/dx**n, dn_V.uy/dx**n, dn_V.uz/dx**n) (x,y,z)
        deriv_u('y', n) = (dn_V.ux/dy**n, dn_V.uy/dy**n, dn_V.uz/dy**n) (x,y,z)
        deriv_u('z', n) = (dn_V.ux/dz**n, dn_V.uy/dz**n, dn_V.uz/dz**n) (x,y,z)

        DO NOT APPLY CROSS PARTIAL DERIVATIVE

        Keyword arguments:
        x_order -- order of differentiation in direction 'x'.
        y_order -- order of differentiation in direction 'y'.
        z_order -- order of differentiation in direction 'z'.
        bc -- see docstring on __deriv().

        Return:
        deriv_u -- (Physical) Array (3, Nx, Ny, Nz).
        """
        assert isinstance(x_order, int) \
            and isinstance(y_order, int) \
            and isinstance(z_order, int), \
            "Order of derivation must be integer."

        if x_order == 0 and y_order == 0 and z_order == 0:
            return self.ff.u.copy()

        assert(self.ff.u.shape[0] == 3), "Derivative method deriv_u " + \
            "is defined to only apply on 3D dataset 'u'."

        # for direct transform, always Z direction (RFFT) first
        #if x_order > 0 or z_order > 0:
        #    self.ff.make_spectral('xz')
        #if y_order > 0:
        #    self.ff.make_spectral('y')

        deriv_u = self.__deriv(self.ff.u, x_order, y_order, z_order, bc)

        # for inverse transform, always Z direction (RFFT) last
        #if y_order > 0:
        #    self.ff.make_physical('y')
        #if x_order > 0 or z_order > 0:
        #    self.ff.make_physical('xz')

        return deriv_u

    def deriv_p(self, x_order=0, y_order=0, z_order=0, bc=False):
        """Wrapper of method __deriv() dedicated to dataset p.
        Derivative of the dataset 'p' of the flowfield 'self.ff'
        in direction 'index' and order 'order'.

        Considering the flowfield V whose shape of dataset p
        is (3, Nx, Ny, Nz):
        deriv_p('x', n) = (dn_V.p/dx**n) (x,y,z)
        deriv_p('y', n) = (dn_V.p/dy**n) (x,y,z)
        deriv_p('z', n) = (dn_V.p/dz**n) (x,y,z)

        Keyword arguments:
        x_order -- order of differentiation in direction 'x'.
        y_order -- order of differentiation in direction 'y'.
        z_order -- order of differentiation in direction 'z'.
        bc -- see docstring on __deriv().

        Return:
        deriv_p -- (Physical) Array (Nx, Ny, Nz).
        """
        assert isinstance(x_order, int) \
            and isinstance(y_order, int) \
            and isinstance(z_order, int), \
            "Order of derivation must be integer."

        if x_order == 0 and y_order == 0 and z_order == 0:
            return self.ff.p.copy()

        assert(self.ff.p.ndim == 3), "Derivative method deriv_p " + \
            "is defined to only apply on 1D dataset 'p'."

        # for direct transform, always Z direction (RFFT) first
        #if x_order > 0 or z_order > 0:
        #    self.ff.make_spectral('xz')
        #if y_order > 0:
        #    self.ff.make_spectral('y')

        deriv_p = self.__deriv(
            self.ff.p[np.newaxis, :, :, :],
            x_order,
            y_order,
            z_order,
            bc)[0, :, :, :]

        # for inverse transform, always Z direction (RFFT) last
        #if x_order > 0 or z_order > 0:
        #    self.ff.make_physical('xz')
        #if y_order > 0:
        #    self.ff.make_physical('y')

        return deriv_p

    def div(self, dataset, order=1, state=None):
        """Divergence of the dataset of the flowfield self.ff.

        Considering the flowfield V whose shape of dataset D
        is (3, Nx, Ny, Nz):
        div(D, n) = (dn_Dx/dx**n + dn_Dy/dy**n + dn_Dz/dz**n) (x,y,z)

        Keyword arguments:
        dataset -- dataset to apply divergence on.
        order -- integer, order of the divergence.
        state -- if the state of the dataset is given
            state= {'x': state_x, 'y': state_y, 'z': state_z}
            with state vairalbe as 'Physical' or 'Fourier'.

        Return:
        div -- (Physical) Array (Nx, Ny, Nz).
        """
        assert(dataset.shape[0] == 3), \
            'Divergence method is define to only apply on 3D dataset.'

        div = np.ndarray((dataset.shape[1],
                          dataset.shape[2],
                          dataset.shape[3]))#, dtype=complex)
        div[:, :, :] = self.ff.dop.deriv(dataset, order, 0, 0, state=state)[0, :, :, :]
        if self.ff.u.shape[0] == 3:
            div[:, :, :] += self.ff.dop.deriv(dataset,
                                                0, order, 0,
                                                state=state)[1, :, :,:] \
                            + self.ff.dop.deriv(dataset,
                                                0, 0, order,
                                                state=state)[2, :, :, :]
        return div

    def div_u(self, order=1):
        """Divergence of the dataset u of the flowfield self.ff.

        Considering the flowfield V whose shape of dataset u
        is (3, Nx, Ny, Nz):
        div(V.u, n) = (dn_V.ux/dx**n + dn_V.uy/dy**n + dn_V.uz/dz**n) (x,y,z)

        Keyword arguments:
        order -- integer, order of the divergence.

        Return:
        div -- (Physical) Array (Nx, Ny, Nz).
        """
        assert(self.ff.u.shape[0] == 3), \
            'Divergence method is define to only apply on 3D dataset.'

        div = np.ndarray((self.ff.u.shape[1],
                          self.ff.u.shape[2],
                          self.ff.u.shape[3]))#, dtype=complex)

        div[:, :, :] = self.ff.dop.deriv_u(order, 0, 0)[0, :, :, :]
        if self.ff.u.shape[0] == 3:
            div[:, :, :] += self.ff.dop.deriv_u(0, order, 0)[1, :, :, :] + \
                self.ff.dop.deriv_u(0, 0, order)[2, :, :, :]
        return div

    def grad(self, dataset, order=1, state=None):
        """Gradient of the dataset of the flowfield self.ff.

        Considering the flowfield V whose shape of dataset D
        is (n, Nx, Ny, Nz).
        grad(D) = ( ( dDx/dx, dDY/dx, dDz/dx ) (x,y,z),
                    ( dDx/dy, dDy/dy, dDz/dy ) (x,y,z),
                    ( dDx/dz, dDy/dz, dDz/dz ) (x,y,z) )

        The dataset D can be of size (3, Nx, Ny, Nz) or (1, Nx, Ny, Nz).
        n = 1 or n = 3.

        Keyword arguments:
        dataset -- dataset to apply gradient
        order -- interger, order of the gradient.
        state -- if the state of the dataset is given
            state= {'x': state_x, 'y': state_y, 'z': state_z}
            with state vairalbe as 'Physical' or 'Fourier'.

        Return:
        grad -- (Physical) Array (3, n, Nx, Ny, Nz).
        """
        assert(dataset.shape[0] == 3 or dataset.ndim == 3), \
            "Grad method grad is defined to only apply on 1D or 3D dataset'."
        if dataset.ndim == 3:
            dataset = dataset[np.newaxis, :, :, :]

        grad = np.ndarray((3, dataset.shape[0],
                              dataset.shape[1],
                              dataset.shape[2],
                              dataset.shape[3]))#, dtype=complex)

        grad[:, :, :, :, :] = [
                self.ff.dop.deriv(dataset, order, 0, 0, state=state)[:, :, :, :],
                self.ff.dop.deriv(dataset, 0, order, 0, state=state)[:, :, :, :],
                self.ff.dop.deriv(dataset, 0, 0, order, state=state)[:, :, :, :]]
        return grad

    def grad_u(self, order=1):
        """Gradient of the dataset 'u' of the flowfield self.ff.

        Considering the flowfield V whose shape of dataset u
        is (3, Nx, Ny, Nz).
        grad(V.u) = ( ( dV.ux/dx, dV.uy/dx, dV.uz/dx ) (x,y,z),
                      ( dV.ux/dy, dV.uy/dy, dV.uz/dy ) (x,y,z),
                      ( dV.ux/dz, dV.uy/dz, dV.uz/dz ) (x,y,z) )

        Keyword arguments:
        order -- interger, order of the gradient.

        Return:
        grad -- (Physical) Array (3, n, Nx, Ny, Nz).
        """
        assert(self.ff.u.shape[0] == 3), \
            "Grad method grad_u is defined to only apply on 3D dataset 'u'."

        grad_u = np.ndarray((3, self.ff.u.shape[0],
                             self.ff.u.shape[1],
                             self.ff.u.shape[2],
                             self.ff.u.shape[3]))#, dtype=complex)

        grad_u[:, :, :, :, :] = [self.ff.dop.deriv_u(order, 0, 0)[:, :, :, :],
                                 self.ff.dop.deriv_u(0, order, 0)[:, :, :, :],
                                 self.ff.dop.deriv_u(0, 0, order)[:, :, :, :]]
        return grad_u

    def grad_p(self, order=1):
        """Gradient of the dataset 'p' of the flowfield self.ff.

        Considering the flowfield V whose shape of dataset p is (Nx, Ny, Nz).
        grad(V.p) = ( ( dV.p/dx ) (x,y,z),
                      ( dV.p/dy ) (x,y,z),
                      ( dV.p/dz ) (x,y,z) )

        Keyword arguments:
        order -- interger, order of the gradient.

        Return:
        grad -- (Physical) Array (3, 1, Nx, Ny, Nz).
        """
        assert(self.ff.p.ndim == 3), \
            "Grad method grad_p is defined to only apply on 1D dataset 'p'."

        grad_p = np.ndarray((3, 1, self.ff.u.shape[1],
                             self.ff.u.shape[2],
                             self.ff.u.shape[3]))#, dtype=complex)

        grad_p[:, 0, :, :, :] = [self.ff.dop.deriv_p(order, 0, 0)[:, :, :],
                                 self.ff.dop.deriv_p(0, order, 0)[:, :, :],
                                 self.ff.dop.deriv_p(0, 0, order)[:, :, :]]
        return grad_p

    def curl(self, dataset, state=None):
        """Curl of the dataset of the flowfield self.ff.

        Considering the flowfield V whose shape of dataset D
        is (n, Nx, Ny, Nz).
        WARNING: Curl only apply on 3D dataset: n == 3

        curl(D) = ( dDz/dy - dDy/dz,
                    dDx/dz - dDz/dx,
                    dDy/dx - dDx/dy ) (x,y,z)

        Keyword arguments:
        dataset -- dataset to apply gradient.
        state -- if the state of the dataset is given
            state= {'x': state_x, 'y': state_y, 'z': state_z}
            with state vairalbe as 'Physical' or 'Fourier'.

        Return:
        curl -- (Physical) Array (n==3, Nx, Ny, Nz).
        """
        assert(dataset.shape[0] == 3), 'Curl only apply on 3D dataset.'

        curl = np.ndarray((3, dataset.shape[1],
                           dataset.shape[2],
                           dataset.shape[3]))#, dtype=complex)

        curl[:, :, :, :] = \
            [self.ff.dop.deriv(dataset, 0, 1, 0, state=state)[2, :, :, :] -
                self.ff.dop.deriv(dataset, 0, 0, 1, state=state)[1, :, :, :],
             self.ff.dop.deriv(dataset, 0, 0, 1, state=state)[0, :, :, :] -
                self.ff.dop.deriv(dataset, 1, 0, 0, state=state)[2, :, :, :],
             self.ff.dop.deriv(dataset, 1, 0, 0, state=state)[1, :, :, :] -
                self.ff.dop.deriv(dataset, 0, 1, 0, state=state)[0, :, :, :]]
        return curl

    def curl_u(self):
        """Curl of the dataset 'u' of the flowfield self.ff.

        Considering the flowfield V whose shape of dataset u
        is (n, Nx, Ny, Nz).
        WARNING: Curl only apply on 3D dataset: n == 3

        curl_u(V.u) = ( dV.uz/dy - dV.uy/dz,
                      dV.ux/dz - dV.uz/dx,
                      dV.uy/dx - dV.ux/dy ) (x,y,z)

        Return:
        curl -- (Physical) Array (n==3, Nx, Ny, Nz).
        """
        assert(self.ff.u.shape[0] == 3), 'Curl only apply on 3D dataset.'

        curl = np.ndarray((3, self.ff.u.shape[1],
                           self.ff.u.shape[2],
                           self.ff.u.shape[3]))#, dtype=complex)

        curl[:, :, :, :] = [self.ff.dop.deriv_u(0, 1, 0)[2, :, :, :] -
                                self.ff.dop.deriv_u(0, 0, 1)[1, :, :, :],
                            self.ff.dop.deriv_u(0, 0, 1)[0, :, :, :] -
                                self.ff.dop.deriv_u(1, 0, 0)[2, :, :, :],
                            self.ff.dop.deriv_u(1, 0, 0)[1, :, :, :] -
                                self.ff.dop.deriv_u(0, 1, 0)[0, :, :, :]]
        return curl

    def laplacian(self, dataset, state=None):
        """Laplacian of the dataset of the flowfield self.ff.

        Considering the flowfield V whose shape of dataset D
        is (n, Nx, Ny, Nz).

        laplacian_u(D) = ( d2_Dx/dx**2 + d2_Dx/dy**2 + d2_Dx/dz**2 (x,y,z),
                           d2_Dy/dx**2 + d2_Dy/dy**2 + d2_Dy/dz**2 (x,y,z),
                           d2_Dz/dx**2 + d2_Dz/dy**2 + d2_Dz/dz**2 (x,y,z) )

        Keyword Argument:
        state -- if the state of the dataset is given
            state= {'x': state_x, 'y': state_y, 'z': state_z}
            with state vairalbe as 'Physical' or 'Fourier'.

        Return:
        laplacian -- (Physical) Array (n, Nx, Ny, Nz).
        """
        laplacian = np.ndarray((dataset.shape[0],
                                dataset.shape[1],
                                dataset.shape[2],
                                dataset.shape[3]))#, dtype=complex)

        for i in range(dataset.shape[0]):
            laplacian[i, :, :, :] = \
                self.ff.dop.deriv(dataset, 2, 0, 0, state=state)[i, :, :, :] \
                + self.ff.dop.deriv(dataset, 0, 2, 0, state=state)[i, :, :, :] \
                + self.ff.dop.deriv(dataset, 0, 0, 2, state=state)[i, :, :, :]
        return laplacian

    def laplacian_u(self):
        """Laplacian of the dataset 'u' of the flowfield self.ff.

        Considering the flowfield V whose shape of dataset u
        is (3, Nx, Ny, Nz).

        laplacian_u(V.u) =
                ( d2_V.ux/dx**2 + d2_V.ux/dy**2 + d2_V.ux/dz**2 (x,y,z),
                  d2_V.uy/dx**2 + d2_V.uy/dy**2 + d2_V.uy/dz**2 (x,y,z),
                  d2_V.uz/dx**2 + d2_V.uz/dy**2 + d2_V.uz/dz**2 (x,y,z) )

        Return:
        laplacian -- (Physical) Array (3, Nx, Ny, Nz).
        """
        laplacian = np.ndarray((self.ff.u.shape[0],
                                self.ff.u.shape[1],
                                self.ff.u.shape[2],
                                self.ff.u.shape[3]))#, dtype=complex)

        for i in range(self.ff.u.shape[0]):
            laplacian[i, :, :, :] = self.ff.dop.deriv_u(2, 0, 0)[i, :, :, :] \
                + self.ff.dop.deriv_u(0, 2, 0)[i, :, :, :] \
                + self.ff.dop.deriv_u(0, 0, 2)[i, :, :, :]
        return laplacian

    def antilaplacian(self, dataset, state=None):
        """ ''Anti''-Laplacian of the dataset of the flowfield self.ff.

        Considering the flowfield V whose shape of dataset D
        is (n, Nx, Ny, Nz).

        antilaplacian_u(D) = ( d2_Dx/dx**2 - d2_Dx/dy**2 + d2_Dx/dz**2 (x,y,z),
                           d2_Dy/dx**2 - d2_Dy/dy**2 + d2_Dy/dz**2 (x,y,z),
                           d2_Dz/dx**2 - d2_Dz/dy**2 + d2_Dz/dz**2 (x,y,z) )

        Keyword Argument:
        state -- if the state of the dataset is given
            state= {'x': state_x, 'y': state_y, 'z': state_z}
            with state vairalbe as 'Physical' or 'Fourier'.

        Return:
        antilaplacian -- (Physical) Array (n, Nx, Ny, Nz).
        """
        antilaplacian = np.ndarray((dataset.shape[0],
                                dataset.shape[1],
                                dataset.shape[2],
                                dataset.shape[3]))#, dtype=complex)

        for i in range(dataset.shape[0]):
            antilaplacian[i, :, :, :] = \
                self.ff.dop.deriv(dataset, 2, 0, 0, state=state)[i, :, :, :] \
                - self.ff.dop.deriv(dataset, 0, 2, 0, state=state)[i, :, :, :] \
                + self.ff.dop.deriv(dataset, 0, 0, 2, state=state)[i, :, :, :]
        return antilaplacian

    def grad_stress_tensor(self):
        """
        Gradient of the Reynolds Stress Tensor
        of the dataset u of the flowfield self.ff.

        Considering the flowfield V whose shape of dataset u
        is (3, Nx, Ny, Nz) and stress tensor u.u:
        grad_stress_tensor(u)
                = grad(u.u)
                = ( (grad(ux.ux), grad(ux.uy), grad(ux,uz)) (x,y,z),
                    (grad(uy.ux), grad(uy.uy), grad(uy.uz)) (x,y,z),
                    (grad(uz.ux), grad(uz.uy), grad(uz.uz)) (x,y,z) )
        ...which dimension is (3, 3, 3, Nx, Ny, Nz).

        Explanation about dimension:
            - 1st parameter (==3) : grad( u[?].u[:] )[:],
                    1st compenent of stress tensor
            - 2nd parameter (==3) : grad( u[:].u[?] )[:],
                    2nd component of stress tensor
            - 3th parameter (==3) : grad( u[:].u[:] )[?],
                    direction of gradient
            - 4th parameter (==Nx) : x coordinate
            - 5th parameter (==Ny) : y coordinate
            - 6th parameter (==Nz) : z coordinate

        Return:
        grad_stress_tensor -- Array(3, 3, 3, Nx, Ny, Nz)
        """
        self.ff.generate_stress_tensor()

        grad_stress_tensor = np.ndarray((3, 3, 3, self.ff.u.shape[1],
                                         self.ff.u.shape[2],
                                         self.ff.u.shape[3]))#, dtype=complex)

        grad_stress_tensor[
            :, :, :, :, :, :] = [
            [
                np.gradient(
                    self.ff.uu[
                        0, 0, :, :, :])[:], np.gradient(
                    self.ff.uu[
                        0, 1, :, :, :]), np.gradient(
                    self.ff.uu[
                        0, 2, :, :, :])[:]], [
                np.gradient(
                    self.ff.uu[
                        1, 0, :, :, :])[:], np.gradient(
                    self.ff.uu[
                        1, 1, :, :, :]), np.gradient(
                    self.ff.uu[
                        1, 2, :, :, :])[:]], [
                np.gradient(
                    self.ff.uu[
                        2, 0, :, :, :])[:], np.gradient(
                    self.ff.uu[
                        2, 1, :, :, :]), np.gradient(
                    self.ff.uu[
                        2, 2, :, :, :])[:]]]

        return grad_stress_tensor


class BoundaryCondition_FlowField:
    """
    Class to implement boundary conditions.
    """

    def __init__(self, flowfield):
        #self.ff = flowfield
        pass


class MeanFlow_FlowField:
    """
    Define and apply different mean flow to a FlowField.
    """

    def __init__(self, flowfield):
        self.ff = flowfield

    def apply_mean_flow(self, kind='uniform', value=0):
        self.ff.make_physical()
        if kind == 'uniform':
            self.ff.state0 = {'xz': 'Physical', 'y': 'Physical'}
            self.ff.conf_y0_axis = self.ff.evaluate_conf_axis('y')

            self.ff.u0 = np.ndarray(shape=(3, self.ff.Nx,
                                              self.ff.Ny,
                                              self.ff.Nz ))
            self.ff.u0.fill(value)
        else:
            print("Kind of mean flow noti recognized.")
            pass

    def add_mean_flow(self, kind='uniform', value=0):
        """
        for PHYSICAL baseflow
        """
        #self.ff.make_spectral(index='xyz', baseflow='yes')

        if kind == 'uniform':
            self.ff.state0 = {'xz': 'Physical', 'y': 'Physical'}
            self.ff.conf_y0_axis = self.ff.evaluate_conf_axis('y')

            supplement = np.ndarray(shape=(3, self.ff.Nx,
                                              self.ff.Ny,
                                              self.ff.Nz ))
            supplement.fill(value)
            self.ff.u0 += supplement
        else:
            print("Kind of mean flow not recognized.")
            pass
