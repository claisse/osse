#!/usr/bin/env python3
"""
    svd-based dmd for channelflow hd5 fields with symmetry operations applied.
    A Sharma 2015, 2016
"""
from channel import Channel
from plotfield import plot_slice
import numpy as np
from scipy.sparse.linalg import svds
import sys
import os
from configparser import RawConfigParser
# Raw because using % in variables


def mkdir_p(path):
    import errno
    import os
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def read_config(config_file):
    config = RawConfigParser(allow_no_value=False, empty_lines_in_values=False)
    config.read(config_file)
    return config


def write_config_section(config, section, config_file):
    """
    generates and writes a new config file with only
    the relevant section, in the appropriate directory
    """
    short_config = RawConfigParser()
    short_config.read_dict(config)
    for sec in config.sections():
        if sec != section:
            short_config.remove_section(sec)
    with open(config_file, 'w') as cf:
        short_config.write(cf)
    return short_config


def write_config(config, config_file):
    with open(config_file, 'w') as cf:
        config.write(cf)


def apply_symmetries(ff, symmetries):
    """
        Apply symmetries to a flowfield object.
        Apply reflections first.
    """
    if symmetries['reflect_y']:
        ff.reflect('y')
    if symmetries['reflect_z']:
        ff.reflect('z')
    if symmetries['Dx']:
        ff.shift(symmetries['Dx'], 'x')
    if symmetries['Dz']:
        ff.shift(symmetries['Dz'], 'z')
    return ff


def eigsort(l, V):
    """ sorts eigenvalues and eigenvectors by decreasing magnitude """
    lmag = np.abs(l)
    idx = lmag.argsort()[::-1]
    l = l[idx]
    V = V[:, idx]
    return l, V


def dmd(symmetries, dataset, dmd_params):
    """
        function to do the SVD-based DMD for a single set of parameters.
    """

    for option, val in sorted(dataset.items()):
        print("{} = {}".format(option, val))
    for option, val in sorted(dmd_params.items()):
        print("{} = {}".format(option, val))
    for option, val in sorted(symmetries.items()):
        print("{} = {}".format(option, val))

    t = dataset['start_time']
    T = dataset['end_time']
    dt = dataset['sampling_period']
    datadir = dataset['data_directory']
    savedir = dataset['save_directory']
    strformat = dataset['strformat']
    max_snapshots = dmd_params['max_snapshots']
    M = dmd_params['POD_basis_size']
    tol = dmd_params['svd_convergence_tolerance']
    Dt = symmetries['Dt']
    kx = symmetries['kx']
    kz = symmetries['kz']

    N = min(int((T - t - Dt) / dt), max_snapshots)

    filename_template = datadir + '/' + strformat % 0.0
    mode = Channel(filename_template)

    """ allocate big snapshot matrices """
    dummy = Channel(filename_template).extract_fourier_line((kx, kz))
    n = np.shape(dummy)[0]
    L = np.zeros((n, N), dtype=complex)
    R = np.zeros((n, N), dtype=complex)

    """ loop over snapshots """
    for i in range(0, N):
        time = t + dt * i
        sys.stdout.write("\rt = {0:09.2f}, i={1}".format(time, i))
        sys.stdout.flush()

        filename_l = datadir + '/' + strformat % (t + Dt)
        l = Channel(filename_l)
        apply_symmetries(l, symmetries)
        l.scale()
        l = l.extract_fourier_line((kx, kz))
        L[:, i] = l

        filename_r = datadir + '/' + strformat % t
        r = Channel(filename_r)
        r.scale()
        r = r.extract_fourier_line((kx, kz))
        R[:, i] = r

    """ do the decomposition """
    sys.stdout.write(" doing svds... ")
    sys.stdout.flush()
    """ see http://dx.doi.org/10.1063/1.4863670 """
    M = min(min(R.shape)-2, M)
    U, S, Vh = svds(R, M, tol=tol)
    Si = np.diag(1.0 / S)
    F = np.dot(
        np.dot(U.conj().transpose(), np.dot(L, Vh.conj().transpose())), Si)

    sys.stdout.write("doing eig... ")
    sys.stdout.flush()
    E, V = np.linalg.eig(F)
    E, V = eigsort(E, V)

    """ output DMD eigenvalues """
    sys.stdout.write("saving...\n")
    sys.stdout.flush()
    np.savetxt(savedir + '/DMD-eigs.txt',
               E,                      fmt='%+6.5f')
    np.savetxt(savedir + '/DMD-eigs-continuous.txt',
               np.log(E) / Dt,           fmt='%+6.5f')
    np.savetxt(savedir + '/DMD-eigs-phase.txt',
               np.abs(np.angle(E)),    fmt='%.5f')
    np.savetxt(savedir + '/DMD-eigs-mag.txt',
               np.abs(E),              fmt='%.5f')

    """ output DMD mode shapes, plots """
    for n in range(0, min(V.shape[0] / 2, dmd_params['save_modes'])):
        mode.u *= 0
        mode.insert_fourier_line(np.dot(U, V[:, n]), (kx, kz))
        mode.scaled = True
        mode.unscale()
        plot_slice(mode, cut=0.0, field='u', normal='x', lines='v',
                   filename=savedir + '/slice-x-{0:02d}.png'.format(n))
        plot_slice(mode, cut=0.9, field='u', normal='y', lines='v',
                   filename=savedir + '/slice-y-{0:02d}.png'.format(n))
        plot_slice(mode, cut=0.0, field='u', normal='z', lines='v',
                   filename=savedir + '/slice-z-{0:02d}.png'.format(n))
        mode.save(savedir + '/mode-{0:02d}.h5'.format(n))


def main(argv):
    """
        reads parameters from config file supplied as argument on command line.
        One run per section in the config file.
        Then, runs DMD having applied specified symmetries.
    """

    os.system('clear')

    if len(sys.argv) == 1:
        print(sys.argv[0] + ' requires a config file as an argument')
    for config_file in sys.argv[1:]:

        """ default user-specified variables """
        symmetries = {'Dx': 0, 'Dz': 0, 'Dt': 0,
                      'reflect_y': False, 'reflect_z': False}
        dataset = {}
        dmd_params = {'max_snapshots': 1000, 'POD_basis_size': 200}

        config = read_config(config_file)
        """ loop over sections in config file """
        for section in config.sections():
            case = config[section]

            """ read config options """
            dataset['data_directory'] = case.get('data_directory')
            dataset['start_time'] = case.getfloat('start_time')
            dataset['end_time'] = case.getfloat('end_time')
            dataset['sampling_period'] = case.getfloat('sampling_period')
            dataset['strformat'] = case.get('strformat',
                                            u'u%08.3f.h5').replace('\'', '')
            dmd_params['max_snapshots'] = case.getint('max_snapshots')
            dmd_params['POD_basis_size'] = case.getint('POD_basis_size')
            dmd_params['save_modes'] = case.getint('save_modes', 30)
            dmd_params['svd_convergence_tolerance'] = case.getfloat(
                'svd_convergence_tolerance', 0)
            symmetries['kx'] = case.getint('kx')
            symmetries['kz'] = case.getint('kz')
            symmetries['Dx'] = case.getfloat('x_shift', 0)
            symmetries['Dz'] = case.getfloat('z_shift', 0)
            symmetries['Dt'] = case.getfloat('t_shift', 0)
            symmetries['reflect_y'] = case.getboolean('reflect_y')
            symmetries['reflect_z'] = case.getboolean('reflect_z')

            """
            create a results directory for each section in the file
            and write the .cfg file
            """
            dataset['save_directory'] = section
            mkdir_p(dataset['save_directory'])
            write_config_section(
                config, section, dataset['save_directory'] + '/params.cfg')

            """ show the user the case that is running """
            print('\n')
            print(case)

            dmd(symmetries, dataset, dmd_params)
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
