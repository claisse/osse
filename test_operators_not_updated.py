"""
DEPRECATED

test_operators.py
"""
import numpy as np
from math import *
import unittest
import warnings
import h5py
import itertools
import matplotlib.pyplot as plt
#matplotlib.rcParams['text.usetex'] = True
#matplotlib.rcParams['text.latex.unicode'] = True
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
import matplotlib.pyplot as plt

from scipy.sparse.linalg import eigs
from scipy.sparse.linalg import LinearOperator
from scipy.sparse.linalg import svds

import scipy
import scipy.integrate
import scipy.sparse.linalg
from numpy import linalg

from flowfield import FlowField
from flowfield import DiffOperator_FlowField
from flowfield import BoundaryCondition_FlowField
from flowfield import MeanFlow_FlowField
from channel import Channel
from channel import DiffOperator_Channel
from channel import BoundaryCondition_Channel
from channel import MeanFlow_Channel
from boundary_layer import Boundary_Layer
from boundary_layer import DiffOperator_Boundary_Layer
from boundary_layer import BoundaryCondition_Boundary_Layer
from boundary_layer import MeanFlow_Boundary_Layer
from operators import Operators as OP
import chebyshev


class OperatorsTest(unittest.TestCase):
    """ Test of methods implemented in Operators."""

    def init_flow(self, kind_of_flow):
        warnings.filterwarnings('ignore',
                                category=np.VisibleDeprecationWarning)

        filename = 'database/channel_sample.h5'

        # Creation of a new h5-file similar to a ChannelFlow flowfield
        file = h5py.File(filename, 'w')
        data = file.create_group('data')
        geom = file.create_group('geom')

        # DOMAIN SIZE
        self.a = -1
        self.b = 1
        self.Ly = self.b - self.a
        file.attrs['Lx'] = 2*pi
        file.attrs['Ly'] = self.Ly
        file.attrs['Lz'] = 2*pi
        file.attrs['a'] = self.a
        file.attrs['b'] = self.b

        # BE CAREFUL, VALUES BELOW 10 DOES NOT WORK
        self.Nx = 5
        self.Ny = 5
        self.Nz = 7
        file.attrs['Nx'] = self.Nx
        file.attrs['Ny'] = self.Ny
        file.attrs['Nz'] = self.Nz

        # Creation of a basis (x, y, z) of dim (Nx, Ny, Nz)
        array_x = np.arange(0, file.attrs['Lx'], file.attrs['Lx']/self.Nx)

        if kind_of_flow == 'poiseuille' \
                or kind_of_flow == 'channel-inv-sol'\
                or kind_of_flow == 'couette':
            array_y = np.linspace(self.a, self.b, num=self.Ny, endpoint=True)
        elif kind_of_flow == 'bl':
            stretching_factor = 5
            array_y_temp = chebyshev.chebnodes(2 * self.Ny)
            array_y = - stretching_factor * np.log(array_y_temp[:self.Ny])

        array_z = np.arange(0, file.attrs['Lz'], file.attrs['Lz']/self.Nz)

        # Creation of a dataset u(x,y,z)
        coef_cos_Z = 3
        coef_sin_Z = 2
        offset = np.power(np.tan(array_y[0]*(np.pi/2-0.5)), 2)
        array_u = np.ndarray(shape=(3, self.Nx, self.Ny, self.Nz))
        for i, nx, ny, nz in itertools.product(
                                range(array_u.shape[0]),
                                range(array_u.shape[1]),
                                range(array_u.shape[2]),
                                range(array_u.shape[3])):
            array_u[i, nx, ny, nz] = \
                - 0 * (np.power(np.tan(array_y[ny]*(np.pi/2-0.5)), 2)
                       - offset) \
                + 0 * np.power(2*(ny-(self.Ny-1)/2)/(self.Ny-1), 4) \
                + 0 * np.sin(2*np.pi * 4*array_x[nx]/file.attrs['Lx']) \
                + 1 * np.cos(
                        2*np.pi * 2*array_y[ny]/(self.b-self.a) + np.pi/2)\
                + 0 * np.cos(
                        2*np.pi * coef_cos_Z*array_z[nz]/file.attrs['Lz'])\
                    * np.sin(
                        2*np.pi * coef_sin_Z*array_z[nz]/file.attrs['Lz'])

        x = geom.create_dataset('x', data=array_x)
        y = geom.create_dataset('y', data=array_y)
        z = geom.create_dataset('z', data=array_z)
        u = data.create_dataset('u', data=array_u)

        file.close()

        # Load the saved h5-file to a new flowfield
        if kind_of_flow == 'couette':
            self.ff = Channel(filename)
            self.ff.uniform2chebyshev()
            self.ff.meanFlow.apply_mean_flow('couette')

        if kind_of_flow == 'poiseuille':
            self.ff = Channel(filename)
            self.ff.uniform2chebyshev()
            self.ff.meanFlow.apply_mean_flow('poiseuille')

        elif kind_of_flow == 'channel-inv-sol':
            self.ff = Channel(filename)
            self.ff.uniform2chebyshev()
            self.ff.u0 = np.ndarray(shape=(self.ff.u.shape[0],
                                           self.ff.u.shape[1],
                                           self.ff.u.shape[2],
                                           self.ff.u.shape[3] ))
            for ny in range(self.ff.u.shape[2]):
                self.ff.u0[0, :, ny, :] = self.ff.y[ny]#1 - self.ff.y[ny]**2
                self.ff.u0[1, :, ny, :] = 0#1 - self.ff.y[ny]**2
                self.ff.u0[2, :, ny, :] = 0#1 - self.ff.y[ny]**2

        elif kind_of_flow == 'bl':
            self.ff = Boundary_Layer(filename)
            self.ff.Y = stretching_factor
            self.ff.conf_y_axis = 'Boundary_Layer'
            # self.ff.chebyshev2boundarylayer()
            self.ff.meanFlow.apply_mean_flow('blasius')

    def test_oss(self):
        kind_of_flow = 'poiseuille'
        self.init_flow(kind_of_flow)

        # self.ff = Channel('database/eq1.h5')
        kx = 1
        kz = 0
        Re = 10000
        self.ff.make_spectral('xz')
        N = self.ff.u.shape[2]

        oss_op = OP(self.ff)
        # oss_op_lo = OP_lo(self.ff)

        # Los, Lap, OS, Lsq, Lc = oss_op.oss_operator(self.ff,
        #                                            Re,
        #                                            self.ff.u0,
        #                                            kx,
        #                                            kz,
        #                                            omega=-1)

        # Los_lo, Lap_lo, OS_lo, Lsq_lo, Lc_lo = oss_op_lo.oss_operator(
        #                                            self.ff,
        #                                            Re,
        #                                            self.ff.u0,
        #                                            kx,
        #                                            kz,
        #                                            omega=-1)
        # abscissa = np.linspace( 0, N-2-1, N-2)
        # vector = 10 * (np.ones(N-2) - self.ff.y[1:N-1]**2)
        # plt.figure()
        # plt.plot(abscissa, np.dot(Lsq, vector), 'bx-')
        # plt.plot(abscissa, Lsq_lo.matvec(vector), 'go')
        # plt.show()

        vals, vecs, vals2, vecs2 = oss_op.oss_eigen(self.ff,
                                                    Re,
                                                    self.ff.u0,
                                                    kx,
                                                    kz,
                                                    omega=-1)

        print('\nEigenvalues (scipy.sparse.linalg) : \n',
              np.round(np.sort(vals), 6))

        plt.figure()
        plt.plot(vals.real, np.absolute(vals.imag),
                 'rx', ms='5', mew='1',
                 label='Eigenvalues Orr-Sommerfeld')
        plt.plot(vals2.real, np.absolute(vals2.imag), 'bx', ms='5', mew='1',
                 label='Eigenvalues Squire')

        plt.legend(fontsize='small')
        plt.title('Eigenvalues of the Orr-Sommerfeld-Squire equation.')
        plt.grid(b=True, which='Major')
        plt.xlim(-0.5, 0.1)
        plt.ylim(0, 1)
        plt.xlabel('real')
        plt.ylabel('imag')
        plt.show()

    def test_oss_resolvent(self):
        self.init_flow('poiseuille')
        # self.ff = Channel('database/eq1.h5')
        self.ff.make_spectral('xz')
        N = self.ff.u.shape[2]

        # example taken or eigenvector 30 and Ny = 20
        case = 1
        if case == 1:
            kx = 1
            kz = 0
            Re = 10000
        elif case == 2:
            kx = 0
            kz = 2.044
            Re = 5000
        omega = 1
        rank = 3*(N-2)-2

        op = OP(self.ff)
        op_lo = OP_lo(self.ff)

        H, U, S, V = op.oss_resolvent(self.ff,
                                      Re,
                                      self.ff.u0,
                                      kx,
                                      kz,
                                      omega)

        resolvent_lo, U_lo, S_lo, VT_lo = op_lo.oss_resolvent(self.ff,
                                               Re,
                                               self.ff.u0,
                                               kx,
                                               kz,
                                               omega,
                                               rank)

        np.set_printoptions(suppress=True, precision=6)
        # print('U: \n', U)
        # print('VT: \n', VT)
        print('S :\n', S)
        # print('resolvent : \n', resolvent)

        ###############################
        # Definition of vector = [u,v,w] with an arbitrary vector
        ###############################
        if False:
            print('Arbitrary vector used as forcing')
            X = 1
            vector1 = 10 * (np.ones(N-2*X) + self.ff.y[X:N-X] - self.ff.y[X:N-X]**3)
            # vector2 = 2* (np.ones(N-2*X) - self.ff.y[X:N-X]**2)
            # vector2 = 5 * np.cos(2 * pi * 2 * np.linspace(0, N-2-1, N-2) / (N-2))
            vector = np.concatenate((vector1, vector1, vector1))

        ###############################
        # Definition of vector = [u,v,w] with a eigenvector of the resolvent matrix
        ###############################
        if True:
            print('Resolvent eigenvector used as forcing')
            print(H.shape)
            w, eigenvector = np.linalg.eig(H)
            vector = eigenvector[:,0]
            print(vector)

        #L1 = resolvent_lo.matvec(vector)  # [1+0*N:1*N-1]
        L2 = np.dot(resolvent_lo, vector)
        L1 = np.dot(H, vector)

        abscissa = np.linspace(0, 3*(N-2)-1, 3*(N-2))
        # plt.figure()
        # # plt.plot(abscissa, np.absolute(L), 'b+-')
        #plt.plot(abscissa, L1.real, 'b+-')
        #plt.plot(abscissa, L1.imag, 'b+--')
        #plt.plot(abscissa, L2.real, 'r+-')
        #plt.plot(abscissa, L2.imag, 'r+--')
        #plt.show()

        fig, ((ax1, ax2, ax3)) = plt.subplots(1,3,sharex='col', sharey='row')

        #plt.figure()

        ax1.grid(b=True, which='Major')
        ax2.grid(b=True, which='Major')
        ax3.grid(b=True, which='Major')
        #ax4.grid(b=True, which='Major')
        ax1.set_xlabel(r'$u = \mathcal{R} f_u$', fontsize=36)
        ax2.set_xlabel(r'$v = \mathcal{R} f_v$', fontsize=36)
        ax3.set_xlabel(r'$w = \mathcal{R} f_w$', fontsize=36)
        #ax4.set_xlabel(r'$p = \mathcal{R} f_p$', fontsize=36)
        ax1.tick_params(labelsize=24)
        ax2.tick_params(labelsize=24)
        ax3.tick_params(labelsize=24)
        #ax4.tick_params(labelsize=24)
        #ax1.xlabel(r'$u = \mathcal{R} f_u$', fontsize=36)
        #plt.ylabel(r'$y$', fontsize=36)
        ax1.set_ylabel('y', fontsize=36)
        ax3.set_ylabel('y', fontsize=36)

        # plt.plot(abscissa, np.absolute(L), 'b+-')
        line1, = ax1.plot(L1[0:N-2].real, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'r-', ms='10', lw='3',
                 label='Matrix - Real')
        line2, = ax1.plot(L1[0:N-2].imag, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'r--', ms='10', lw='3',
                 label='Matrix - Imag.')
        line3, = ax1.plot(L2[0:N-2].real, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'ko-', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Real')
        line4, = ax1.plot(L2[0:N-2].imag, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'k^--', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Imag.')
        
        ax2.plot(L1[N-2:2*(N-2)].real, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'r-', ms='10', lw='3',
                 label='Matrix - Real')
        ax2.plot(L1[N-2:2*(N-2)].imag, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'r--', ms='10', lw='3',
                 label='Matrix - Imag.')
        ax2.plot(L2[N-2:2*(N-2)].real, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'ko-', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Real')
        ax2.plot(L2[N-2:2*(N-2)].imag, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'k^--', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Imag.')

        ax3.plot(L1[2*(N-2):3*(N-2)].real, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'r-', ms='10', lw='3',
                 label='Matrix - Real')
        ax3.plot(L1[2*(N-2):3*(N-2)].imag, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'r--', ms='10', lw='3',
                 label='Matrix - Imag.')
        ax3.plot(L2[2*(N-2):3*(N-2)].real, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'ko-', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Real')
        ax3.plot(L2[2*(N-2):3*(N-2)].imag, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'k^--', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Imag.')

        fig.legend((line1, line2, line3, line4),
                   ('Matrix - Real', 'Matrix - Imag.', 'Matrix-free - Real', 'Matrix-free - Imag.'),
                   loc=9, ncol=2, fontsize=24)
                   #bbox_to_anchor=(0., 1.02, 1., .102),
        plt.show()

    def test_oss_full(self):
        """
        OLD METHOD: DOES NOT CONSIDER MODE INTERACTION
        """
        self.init_flow('couette')
        # self.ff = Channel('database/eq1.h5')
        N = self.ff.u.shape[2]
        if False:
            self.ff.load_baseflow('database/eq1.h5')
        else:
            for nx in range(self.ff.u0.shape[1]):
                for nz in range(self.ff.u0.shape[3]):
                    self.ff.u0[0,nx,:,nz] = 1-self.ff.y**2
            self.ff.u0[1,:,1:N-1,:] = 0
            self.ff.u0[2,:,1:N-1,:] = 0
            self.ff.state0 = {'xz': 'Physical', 'y': 'Physical'}
            #self.ff.state0['xz'] = 'Fourier'

        self.ff.make_spectral('xz', baseflow='no')

        #for i in range(self.ff.u0.shape[0]):
        #    for nx in range(self.ff.u0.shape[1]):
        #        for nz in range(self.ff.u0.shape[3]):
        #            print('(',i,',',nx,',:,',nz,') : \n', self.ff.u0[i,nx,:,nz])
        #    print('-----')

        # example taken or eigenvector 30 and Ny = 20
        case = 1
        if case == 1:
            kx = 1
            kz = 0
            Re = 10000
        elif case == 2:
            kx = 0
            kz = 2.044
            Re = 5000
        elif case == 3:
            kx = 1
            kz = 1
            Re= 1000
        omega = -1
        #rank = 3*(N-2)-2

        op = OP(self.ff)

        Los0, Lap0, OS0, Lsq0, Lc0 = op.oss_operator(self.ff,
                                                    Re,
                                                    self.ff.u0,
                                                    kx,
                                                    kz,
                                                    omega=-1)

        Los, Lap, OS, Lsq, Lc, Lc2 = op.oss_full(self.ff,
                                                    Re,
                                                    kx,
                                                    kz,
                                                    omega=-1)

        nx = 0
        nz = 0

        np.set_printoptions(suppress=True, precision=6)
        print('Los diff :\n', (Los[nx,nz,:,:] - Los0))
        #print('Los[nx,nz,:,:] : \n', Los)
        #print('Los0 : \n', Los0)
        print('Lsq diff :\n', (Lsq[nx,nz,:,:] - Lsq0))
        #print('Lsq[nx,nz,:,:] :\n', Lsq)
        #print('Lsq0 :\n', Lsq0)
        print('Lc diff :\n', (Lc[nx,nz,:,:] - Lc0))
        #print('Lc[nx,nz,:,:] :\n', Lc)
        #print('Lc0 :\n', Lc0)
        print('Lc2: \n', Lc2[nx,nz,:,:])

        ###############################
        # Definition of vector = [u,v,w] with an arbitrary vector
        ###############################
        if True:
            print('Arbitrary vector for [v, eta_y]')
            X = 0
            vector1 = 10 * (np.ones(N-2*X) + self.ff.y[X:N-X] - self.ff.y[X:N-X]**3)
            # vector2 = 2* (np.ones(N-2*X) - self.ff.y[X:N-X]**2)
            # vector2 = 5 * np.cos(2 * pi * 2 * np.linspace(0, N-2-1, N-2) / (N-2))
            vector = np.concatenate((vector1, vector1))
            abscissa = np.linspace(0, 2*(N-2*X)-1, 2*(N-2*X))

        ###############################
        # Definition of vector = [u,v,w] with a eigenvector of the resolvent matrix
        ###############################
        if False:
            print('Resolvent eigenvector for [v, eta_y]')
            print(Los.shape)
            w1, eigenvector1 = np.linalg.eig(Los)
            print(Lsq.shape)
            w2, eigenvector2 = np.linalg.eig(Lsq)
            vector = np.concatenate(eigenvector1[:,0], eigenvector2[:,0])
            print(vector)
            abscissa = np.linspace(0, 2*(N-2)-1, 2*(N-2))

        L1 = np.dot(0, vector)

        # plt.figure()
        # # plt.plot(abscissa, np.absolute(L), 'b+-')
        #plt.plot(abscissa, L1.real, 'b+-')
        #plt.plot(abscissa, L1.imag, 'b+--')
        #plt.plot(abscissa, L2.real, 'r+-')
        #plt.plot(abscissa, L2.imag, 'r+--')
        #plt.show()

        """
        fig, ((ax1, ax2, ax3)) = plt.subplots(1,3,sharex='col', sharey='row')

        #plt.figure()

        ax1.grid(b=True, which='Major')
        ax2.grid(b=True, which='Major')
        ax3.grid(b=True, which='Major')
        #ax4.grid(b=True, which='Major')
        ax1.set_xlabel(r'$u = \mathcal{R} f_u$', fontsize=36)
        ax2.set_xlabel(r'$v = \mathcal{R} f_v$', fontsize=36)
        ax3.set_xlabel(r'$w = \mathcal{R} f_w$', fontsize=36)
        #ax4.set_xlabel(r'$p = \mathcal{R} f_p$', fontsize=36)
        ax1.tick_params(labelsize=24)
        ax2.tick_params(labelsize=24)
        ax3.tick_params(labelsize=24)
        #ax4.tick_params(labelsize=24)
        #ax1.xlabel(r'$u = \mathcal{R} f_u$', fontsize=36)
        #plt.ylabel(r'$y$', fontsize=36)
        ax1.set_ylabel('y', fontsize=36)
        ax3.set_ylabel('y', fontsize=36)

        # plt.plot(abscissa, np.absolute(L), 'b+-')
        line1, = ax1.plot(L1[0:N-2].real, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'r-', ms='10', lw='3',
                 label='Matrix - Real')
        line2, = ax1.plot(L1[0:N-2].imag, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'r--', ms='10', lw='3',
                 label='Matrix - Imag.')
        line3, = ax1.plot(L2[0:N-2].real, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'ko-', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Real')
        line4, = ax1.plot(L2[0:N-2].imag, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'k^--', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Imag.')

        ax2.plot(L1[N-2:2*(N-2)].real, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'r-', ms='10', lw='3',
                 label='Matrix - Real')
        ax2.plot(L1[N-2:2*(N-2)].imag, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'r--', ms='10', lw='3',
                 label='Matrix - Imag.')
        ax2.plot(L2[N-2:2*(N-2)].real, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'ko-', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Real')
        ax2.plot(L2[N-2:2*(N-2)].imag, abscissa[0:N-2]/(N/2)-(1-1/N),
                 'k^--', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Imag.')

        fig.legend((line1, line2, line3, line4),
                   ('Matrix - Real', 'Matrix - Imag.', 'Matrix-free - Real', 'Matrix-free - Imag.'),
                   loc=9, ncol=2, fontsize=24)
                   #bbox_to_anchor=(0., 1.02, 1., .102),
        plt.show()
        """

    def test_oss_full_sympy(self):
        """
        OLD METHOD: DOES NOT CONSIDER MODE INTERACTION
        """
        self.init_flow('channel-inv-sol')
        # self.ff = Channel('database/eq1.h5')
        N = self.ff.u.shape[2]
        if False:
            self.ff.load_baseflow('database/eq1.h5')
        else:
            for nx in range(self.ff.u0.shape[1]):
                for nz in range(self.ff.u0.shape[3]):
                    self.ff.u0[0,nx,:,nz] = 1-self.ff.y**2
                    self.ff.u0[1,:,1:N-1,:] = 0
                    self.ff.u0[2,:,1:N-1,:] = 0
            self.ff.state0 = {'xz': 'Physical', 'y': 'Physical'}
            #self.ff.state0['xz'] = 'Fourier'

        self.ff.make_spectral('xz', baseflow='no')
        kx = 0
        kz = 1
        Re = 1000
        nx = 0
        nz = 0

        #rank = 3*(N-2)-2

        # CALLING ANALYTICAL EXPRESSION
        op = OP(self.ff)

        Los0, Lap0, OS0, Lsq0, Lc0 = op.oss_operator(self.ff,
                                                    Re,
                                                    self.ff.u0,
                                                    kx,
                                                    kz,
                                                    omega=-1)

        Los, Lap, OS, Lsq, Lc, Lc2 = op.oss_full(self.ff,
                                                    Re,
                                                    kx,
                                                    kz,
                                                    omega=-1)


        ###############################
        # Definition of vector = [u,v,w] with an arbitrary vector
        ###############################
        if True:
            print('Arbitrary vector for [v, eta_y]')
            X = 1
            vector1 = 10 * (np.ones(N-2*X) + self.ff.y[X:N-X] - self.ff.y[X:N-X]**3)
            # vector2 = 2* (np.ones(N-2*X) - self.ff.y[X:N-X]**2)
            # vector2 = 5 * np.cos(2 * pi * 2 * np.linspace(0, N-2-1, N-2) / (N-2))
            vector = vector1 #np.concatenate((vector1, vector1))
            abscissa = np.linspace(0, 2*(N-2*X)-1, 2*(N-2*X))

        ###############################
        # Definition of vector = [u,v,w] with a eigenvector of the resolvent matrix
        ###############################
        if False:
            print('Resolvent eigenvector for [v, eta_y]')
            print(Los.shape)
            w1, eigenvector1 = np.linalg.eig(Los)
            print(Lsq.shape)
            w2, eigenvector2 = np.linalg.eig(Lsq)
            vector = np.concatenate(eigenvector1[:,0], eigenvector2[:,0])
            print(vector)
            abscissa = np.linspace(0, 2*(N-2)-1, 2*(N-2))

        L1 = np.dot(Los0, vector)
        #L1 = np.dot(Los[nx,nz,:,:], vector)

        # CALLING SYMPY EXPRESSION
        # dont forget to set up the baseflow function in function oss_sympy()
        #operators_sympy.oss_sympy(self.ff,
        #            Re,
        #            kx,
        #            kz,
        #            vector)

    def test_lnse(self):
        kind_of_flow = 'poiseuille'
        self.init_flow(kind_of_flow)

        # self.ff = Channel('database/eq1.h5')
        kx = 5
        kz = 5
        Re = 1000
        self.ff.make_spectral('xz')
        N = self.ff.u.shape[2]

        oss_op = OP(self.ff)
        oss_op_lo = OP_lo(self.ff)

        A = oss_op.lnse_operator(self.ff, Re, self.ff.u0, kx, kz)
        lnse = oss_op_lo.lnse_operator(self.ff, Re, self.ff.u0, kx, kz)

        vector1 = 10 * (np.ones(N) - self.ff.y[:]**2)
        # vector2 = 2* (np.ones(N-2*X) - self.ff.y[X:N-X]**2)
        # vector2 = 5 * np.cos(2 * pi * 2 * np.linspace(0, N-2-1, N-2) / (N-2))
        vector = np.concatenate((vector1, vector1, vector1, vector1))

        # L = resolvent_lo.matvec(vector)  # [1+0*N:1*N-1]
        L = np.dot(A, vector)
        L2 = lnse.matvec(vector)

        abscissa = np.linspace(0, 4*(N)-1, 4*(N))
        plt.figure()
        # plt.plot(abscissa, np.absolute(L), 'b+-')
        plt.plot(abscissa, L.real, 'b+-')
        plt.plot(abscissa, L.imag, 'b+--')
        plt.plot(abscissa, L2.real, 'gx-')
        plt.plot(abscissa, L2.imag, 'gx--')
        plt.show()

    def test_lnse_resolvent(self):
        kind_of_flow = 'poiseuille'
        self.init_flow(kind_of_flow)

        # self.ff = Channel('database/eq1.h5')
        kx = 0
        kz = 2.044
        Re = 5000
        omega = 1
        self.ff.make_spectral('xz')
        N = self.ff.u.shape[2]

        oss_op = OP(self.ff)
        oss_op_lo = OP_lo(self.ff)

        A, U, S, V = oss_op.lnse_resolvent(self.ff,
                                           Re,
                                           self.ff.u0,
                                           kx, kz,
                                           omega)
        lnse, U2, S2, V2 = oss_op_lo.lnse_resolvent(self.ff,
                                                    Re,
                                                    self.ff.u0,
                                                    kx, kz,
                                                    omega)

        ###############################
        # Definition of vector = [u,v,w] with an arbitrary vector
        ###############################
        if False:
            print('Arbitrary vector used as forcing')
            vector1 = 10 * (np.ones(N) + self.ff.y[:] - self.ff.y[:]**3)
            # vector2 = 2* (np.ones(N-2*X) - self.ff.y[X:N-X]**2)
            # vector2 = 5 * np.cos(2 * pi * 2 * np.linspace(0, N-2-1, N-2) / (N-2))
            vector = np.concatenate((vector1, vector1, vector1, vector1))

        ###############################
        # Definition of vector = [u,v,w] with a eigenvector of the resolvent matrix
        ###############################
        if True:
            print('Resolvent eigenvector used as forcing')
            print(A.shape)
            X = 0
            w, eigenvector = np.linalg.eig(A)
            #vector1 = eigenvector[0*N:1*N,0+X]
            #vector2 = eigenvector[1*N:2*N,N+X]
            #vector3 = eigenvector[2*N:3*N,2*N+X]
            #vector4 = eigenvector[3*N:4*N,3*N+X]
            #vector = np.concatenate((vector1, vector2, vector3, vector4))
            print(eigenvector)
            vector=eigenvector[:,300]
            # WORK WITH N = 100 AND EIGENVECTOR 300

        # L = resolvent_lo.matvec(vector)  # [1+0*N:1*N-1]
        L = np.dot(A, vector)
        L2 = lnse.matvec(vector)

        abscissa = np.linspace(0, 4*(N)-1, 4*(N))

        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2,sharex='col', sharey='row')

        #plt.figure()

        ax1.grid(b=True, which='Major')
        ax2.grid(b=True, which='Major')
        ax3.grid(b=True, which='Major')
        ax4.grid(b=True, which='Major')
        ax1.set_xlabel(r'$u = \mathcal{R} f_u$', fontsize=36)
        ax2.set_xlabel(r'$v = \mathcal{R} f_v$', fontsize=36)
        ax3.set_xlabel(r'$w = \mathcal{R} f_w$', fontsize=36)
        ax4.set_xlabel(r'$p = \mathcal{R} f_p$', fontsize=36)
        ax1.tick_params(labelsize=24)
        ax2.tick_params(labelsize=24)
        ax3.tick_params(labelsize=24)
        ax4.tick_params(labelsize=24)
        #ax1.xlabel(r'$u = \mathcal{R} f_u$', fontsize=36)
        #plt.ylabel(r'$y$', fontsize=36)
        ax1.set_ylabel('y', fontsize=36)
        ax3.set_ylabel('y', fontsize=36)
        ax1.set_xlim([-0.4,0.2])

        # plt.plot(abscissa, np.absolute(L), 'b+-')
        line1, = ax1.plot(L[0:N].real, abscissa[0:N]/50-1,
                 'r-', ms='10', lw='3',
                 label='Matrix - Real')
        line2, = ax1.plot(L[0:N].imag, abscissa[0:N]/50-1,
                 'r--', ms='10', lw='3',
                 label='Matrix - Imag.')
        line3, = ax1.plot(L2[0:N].real, abscissa[0:N]/50-1,
                 'bo', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Real')
        line4, = ax1.plot(L2[0:N].imag, abscissa[0:N]/50-1,
                 'b^', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Imag.')

        ax2.plot(L[N:2*N].real, abscissa[0:N]/50-1,
                 'r-', ms='10', lw='3',
                 label='Matrix - Real')
        ax2.plot(L[N:2*N].imag, abscissa[0:N]/50-1,
                 'r--', ms='10', lw='3',
                 label='Matrix - Imag.')
        ax2.plot(L2[N:2*N].real, abscissa[0:N]/50-1,
                 'bo', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Real')
        ax2.plot(L2[N:2*N].imag, abscissa[0:N]/50-1,
                 'b^', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Imag.')

        ax3.plot(L[2*N:3*N].real, abscissa[0:N]/50-1,
                 'r-', ms='10', lw='3',
                 label='Matrix - Real')
        ax3.plot(L[2*N:3*N].imag, abscissa[0:N]/50-1,
                 'r--', ms='10', lw='3',
                 label='Matrix - Imag.')
        ax3.plot(L2[2*N:3*N].real, abscissa[0:N]/50-1,
                 'bo', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Real')
        ax3.plot(L2[2*N:3*N].imag, abscissa[0:N]/50-1,
                 'b^', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Imag.')

        ax4.plot(L[3*N:4*N].real, abscissa[0:N]/50-1,
                 'r-', ms='10', lw='3',
                 label='Matrix - Real')
        ax4.plot(L[3*N:4*N].imag, abscissa[0:N]/50-1,
                 'r--', ms='10', lw='3',
                 label='Matrix - Imag.')
        ax4.plot(L2[3*N:4*N].real, abscissa[0:N]/50-1,
                 'bo', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Real')
        ax4.plot(L2[3*N:4*N].imag, abscissa[0:N]/50-1,
                 'b^', ms='10', mew='2',
                 markerfacecolor='None',
                 label='Matrix-free - Imag.')

        fig.legend((line1, line2, line3, line4),
                   ('Matrix - Real', 'Matrix - Imag.', 'Matrix-free - Real', 'Matrix-free - Imag.'),
                   loc=9, ncol=2, fontsize=24)
                   #bbox_to_anchor=(0., 1.02, 1., .102),
        plt.show()

test = unittest.TestSuite()
test.addTest(OperatorsTest("test_oss"))
#test.addTest(OperatorsTest("test_oss_full"))
#test.addTest(OperatorsTest("test_oss_full_sympy"))
# test.addTest(OperatorsTest("test_oss_resolvent"))
# test.addTest(OperatorsTest("test_lnse"))
# test.addTest(OperatorsTest("test_lnse_resolvent"))
# test.addTest(OperatorsTest("test_oss_wall_actuation_v"))

runner = unittest.TextTestRunner()
runner.run(test)
