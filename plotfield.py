#!/usr/bin/env python3
"""
simple plotting routines for FlowField class
A Sharma 2015, 2016
"""
from flowfield import FlowField
import matplotlib.pyplot as plt
import numpy as np
import sys
import os
from optparse import OptionParser

def plot_slice(ff, cut=0, field='u', normal='z', lines=None, contourstep=0.1, quiver=None, decimate=3, filename=None):
    """
    plots a slice of a flowfield object
    """

    x, y, z = (ff.x, ff.y, ff.z)
    u = np.real(ff.u)
    scale = np.max(np.linalg.norm(u, axis=0))       # scale by maximum velocity in any direction
    indices = {'u':0, 'v':1, 'w':2}                 # can extend to vorticity etc

    """ plotting style options """
    contourcolors = 128
    contourlines  = np.arange(-1,1,contourstep)
    contourformat = '%1.2f'
    plt.set_cmap('RdBu_r')                          # prefer a diverging colormap
    font = {'size'    : 16}
    plt.rc('font', **font)
    plt.rc('text', usetex=True)
    plt.rc('savefig', dpi=180)
    plt.style.use(u'fivethirtyeight')
    #plt.hold(False)

    if normal is 'mean':
        u00 = ff.mean()
        plt.plot(np.add(np.add(u00, 1.0), -np.square(y)),y)
        abscissa = '$u_0$'
        ordinate = '$y$'
        quantity = '$u_0$'
        field    = 3
    else:
        """ take a slice through the data """
        if normal is 'x':
            idx = (np.abs(x-cut)).argmin()
            B, A = np.meshgrid(y, z)
            Aq = A[:, ::decimate]
            Bq = B[:, ::decimate]
            DATA = u[:,  idx, :, :].transpose(0, 2, 1)
            quiver_o = DATA[2, :, ::decimate]
            quiver_a = DATA[1, :, ::decimate]
            quiver_width = 0.0003*abs(z[0]-z[-1])
            title = "$x=%2.2f$" % cut
            abscissa = '$z$'
            ordinate = '$y$'
            orientation = 'horizontal'
        elif normal is 'y':
            idx = (np.abs(y-cut)).argmin()
            A, B = np.meshgrid(z, x)
            Aq = A
            Bq = B
            DATA = u[:, :, idx, :]
            quiver_o = DATA[2, :, :]
            quiver_a = DATA[0, :, :]
            quiver_width = 0.0003*abs(x[0]-x[-1])
            title = "$y=%2.2f$" % cut
            abscissa = '$z$'
            ordinate = '$x$'
            orientation = 'vertical'
        elif normal is 'z':
            idx = (np.abs(z-cut)).argmin()
            A, B = np.meshgrid(x, y)
            Aq = A[::decimate, :]
            Bq = B[::decimate, :]
            DATA = u[:, :,:, idx].transpose(0, 2, 1)
            quiver_o = DATA[0, ::decimate, :]
            quiver_a = DATA[1, ::decimate, :]
            quiver_width = 0.0005*abs(y[0]-y[-1])
            title = "$z=%2.2f$" % cut
            abscissa = '$x$'
            ordinate = '$y$'
            orientation = 'horizontal'

        """ filled contour plots """
        if field is not None:
            title += ', $' + field + '$'
            field_index = indices[field]
            #scale = np.max(np.abs(DATA[field_index,:,:]))
            cnt = plt.contourf(A, B, DATA[field_index,:,:]/scale, contourcolors)
            plt.clim(-1, 1)
            plt.colorbar(cnt, orientation=orientation)
            for c in cnt.collections:
                c.set_edgecolor("face")         # get rid of edges in plot

        """ line contour plots """
        if lines is not None:
            line_index = indices[lines]
            #scale = np.max(np.abs(DATA[line_index,:,:]))
            plt.hold(True)
            cnt = plt.contour(A, B, DATA[line_index, :,:]/scale, contourlines, colors='k', linewidths=0.75)
            plt.clabel(cnt, inline=1, fontsize=9, fmt=contourformat, levels=contourlines[::2])

        """ quiver plots """
        if quiver is True:
            plt.hold(True)
            cnt = plt.quiver(Aq, Bq, quiver_o, quiver_a, units='width', width=quiver_width, pivot='mid', color='k', angles='xy', scale_units='xy', scale=scale*2.5)

    plt.title(title)
    plt.xlabel(abscissa)
    plt.ylabel(ordinate)
    plt.gca().set_aspect('equal')
    plt.autoscale(tight=True)

    if filename is not None:
        plt.savefig(filename, bbox_inches='tight', transparent=True)
        plt.close()
    else:
        plt.show()

    return 0

def main(argv):
    """
    plot FlowField object from a filename
    """

    usage = "plotfield [--output=plot.png] [--cut=0] [--lines=u|v|w [--contourstep=0.1]] [--quiver] [--field=u|v|w|mean] [--normal=x|y|z|mean] [--addmean=mean.asc] flowfield.hd5"
    parser=OptionParser(usage)
    parser.add_option("--output",           "-o", default=None)
    parser.add_option("--cut",              "-c", default=0,    type=float)
    parser.add_option("--normal",           "-n", default='z')
    parser.add_option("--field",            "-f", default=None)
    parser.add_option("--lines",            "-l", default=None)
    parser.add_option("--quiver",           "-q", action="store_true", dest='quiver', default=False)
    parser.add_option("--contourstep",      "-s", default=0.1,  type=float)
    parser.add_option("--addmean",          "-m", default=False)
    (options, filenames) = parser.parse_args(sys.argv)

    filename = filenames[1]
    ff = FlowField()
    ff.load(filename)
    if options.addmean:
        try:
            ff.add_mean(options.addmean)
        except:
            print("Adding mean failed for " + filename)
            raise

    plot_slice(ff, cut=options.cut, field=options.field, lines=options.lines, contourstep=options.contourstep, quiver=options.quiver, normal=options.normal, filename=options.output)

    return 0


if __name__=="__main__":
    sys.exit(main(sys.argv))

