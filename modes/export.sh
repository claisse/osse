#!/usr/bin/env bash

FROMDIR="$1"
TODIR="$HOME/Dropbox/Koopman-resolvent/latex/figures/channel"/${1::3}

rm -r $TODIR
mkdir -p $TODIR
cp "$FROMDIR"/*.txt "$TODIR"/
cp "$FROMDIR"/*.png "$TODIR"/
cp "$FROMDIR"/*.eps "$TODIR"/
