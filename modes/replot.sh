#!/usr/bin/env bash

for FILE in $@
do
    BASE=$(basename $FILE .h5)
    BASE=$( echo ${BASE:${#BASE} -2} | sed 's/^0*//' )
    MODE=$(printf "%0*d" 2 $BASE)
    DIR=$(dirname $FILE)
    ../plotfield.py --output="$DIR"/slice-x-$MODE.png --normal=x --cut=0   --field=u --lines=v --contourstep=0.1 $FILE 
    ../plotfield.py --output="$DIR"/slice-y-$MODE.png --normal=y --cut=0.9 --field=u --lines=v --contourstep=0.1 $FILE 
    ../plotfield.py --output="$DIR"/slice-z-$MODE.png --normal=z --cut=0   --field=u --lines=v --contourstep=0.1 $FILE 
done
