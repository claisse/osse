#!/usr/bin/env bash

cd $1

cat params.txt
cat \
    <(echo "# mag phase continuous") \
    <(join \
        <(join \
            <(nl -v0 DMD-eigs-mag.txt) \
            <(nl -v0 DMD-eigs-phase.txt) \
        ) \
        <(nl -v0 DMD-eigs-continuous.txt) \
    ) \
| head -n31 \
| column -t
