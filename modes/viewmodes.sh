#!/usr/bin/env bash

cd $1

MODE=$(printf "%02d" $2)
DELAY=4
feh -R$DELAY "slice-x-$MODE.png" &
feh -R$DELAY "slice-y-$MODE.png" &
feh -R$DELAY "slice-z-$MODE.png" &
