"""
test_flowfield.py implementing class FlowFieldTest(unittest.TestCase)
FlowFieldTest class applies test to the functions implemented in
flowfield.py: FlowField, DiffOperator_FlowField.

DEPRECATED : THESE TEST WILL RETURN 'FALSE' EVERYWHERE AS I DID CHANGE
THE METHOD MAKE_SPECTRAL AND MAKE_PHYSICAL BY ADDING A NORMALISATION
(I mean multiplying/dividing by u.shape[.] during the FFT).

G Claisse 2016
"""
import h5py
import numpy as np
from math import *
import unittest
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

#from flowfield import *
#from channel import *

from flowfield import FlowField
from flowfield import DiffOperator_FlowField as dop
#from channel import Channel
#from channel import DiffOperator as dop


class FlowFieldTest(unittest.TestCase):
    """ Tests of methods implemented in FlowField."""

    def setUp(self, filename='database/test_files/flowfield_sample.h5'):
        """ This method is called before each test.

        Creation of a new flowfield in an hdf5 file.
        Reading of this file to create a new flowfield with method load.

        """
        filename = 'database/test_files/flowfield_sample.h5'

        # Creation of a new h5-file similar to a ChannelFlow flowfield
        file = h5py.File(filename, 'w')
        data = file.create_group('data')
        geom = file.create_group('geom')

        file.attrs['Lx'] = 1 * pi
        file.attrs['Ly'] = 2
        file.attrs['Lz'] = 0.5 * pi

        # BE CAREFUL, VALUES BELOW 10 DOES NOT WORK
        self.Nx = 13
        self.Ny = 15
        self.Nz = 14
        file.attrs['Nd'] = 3
        file.attrs['Nx'] = self.Nx
        file.attrs['Ny'] = self.Ny
        file.attrs['Nz'] = self.Nz

        # Creation of a basis (x, y, z) of dim (Nx, Ny, Nz)
        array_x = np.arange(0, file.attrs['Lx'], file.attrs['Lx'] / self.Nx)
        array_y = np.arange(0, file.attrs['Ly'], file.attrs['Ly'] / self.Ny)
        array_z = np.arange(0, file.attrs['Lz'], file.attrs['Lz'] / self.Nz)

        x = geom.create_dataset('x', data=array_x)
        y = geom.create_dataset('y', data=array_y)
        z = geom.create_dataset('z', data=array_z)

        # Creation of a dataset u(3,x,y,z)
        coef_cos_Z = 3
        coef_sin_Z = 2
        array_u = np.ndarray(shape=(3, self.Nx, self.Ny, self.Nz))#, dtype=complex)
        for i in range(array_u.shape[0]):
            for nx in range(array_u.shape[1]):
                for ny in range(array_u.shape[2]):
                    for nz in range(array_u.shape[3]):
                        array_u[
                            i, nx, ny, nz] = 0 * i + 1 * np.sin(
                            2 * np.pi * 4 * array_x[nx] / file.attrs['Lx']) + 1 * np.cos(
                            2 * np.pi * 2 * array_y[ny] / file.attrs['Ly']) + 1 * np.cos(
                            2 * np.pi * coef_cos_Z * array_z[nz] / file.attrs['Lz']) * np.sin(
                            2 * np.pi * coef_sin_Z * array_z[nz] / file.attrs['Lz'])

        u = data.create_dataset('u', data=array_u)

        # Creation of a dataset p(x,y,z)
        #coef_cos_Z = 3
        #coef_sin_Z = 2
        array_p = np.ndarray(shape=(self.Nx, self.Ny, self.Nz))
        for nx in range(array_p.shape[0]):
            for ny in range(array_p.shape[1]):
                for nz in range(array_p.shape[2]):
                    array_p[
                        nx, ny, nz] = + 1 * np.sin(
                         2 * np.pi * 4 * array_x[nx] / file.attrs['Lx']) + 1 * np.cos(
                         2 * np.pi * 2 * array_y[ny] / file.attrs['Ly']) + 1 * np.cos(
                         2 * np.pi * coef_cos_Z * array_z[nz] / file.attrs['Lz']) * np.sin(
                         2 * np.pi * coef_sin_Z * array_z[nz] / file.attrs['Lz'])

        p = data.create_dataset('p', data=array_p)

        file.close()

        # Load the saved h5-file to a new flowfield
        self.ff = FlowField(filename)

    def tearDown(self):
        """ This method is called after each test."""

    def test_init(self, filename='database/test_files/flowfield_sample.h5'):
        """ Test of the flowfield.

        This tests verify if the flowfield in method setUp is correctly defined:
        - 2 groups : data, geom
        - 3 datasets in geom (x,y,z)
        - 1 dataset in data (u)

        """
        pass
        # Test of the dimension of datasets (x,y,z)

        assert self.ff.len() == 630, 'Flowfield does not have th expected size.'
        assert len(
            self.ff.x) == 7, 'Flowfield dataset x does not have the expected size (6).'
        assert len(
            self.ff.y) == 5, 'Flowfield dataset y does not have the expected size (5).'
        assert len(
            self.ff.z) == 6, 'Flowfield dataset z does not have the expected size (6).'
        assert self.ff.x.all() == np.array([0, 2 *
                                            pi /
                                            6, 4 *
                                            pi /
                                            6, 6 *
                                            pi /
                                            6, 8 *
                                            pi /
                                            6, 10 *
                                            pi /
                                            6, 2 *
                                            pi]).all(), 'Flowfield dataset x is not defined as expected.'
        assert self.ff.y.all() == np.array(
            [0, pi / 2, pi, 3 * pi / 2, 2 * pi]).all(), 'Flowfield dataset y is not defined as expected.'
        assert self.ff.z.all() == np.array([0, 2 *
                                            pi /
                                            5, 4 *
                                            pi /
                                            5, 6 *
                                            pi /
                                            5, 8 *
                                            pi /
                                            5, 2 *
                                            pi]).all(), 'Flowfield dataset z is not defined as expected.'

        # Test of the datasets values

        assert self.ff.u.shape[
            0] == 3, 'Flowfield dataset u does not have the expected shape[0].'
        assert self.ff.u.shape[
            1] == 7, 'Flowfield dataset u does not have the expected shape[1].'
        assert self.ff.u.shape[
            2] == 5, 'Flowfield dataset u does not have the expected shape[2].'
        assert self.ff.u.shape[
            3] == 6, 'Flowfield dataset u does not have the expected shape[3].'
        assert self.ff.u[0, :, 0, 0].all() == np.array([0, 2, 4, 6, 8, 10]).all(), \
            'Flowfield dataset u has not the expected values.'
        assert self.ff.u[0, 0, :, 0].all() == np.array([0, 3, 6, 9, 12]).all(), \
            'Flowfield dataset u has not the expected values.'
        assert self.ff.u[0, 0, 0, :].all() == np.array([0, 5, 10, 15, 20]).all(), \
            'Flowfield dataset x is not defined as expected.'
        assert self.ff.u[2, 0, 0, :].all() == np.array([2, 7, 12, 17, 22]).all(), \
            'Flowfield dataset x is not defined as expected.'

    def test_save_load(self):
        """ Test of methods save() and load()."""
        self.ff.save('database/test_files/flowfield_sample_save.h5')
        ff2 = FlowField()
        ff2.load('database/test_files/flowfield_sample_save.h5')
        #assert ff2.len() ==  630, 'Flowfield does not have the expected size.'
        assert self.ff.isEqual(ff2), 'A loaded flowfield is different from the original one.'

    def test_copy(self):
        """ Test of method copy()."""
        ff3 = FlowField()
        ff3.copy(self.ff)
        assert self.ff.isEqual(ff3), 'The copied flowfield is different from the orignal one.'
        #ff4 = dop.derivative(self.ff, 'x', 1)
        #self.assertFalse( (self.ff==ff4).all(), msg='A different flowfield is not seen as different from the orignal one.')

    def test_change_flowfield(self):
        """ Test if Class DiffOperator_FlowField reacts if the flowfield changes."""
        # create a new flowfield
        filename_bis = 'database/test_files/flowfield_sample_bis.h5'
        file_bis = h5py.File(filename_bis, 'w')
        data = file_bis.create_group('data')
        geom = file_bis.create_group('geom')

        file_bis.attrs['Lx'] = 1 * pi
        Lx = file_bis.attrs['Lx']
        file_bis.attrs['Ly'] = 2
        file_bis.attrs['Lz'] = 0.5 * pi

        self.Nx = 25
        self.Ny = 1
        self.Nz = 2
        file_bis.attrs['Nd'] = 3
        file_bis.attrs['Nx'] = self.Nx
        file_bis.attrs['Ny'] = self.Ny
        file_bis.attrs['Nz'] = self.Nz

        # Creation of a basis (x, y, z) of dim (Nx, Ny, Nz)
        array_x = np.arange(
            0,
            file_bis.attrs['Lx'],
            file_bis.attrs['Lx'] /
            self.Nx)
        array_y = np.arange(
            0,
            file_bis.attrs['Ly'],
            file_bis.attrs['Ly'] /
            self.Ny)
        array_z = np.arange(
            0,
            file_bis.attrs['Lz'],
            file_bis.attrs['Lz'] /
            self.Nz)

        x = geom.create_dataset('x', data=array_x)
        y = geom.create_dataset('y', data=array_y)
        z = geom.create_dataset('z', data=array_z)

        # Creation of a dataset u(3,x,y,z)
        array_u = np.ndarray(shape=(3, self.Nx, self.Ny, self.Nz))
        for i in range(array_u.shape[0]):
            for nx in range(array_u.shape[1]):
                for ny in range(array_u.shape[2]):
                    for nz in range(array_u.shape[3]):
                        array_u[
                            i, nx, ny, nz] = 4 * np.sin(2 * np.pi * 4 * array_x[nx] / file_bis.attrs['Lx'])
        u = data.create_dataset('u', data=array_u)
        file_bis.close()

        # load the new flowfield and test it
        self.ff.load(filename_bis)

        diff_xbis = self.ff.dop.deriv_u(1, 0, 0)
        np.testing.assert_almost_equal(
            diff_xbis[0, :, 0, 0],
            4 * 2 * np.pi / Lx * 4 * np.cos(2 * np.pi / Lx * 4 * np.arange(0, Lx, Lx / self.Nx)),
            err_msg='FlowField changed but DiffOperator did not change.')

    def test_deriv_cross_partial(self):
        filename_bis = 'database/test_files/flowfield_sample_tri.h5'
        file_bis = h5py.File(filename_bis, 'w')
        data = file_bis.create_group('data')
        geom = file_bis.create_group('geom')

        file_bis.attrs['Lx'] = 1 * pi
        Lx = file_bis.attrs['Lx']
        file_bis.attrs['Ly'] = 2
        file_bis.attrs['Lz'] = 0.5 * pi

        self.Nx = 12
        self.Ny = 13
        self.Nz = 15
        file_bis.attrs['Nx'] = self.Nx
        file_bis.attrs['Ny'] = self.Ny
        file_bis.attrs['Nz'] = self.Nz

        # Creation of a basis (x, y, z) of dim (Nx, Ny, Nz)
        array_x = np.arange(
            0,
            file_bis.attrs['Lx'],
            file_bis.attrs['Lx'] /
            self.Nx)
        array_y = np.arange(
            0,
            file_bis.attrs['Ly'],
            file_bis.attrs['Ly'] /
            self.Ny)
        array_z = np.arange(
            0,
            file_bis.attrs['Lz'],
            file_bis.attrs['Lz'] /
            self.Nz)

        x = geom.create_dataset('x', data=array_x)
        y = geom.create_dataset('y', data=array_y)
        z = geom.create_dataset('z', data=array_z)

        # Creation of a dataset u(3,x,y,z)
        array_u = np.ndarray(shape=(3, self.Nx, self.Ny, self.Nz))
        for i in range(array_u.shape[0]):
            for nx in range(array_u.shape[1]):
                for ny in range(array_u.shape[2]):
                    for nz in range(array_u.shape[3]):
                        array_u[i, nx, ny, nz] = \
                            1 * np.sin(2 * np.pi * 1 * array_x[nx] / file_bis.attrs['Lx']) \
                            + 1 * np.sin(2 * np.pi * 1 * array_y[ny] / file_bis.attrs['Ly']) \
                            + 1 * np.cos(2 * np.pi * 1 * array_z[nz] / file_bis.attrs['Lz'])

        u = data.create_dataset('u', data=array_u)

        # load the new flowfield and test it
        self.ff.load(filename_bis)

        # TEST
        x, y, z = 1, 0, 0
        plt.figure()
        deriv = self.ff.dop.deriv(self.ff.u, x, y, z)
        deriv_u = self.ff.dop.deriv_u(x, y, z)

        plt.plot(self.ff.x, deriv[0, :, 5, 6], 'b-o')
        plt.plot(self.ff.y, deriv[0, 5, :, 6], 'r-o')
        plt.plot(self.ff.z, deriv[0, 5, 6, :], 'g-o')
        plt.plot(self.ff.x, deriv_u[0, :, 5, 6], 'b--o')
        plt.plot(self.ff.y, deriv_u[0, 5, :, 6], 'r--o')
        plt.plot(self.ff.z, deriv_u[0, 5, 6, :], 'g--o')
        plt.savefig('figures/test_flowfield_deriv_cross_partial.png',
                    bbox_inches='tight')
        plt.show()

    def test_deriv(self):
        deriv_ux = self.ff.dop.deriv_u(1, 0, 0)
        deriv_uy = self.ff.dop.deriv_u(0, 1, 0)
        deriv_uz = self.ff.dop.deriv_u(0, 0, 1)
        deriv_px = self.ff.dop.deriv_p(1, 0, 0)
        deriv_py = self.ff.dop.deriv_p(0, 1, 0)
        deriv_pz = self.ff.dop.deriv_p(0, 0, 1)

        deriv_dat_ux = self.ff.dop.deriv(self.ff.u, 1, 0, 0)
        deriv_dat_uy = self.ff.dop.deriv(self.ff.u, 0, 1, 0)
        deriv_dat_uz = self.ff.dop.deriv(self.ff.u, 0, 0, 1)
        deriv_dat_px = self.ff.dop.deriv(self.ff.p, 1, 0, 0)
        deriv_dat_py = self.ff.dop.deriv(self.ff.p, 0, 1, 0)
        deriv_dat_pz = self.ff.dop.deriv(self.ff.p, 0, 0, 1)

        np.testing.assert_almost_equal(
            deriv_ux,
            deriv_dat_ux,
            err_msg="deriv_u('x') != deriv(self.ff.u, 'x')")
        np.testing.assert_almost_equal(
            deriv_uy,
            deriv_dat_uy,
            err_msg="deriv_u('y') != deriv(self.ff.u, 'y')")
        np.testing.assert_almost_equal(
            deriv_uz,
            deriv_dat_uz,
            err_msg="deriv_u('z') != deriv(self.ff.u, 'z')")

        np.testing.assert_almost_equal(
            deriv_px,
            deriv_dat_px,
            err_msg="deriv_p('x') != deriv(self.ff.p, 'x')")
        np.testing.assert_almost_equal(
            deriv_py,
            deriv_dat_py,
            err_msg="deriv_p('y') != deriv(self.ff.p, 'y')")
        np.testing.assert_almost_equal(
            deriv_pz,
            deriv_dat_pz,
            err_msg="deriv_p('z') != deriv(self.ff.p, 'z')")

    def test_deriv_u(self):
        """ Test of method derivative for 'u' for all direction and some indexes."""
        #b = self.ff.attributes['b']
        #a = self.ff.attributes['a']
        Lx = self.ff.attributes['Lx']
        Ly = self.ff.attributes['Ly']
        Lz = self.ff.attributes['Lz']

        #print(np.round(self.ff.u[0,:,:,:], 2))

        # DERIVATIVE X 1st order
        diff_x = self.ff.dop.deriv_u(1, 0, 0)
        np.testing.assert_almost_equal(
            diff_x[0, :, 0, 0],
            2 * np.pi / Lx * 4 * np.cos(2 * np.pi / Lx * 4 * np.arange(0, Lx, Lx / self.Nx)),
            err_msg='Derivative 1st X is wrong')

        # DERIVATIVE Y 1st order
        diff_y = self.ff.dop.deriv_u(0, 1, 0)
        np.testing.assert_almost_equal(
            diff_y[1, 0, :, 0],
            2 * np.pi / Ly * -1 * 2 * np.sin(2 * np.pi / Ly * 2 * np.arange(0, Ly, Ly / self.Ny)),
            err_msg='Derivative 1st Y is wrong')

        # DERIVATIVE Z 1st order
        diff_z = self.ff.dop.deriv_u(0, 0, 1)
        coef_cos = 3
        coef_sin = 2
        np.testing.assert_almost_equal(
            diff_z[2, 0, 0, :],
            np.pi / Lz * (
                (coef_cos + coef_sin) *
                    np.cos(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(0, Lz, Lz / self.Nz)) -
                (coef_cos - coef_sin) *
                    np.cos(2 *np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz))),
            err_msg='Derivative 1st Z is wrong')
        #2*np.pi/Lz * +1*np.cos(2 * 2*np.pi/Lz * 1*np.arange(0, Lz, Lz/self.Nz)),
        # err_msg = 'Derivative 1st Z is wrong')

        # DERIVATIVE Y 4th order
        diff_y_4 = self.ff.dop.deriv_u(0, 4, 0)
        np.testing.assert_almost_equal(
            diff_y_4[1, 0, :, 0],
            (2 * 2 * np.pi / Ly)**4 * self.ff.u[0, 0, :, 0],
            err_msg='Derivative 4th Y is wrong')

        # CHECK DERIVATIVE RESULT
        #print( ' ' )
        # print(diff_x.u[0,:,0,0])
        #print(2*np.pi/Lx * np.cos(2*np.pi/Lx * np.arange(0, Lx, Lx/self.Nx)))
        #print( ' ' )
        #print(np.round(diff_z.u[0,0,0,:], 2))
        ##print(2*np.pi/Lz * np.cos(2*np.pi/Lz * np.arange(0, Lz, Lz/self.Nz)))
        #print( ' ' )
        #print(np.round(diff_y.u[0,0,:,0], 2))
        #print(2*np.pi/Ly * -1 * np.sin(2*np.pi/Ly * np.arange(0, Ly, Ly/self.Ny)))

        # CHECK IF THE ORIGINAL FLOWFIELD IS MODIFIED
        #print(np.round(self.ff.u[0,:,:,:], 2))

    def test_deriv_p(self):
        """ Test of method derivative for 'p' for all direction and some indexes."""
        #b = self.ff.attributes['b']
        #a = self.ff.attributes['a']
        Lx = self.ff.attributes['Lx']
        Ly = self.ff.attributes['Ly']
        Lz = self.ff.attributes['Lz']

        #print(np.round(self.ff.u[:,:,:], 2))

        # DERIVATIVE X 1st order
        diff_x = self.ff.dop.deriv_p(1, 0, 0)
        np.testing.assert_almost_equal(
            diff_x[
                :,
                0,
                0],
            2 *
            np.pi /
            Lx *
            4 *
            np.cos(
                2 *
                np.pi /
                Lx *
                4 *
                np.arange(
                    0,
                    Lx,
                    Lx /
                    self.Nx)),
            err_msg='Derivative 1st X is wrong')

        # DERIVATIVE Y 1st order
        diff_y = self.ff.dop.deriv_p(0, 1, 0)
        np.testing.assert_almost_equal(
            diff_y[0, :, 0],
            2 * np.pi / Ly * -1 * 2 * np.sin(2 * np.pi / Ly * 2 * np.arange(0, Ly, Ly / self.Ny)),
            err_msg='Derivative 1st Y is wrong')

        # DERIVATIVE Z 1st order
        diff_z = self.ff.dop.deriv_p(0, 0, 1)
        coef_cos = 3
        coef_sin = 2
        np.testing.assert_almost_equal(diff_z[0, 0, :], np.pi /
                                       Lz *
                                       ((coef_cos +
                                         coef_sin) *
                                        np.cos(2 *
                                               np.pi /
                                               Lz *
                                               (coef_cos +
                                                coef_sin) *
                                               np.arange(0, Lz, Lz /
                                                         self.Nz)) -
                                        (coef_cos -
                                           coef_sin) *
                                        np.cos(2 *
                                               np.pi /
                                               Lz *
                                               (coef_cos -
                                                coef_sin) *
                                               np.arange(0, Lz, Lz /
                                                         self.Nz))), err_msg='Derivative 1st Z is wrong')
        #2*np.pi/Lz * +1*np.cos(2 * 2*np.pi/Lz * 1*np.arange(0, Lz, Lz/self.Nz)),
        # err_msg = 'Derivative 1st Z is wrong')

        # DERIVATIVE Y 4th order
        diff_y_4 = self.ff.dop.deriv_p(0, 4, 0)
        np.testing.assert_almost_equal(
            diff_y_4[0, :, 0],
            (2 * 2 * np.pi / Ly)**4 * self.ff.p[0, :, 0],
            err_msg='Derivative 4th Y is wrong')

        # CHECK DERIVATIVE RESULT
        #print( ' ' )
        # print(diff_x.u[:,0,0])
        #print(2*np.pi/Lx * np.cos(2*np.pi/Lx * np.arange(0, Lx, Lx/self.Nx)))
        #print( ' ' )
        #print(np.round(diff_z.u[0,0,:], 2))
        ##print(2*np.pi/Lz * np.cos(2*np.pi/Lz * np.arange(0, Lz, Lz/self.Nz)))
        #print( ' ' )
        #print(np.round(diff_y.u[0,:,0], 2))
        #print(2*np.pi/Ly * -1 * np.sin(2*np.pi/Ly * np.arange(0, Ly, Ly/self.Ny)))

        # CHECK IF THE ORIGINAL FLOWFIELD IS MODIFIED
        #print(np.round(self.ff.u[:,:,:], 2))

    def test_div(self):
        div_u = self.ff.dop.div_u()
        div_dat_u = self.ff.dop.div(self.ff.u)

        np.testing.assert_almost_equal(
            div_u,
            div_dat_u,
            err_msg="div_u() != div(self.ff.u)")

        div2_ux = self.ff.dop.div_u(2)
        div2_dat_ux = self.ff.dop.div(self.ff.u, 2)

        np.testing.assert_almost_equal(
            div2_ux,
            div2_dat_ux,
            err_msg="div2_u() != div(self.ff.u,2)")

    def test_div_u(self):
        """Test of method div for different order."""
        #b = self.ff.attributes['b']
        #a = self.ff.attributes['a']
        Lx = self.ff.attributes['Lx']
        Ly = self.ff.attributes['Ly']
        Lz = self.ff.attributes['Lz']

        #diff_x = self.ffdop.derivative('x', 1)
        #print('--- DIFF X ---\n', np.round(diff_x[0,:,:,:], 2).real)
        #diff_y = self.ff.dop.derivative('y', 1)
        #print('--- DIFF Y ---\n', np.round(diff_y[1,:,:,:], 2).real)
        #diff_z = self.ff.dop.derivative('z', 1)
        #print('--- DIFF Z ---\n', np.round(diff_z[2,:,:,:], 2).real)

        coef_cos = 3
        coef_sin = 2

        # TEST DIVERGENCE ORDER 1
        div = self.ff.dop.div_u()
        #print('--- DIV --- \n', np.round(div[:,:,:],3).real)
        array_x = 2 * np.pi / Lx * 4 * \
            np.cos(2 * np.pi / Lx * 4 * np.arange(0, Lx, Lx / self.Nx)[:])
        array_y = 2 * np.pi / Ly * -2 * \
            np.sin(2 * np.pi / Ly * 2 * np.arange(0, Ly, Ly / self.Ny)[:])
        array_z = 1 * np.pi / Lz * ((coef_cos + coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(0, Lz, Lz / self.Nz)[
                                    :]) - (coef_cos - coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz)[:]))

        array_x = array_x[:, np.newaxis, np.newaxis]
        array_y = array_y[np.newaxis, :, np.newaxis]
        array_z = array_z[np.newaxis, np.newaxis, :]
        array_solution = array_x + array_y + array_z

        np.testing.assert_almost_equal(
            div[:, :, :],
            array_solution[:, :, :],
            err_msg='Real part of divergence is wrong.')

        np.testing.assert_almost_equal(
            div[:, :, :].imag,
            np.zeros((self.Nx, self.Ny, self.Nz)),
            err_msg='Imaginary part of divergence is not zero.')

        # TEST DIVERGENCE ORDER 2
        div_2 = self.ff.dop.div_u(2)
        array_x_2 = -(2 * np.pi / Lx * 4)**2 * np.sin(2 * np.pi /
                                                      Lx * 4 * np.arange(0, Lx, Lx / self.Nx)[:])
        array_y_2 = -(2 * np.pi / Ly * 2)**2 * np.cos(2 * np.pi /
                                                      Ly * 2 * np.arange(0, Ly, Ly / self.Ny)[:])
        array_z_2 = -0.5 * (2 * np.pi / Lz)**2 * ((coef_cos + coef_sin)**2 * np.sin(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(
            0, Lz, Lz / self.Nz)[:]) - (coef_cos - coef_sin)**2 * np.sin(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz)[:]))

        array_x_2 = array_x_2[:, np.newaxis, np.newaxis]
        array_y_2 = array_y_2[np.newaxis, :, np.newaxis]
        array_z_2 = array_z_2[np.newaxis, np.newaxis, :]
        array_solution_2 = array_x_2 + array_y_2 + array_z_2

        np.testing.assert_almost_equal(
            div_2[:, :, :],
            array_solution_2[:, :, :],
            err_msg='Real part of divergence order 2 is wrong.')

        np.testing.assert_almost_equal(
            div_2[:, :, :].imag,
            np.zeros((self.Nx, self.Ny, self.Nz)),
            err_msg='Imaginary part of divergence order 2 is not zero.')

    def test_grad(self):
        grad_ux = self.ff.dop.grad_u()
        grad_uy = self.ff.dop.grad_u()
        grad_uz = self.ff.dop.grad_u()
        grad_px = self.ff.dop.grad_p()
        grad_py = self.ff.dop.grad_p()
        grad_pz = self.ff.dop.grad_p()

        grad_dat_ux = self.ff.dop.grad(self.ff.u)
        grad_dat_uy = self.ff.dop.grad(self.ff.u)
        grad_dat_uz = self.ff.dop.grad(self.ff.u)
        grad_dat_px = self.ff.dop.grad(self.ff.p)
        grad_dat_py = self.ff.dop.grad(self.ff.p)
        grad_dat_pz = self.ff.dop.grad(self.ff.p)

        np.testing.assert_almost_equal(
            grad_px,
            grad_dat_px,
            err_msg="grad_p() != grad(self.ff.p)")
        np.testing.assert_almost_equal(
            grad_py,
            grad_dat_py,
            err_msg="grad_p() != grad(self.ff.p)")
        np.testing.assert_almost_equal(
            grad_pz,
            grad_dat_pz,
            err_msg="grad_p() != grad(self.ff.p)")

        grad2_ux = self.ff.dop.grad_u(2)
        grad2_uy = self.ff.dop.grad_u(2)
        grad2_uz = self.ff.dop.grad_u(2)
        grad2_px = self.ff.dop.grad_p(2)
        grad2_py = self.ff.dop.grad_p(2)
        grad2_pz = self.ff.dop.grad_p(2)

        grad2_dat_ux = self.ff.dop.grad(self.ff.u, 2)
        grad2_dat_uy = self.ff.dop.grad(self.ff.u, 2)
        grad2_dat_uz = self.ff.dop.grad(self.ff.u, 2)
        grad2_dat_px = self.ff.dop.grad(self.ff.p, 2)
        grad2_dat_py = self.ff.dop.grad(self.ff.p, 2)
        grad2_dat_pz = self.ff.dop.grad(self.ff.p, 2)

        np.testing.assert_almost_equal(
            grad2_ux,
            grad2_dat_ux,
            err_msg="grad2_u() != grad(self.ff.u,2)")
        np.testing.assert_almost_equal(
            grad2_uy,
            grad2_dat_uy,
            err_msg="grad2_u() != grad(self.ff.u,2)")
        np.testing.assert_almost_equal(
            grad2_uz,
            grad2_dat_uz,
            err_msg="grad2_u() != grad(self.ff.u,2)")

        np.testing.assert_almost_equal(
            grad2_px,
            grad2_dat_px,
            err_msg="grad2_p() != grad(self.ff.p,2)")
        np.testing.assert_almost_equal(
            grad2_py,
            grad2_dat_py,
            err_msg="grad2_p() != grad(self.ff.p,2)")
        np.testing.assert_almost_equal(
            grad2_pz,
            grad2_dat_pz,
            err_msg="grad2_p() != grad(self.ff.p,2)")

    def test_grad_u(self):
        # Definition of parameters.
        Lx = self.ff.attributes['Lx']
        Ly = self.ff.attributes['Ly']
        Lz = self.ff.attributes['Lz']
        coef_cos = 3
        coef_sin = 2

        # Calculation of gradient order 2
        gradient = self.ff.dop.grad_u(2)

        # Calculation of the expected results
        array_x = -(2 * np.pi / Lx * 4)**2 * np.sin(2 * np.pi / \
                    Lx * 4 * np.arange(0, Lx, Lx / self.Nx)[:])
        array_y = -(2 * np.pi / Ly * 2)**2 * np.cos(2 * np.pi /
                                                    Ly * 2 * np.arange(0, Ly, Ly / self.Ny)[:])
        array_z = -0.5 * (2 * np.pi / Lz)**2 * ((coef_cos + coef_sin)**2 * np.sin(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(
            0, Lz, Lz / self.Nz)[:]) - (coef_cos - coef_sin)**2 * np.sin(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz)[:]))

        # Comparison

        # gradient( dx/dy/dz, ux/uy/uz, x, y, z)
        # ux = uy = uz

        # shape
        np.testing.assert_almost_equal(
            gradient.shape,
            (3, 3, self.Nx, self.Ny, self.Nz),
            err_msg='Shape is wrong.')

        # dux/dx
        np.testing.assert_almost_equal(
            gradient[0, 0, :, 0, 0],
            array_x[:],
            err_msg='dux/dx is wrong.')
        # duy/dx
        np.testing.assert_almost_equal(
            gradient[0, 1, :, 0, 0],
            array_x[:],
            err_msg='duy/dx is wrong.')
        # duz/dx
        np.testing.assert_almost_equal(
            gradient[0, 2, :, 0, 0],
            array_x[:],
            err_msg='duz/dx is wrong.')
        # /dx is constant in y and z direction
        np.testing.assert_almost_equal(
            gradient[0, 2, 0, :, 0],
            array_x[0],
            err_msg='d./dx is not constant in y direction.')
        np.testing.assert_almost_equal(
            gradient[0, 2, 0, 0, :],
            array_x[0],
            err_msg='d./dx is not constant in z direction.')

        # dux/dy
        np.testing.assert_almost_equal(
            gradient[1, 0, 0, :, 0],
            array_y[:],
            err_msg='dux/dy is wrong.')
        # duy/dy
        np.testing.assert_almost_equal(
            gradient[1, 1, 0, :, 0],
            array_y[:],
            err_msg='duy/dy is wrong.')
        # duz/dy
        np.testing.assert_almost_equal(
            gradient[1, 2, 0, :, 0],
            array_y[:],
            err_msg='duz/dy is wrong.')
        # /dy is constant in x and z direction
        np.testing.assert_almost_equal(
            gradient[1, 2, :, 0, 0],
            array_y[0],
            err_msg='d./dy is not constant in x direction.')
        np.testing.assert_almost_equal(
            gradient[1, 2, 0, 0, :],
            array_y[0],
            err_msg='d./dy is not constant in z direction.')

        # dux/dz
        np.testing.assert_almost_equal(
            gradient[2, 0, 0, 0, :],
            array_z[:],
            err_msg='dux/dz is wrong.')
        # duy/dz
        np.testing.assert_almost_equal(
            gradient[2, 1, 0, 0, :],
            array_z[:],
            err_msg='duy/dz is wrong.')
        # duz/dz
        np.testing.assert_almost_equal(
            gradient[2, 2, 0, 0, :],
            array_z[:],
            err_msg='duz/dz is wrong.')
        # /dz is constant in x and y direction
        np.testing.assert_almost_equal(
            gradient[2, 2, :, 0, 0],
            array_z[0],
            err_msg='d./dz is not constant in x direction.')
        np.testing.assert_almost_equal(
            gradient[2, 2, :, 0, 0],
            array_z[0],
            err_msg='d./dz is not constant in y direction.')

    def test_grad_p(self):
        # Definition of parameters.
        Lx = self.ff.attributes['Lx']
        Ly = self.ff.attributes['Ly']
        Lz = self.ff.attributes['Lz']
        coef_cos = 3
        coef_sin = 2

        # Calculation of gradient order 2
        gradient = self.ff.dop.grad_p(2)

        # Calculation of the expected results
        array_x = -(2 * np.pi / Lx * 4)**2 * np.sin(2 * np.pi / \
                    Lx * 4 * np.arange(0, Lx, Lx / self.Nx)[:])
        array_y = -(2 * np.pi / Ly * 2)**2 * np.cos(2 * np.pi /
                                                    Ly * 2 * np.arange(0, Ly, Ly / self.Ny)[:])
        array_z = -0.5 * (2 * np.pi / Lz)**2 * ((coef_cos + coef_sin)**2 * np.sin(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(
            0, Lz, Lz / self.Nz)[:]) - (coef_cos - coef_sin)**2 * np.sin(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz)[:]))

        # Comparison
        # gradient( dx/dy/dz, x, y, z)
        # shape
        np.testing.assert_almost_equal(
            gradient.shape,
            (3, 1, self.Nx, self.Ny, self.Nz),
            err_msg='Shape is wrong.')

        # dp/dx
        np.testing.assert_almost_equal(
            gradient[0, 0, :, 0, 0],
            array_x[:],
            err_msg='dp/dx is wrong.')

        # dp/dy
        np.testing.assert_almost_equal(
            gradient[1, 0, 0, :, 0],
            array_y[:],
            err_msg='dp/dy is wrong.')

        # dp/dz
        np.testing.assert_almost_equal(
            gradient[2, 0, 0, 0, :],
            array_z[:],
            err_msg='dp/dz is wrong.')

    def test_curl(self):
        curl_u = self.ff.dop.curl_u()
        curl_dat_u = self.ff.dop.curl(self.ff.u)

        np.testing.assert_almost_equal(
            curl_u,
            curl_dat_u,
            err_msg="curl_u() != curl(self.ff.u)")

    def test_curl_u(self):
        # Definition of parameters.
        Lx = self.ff.attributes['Lx']
        Ly = self.ff.attributes['Ly']
        Lz = self.ff.attributes['Lz']
        coef_cos = 3
        coef_sin = 2

        # Calculation of curl
        curl = self.ff.dop.curl_u()

        # Calculation of the expected results
        array_x = 2 * np.pi / Lx * 4 * \
            np.cos(2 * np.pi / Lx * 4 * np.arange(0, Lx, Lx / self.Nx)[:])
        array_y = 2 * np.pi / Ly * -2 * \
            np.sin(2 * np.pi / Ly * 2 * np.arange(0, Ly, Ly / self.Ny)[:])
        array_z = 1 * np.pi / Lz * ((coef_cos + coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos + coef_sin) * np.arange(0, Lz, Lz / self.Nz)[
                                    :]) - (coef_cos - coef_sin) * np.cos(2 * np.pi / Lz * (coef_cos - coef_sin) * np.arange(0, Lz, Lz / self.Nz)[:]))

        # Comparison

        # shape
        np.testing.assert_almost_equal(
            curl.shape,
            (3, self.Nx, self.Ny, self.Nz),
            err_msg='shape is wrong.')

        np.testing.assert_almost_equal(
            curl[0, 0, :, :],
            array_y[:, np.newaxis] - array_z[np.newaxis, :],
            err_msg='First component is wrong.')

        np.testing.assert_almost_equal(
            curl[1, :, 0, :],
            array_z[np.newaxis, :] - array_x[:, np.newaxis],
            err_msg='Second component is wrong.')

        np.testing.assert_almost_equal(
            curl[2, :, :, 0],
            array_x[:, np.newaxis] - array_y[np.newaxis, :],
            err_msg='Third component is wrong.')

    def test_laplacian(self):
        laplacian_u = self.ff.dop.laplacian_u()
        laplacian_dat_u = self.ff.dop.laplacian(self.ff.u)

        np.testing.assert_almost_equal(
            laplacian_u,
            laplacian_dat_u,
            err_msg="laplacian_u() != laplacian(self.ff.u)")

    def test_laplacian_u(self):
        laplacian = self.ff.dop.laplacian_u()

        np.testing.assert_almost_equal(
            laplacian[0, :, :, :],
            self.ff.dop.div_u(2),
            err_msg='X-component of Laplacian is wrong.')

        np.testing.assert_almost_equal(
            laplacian[1, :, :, :],
            self.ff.dop.div_u(2),
            err_msg='Y-component of Laplacian is wrong.')

        np.testing.assert_almost_equal(
            laplacian[2, :, :, :],
            self.ff.dop.div_u(2),
            err_msg='Z-component of Laplacian is wrong.')

        pass

    def test_generate_stress_tensor(self):
        array_u = np.ndarray(shape=(3, self.Nx, self.Ny, self.Nz))
        for i in range(array_u.shape[0]):
            for nx in range(array_u.shape[1]):
                for ny in range(array_u.shape[2]):
                    for nz in range(array_u.shape[3]):
                        array_u[i, nx, ny, nz] = 1 * \
                            i + 2 * nx + 3 * ny + 4 * nz
        self.ff.u = array_u
        self.ff.generate_stress_tensor()

        np.testing.assert_almost_equal(
            self.ff.uu[0, 0],
            array_u[0, :, :, :] * array_u[0, :, :, :],
            err_msg='ux.ux component of stress tensor is wrong.')

        np.testing.assert_almost_equal(
            self.ff.uu[0, 1],
            array_u[0, :, :, :] * array_u[1, :, :, :],
            err_msg='ux.uy component of stress tensor is wrong.')

        np.testing.assert_almost_equal(
            self.ff.uu[0, 2],
            array_u[0, :, :, :] * array_u[2, :, :, :],
            err_msg='ux.uz component of stress tensor is wrong.')

        np.testing.assert_almost_equal(
            self.ff.uu[1, 1],
            array_u[1, :, :, :] * array_u[1, :, :, :],
            err_msg='uy.uy component of stress tensor is wrong.')

        np.testing.assert_almost_equal(
            self.ff.uu[2, 2],
            array_u[2, :, :, :] * array_u[2, :, :, :],
            err_msg='uz.uz component of stress tensor is wrong.')

        np.testing.assert_almost_equal(
            self.ff.uu[1, 2],
            array_u[1, :, :, :] * array_u[2, :, :, :],
            err_msg='uy.uz component of stress tensor is wrong.')

    def test_grad_stress_tensor(self):
        array_u = np.ndarray(shape=(3, self.Nx, self.Ny, self.Nz))
        for i in range(array_u.shape[0]):
            for nx in range(array_u.shape[1]):
                for ny in range(array_u.shape[2]):
                    for nz in range(array_u.shape[3]):
                        array_u[i, nx, ny, nz] = 1 * \
                            i + 2 * nx + 3 * ny + 4 * nz
        self.ff.u = array_u

        # print(self.ff.u[0,:,:,:])
        # print(self.ff.uu[0,0,:,:,:])
        # print(self.ff.dop.grad_stress_tensor()[0,0,2,:,:,:])

    def test_attribute(self):
        file2 = h5py.File('database/test_files/test_attrs.h5', 'r')
        for a in file2.attrs:
            print(a)
        file2.close()
        print('---')
        ff3 = FlowField()
        ff3.load('database/test_files/test_attrs.h5')
        for a in ff3.attributes:
            print(a)
        print(' -> ', ff3.attributes['a'])

test = unittest.TestSuite()
# test.addTest(FlowFieldTest("test_init"))

test.addTest(FlowFieldTest("test_save_load"))
test.addTest(FlowFieldTest("test_copy"))
test.addTest(FlowFieldTest("test_change_flowfield"))
test.addTest(FlowFieldTest("test_deriv_cross_partial"))
test.addTest(FlowFieldTest("test_deriv"))
test.addTest(FlowFieldTest("test_deriv_u"))
test.addTest(FlowFieldTest("test_deriv_p"))
test.addTest(FlowFieldTest("test_div"))
test.addTest(FlowFieldTest("test_div_u"))
test.addTest(FlowFieldTest("test_grad"))
test.addTest(FlowFieldTest("test_grad_u"))
test.addTest(FlowFieldTest("test_grad_p"))
test.addTest(FlowFieldTest("test_curl"))
test.addTest(FlowFieldTest("test_curl_u"))
test.addTest(FlowFieldTest("test_laplacian"))
test.addTest(FlowFieldTest("test_laplacian_u"))
test.addTest(FlowFieldTest("test_generate_stress_tensor"))
test.addTest(FlowFieldTest("test_grad_stress_tensor"))

# test.addTest(FlowFieldTest("test_attribute"))
runner = unittest.TextTestRunner()
runner.run(test)
