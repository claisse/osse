import h5py
import numpy as np
import os, errno

class interpolate:
    """
    Interpolate class.

    Possible to call it as:
    interp_value = interpolate.interpolate(foldername, var)

    and use it as a function as:
    interp_value(time)

    order 1E+2 faster than scipy.interpolate.interp1d
    """

    def __init__(self, foldername, var, var_data, timesteps):
        """
        Save the 'var_data', array of data of the variable 'var',
        and timesteps list in a folder named 'foldername',
        and initialize all necessary variables.

        It creates *foldername* folder including:
        - a file called *timesteps.h5* containing a dataset ' timesteps',
            corresponding to a vector timescale
        - a folder called *x* containing many file *x_i.h5* (or other variable),
            where i is the corresponding steps,
            containing a data 'x' for a vector data at this time-step

        Keyword arguments :
        foldername --
        var -- string, i.e. 'x', 'q', 'p'...
        var_data -- array of variable of shape [N_timesteps, N_state]
        timestes_list -- 1D vector, list of timesteps where var_data is given
        """
        # setting initial inputs
        self.foldername = str(foldername)
        self.var = var
        self.var_data = var_data

        self.timesteps_filename = self.foldername + "/timesteps.h5"
        self.timesteps_list = timesteps
        self.N_step = timesteps.shape[0]

        # create container folder if non-existant
        try:
            os.makedirs(self.foldername)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        # save the timescale in foldername/timesteps.h5
        self.save_timesteps_h5()

        # save this string correspond to the name of the data file to load later
        self.format_step = "{0:0"+str(len(format(self.N_step)))+"d}"

        # save the data in foldername/var_name/var_name_index.h5
        self.save_variable_h5()

        # just to save the indices later
        self.index0 = None 
        self.index1 = None
        self.index2 = None
        self.index3 = None

    def __call__(self, time):
        # Determine index0, index1, index2, index3 such that
        # t(index0) < t(index1) < t < t(index2) < t(index3).
        # In case time is outside of the timescale,
        # set index2 to the corresponding extrema
        if time < self.timesteps_list[0]:
            index2 = 0
        elif time >= self.timesteps_list[-1]:
            index2 = self.N_step - 1
        else:
            index2 = np.nonzero(self.timesteps_list > time)[0][0]

        # set index2 such that the minimal index 'index0' is at least 0
        # or that the maximal index 'index3' is at most N_step - 1
        # so we can take timesteps_list[index0].
        if index2 < 2:
            index2 = 2 # so that index0 = 0, index = 1, index3 = 3
        elif index2 > self.N_step - 2:
            index2 = self.N_step - 2 # so that index3 = N_step - 1

        # save indexes and data in order not to load them each time
        # the method is often called for t included between the same t0, t1, t2, t3.
        if index2 == self.index2:
            pass

        elif index2 == self.index3:
            self.index0 = self.index1 # = index2 - 2
            self.data0 = self.data1

            self.index1 = self.index2 # = index2 - 1
            self.data1 = self.data2

            self.index2 = self.index3 # = index2
            self.data2 = self.data3

            self.index3 = index2 + 1
            file3_name = self.foldername+self.var \
                +"/"+self.var+"_"+self.format_step.format(self.index3)+".h5"
            file3 = h5py.File(file3_name, 'r')
            self.data3 = np.array(file3['data'])

        elif index2 == self.index1:
            self.index3 = self.index2 # = index2 + 1
            self.data3 = self.data2

            self.index2 = self.index1 # = index2
            self.data2 = self.data1

            self.index1 = self.index0 # = index2 - 1
            self.data1 = self.data0

            self.index0 = index2 - 2
            file0_name = self.foldername+self.var \
                +"/"+self.var+"_"+self.format_step.format(self.index0)+".h5"
            file0 = h5py.File(file0_name, 'r')
            self.data0 = np.array(file0['data'])

        elif index2 == self.index0:
            self.index3 = self.index1 # = index2 + 1
            self.data3 = self.data1

            self.index2 = self.index0 # = index2
            self.data2 = self.data0

            self.index1 = index2 - 1
            file1_name = self.foldername+self.var \
                +"/"+self.var+"_"+self.format_step.format(self.index1)+".h5"
            file1 = h5py.File(file1_name, 'r')
            self.data1 = np.array(file1['data'])

            self.index0 = index2 - 2
            file0_name = self.foldername+self.var \
                +"/"+self.var+"_"+self.format_step.format(self.index0)+".h5"
            file0 = h5py.File(file0_name, 'r')
            self.data0 = np.array(file0['data'])

        # at the first call, this code is run as indices are set to None.
        else:
            self.index0 = index2 - 2
            file0_name = self.foldername+self.var \
                +"/"+self.var+"_"+self.format_step.format(self.index0)+".h5"
            file0 = h5py.File(file0_name, 'r')
            self.data0 = np.array(file0['data'])

            self.index1 = index2 - 1
            file1_name = self.foldername+self.var \
                +"/"+self.var+"_"+self.format_step.format(self.index1)+".h5"
            file1 = h5py.File(file1_name, 'r')
            self.data1 = np.array(file1['data'])

            self.index2 = index2
            file2_name = self.foldername+self.var \
                +"/"+self.var+"_"+self.format_step.format(self.index2)+".h5"
            file2 = h5py.File(file2_name, 'r')
            self.data2 = np.array(file2['data'])

            self.index3 = index2 + 1
            file3_name = self.foldername+self.var \
                +"/"+self.var+"_"+self.format_step.format(self.index3)+".h5"
            file3 = h5py.File(file3_name, 'r')
            self.data3 = np.array(file3['data'])

        # actual interpolation
        return lagrange(time,
                        self.timesteps_list[self.index0],
                        self.timesteps_list[self.index1],
                        self.timesteps_list[self.index2],
                        self.timesteps_list[self.index3],
                        self.data0, self.data1, self.data2, self.data3)

    def save_timesteps_h5(self):
        # remove previous file
        try:
            #print(self.timesteps_filename)
            os.remove(self.timesteps_filename)
        except OSError:
            pass

        # create new file
        f = h5py.File(self.timesteps_filename, 'w')
        f.create_dataset('timesteps', data = self.timesteps_list, compression='gzip')
        f.attrs['N_step'] = self.N_step
        return f

    def save_variable_h5(self):
        # create directory if not present
        try:
            os.makedirs(self.foldername + self.var)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        # remove all previous files
        for root, dirs, files in os.walk(self.foldername + self.var):
            for f in files:
                fullname = os.path.join(root, f)
                try:
                    #print(fullname)
                    os.remove(fullname)
                except OSError:
                    continue

        # saving into H5
        for i in np.arange(self.N_step):
            format_i = (self.format_step).format(i)
            fi = h5py.File(self.foldername + self.var + "/" + self.var + "_" + format_i + ".h5", 'w')
            fi.create_dataset('data', data = self.var_data[i, :], compression='gzip')
            fi.close()

        return 0

#############################################################################
# INTERPOLATION METHODS
#############################################################################
#
#   -> always use lagrange, fast and robust
#
# otherwise, there are 
# if t is not at the beginning or end of the time set,
# such that mu is between 0 and 1 :
#   -> use Breeuwsma
#       very fast but analytical, so case limited.
#   -> or cubic or Hermite
#
# and also :
# if the time scale does not have constant time steps,
# if t is outside of the time set (extrapolation),
# if t is on the boundary of the time set :
#   -> use interp_np_solver
#       slower but robust
#

def lagrange(t,
             t0, t1, t2, t3,
             data0, data1, data2, data3):
    """
    Lagrange interpolation.

    WORKS FOR ALL TIME INPUT

    Given the time t such that
    t0 < t1 < t < t2 < t3,

    data0, data1, data2 and data3 correspond to the function
    at respective times t0, t1, t2, t3.

    Keyword parameter:
    t -- time to interpolate the value
    t0, t1, t2, t3 -- 4 given time
    data0, data1, data2, data3 -- associated data

    Return:
    interpolated value
    """
    w0 = ( (t-t1) * (t-t2) * (t-t3) ) / ( (t0-t1) * (t0-t2) * (t0-t3) )
    w1 = ( (t-t0) * (t-t2) * (t-t3) ) / ( (t1-t0) * (t1-t2) * (t1-t3) )
    w2 = ( (t-t0) * (t-t1) * (t-t3) ) / ( (t2-t0) * (t2-t1) * (t2-t3) )
    w3 = ( (t-t0) * (t-t1) * (t-t2) ) / ( (t3-t0) * (t3-t1) * (t3-t2) )

    return w0 * data0 + w1 * data1 + w2 * data2 + w3 * data3

def cubic(t,
          t0, t1, t2, t3,
          data0, data1, data2, data3):
    """
    Cubic interpolation.

    ONLY WORKs FOR REGULAR TIME DISCRETISATION
    delta = t1 - t0 = t2 - t1 = t3 - t2 = cste

    From the website:
    paulbourke.net/miscellaneous/interpolation/

    Given the time t such that
    t0 < t1 < t < t2 < t3,
    the parameter mu(t) is given as
    mu (t)= (t - t1) / (t2 - t1)
    so that mu(t = t1) = 0 and mu(t = t2) = 1.

    data0, data1, data2 and data3 correspond to the function
    at respective times t0, t1, t2, t3.

    Keyword parameter:
    t -- time to interpolate the value
    t0, t1, t2, t3 -- 4 given time
    data0, data1, data2, data3 -- associated data

    Return:
    interpolated value
    """
    mu = (t - t1) / (t2 - t1)

    a0 = data3 - data2 - data0 + data1
    a1 = data0 - data1 - a0
    a2 = data2 - data0
    a3 = data1

    return a0 * mu**3 + a1 * mu**2 + a2 * mu + a3

def breeuwsma(t,
        t0, t1, t2, t3,
        data0, data1, data2, data3):
    """
    Breeuwsma cubic interpolation.

    ONLY WORKs FOR REGULAR TIME DISCRETISATION
    delta = t1 - t0 = t2 - t1 = t3 - t2 = cste

    From the website:
    paulbourke.net/miscellaneous/interpolation/

    Given the time t such that
    t0 < t1 < t < t2 < t3,
    the parameter mu(t) is given as
    mu (t)= (t - t1) / (t2 - t1)
    so that mu(t = t1) = 0 and mu(t = t2) = 1.

    data0, data1, data2 and data3 correspond to the function
    at respective times t0, t1, t2, t3.

    Keyword parameter:
    t -- time to interpolate the value
    t0, t1, t2, t3 -- 4 given time
    data0, data1, data2, data3 -- associated data

    Return:
    interpolated value
    """
    mu = (t - t1) / (t2 - t1)

    a0 = 0.5 * data3 - 1.5 * data2 - 0.5 * data0 + 1.5 * data1
    a1 = data0 - 2.5 * data1 + 2 * data2 - 0.5 * data3
    a2 = 0.5 * data2 - 0.5 * data0
    a3 = data1

    return a0 * mu**3 + a1 * mu**2 + a2 * mu + a3

def hermite(t,
        t0, t1, t2, t3,
        data0, data1, data2, data3,
        bias=0, tension=0):
    """
    Hermite interpolation, with tension and bias input.

    ONLY WORKs FOR REGULAR TIME DISCRETISATION
    delta = t1 - t0 = t2 - t1 = t3 - t2 = cste

    From the website:
    paulbourke.net/miscellaneous/interpolation/

    Given the time t such that
    t0 < t1 < t < t2 < t3,
    the parameter mu(t) is given as
    mu (t)= (t - t1) / (t2 - t1)
    so that mu(t = t1) = 0 and mu(t = t2) = 1.

    data0, data1, data2 and data3 correspond to the function
    at respective times t0, t1, t2, t3.

    Keyword parameter:
    t -- time to interpolate the value
    t0, t1, t2, t3 -- 4 given time
    data0, data1, data2, data3 -- associated data
    bias:
        0 is even,
        positve is towards 1st segment,
        negative towards the other.
    tension:
        1 is high,
        0 normal,
        -1 low.

    Return:
    interpolated value
    """
    mu = (t - t1) / (t2 - t1)

    m0 = (data1 - data0) * (1 + bias) * (1 - tension) / 2 \
            + (data2 - data1) * (1 - bias) * (1 - tension) / 2
    m1 = (data2 - data1) * (1 + bias) * (1 - tension) / 2 \
            + (data3 - data2) * (1 - bias) * (1 - tension) / 2

    a0 =  2 * mu**3 - 3 * mu**2 + 1
    a1 =      mu**3 - 2 * mu**2 + mu
    a2 =      mu**3 -     mu**2
    a3 = -2 * mu**3 + 3 * mu**2

    return a0 * data1 + a1 * m0 + a2 * m1 + a3 * data2

def interp_np_solver(time,
                     t0, t1, t2, t3,
                     data0, data1, data2, data3):
    """
    Numpy solver interpolation, by solving a system of 4 equation of N variables.

    WORKS FOR ALL TIME INPUT

    Given the time t such that
    t0 < t1 < t < t2 < t3,

    data0, data1, data2 and data3 correspond to the function
    at respective times t0, t1, t2, t3.

    Keyword parameter:
    t -- time to interpolate the value
    t0, t1, t2, t3 -- 4 given time
    data0, data1, data2, data3 -- associated data

    Return:
    interpolated value
    """
    I = np.eye(data0.shape[0])
    B = np.concatenate((data0,
                        data1,
                        data2,
                        data3))
    A = np.vstack((np.hstack((t0**3 * I, t0**2 * I, t0 * I, I)),
                   np.hstack((t1**3 * I, t1**2 * I, t1 * I, I)),
                   np.hstack((t2**3 * I, t2**2 * I, t2 * I, I)),
                   np.hstack((t3**3 * I, t3**2 * I, t3 * I, I))))
    solution = np.linalg.solve(a = A, b = B)
    a0, a1, a2, a3 = np.split(solution, 4)

    return a0 * time**3 + a1 * time**2 + a2 * time + a3
