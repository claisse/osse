#!/usr/bin/env python
"""
streamDMD
Implements streaming Dynamic Mode Decomposition as per Hemati, Williams & Rowley, Phys. Fl. 26 111701 (2014)
(the pure, first version presented in that paper).
Also implements the dimension reduction described in that paper.
http://dx.doi.org/10.1063/1.4901016
A Sharma 2015
"""
import numpy as np
import h5py


def pad_matrix(M, rows=0, cols=0):
    """ pad matrix with zeros """
    M = np.hstack( (M, np.zeros( (M.shape[0], cols) ) ) )    # pad cols with zeros
    M = np.vstack( (M, np.zeros( (rows, M.shape[1]) ) ) )    # pad rows with zeros
    return M


def vector(x):
    return x.reshape(x.shape[0], 1)


def eigsort(l, V):
    lmag = np.abs(l)
    idx = lmag.argsort()[::-1]   
    l = l[idx]
    V = V[:,idx]
    return l, V


class DMD(object):
    
    def __init__(self, n, tol=1.0e-3, maxdim=750):
        """ Inits. Pass dimension of snapshot, n. """
        # Yes, I have a zero vector in Qx, Qy bases. No it isn't needed.
        # I do it to reduce code complexity. It shouldn't affect the result.
        self.tol = tol
        self.Gx  = np.zeros( (1,1), dtype=complex )
        self.Gy  = np.zeros( (1,1), dtype=complex )
        self.A   = np.zeros( (1,1), dtype=complex )
        self.Qx  = np.zeros( (n,1), dtype=complex )
        self.Qy  = np.zeros( (n,1), dtype=complex )
        self.n   = n
        self.last_e_norm = 0
        self.maxdim = maxdim
        self.version_str = u'streamDMD v1.0.0'


    def save(self, filename):
        f = h5py.File(filename, 'w')
        f.create_dataset('Gx', data = self.Gx, compression = 'gzip', fletcher32 = True)
        f.create_dataset('Gy', data = self.Gy, compression = 'gzip', fletcher32 = True)
        f.create_dataset('Qx', data = self.Qx, compression = 'gzip', fletcher32 = True)
        f.create_dataset('Qy', data = self.Qy, compression = 'gzip', fletcher32 = True)
        f.create_dataset('A', data = self.A, compression = 'gzip', fletcher32 = True)
        f.create_dataset('n', data = self.n)
        #f.name.attrs['version_str'] = self.version_str
        f.close()


    def load(self, filename):
        self.filename = filename
        f = h5py.File(self.filename, 'r')
        self.Gx  = np.array(f['Gx']) 
        self.Gy  = np.array(f['Gy']) 
        self.A   = np.array(f['A']) 
        self.Qx  = np.array(f['Qx']) 
        self.Qy  = np.array(f['Qx']) 
        self.n   = np.array(f['n']) 
        f.close()


    def update(self, x, y):
        """ updates the orthonormal basis with new information contained in snapshots x, y """
        """ not doing orthogonalisation for accuracy yet """
        e_x = x - np.dot(self.Qx, np.dot(self.Qx.conj().transpose(), x))   # the residual
        e_x_norm = np.linalg.norm(e_x)
        if e_x_norm > self.tol:
            self.A  = pad_matrix(self.A, cols=1)
            self.Gx = pad_matrix(self.Gx, rows=1, cols=1)
            self.Qx = np.hstack((self.Qx, vector(e_x)/e_x_norm))    # augment with new basis vector

        e_y = y - np.dot(self.Qy, np.dot(self.Qy.conj().transpose(), y))   # the residual
        e_y_norm = np.linalg.norm(e_y)
        if e_y_norm > self.tol:
            self.A  = pad_matrix(self.A, rows=1)
            self.Gy = pad_matrix(self.Gy, rows=1, cols=1)
            self.Qy = np.hstack((self.Qy, vector(e_y)/e_y_norm))    # augment with new basis vector

        xt       = np.dot(self.Qx.conj().transpose(), x)
        yt       = np.dot(self.Qy.conj().transpose(), y)
        self.A  += np.outer(vector(yt), vector(xt))
        self.Gx += np.outer(vector(xt), vector(xt))
        self.Gy += np.outer(vector(yt), vector(yt))
        self.last_e_norm = e_x_norm + e_y_norm


    def compress(self):
        """ compute leading eigenvectors (to tolerance tol) of Gx and set Gx = Vx' Gx Vx """
        w, Vx = np.linalg.eigh(self.Gx)
        ix = np.min((w > w[-1]*self.tol).nonzero())
        ix = max(ix, Vx.shape[1]-self.maxdim)
        Vx = Vx[:, ix:]
        xratio = 0 if (ix == 0) else w[ix-1]/w[-1]

        """ compute leading eigenvectors (to tolerance tol) of Gy and set Gy = Vy' Gy Vy """
        w, Vy = np.linalg.eigh(self.Gy)
        iy = np.min((w > w[-1]*self.tol).nonzero())
        iy = max(iy, Vy.shape[1]-self.maxdim)
        Vy = Vy[:, iy:]
        yratio = 0 if (iy == 0) else w[iy-1]/w[-1]

        self.Gx = np.dot(Vx.conj().transpose(), np.dot(self.Gx, Vx))
        self.Gy = np.dot(Vy.conj().transpose(), np.dot(self.Gy, Vy))
        self.A  = np.dot(Vy.conj().transpose(), np.dot(self.A, Vx))
        self.Qx = np.dot(self.Qx, Vx)
        self.Qy = np.dot(self.Qy, Vy)

        return xratio, yratio
        

    def modes(self):
        """ Kt = Qx' * Qy * A * pinv(Gx) """
        Kt = np.dot( np.dot( np.dot(self.Qx.conj().transpose(), self.Qy), self.A), np.linalg.pinv(self.Gx) )
        L, V = np.linalg.eig(Kt)
        L, V = eigsort(L, V)
        """ mode = Qx*v """
        return L, V, self.Qx


    def dimension(self):
        return self.Qx.shape[1], self.Qy.shape[1]


