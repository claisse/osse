#!/usr/bin/env python
import streamDMD
from flowfield import FlowField
from plotfield import plot_slice
import numpy as np
import sys
import os
import time


def test():
    t  = 2000.0
    T  = 10000.0
    dt = 0.3    # sampling rate
    Dt = 0.6    # time shift; dt=0.6 roughly corresponds to U(y=0.8)
    Dx = 0.3    # we are undersampled in x in this data set
    c = Dx/Dt   # 0.25 is roughly y+=20; CL is about 0.793
    compress_interval = 200
    plot_interval = 200
    maxdim = 1000
    datadir = "/home/as5v12/channelflow/build/examples/data-channel"

    os.system('clear')
    sys.stdout.write("c = {0}, compress every {1} steps or above {2}, Dt = {3}  \n".format(c, compress_interval, maxdim, dt) )

    x = FlowField(t+Dt, datadir)
    y = FlowField(t, datadir)
    DMD = streamDMD.DMD(x.len(), maxdim=maxdim)

    try:
        DMD.load('modes/restart_DMD_model.h5')    # optional reload of previous rough pass
        sys.stdout.write("Loaded previous restart file\n")
    except:
        sys.stdout.write("No restart file loaded; starting afresh\n")

    counter = 0
    while (t < T-dt-Dt):
        t += dt
        counter += 1

        sys.stdout.write("\rt = {0:07.2f}, e = {1:6.3f}, rx = {2:5d}, ry = {3:5d}  ".format(t, DMD.last_e_norm, DMD.Qx.shape[1], DMD.Qy.shape[1] ) )
        sys.stdout.flush()

        while (os.path.isfile(datadir + "/u{0:08.3f}.h5".format(t+Dt)) is False):
            sys.stdout.write(".")
            time.sleep(2)

        x = FlowField(t, datadir)
        # implement shift
        y = FlowField(t+Dt, datadir) # you might not like this, but IO is not the bottleneck
        y.shift(-Dx, 'x')
        DMD.update(x.flatten(), y.flatten())

        if (counter % compress_interval is 0):
            compress_counter = 0
            dim = DMD.dimension()
            sys.stdout.write("\nCompressing basis from {0:d}x{1:d} ".format(dim[1], dim[0]))
            xr, yr = DMD.compress()
            dim = DMD.dimension()
            sys.stdout.write("to {0:d}x{1:d}; ".format(dim[1], dim[0]))
            sys.stdout.write("x loss {0:5.2f}%, y loss {1:5.2f}%  \n".format(xr*100, yr*100))
            sys.stdout.flush()

        if (counter % plot_interval is 0):
            plot_counter = 0
            sys.stdout.write("\rplotting and saving modes ...")
            sys.stdout.flush()
            L, V, Qx = DMD.modes()
            #sys.stdout.write("\rLeading mode eigenvalues {0}\n".format(L[0:7]))
            np.savetxt('DMD-eigs.txt', L, fmt='%.3f')
            mode = FlowField()
            for n in range(0, 20):
                mode.unFlatten( np.dot(Qx, V[:,n]), x, L[n])       # they seem to come in pairs, with complex-conj eigs; ordering is done by sort in DMD.modes()
                plot_slice(mode, 0.0, 0, 0, 'modes/slice-x-{0:02d}.png'.format(n))
                plot_slice(mode, 0.9, 0, 1, 'modes/slice-y-{0:02d}.png'.format(n))
                plot_slice(mode, 0.0, 0, 2, 'modes/slice-z-{0:02d}.png'.format(n))

            DMD.save('modes/DMD_model.h5')
    
    DMD.compress()
    DMD.save('modes/DMD_model.h5')
    return DMD


def main(argv):
    test()
    return 0


if __name__=="__main__":
    sys.exit(main(sys.argv))
