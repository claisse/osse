"""
boundary_layer.py

implementing:
Boundary_Layer(FlowField)
DiffOperator_Boundary_Layer(DiffOperator_FlowField)

FlowField-inhereting class used to manage boundary layer
dataset and apply differential operators.

A Sharma 2015,
G CLaisse 2016
"""
import scipy.integrate

import numpy as np
import numpy.polynomial.chebyshev as np_cheb

from flowfield import FlowField
from flowfield import DiffOperator_FlowField
from flowfield import BoundaryCondition_FlowField
from flowfield import MeanFlow_FlowField
from channel import BoundaryCondition_Channel

import chebyshev
import warnings
import itertools


class Boundary_Layer(FlowField):
    """
    Class for loading and holding Boundary_Layer objects.
    """

    def __init__(self, filename=None, stretching_factor=1):
        # The stretching factor has to be manually set depending on
        # the compuationnal work. The accuracy of the boundary layer
        # calculation depends on this factor.
        self.Y = stretching_factor
        super().__init__(filename=filename)
        self.dop = DiffOperator_Boundary_Layer(self)
        self.meanFlow = MeanFlow_Boundary_Layer(self)

    def make_spectral(self, index):
        """
        Convert Boundary_Layer to spectral state
        by computing the spectral coefficients
        for dataset 'u' and 'p':
        Fourier for index 'x' and 'z'.
        ??? for index 'y'.

        Keywor arguments:
        index -- direction 'xz', 'y' or 'xyz'.
        """
        if index == 'xz' or index == 'xyz':
            super().make_spectral(index)

        if index == 'y' or index == 'xyz':
            raise NotImplementedError
            pass

        if index != 'xz' and index != 'y' and index != 'xyz':
            warnings.warn(
                "Spectral by this index not possible: only x, y and z")

    def make_spectral_dataset(self, dataset, index):
        """
        Convert Boundary_Layer to spectral state
        by computing the spectral coefficients:
        Fourier for index 'x' and 'z'.
        ??? for index 'y'.

        Keywor arguments:
        dataset -- dataset of size (3,Nx,Ny,Nz) or (1,Nx,Ny,Nz)
        index -- direction 'xz', 'y' or 'xyz'.

        Return:
        data -- Spectral state of 'dataset'.
        """
        if index == 'xz' or index == 'xyz':
            data = super().make_spectral_dataset(dataset, index)

        if index == 'y' or index == 'xyz':
            raise NotImplementedError

        if index != 'xz' and index != 'y' and index != 'xyz':
            warnings.warn(
                "Spectral by this index not possible: only x, y and z")

        return data


    def make_physical(self, index):
        """
        Convert Boundary_Layer to physical state
        for dataset 'u' and 'p'.
        (from Fourier for index 'x' and 'z',
        from ??? for index 'y').

        Keywor arguments:
        index -- direction 'xz', 'y' or 'xyz'.
        """
        if index == 'y' or index == 'xyz':
            raise NotImplementedError
            pass

        if index == 'xz' or index == 'xyz':
            super().make_physical(index)

        if index != 'xz' and index != 'y' and index != 'xyz':
            warnings.warn(
                "Physical by this index not possible: only x, y and z")

    def make_physical_dataset(self, dataset, nz, index):
        """
        Convert Boundary_Layer to physical state
        for dataset 'dataset'.
        (from Fourier for index 'x' and 'z',
        from ??? for index 'y').

        Keywor arguments:
        index -- direction 'xz', 'y' or 'xyz'.

        Return:
        data -- Physical state of 'dataset'.
        """
        if index == 'y' or index == 'xyz':
            raise NotImplementedError

        if index == 'xz' or index == 'xyz':
            data = super().make_physical_dataset(dataset, nz, index)

        if index != 'xz' and index != 'y' and index != 'xyz':
            warnings.warn(
                "Spectral by this index not possible: only x, y and z")

        return data

    def boundarylayer2chebyshev(self):
        """
        Convert Boundary_Layer wall-normal axis ('y')
        for dataset 'u' and 'p'
        from a boundary layer discretisation
        to a Chebyshev discretisation.

        This function changes the 'y'-axis to Chebyshev nodes two time
        longer than the previous 'y'-axis.
        For direction 'y', dataset is mirrored to prevent lost values.
        """
        ###############################################################
        if self.conf_y_axis is 'Boundary_Layer':

            ############
            # If dataset in Chebyshev configuration was stored
            # If current Boundary_Layer dataset is egal
            # to former Boundary_Layer dataset
            # and if corresponding Chebyshev dataset is known
            # just used the saved Chebyshev datasets.
            if hasattr(self, 'u_saved_bl') \
                    and np.array_equal(self.u, self.u_saved_bl) \
                    and np.array_equal(self.p, self.p_saved_bl) \
                    and hasattr(self, 'u_saved_cheb'):
                self.y_saved_bl = self.y.copy()
                self.y = self.y_saved_cheb.copy()
                self.u = self.u_saved_cheb.copy()
                self.p = self.p_saved_cheb.copy()

            ############
            # Otherwise, calculate new values.
            else:
                self.y_saved_bl = self.y.copy()
                self.u_saved_bl = self.u.copy()
                self.p_saved_bl = self.p.copy()

                if hasattr(self, 'y_saved_cheb'):
                    self.y = self.y_saved_cheb.copy()
                else:
                    self.y = chebyshev.chebnodes(2 * self.y.shape[0])

                # temp_u = self.u.copy()
                # temp_u.resize(self.u.shape[0],
                #              self.u.shape[1],
                #              self.u.shape[2]*2,
                #              self.u.shape[3])

                # Resize dataset u
                self.u.resize(self.u.shape[0],
                              self.u.shape[1],
                              self.u.shape[2] * 2,
                              self.u.shape[3],
                              refcheck=False)
                # Mirror dataset u
                self.u[:, :, (self.u.shape[2] / 2):, :] = \
                    self.u[:, :, (self.u.shape[2] / 2 - 1)::-1, :]
                # self.u = temp_u.copy()

                # Resize dataset p
                self.p.resize(self.p.shape[0],
                              self.p.shape[1] * 2,
                              self.p.shape[2],
                              refcheck=False)
                # Mirror dataset p
                self.p[:, (self.p.shape[1] / 2):, :] = \
                    self.p[:, (self.p.shape[1] / 2 - 1)::-1, :]

                self.y_saved_cheb = self.y.copy()
                self.u_saved_cheb = self.u.copy()
                self.p_saved_cheb = self.p.copy()

            self.attributes['Ny'] = self.y.shape[0]
            self.conf_y_axis = 'Chebyshev'

        ###############################################################
        elif self.conf_y_axis is 'Chebyshev':
            self.y_saved_cheb = self.y.copy()
            if not hasattr(self, 'y_saved_bl'):
                cheb_nodes = chebyshev.chebnodes(2 * self.y.shape[0])
                self.y_saved_bl = -self.Y * \
                    np.log(cheb_nodes[:self.y.shape[0]])

        ###############################################################
        else:
            raise ValueError(
                """
                Interpolation is not possible as the configuration
                of the y-axis nodes (self.conf_y_axis)
                is neither 'Boundary_Layer' nor 'Chebyshev'.
                """)

    def chebyshev2boundarylayer(self):
        """
        Convert Boundary_Layer wall-normal axis ('y')
        for dataset 'u' and 'p'
        from a Chebyshev discretisation
        to a Boundary_Layer discretisation.

        This function changes the 'y'-axis (Chebyshev nodes)
        to its value as Boundary Layer nodes (two times shorter).
        """
        ###############################################################
        if self.conf_y_axis is 'Chebyshev':

            ############
            # If former Boundary_Layer dataset was stored
            # If current Chebyshev dataset is egal
            # to former Chebyshev dataset
            # and if corresponding boundary_layer dataset is known
            # just used the saved boudary_layer datasets.
            if hasattr(self, 'u_saved_cheb') \
                    and np.array_equal(self.u, self.u_saved_cheb) \
                    and np.array_equal(self.p, self.p_saved_cheb) \
                    and hasattr(self, 'u_saved_bl'):
                self.y_saved_cheb = self.y.copy()
                self.y = self.y_saved_bl.copy()
                self.u = self.u_saved_bl.copy()
                self.p = self.p_saved_bl.copy()

            ############
            # Otherwise, calculate new values.
            else:
                self.y_saved_cheb = self.y.copy()
                self.u_saved_cheb = self.u.copy()
                self.p_saved_cheb = self.p.copy()

                if hasattr(self, 'y_saved_bl'):
                    self.y = self.y_saved_bl.copy()
                else:
                    cheb_nodes = chebyshev.chebnodes(self.y.shape[0])
                    self.y = -self.Y * np.log(cheb_nodes[:self.y.shape[0]])

                # temp_u = self.u.copy()
                self.u.resize(self.u.shape[0],
                              self.u.shape[1],
                              int(self.u.shape[2] / 2),
                              self.u.shape[3],
                              refcheck=False)
                # self.u = temp_u.copy()

                self.p.resize(self.p.shape[0],
                              int(self.p.shape[1] / 2),
                              self.p.shape[2],
                              refcheck=False)

                self.y_saved_bl = self.y.copy()
                self.u_saved_bl = self.u.copy()
                self.p_saved_bl = self.p.copy()

            self.attributes['Ny'] = self.y.shape[0]
            self.conf_y_axis = 'Boundary_Layer'

        ###############################################################
        elif self.conf_y_axis is 'Boundary_Layer':
            self.y_saved_bl = self.y.copy()
            if not hasattr(self, 'y_saved_cheb'):
                self.y_saved_cheb = chebyshev.chebnodes(2 * self.y.shape[0])

        ###############################################################
        else:
            raise ValueError(
                """
                Interpolation is not possible as the configuration
                of the y-axis nodes (self.conf_y_axis)
                is neither 'Boundary_Layer' nor 'Chebyshev'.
                """)


class DiffOperator_Boundary_Layer(DiffOperator_FlowField):
    """
    Class inhereting from DiffOperator_FlowField.
    Manages differential operation on the Boundary Layer object.
    Contains differential operator dedicated to Boundary Layer object.
    Inherits from DiffOperator_FlowField
        for operations like divergence, gradient, curl or laplacian.

    Attributes :
    self.ff -- Boundary Layer on which apply the operations.

    self.D -- Matrix containing coefficients for function __deriv()
    self.Dorder -- Matrix containing differentiation coefficients.
    self.Dorder_order -- Order of maximal differentiation calculated.
    """

    def __init__(self, boundary_layer):
        self.ff = boundary_layer

        self.D = None
        self.Dorder = None
        self.Dorder_order = 0
        # see method DiffOperator_Boundary_Layer.__deriv() for details.

    def __deriv(self, dataset, order=1, bc='none', transform='none'):
        """
        Derivative of the dataset 'dataset' of the boundary layer 'self.ff'
        in direction 'y' and order 'order'.

        Considering the boundary layer V whose shape
        of dataset u is (3, Nx, Ny, Nz):
        __deriv(V.u, 'y', n) = (dn_V.ux/dy**n, dn_V.uy/dy**n, dn_V.uz/dy**n)
                                                                        (x,y,z)

        IMPORTANT REMARKS:
        - COnfiguration on wall normal axis (variable self.ff.conf_y_axis)
            needs to be 'Boundary_Layer' to apply this method.
        - This function changes instances of variable :
            D, Dorder, Dorder_order

        Keyword arguments:
        dataset -- dataset self.ff.u or self.ff.p
                    on which apply the derivative.

        Optional keyword arguments:
        order -- integer, order of the derivative in y-direction
        bc -- kind of boundary condition,
            see class BoundaryCondition_Boundary_Layer
        transform -- if differentiation matrice DM needs transformation
            before being used, default is 'none'.
            'conjugate'
            'transpose'
            'hermitian' (conjugate-transpose/adjoint)

        Return:
        diff -- Array (3, Nx, Ny, Nz) for dataset (3, Nx, Ny, Nz).
                Array (1, Nx, Ny, Nz) for dataset (1, Nx, Ny, Nz).
        """
        assert self.ff.conf_y_axis == 'Boundary_Layer', \
            """Needs Chebyshev configuration in y-direction to calculate
            derivatives, apply method uniform2chebyshev()."""

        # Condition to prevent building a new matrix self.Dorder,
        # the differentiation matrix,
        # at each call of __deriv()
        if self.Dorder is None \
                or order != self.Dorder_order \
                or bc != self.bc_type:
            self.Dorder_order = order
            self.bc_type = bc
            N = self.ff.y.shape[0]

            # definition of self.D, a weighting coefficient to find self.Dorder
            self.D = np.zeros((order, order))
            self.D[0, 0] = 1
            for a, b in itertools.product(
                    range(1, self.D.shape[0]),
                    range(self.D.shape[1])):
                if b == 0:
                    self.D[a, b] = 1
                else:
                    self.D[a, b] = self.D[a - 1, b - 1] + \
                        (b + 1) * self.D[a - 1, b]

            # definition of DM,
            # differentiation matrix for channel
            # necessary to define the boundary layer differentiation matrix
            eta, DM = BoundaryCondition_Channel.chebdiff_bc(2 * N,
                                                            order,
                                                            bc=bc)

            # definition of self.Dorder,
            # differentiatiom matrix for boundary layer
            self.Dorder = np.zeros((order, N, N))
            for order, index in itertools.product(range(1, order + 1),
                                                  range(1, order + 1)):
                # for index in range(1,order+1):
                self.Dorder[order - 1, :, :] += (-1. / self.ff.Y)**order \
                    * (self.D[order - 1, index - 1] *
                        (eta[0:N].reshape((N, 1))**index) *
                        DM[index - 1, 0:N, 0:N])

        if transform == 'conjugate':
            Dorder = np.conjugate(self.Dorder)
        elif transform == 'transpose':
            Dorder = np.transpose(self.Dorder, (0, 2, 1))
        elif transform == 'hermitian':
            Dorder = np.conjugate(np.transpose(self.Dorder, (0, 2, 1)))
        else:
            Dorder = self.Dorder

        """
        # Next lines are just there for information on how the differentiation
        # matrices are build and what they seem to be.
        if self.D1 is None and self.D2 is None:
            #eta, DM = chebyshev.chebdiff(2*N, 5)
            self.D1 = (-1./self.ff.Y)*eta[0:N].reshape((N, 1))*DM[0, 0:N, 0:N]
            self.D2 = (-1./self.ff.Y)**2 * \
                        (   1 * eta[0:N].reshape((N, 1))    * DM[0, 0:N, 0:N] \
                         +  1 * (eta[0:N].reshape((N, 1))**2)*DM[1, 0:N, 0:N])
            self.D3 = (-1./self.ff.Y)**3 * \
                        (   1 * eta[0:N].reshape((N,1))    * DM[0, 0:N, 0:N] \
                         +  3 * eta[0:N].reshape((N,1))**2 * DM[1, 0:N, 0:N] \
                         +  1 * eta[0:N].reshape((N,1))**3 * DM[2, 0:N, 0:N] )
            self.D4 = (-1./self.ff.Y)**4 * \
                        (   1 * eta[0:N].reshape((N,1))    * DM[0, 0:N, 0:N] \
                         +  7 * eta[0:N].reshape((N,1))**2 * DM[1, 0:N, 0:N] \
                         +  6 * eta[0:N].reshape((N,1))**3 * DM[2, 0:N, 0:N] \
                         +  1 * eta[0:N].reshape((N,1))**4 * DM[3, 0:N, 0:N] )
            self.D5 = (-1./self.ff.Y)**5 * \
                        (   1 * eta[0:N].reshape((N,1))    * DM[0, 0:N, 0:N] \
                         + 15 * eta[0:N].reshape((N,1))**2 * DM[1, 0:N, 0:N] \
                         + 25 * eta[0:N].reshape((N,1))**3 * DM[2, 0:N, 0:N] \
                         + 10 * eta[0:N].reshape((N,1))**4 * DM[3, 0:N, 0:N] \
                         +  1 * eta[0:N].reshape((N,1))**5 * DM[4, 0:N, 0:N] )
        print('D5 : \n', self.D5)
        """

        diff = dataset.copy()
        diff[:, :, :, :] = np.einsum(
            'ij,mnjp->mnip',
            Dorder[order - 1, :, :],
            diff[:, :, :, :])
        """
        # Just to present other methods to replace np.einsum method()
        diff[:,:,:,:] = np.transpose(
                                np.tensordot(
                                np.power(self.D1[:,:],order),
                                diff[:,:,:,:],
                                axes=([1],[2])),
                                axes=(1,2,0,3))
        """
        """
        diff[:,:,:,:] = np.einsum(
                                'ij,mnjp->mnip',
                                np.power(self.D1[:,:],order),
                                diff[:,:,:,:])
        #print('print 2 : \n', diff)
        """
        return diff

    def deriv(self, dataset, x_order=0, y_order=0, z_order=0,
            bc='none', transform='none'):
        """
        Wrapper of method __deriv() for any dataset.
        Derivative of dataset of the boundary layer 'self.ff'
        in direction 'index' and order 'order'.

        Considering the channel V whose shape of dataset D is (3, Nx, Ny, Nz):
        deriv(D, 'x', n) = (dn_Dx/dx**n, dn_Dy/dx**n, dn_Dz/dx**n) (x,y,z)
        deriv(D, 'y', n) = (dn_Dx/dy**n, dn_Dy/dy**n, dn_Dz/dy**n) (x,y,z)
        deriv(D, 'z', n) = (dn_Dx/dz**n, dn_Dy/dz**n, dn_Dz/dz**n) (x,y,z)

        Keyword arguments:
        x_order -- order of differentiation in direction 'x'.
        y_order -- order of differentiation in direction 'y'.
        z_order -- order of differentiation in direction 'z'.
        bc, transform -- see docstring on __deriv().

        Return:
        diff -- Array (3, Nx, Ny, Nz) for dataset (3,Nx,Ny,Nz)
                Array (Nx, Ny, Nz) for dataset (Nx,Ny,Nz)
        """
        assert isinstance(x_order, int) \
            and isinstance(y_order, int) \
            and isinstance(z_order, int), \
            "Order of derivation must be integer."

        if x_order == 0 and y_order == 0 and z_order == 0:
            return dataset.copy()

        diff = dataset.copy()
        if x_order > 0 or z_order > 0:
            diff = super().deriv(dataset, x_order, 0, z_order, bc=bc)
        if y_order > 0:
            self.ff.chebyshev2boundarylayer()
            if dataset.ndim == 4:
                diff = self.__deriv(dataset, y_order,
                                    bc=bc,
                                    transform=transform)
            elif dataset.ndim == 3:
                diff = self.__deriv(dataset[np.newaxis, :, :, :],
                                    y_order,
                                    bc=bc,
                                    transform=transform)[0, :, :, :]
            else:
                raise ValueError(
                    """
                    Derivative method deriv is defined
                    to only apply on 1D or 3D dataset.
                    """)

        return diff

    def deriv_u(self, x_order=0, y_order=0, z_order=0,
            bc='none', transform='none'):
        """
        Wrapper of method __deriv() dedicated to dataset u.
        Derivative of the dataset 'u' of the boundary layer 'self.ff'
        in direction 'index' and order 'order'.

        Considering the channel V whose shape of dataset u is (3, Nx, Ny, Nz):
        deriv_u('x', n) = (dn_V.ux/dx**n, dn_V.uy/dx**n, dn_V.uz/dx**n) (x,y,z)
        deriv_u('y', n) = (dn_V.ux/dy**n, dn_V.uy/dy**n, dn_V.uz/dy**n) (x,y,z)
        deriv_u('z', n) = (dn_V.ux/dz**n, dn_V.uy/dz**n, dn_V.uz/dz**n) (x,y,z)

        Keyword arguments:
        x_order -- order of differentiation in direction 'x'.
        y_order -- order of differentiation in direction 'y'.
        z_order -- order of differentiation in direction 'z'.
        bc, transform -- see docstring on __deriv().

        Return:
        diff -- Array (3, Nx, Ny, Nz).
        """
        assert isinstance(x_order, int) \
            and isinstance(y_order, int) \
            and isinstance(z_order, int), \
            "Order of derivation must be integer."

        if x_order == 0 and y_order == 0 and z_order == 0:
            return self.ff.u.copy()

        diff = self.ff.u.copy()
        if x_order > 0 or z_order > 0:
            diff = super().deriv_u(x_order, 0, z_order,
                                   bc=bc,
                                   transform=transform)
        if y_order > 0:
            assert self.ff.u.shape[0] == 3, \
                """
                Derivative method deriv_u is defined
                to only apply on 3D dataset 'u'.
                """
            # self.ff.chebyshev2boundarylayer()
            diff = self.__deriv(diff, y_order, bc=bc, transform=transform)

        return diff

    def deriv_p(self, x_order=0, y_order=0, z_order=0,
            bc='none', transform='none'):
        """
        Wrapper of method __deriv() dedicated to dataset p.
        Derivative of the dataset 'p' of the boundary layer 'self.ff'
        in direction 'index' and order 'order'.

        Considering the channel V whose shape of dataset p is (3, Nx, Ny, Nz):
        deriv_p('x', n) = (dn_V.p/dx**n) (x,y,z)
        deriv_p('y', n) = (dn_V.p/dy**n) (x,y,z)
        deriv_p('z', n) = (dn_V.p/dz**n) (x,y,z)

        Keyword arguments:
        x_order -- order of differentiation in direction 'x'.
        y_order -- order of differentiation in direction 'y'.
        z_order -- order of differentiation in direction 'z'.  
        bc, transform -- see docstring on __deriv().

        Return:
        diff -- Array (Nx, Ny, Nz).
        """
        assert isinstance(x_order, int) \
            and isinstance(y_order, int) \
            and isinstance(z_order, int), \
            "Order of derivation must be integer."

        if x_order == 0 and y_order == 0 and z_order == 0:
            return self.ff.p.copy()

        diff = self.ff.p.copy()
        if x_order > 0 or z_order > 0:
            diff = super().deriv_p(x_order, 0, z_order,
                                   bc=bc,
                                   transform=transform)
        if y_order > 0:
            assert self.ff.p.ndim == 3, \
                """
                Derivative method deriv_u is defined
                to only apply on 3D dataset 'u'.
                """
            # self.ff.chebyshev2boundarylayer()
            diff = self.__deriv(diff[np.newaxis, :, :, :],
                                y_order,
                                bc=bc,
                                transform=tranform)[0, :, :, :]
        return diff


class BoundaryCondition_Boundary_Layer(BoundaryCondition_FlowField):
    """
    NOT WRITTEN
    Class to apply the boundary condition on a boundary layer.
    """

    def __init__(self, boundary_layer):
        pass

    def chebdiff_bc_bl(N, order, bc='none'):
        """
        To be written.
        Should return the differentiation matrices for a bl
        (see method DiffOperator_Boundary_Layer.__deriv() )
        with the right boundary condition applied
        to the BL differentiation matrices
        and not just the Chebyshev differentiation matrices.
        """
        pass


class MeanFlow_Boundary_Layer(MeanFlow_FlowField):

    def __init__(self, boundary_layer):
        self.ff = boundary_layer

    def apply_mean_flow(self, kind='blasius'):
        self.ff.u0 = np.ndarray(shape=(self.ff.u.shape[0],
                                       self.ff.u.shape[1],
                                       self.ff.u.shape[2],
                                       self.ff.u.shape[3] ))

        if kind == 'blasius':
            blasius_profile = blasius_profile(self.ff.y, K=0.5)
            for i, nx, nz in itertools.product(3,
                                            range(self.ff.u.shape[1]),
                                            range(self.ff.u.shape[3]) ):
                self.ff.u0[i, nx, :, nz] = blasius_profile[:]
    
    def blasius_profile(eta, K=0.5):
        """
        Return the solution of the Blasius equation:
        F(3)(y) + K * F(0)(y) * F(2)(y) = 0,
        with y = [0, y_max] and resolution N.

        The Blasius velocity profile is then defined as:
        U(y) = F(1)(y)

        REMARK:
        At infinity:
        U(eta -> inf) = 1
        which is not a good condition to apply derivative methods
        which require:
        U(eta -> inf) = 0

        eta -- wall normal ordinate
        K -- constant
        """
        #eta = np.linspace(0, max(eta), eta.shape[0])
        initial_cdt = [0, 0, 0.33206]

        # F(3) = - K * F(0) * F(2)
        def blasius_equation(F, eta):
            """Return [F(1), F(2), F(3)]."""
            return [F[1], F[2], -K*F[0]*F[2]]

        # integrate to find F(0), F(1) and F(2)
        sol = scipy.integrate.odeint(func=blasius_equation,
                                     y0=initial_cdt,
                                     t=eta)
        return sol[:,1] #* np.exp(-0.2*eta)

